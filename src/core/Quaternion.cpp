/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Quaternion.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "Quaternion.h"

//CONSTRUCTORS.
Quaternion::Quaternion(){
	m_x = 0.0f;
	m_y = 0.0f;
	m_z = 0.0f; 
	m_w = 1.0f;
};

Quaternion::Quaternion ( float const& in_X,
							float const& in_Y,
							float const& in_Z,
							float const& in_W ){
	m_x = in_X;
	m_y = in_Y;
	m_z = in_Z;
	m_w = in_W;
};

Quaternion::Quaternion ( Quaternion const& in_Quat ){
	m_x = in_Quat.m_x;
	m_y = in_Quat.m_y;
	m_z = in_Quat.m_z;
	m_w = in_Quat.m_w;
};

//DESTRUCTOR.
Quaternion::~Quaternion(){

};

//OPERATORS.
Quaternion& Quaternion::operator= ( Quaternion const& in_Quat ){
	m_x = in_Quat.m_x;
	m_y = in_Quat.m_y;
	m_z = in_Quat.m_z;
	m_w = in_Quat.m_w;

	return *this;
};

bool Quaternion::operator== ( Quaternion const& in_Quat ){
	return Equals ( in_Quat );
};

bool Quaternion::operator!= ( Quaternion const& in_Quat ){
	return ! Equals ( in_Quat );
};

Quaternion& Quaternion::operator+= ( Quaternion const& in_Quat ){
	return Add ( *this, in_Quat );
};

Quaternion& Quaternion::operator-= ( Quaternion const& in_Quat ){
	return Sub ( *this, in_Quat );
};

Quaternion& Quaternion::operator*= ( Quaternion const& in_Quat ){
	return Mul ( *this, in_Quat );
};

//CUSTOM METHODES.
bool Quaternion::Equals ( Quaternion const& in_Quat ) const{
	if (	in_Quat.m_x == m_x &&
			in_Quat.m_y == m_y &&
			in_Quat.m_z == m_z &&
			in_Quat.m_w == m_w  ){
		return true;
	}else{
		return false;
	}
};

Quaternion& Quaternion::Add ( Quaternion const& in_QuatA, Quaternion const& in_QuatB ){
	m_x = in_QuatA.m_x + in_QuatB.m_x;
	m_y = in_QuatA.m_y + in_QuatB.m_y;
	m_z = in_QuatA.m_z + in_QuatB.m_z;
	m_w = in_QuatA.m_w + in_QuatB.m_w;

	return *this;
};

Quaternion& Quaternion::Sub ( Quaternion const& in_QuatA, Quaternion const& in_QuatB ){
	m_x = in_QuatA.m_x - in_QuatB.m_x;
	m_y = in_QuatA.m_y - in_QuatB.m_y;
	m_z = in_QuatA.m_z - in_QuatB.m_z;
	m_w = in_QuatA.m_w - in_QuatB.m_w;

	return *this;
};

Quaternion& Quaternion::Mul ( Quaternion const& in_QuatA, Quaternion const& in_QuatB ){
	float dW, dX, dY, dZ ;

	dX =	in_QuatA.m_w * in_QuatB.m_x + in_QuatB.m_w * in_QuatA.m_x +
			in_QuatA.m_y * in_QuatB.m_z - in_QuatA.m_z * in_QuatB.m_y ;

	dY =	in_QuatA.m_w * in_QuatB.m_y - in_QuatB.m_z * in_QuatA.m_x +
			in_QuatA.m_y * in_QuatB.m_w + in_QuatA.m_z * in_QuatB.m_x ;

	dZ =	in_QuatA.m_w * in_QuatB.m_z + in_QuatB.m_y * in_QuatA.m_x -
			in_QuatA.m_y * in_QuatB.m_x + in_QuatA.m_z * in_QuatB.m_w ;

	dW =	in_QuatA.m_w * in_QuatB.m_w - in_QuatA.m_x * in_QuatB.m_x - in_QuatA.m_y * in_QuatB.m_y - in_QuatA.m_z * in_QuatB.m_z ;

	m_x = dX;
	m_y = dY;
	m_z = dZ;
	m_w = dW;

	return *this;
};

Quaternion& Quaternion::Negate ( Quaternion const& in_Quat ){
	m_x = -in_Quat.m_x;
	m_y = -in_Quat.m_y;
	m_z = -in_Quat.m_z;
	m_w = -in_Quat.m_w;

	return *this;
};

Quaternion& Quaternion::Conjugate (  Quaternion const& in_Quat ){
	m_x = -in_Quat.m_x;
	m_y = -in_Quat.m_y;
	m_z = -in_Quat.m_z;
	m_w = in_Quat.m_w;

	return *this;
};

Quaternion& Quaternion::Invert ( Quaternion const& in_Quat ){
	return Conjugate ( in_Quat );
};


Quaternion& Quaternion::Normalize ( Quaternion const& in_Quat ){
	float vectorMagnitude = in_Quat.m_x*in_Quat.m_x + in_Quat.m_y*in_Quat.m_y + in_Quat.m_z*in_Quat.m_z;
	float PicoEPS = 2.107342e-08f;
	if ( fabs( vectorMagnitude - 1.0f ) >= PicoEPS ){
		if ( vectorMagnitude < PicoEPS ){
			m_x = m_y = m_z = 0.0f ;
			m_w = 1.0f;
		}else{
			float lengthFraction = 1.0f / sqrt ( vectorMagnitude ) ;
			m_w *= lengthFraction;
			m_x *= lengthFraction;
			m_y *= lengthFraction;
			m_z *= lengthFraction;

		    if ( m_w > 1.0f )
				m_w = 1.0f;
			else if ( m_w < -1.0f )
				m_w = -1.0f;
		}
	}else{
		if ( m_w > 1.0f )
			m_w = 1.0f;
		else if ( m_w < -1.0f )
			m_w = -1.0f;
	}

	return *this;
};

float Quaternion::DotProduct ( Quaternion const& in_Quat ) const{
	return ( m_x*in_Quat.m_x +  m_y*in_Quat.m_y +  m_z*in_Quat.m_z +  m_w*in_Quat.m_w );
};

Quaternion& Quaternion::Slerp ( Quaternion const& in_QuatA,
								Quaternion const& in_QuatB,
								float const in_Blend,
								bool const in_ReduceTo360 )
{
	float w1, w2;
	bool bFlip = 0;

	float cosTheta = in_QuatA.DotProduct(in_QuatB);

	if ( in_ReduceTo360 && cosTheta < 0.0f ){
		cosTheta = -cosTheta;
		bFlip = 1;
	}

	float theta = acos( cosTheta );
	float sinTheta = sin( theta );

	if ( sinTheta > 0.0005f ){
		w1 = sin( ( 1.0f - in_Blend ) * theta ) / sinTheta ;
		w2 = sin( in_Blend * theta ) / sinTheta;
	}else{
		w1 = 1.0f - in_Blend;
		w2 = in_Blend;
	}

	if ( bFlip ){
		w2 = -w2;
	}

	m_x = in_QuatA.m_x * w1 + in_QuatB.m_x * w2 ;
	m_y = in_QuatA.m_y * w1 + in_QuatB.m_y * w2 ;
	m_z = in_QuatA.m_z * w1 + in_QuatB.m_z * w2 ;
	m_w = in_QuatA.m_w * w1 + in_QuatB.m_w * w2 ;

	//Normalize( *this);

	return *this;
};

Quaternion Quaternion::Lerp(Quaternion const& in_QuatA,
							Quaternion const& in_QuatB,
							float const in_Blend){

	float invBlend = 1.0f - in_Blend;
	float quatDot = in_QuatA.m_x * in_QuatB.m_x + in_QuatA.m_y * in_QuatB.m_y + in_QuatA.m_z * in_QuatB.m_z + in_QuatA.m_w * in_QuatB.m_w;
	if(quatDot < 0.0f){
		m_x = in_QuatA.m_x * invBlend - in_QuatB.m_x * in_Blend ;
		m_y = in_QuatA.m_y * invBlend - in_QuatB.m_y * in_Blend ;
		m_z = in_QuatA.m_z * invBlend - in_QuatB.m_z * in_Blend ;
		m_w = in_QuatA.m_w * invBlend - in_QuatB.m_w * in_Blend ;
	}else{
		m_x = in_QuatA.m_x * invBlend + in_QuatB.m_x * in_Blend ;
		m_y = in_QuatA.m_y * invBlend + in_QuatB.m_y * in_Blend ;
		m_z = in_QuatA.m_z * invBlend + in_QuatB.m_z * in_Blend ;
		m_w = in_QuatA.m_w * invBlend + in_QuatB.m_w * in_Blend ;
	}

	//Normalize( *this);

	return *this;
};

Quaternion& Quaternion::SelfAdd ( Quaternion const& in_Quat ){
	return Add ( *this, in_Quat );
};

Quaternion& Quaternion::SelfSub ( Quaternion const& in_Quat ){
	return Sub ( *this, in_Quat );
};

Quaternion& Quaternion::SelfMul ( Quaternion const& in_Quat ){
	return Mul ( *this, in_Quat );
};

Quaternion& Quaternion::SelfNegate(){
	return Negate ( *this );
};

Quaternion& Quaternion::SelfInvert(){
	return Invert ( *this );
};

Quaternion& Quaternion::SelfConjugate (){
	return Conjugate ( *this );
};

Quaternion& Quaternion::SelfNormalize(){
	return Normalize ( *this );
};

Matrix3x3 Quaternion::ToMatrix3x3 () const{
	Matrix3x3 rtnMat;
	float xx2, yy2, zz2;
	float xy2, xz2, yz2;
	float wx2, wy2, wz2;

	xx2 = m_x * m_x * 2.0f;
	yy2 = m_y * m_y * 2.0f;
	zz2 = m_z * m_z * 2.0f;
	xy2	= m_x * m_y * 2.0f;
	xz2	= m_x * m_z * 2.0f;
	yz2	= m_y * m_z * 2.0f;
	wx2	= m_w * m_x * 2.0f;
	wy2	= m_w * m_y * 2.0f;
	wz2	= m_w * m_z * 2.0f;

	rtnMat.Set (	( 1.0f -  yy2 - zz2 ), ( xy2 + wz2 ), ( xz2 - wy2 ),
					( xy2 - wz2), ( 1.0f - xx2 - zz2 ), ( yz2 + wx2 ),
					( xz2 + wy2 ), ( yz2 - wx2 ), ( 1 - xx2 - yy2 ) );

	return rtnMat;
};

float Quaternion::ReciprocalSqrt ( float in_X){

	long i;
	float y, r;

	y = in_X * 0.5f;
	i = *(long *) &in_X;
	i = 0x5f3759df - ( i >> 1);
	r =  *(float *) (&i);
	r = r * (1.5f - r * r * y);

	return r;
}

Quaternion& Quaternion::FromMatrix3x3 ( Matrix3x3 const& in_Matrix ){

	float trace = in_Matrix.GetValue(0,0)+in_Matrix.GetValue(1,1)+in_Matrix.GetValue(2,2);
	if ( trace > 0 ){
		float s = 0.5f / sqrt(trace+1.0f);
		m_w = 0.25f / s;
		m_x = (in_Matrix.GetValue(2,1) - in_Matrix.GetValue(1,2)) * s;
		m_y = (in_Matrix.GetValue(0,2) - in_Matrix.GetValue(2,0)) * s;
		m_z = (in_Matrix.GetValue(1,0) - in_Matrix.GetValue(0,1)) * s;
	}else if (in_Matrix.GetValue(0,0)>in_Matrix.GetValue(1,1) && in_Matrix.GetValue(0,0)>in_Matrix.GetValue(2,2)){
		float s = 2.0f * sqrt(1.0f + in_Matrix.GetValue(0,0)-in_Matrix.GetValue(1,1)-in_Matrix.GetValue(2,2));
		m_w = (in_Matrix.GetValue(2,1) - in_Matrix.GetValue(1,2)) / s;
		m_x = 0.25f * s;
		m_y = (in_Matrix.GetValue(0,1) + in_Matrix.GetValue(1,0)) / s;
		m_z = (in_Matrix.GetValue(0,2) + in_Matrix.GetValue(2,0)) / s;
	}else if(in_Matrix.GetValue(1,1)>in_Matrix.GetValue(2,2)){
		float s = 2.0f * sqrt(1.0f + in_Matrix.GetValue(1,1)-in_Matrix.GetValue(0,0)-in_Matrix.GetValue(2,2));
		m_w = (in_Matrix.GetValue(0,2) - in_Matrix.GetValue(2,0)) / s;
		m_x = (in_Matrix.GetValue(0,1) + in_Matrix.GetValue(1,0)) / s;
		m_y = 0.25f * s;
		m_z = (in_Matrix.GetValue(1,2) + in_Matrix.GetValue(2,1)) / s;
	}else{
		float s = 2.0f * sqrt(1.0f + in_Matrix.GetValue(2,2)-in_Matrix.GetValue(0,0)-in_Matrix.GetValue(1,1));
		m_w = (in_Matrix.GetValue(1,0) - in_Matrix.GetValue(0,1)) / s;
		m_x = (in_Matrix.GetValue(0,2) + in_Matrix.GetValue(2,0)) / s;
		m_y = (in_Matrix.GetValue(1,2) + in_Matrix.GetValue(2,1)) / s;
		m_z = 0.25f * s;
	}

	Invert ( *this );

	return *this;
};

//GET METHODES.
float Quaternion::GetX () const{
	return m_x;
};

float Quaternion::GetY () const{
	return m_y;
};

float Quaternion::GetZ () const{
	return m_z;
};

float Quaternion::GetW () const{
	return m_w;
};

float Quaternion::GetLength() const{
	return ( sqrt ( m_x*m_x + m_y*m_y + m_z*m_z + m_w*m_w ) );
};

//SET METHODES.
void Quaternion::Set ( float const& in_X,
						float const& in_Y,
						float const& in_Z,
						float const& in_W ){
	m_x = in_X;
	m_y = in_Y;
	m_z = in_Z;
	m_w = in_W;	
};

void Quaternion::SetX ( float const& in_X ){
	m_x = in_X;
};

void Quaternion::SetY ( float const& in_Y ){
	m_y = in_Y;
};

void Quaternion::SetZ ( float const& in_Z ){
	m_z = in_Z;
};

void Quaternion::SetW ( float const& in_W ){
	m_w = in_W;
};

void Quaternion::FromAxisAndAngle ( float const& in_X,
									float const& in_Y,
									float const& in_Z,
									float const& in_Angle ){
	float halfAngle = in_Angle * 0.5f;
	float s = sin ( halfAngle );
	m_x = in_X * s;
	m_y = in_Y * s;
	m_z = in_Z * s;
	m_w = cos ( halfAngle);
};

void Quaternion::ToEuler ( float &out_X, float &out_Y, float &out_Z){
	
	float testSingularity = m_x * m_y + m_z * m_w;
	float pi = 3.14159265359f;
	float epsilon = 0.49999999f;
	if(testSingularity > epsilon){
		out_Y = 2.0f * atan2f(m_x, m_w);
		out_Z = pi / 2.0f;
		out_X = 0.0f;
	}else if(testSingularity < -epsilon){
		out_Y = -2.0f * atan2f(m_x, m_w);
		out_Z = - pi / 2.0f;
		out_X = 0.0f;
	}else{
		float sqX = m_x * m_x;
		float sqY = m_y * m_y;
		float sqZ = m_z * m_z;

		out_Y = atan2f((2.0f * m_y * m_w - 2.0f * m_x * m_z), (1.0f - 2.0f * sqY - 2.0f * sqZ));
		out_Z = asinf(2.0f * testSingularity);
		out_X = atan2f((2.0f * m_x * m_w - 2.0f * m_y * m_z), (1.0f - 2.0f * sqX - 2.0f * sqZ));
	}
}