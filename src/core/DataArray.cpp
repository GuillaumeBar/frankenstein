/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file DataArray.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "DataArray.h"
/*
template <class T>
DataArray<T>::DataArray ()
{
	//Allocat the point.
	m_Ptr = 0;
	//Define the Size value.
	m_Size = 0;
};

template <class T>
DataArray<T>::DataArray ( DataArray const& in_DataArray )
{
	m_Size = in_DataArray.m_Size;
	m_Ptr = new T[m_Size];
	for ( int i=0; i<m_Size ; i++ )
	{	
		m_Ptr[i] = in_DataArray.m_Ptr[i];
	}
};

template <class T>
DataArray<T>::DataArray ( unsigned int in_Size, T const& in_Item )
{
	m_Size = in_Size;
	m_Ptr = new T[m_Size];
	for ( int i=0; i<m_Size; i++ )
	{
		m_Ptr[i] = in_Item;
	}
};

template <class T>
DataArray<T>::~DataArray()
{
	//Free the memory.
	delete [] m_Ptr;
};

template <class T>
T& DataArray<T>::operator[] ( unsigned int in_Index )
{
	if ( in_Index < m_Size && m_Size != 0 )
	{
		return m_Ptr[in_Index];
	}
};

template <class T>
DataArray<T>& DataArray<T>::operator= ( DataArray const& in_DataArray )
{
	if ( in_DataArray.m_Size == 0 )
	{
		Clear();
	}
	else
	{
		delete [] m_Ptr;
		m_Size = in_DataArray.m_Size;
		m_Ptr = new T[m_Size];
		for ( int i=0; i<m_Size; i++ )
		{
			m_Ptr[i] = in_DataArray.m_Ptr[i];
		}
	}

	return *this;
};

template <class T>
void DataArray<T>::Add ( T const& in_Item )
{
	//Check if the array is not null.
	bool checkCopy = false;
	if m_Size != 0
		checkCopy = true;

	//Add one to the size of the array.
	m_Size += 1;
	//Create a new point for the resized array.
	T *newPtr = new T[m_Size];
	if ( checkCopy )
	{
		//Copy the old array value.
		for ( int i=0; i<m_Size-1 ; i++ )
		{
			newPtr[i] = m_Ptr[i];
		}
	}

	//Add the new value in array.
	newPtr[m_Size-1] = in_Item;
	//Release old memory allocation.
	delete [] m_Ptr;
	//Retarget the class point to the new point.
	m_Ptr = newPtr;
};

template <class T>
bool DataArray<T>::InsertAt ( int in_Index, T const& in_Item )
{
	//Variable for the success of the insertion.
	bool success = false;
	//Test if the index is inside the array.
	if ( in_Index < m_Size )
	{
		//Update the new of the array.
		m_Size += 1;
		//Create the resized array.
		T *newPtr = new T[m_Size];
		//Variable for check when we add the item in the new array.
		bool checkAdded = false;
		for ( int i=0; i<m_Size; i++ )
		{
			//Test if we are at the good index to add the item.
			if ( i == in_Index )
			{
				checkAdded = true;
				newPtr[i] = in_Item;
			}
			else
			{
				if ( checkAdded )
				{
					newPtr[i] = m_Ptr[i-1];
				}
				else
				{
					newPtr[i] = m_Ptr[i];
				}
			}
		}

		delete [] m_Ptr;

		m_Ptr = newPtr;

		sucess = true;
	}

	return sucess;
};

template <class T>
bool DataArray<T>::ReplaceAt ( int in_Index, T const& in_Item )
{
	bool success = false;

	if ( in_Index < m_Size )
	{
		for ( int i=0; i<m_Size; i++ )
		{
			if ( i == in_Index )
			{
				m_Ptr[i] = in_Item;
			}
		}

		success = true;
	}

	return sucess;
};

template <class T>
bool DataArray<T>::Remove ( T const& in_Item )
{
	bool checkExist;
	//Test if the item exist in the Array.
	checkExist = IsExist ( in_Item );
	//If exist we remove it.
	if ( checkExist )
	{
		//Define the new point for the resize array.
		T *newPtr = new T[m_Size-1];

		//Variable for check when we need to skip the element to remove.
		bool findItem = false;
		//Copy old array elments without the item.
		for ( int i=0; i<m_Size; i++ )
		{
			//Check if the current element is the same that we want to remove.
			if ( m_Ptr[i] == in_Item )
			{
				findItem = true; 
			}
			else
			{
				if ( findItem )
				{
					newPtr[i-1] = m_Ptr[i];
				}
				else
				{
					newPtr[i] = m_Ptr[i];
				}
			}
		}

		//Release the old memory.
		delete [] m_Ptr;
		//Re Assign the class point.
		m_Ptr = newPtr;
		//Update the new size.
		m_Size -= 1;
	}

	return checkExist;
};

template <class T>
bool DataArray<T>::IsExist ( T const& in_Item )
{
	bool checkExist = false;

	if ( m_Size != 0 )
	{
		for ( int i=0; i<m_Size ; i++ )
		{
			if ( m_Ptr[i] == in_Item )
			{
				checkExist = true;
			}
		}
	}

	return checkExist;
};

template <class T>
int DataArray<T>::GetSize () const
{
	return m_Size;
};

template <class T>
void DataArray<T>::Clear()
{
	delete [] m_Ptr;
	m_Ptr = 0;
	m_Size = 0;
};

template <class T>
void DataArray<T>::AddDataArray ( DataArray const& in_DataArray )
{
	//Calcule the new array Size.
	int newArraySize = m_Size+in_DataArray.m_Size;
	//Create the new Array.
	T *newPtr = new T[newArraySize];
	//Copy the first array in the new array.
	for ( int i=0; i<m_Size; i++ )
	{
		newPtr[i] = m_Ptr[i]
	}
	//Copy the second array in the new array.
	for ( int i=m_Size; i< newArraySize; i++ )
	{
		newPtr[i] = in_DataArray.m_Ptr[i-m_Size];
	}
	//Clear the old point array.
	delete [] m_Ptr;
	//Update the point.
	m_Ptr = newPtr;
	//Update the Size.
	m_Size = newArraySize;
};

*/