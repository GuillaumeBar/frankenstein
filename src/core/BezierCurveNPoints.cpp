/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file BezierCurveNPoints.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "BezierCurveNPoints.h"
#include <iostream>
using namespace std;

const int BezierCurveNPoints::m_MemoryBaseSize = 500;

//CONSTRUCTORS.
BezierCurveNPoints::BezierCurveNPoints (){

	m_pointNumber = 1;
	m_ArrayPointsControl.SetSizeAndClear ( 2 );
	m_Length = 0.0f;
	m_StartRange = 0.0f;
	m_EndRange = 1.0f;
	m_MoveRange = 0.0f;
	m_MoveStartRange = m_StartRange + m_MoveRange;
	m_MoveEndRange = m_EndRange + m_MoveRange;
	m_ArrayPoints.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayT.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayLengths.SetSizeAndClear(m_MemoryBaseSize);
	m_ArraySubVectors.SetSizeAndClear(m_MemoryBaseSize);
	m_ArraySubTangents.SetSizeAndClear(m_MemoryBaseSize);
};

BezierCurveNPoints::BezierCurveNPoints ( int in_PointNumber,
											DataArray<Vector3D> const& in_PointsControl ){

	m_pointNumber = in_PointNumber;
	m_ArrayPointsControl = in_PointsControl;
	m_Length = 0.0f;
	m_StartRange = 0.0f;
	m_EndRange = 1.0f;
	m_MoveRange = 0.0f;
	m_MoveStartRange = m_StartRange + m_MoveRange;
	m_MoveEndRange = m_EndRange + m_MoveRange;

	if(in_PointNumber > m_MemoryBaseSize){
		m_ArrayPoints.SetSizeAndClear ( in_PointNumber );
		m_ArrayT.SetSizeAndClear ( in_PointNumber );
		m_ArrayLengths.SetSizeAndClear ( in_PointNumber );
		m_ArraySubVectors.SetSizeAndClear(in_PointNumber);
		m_ArraySubTangents.SetSizeAndClear(in_PointNumber);
	}else{
		m_ArrayPoints.SetSizeAndClear ( m_MemoryBaseSize );
		m_ArrayT.SetSizeAndClear ( m_MemoryBaseSize );
		m_ArrayLengths.SetSizeAndClear ( m_MemoryBaseSize );
		m_ArraySubVectors.SetSizeAndClear(m_MemoryBaseSize);
		m_ArraySubTangents.SetSizeAndClear(m_MemoryBaseSize);
	}
};

//DESTRUCTOR.
BezierCurveNPoints::~BezierCurveNPoints (){

};

// CUSTOM METHODES.
DataArray<Vector3D> BezierCurveNPoints::GetCurveSubVectors(){
	return m_ArraySubVectors;
}

DataArray<Vector3D> BezierCurveNPoints::GetCurvePoints(){
	return m_ArrayPoints;
};

DataArray<float> BezierCurveNPoints::GetCurveTs(){
	return m_ArrayT;
};

DataArray<float> BezierCurveNPoints::GetCurveLengths(){
	return m_ArrayLengths;
};

DataArray<Vector3D> BezierCurveNPoints::GetCurveSubTangents(){
	return m_ArraySubTangents;
};


void BezierCurveNPoints::ComputeCurve(){

	Vector3D distVector, prevPos, nextPos;
	Vector3D prevTan, nextTan;

	m_Length = 0.0f;
	m_ArrayLengths[0] = 0.0f;
	for(unsigned int i=0; i<m_pointNumber; i++){
		// Compute the point t value of the curve.
		ComputeOneT(i);
		// Compute the point position of the curve.
		ComputeOnePointPosition(i);
		// Compute the length of the curve.
		if(i > 0){
			// Get the previous and next point position.
			prevPos = GetPointPositionAt(i-1);
			nextPos = GetPointPositionAt(i);
			// Compute the vector between the prev and next point.
			distVector = nextPos - prevPos;
			// Compute the length of the vector and add to the length.
			m_Length += distVector.GetLength();
			// Store the point length.
			m_ArrayLengths[i] = m_Length;
			// Store the point vector for computing tangent.
			m_ArraySubVectors[i - 1] = distVector;
		}
		// Compute the tangents.
		if(i == 2){
			// Compute the tangent for the first point.
			m_ArraySubTangents[0] = m_ArraySubVectors[0];
		}
		if(i > 1){
			// Compute the tangent fot the previous point.
			prevTan.MulByFloat(m_ArraySubVectors[i - 2], 0.5f);
			nextTan.MulByFloat(m_ArraySubVectors[i - 1], 0.5f);
			m_ArraySubTangents[i - 1].Add(prevTan, nextTan);
		}
		if(i == m_pointNumber - 1){
			m_ArraySubTangents[i] = m_ArraySubTangents[i - 1];
		}
	}
}

void BezierCurveNPoints::ComputeArrayT (){

	for ( int i=0; i<m_pointNumber; i++ )
	{
		ComputeOneT(i) ;
	}
};

float BezierCurveNPoints::ComputeFactoriel ( int in_N ){

	float rsltFact = 1;
	float pi = 3.14159265359f;
	float e = 2.71828f;

	if ( in_N != 0 )
	{
		//Approximation of Ramanujan.
		if ( in_N > 10 )
		{
			rsltFact = sqrt ( pi ) * pow ( ( in_N / e ), in_N);
			rsltFact *= pow ( ( ( ( 8.0f * in_N + 4.0f ) * in_N + 1.0f ) * in_N + 1.0f / 30.0f ), ( 1.0f / 6.0F ) );
		}
		//Classic Computing.
		else
		{
			for ( int i=1 ; i<in_N ; i++ )
			{
				rsltFact = rsltFact * ( i + 1 );
			}
		}
	}

	return rsltFact;
};

float BezierCurveNPoints::ComputeCoeficientBinomial ( int in_N, int in_I ){

	float rsltCoefBi = 1.0f ;

	int m = in_N - in_I ;

	float fn, fi, fm ;

	if ( in_I != 0 && in_I != in_N )
	{
		fn = ComputeFactoriel ( in_N );
		fi = ComputeFactoriel ( in_I );
		fm = ComputeFactoriel ( m );

		rsltCoefBi = fn / ( fi * fm );
	}

	return rsltCoefBi;
};

void BezierCurveNPoints::ComputeOnePointPosition ( int in_Index ){

	//Compute t.
	ComputeOneT ( in_Index );
	if ( m_ArrayPointsControl.GetSize() == 4 )
	{
		m_ArrayPoints[in_Index] = ComputeBezierCurve4PointOnePoint ( m_ArrayT[in_Index] );
	}
	else if ( m_ArrayPointsControl.GetSize() == 8 )
	{
		m_ArrayPoints[in_Index] = ComputeBezierCurve8PointOnePoint ( m_ArrayT[in_Index] );
	}
	else
	{
		m_ArrayPoints[in_Index] = ComputeBezierCurveNPointOnePoint ( m_ArrayT[in_Index] );
	}
};

void BezierCurveNPoints::ComputeArrayPointsPosition (){

	//Loop for each points.
	for ( int i=0 ; i<m_pointNumber; i++ )
	{
		ComputeOnePointPosition ( i );
	}
};

void BezierCurveNPoints::Set ( int in_PointNumber,
								DataArray<Vector3D> const& in_PointsControl,
								float const& in_StartRange,
								float const& in_EndRange,
								float const& in_MoveRange ){

	m_pointNumber = in_PointNumber;
	m_ArrayPointsControl = in_PointsControl;
	m_StartRange = in_StartRange;
	m_EndRange = in_EndRange;
	m_MoveRange = in_MoveRange;
	m_MoveStartRange = m_StartRange + m_MoveRange;
	m_MoveEndRange = m_EndRange + m_MoveRange;
};

void BezierCurveNPoints::SetPointsNumbers ( int in_PointNumber ){

	m_pointNumber = in_PointNumber;
	if ( in_PointNumber > m_MemoryBaseSize )
	{
		m_ArrayPoints.SetSizeAndClear(m_pointNumber);
		m_ArrayT.SetSizeAndClear(m_pointNumber);
	}
};

void BezierCurveNPoints::SetRange ( float const& in_StartRange,
									float const& in_EndRange,
									float const& in_MoveRange ){

	m_StartRange = in_StartRange;
	m_EndRange = in_EndRange;
	m_MoveRange = in_MoveRange;
	m_MoveStartRange = m_StartRange + m_MoveRange;
	m_MoveEndRange = m_EndRange + m_MoveRange;
};

Vector3D BezierCurveNPoints::GetPointPositionAt ( int in_Index ){

	return m_ArrayPoints[in_Index];
};

void BezierCurveNPoints::SetPointsControl ( DataArray<Vector3D> const& in_PointsControl ){

	m_ArrayPointsControl = in_PointsControl;
};

void BezierCurveNPoints::ComputeCurveLength (){

	Vector3D vBetween2Points;
	m_Length = 0;

	for ( int i=0; i<m_pointNumber-1; i++ ){
		vBetween2Points.Sub ( m_ArrayPoints[i], m_ArrayPoints[i+1] );
		m_Length += vBetween2Points.GetLength();
	}
};

float BezierCurveNPoints::GetCurveLength () const{

	return m_Length;
};

void BezierCurveNPoints::ComputeOneT ( int in_Index ){

	float tFraction;

	//Compute the t fractoin.
	tFraction = float(in_Index) / float( m_pointNumber - 1 );

	//Retarget the t Fraction value in Range m_MoveStartRange - m_MoveEndRange.
	m_ArrayT[in_Index] = m_MoveStartRange + tFraction * ( m_MoveEndRange - m_MoveStartRange ); 
};

void BezierCurveNPoints::ComputeStartEndRangeDistribution (){

	m_MoveStartRange = m_StartRange + m_MoveRange;
	m_MoveEndRange = m_EndRange + m_MoveRange;

	//Clamp the startWithMove between 0 - 1.
	if ( m_MoveStartRange > 1.0f ){
		m_MoveStartRange = 1.0f;
	}else if ( m_MoveStartRange < 0.0f ){
		m_MoveStartRange = 0.0f;
	}
	//Clamp the endWithMove between 0 - 1.
	if ( m_MoveEndRange > 1.0f ){
		m_MoveEndRange = 1.0f;
	}else if ( m_MoveEndRange < 0.0f ){
		m_MoveEndRange = 0.0f;
	}
};

Vector3D BezierCurveNPoints::ComputeBezierCurve8PointOnePoint ( float in_T ){

	Vector3D			rsltPosition;
	DataArray<float>	arrayT(7);
	DataArray<float>	arrayInvT(7);
	DataArray<float>	arrayBase(8);

	// Compute t and invert t.
	arrayT[0] = in_T;
	arrayT[1] = arrayT[0] * arrayT[0];
	arrayT[2] = arrayT[1] * arrayT[0];
	arrayT[3] = arrayT[1] * arrayT[1];
	arrayT[4] = arrayT[3] * arrayT[0];
	arrayT[5] = arrayT[2] * arrayT[2];
	arrayT[6] = arrayT[5] * arrayT[0];

	arrayInvT[0] = 1.0f - arrayT[0];
	arrayInvT[1] = arrayInvT[0] * arrayInvT[0];
	arrayInvT[2] = arrayInvT[1] * arrayInvT[0];
	arrayInvT[3] = arrayInvT[1] * arrayInvT[1];
	arrayInvT[4] = arrayInvT[3] * arrayInvT[0];
	arrayInvT[5] = arrayInvT[2] * arrayInvT[2];
	arrayInvT[6] = arrayInvT[5] * arrayInvT[0];

	//Compute the base.
	arrayBase[0] = arrayInvT[6];
	arrayBase[1] = arrayT[0] * arrayInvT[5] * 7;
	arrayBase[2] = arrayT[1] * arrayInvT[4] * 21;
	arrayBase[3] = arrayT[2] * arrayInvT[3] * 35;
	arrayBase[4] = arrayT[3] * arrayInvT[2] * 35 ;
	arrayBase[5] = arrayT[4] * arrayInvT[1] * 21;
	arrayBase[6] = arrayT[5] * arrayInvT[0] * 7;
	arrayBase[7] = arrayT[6];

	//Compute the point position.
	for ( int i=0 ; i<8 ; i++ ){
		rsltPosition.Set (	rsltPosition.GetX() + m_ArrayPointsControl[i].GetX() * arrayBase[i],
							rsltPosition.GetY() + m_ArrayPointsControl[i].GetY() * arrayBase[i],
							rsltPosition.GetZ() + m_ArrayPointsControl[i].GetZ() * arrayBase[i] );
	}

	return rsltPosition;
};

Vector3D BezierCurveNPoints::ComputeBezierCurve4PointOnePoint ( float in_T ){

	Vector3D			rsltPosition;
	DataArray<float>	arrayT(3);
	DataArray<float>	arrayInvT(3);
	DataArray<float>	arrayBase(4);

	// Compute t
	arrayT[0] = in_T;
	arrayT[1] = arrayT[0] * arrayT[0];
	arrayT[2] = arrayT[1] * arrayT[0];

	// Compute invT.
	arrayInvT[0] = 1 - arrayT[0];
	arrayInvT[1] = arrayInvT[0] * arrayInvT[0];
	arrayInvT[2] = arrayInvT[1] * arrayInvT[0];

	// Compute Base.
	arrayBase[0] = arrayInvT[2];
	arrayBase[1] = arrayT[0] * arrayInvT[1] * 3;
	arrayBase[2] = arrayT[1] * arrayInvT[0] * 3;
	arrayBase[3] = arrayT[2];

	//Compute the point position.
	for ( int i=0 ; i<4 ; i++ ){

		rsltPosition.Set (	rsltPosition.GetX() + m_ArrayPointsControl[i].GetX() * arrayBase[i],
							rsltPosition.GetY() + m_ArrayPointsControl[i].GetY() * arrayBase[i],
							rsltPosition.GetZ() + m_ArrayPointsControl[i].GetZ() * arrayBase[i] );
	}

	return rsltPosition;
};

Vector3D BezierCurveNPoints::ComputeBezierCurveNPointOnePoint ( float in_T ){

	int			n;
	float		t, invT, baseP, coefBi;
	Vector3D	finalPos;

	t = in_T;
	invT = 1 - t ;

	//Compute n.
	n = m_ArrayPointsControl.GetSize() - 1;

	//Loop on the array of controllers.
	for ( int i=0 ; i<m_ArrayPointsControl.GetSize() ; i++ ){
		if ( i == 0 ){
			baseP = pow ( invT, n );
		}else if ( i == n ){
			baseP = pow ( t, n );
		}else{
			coefBi = ComputeCoeficientBinomial ( n, i );
			baseP = coefBi * pow ( t, i ) * pow ( invT, ( n - i ) );
		}

		finalPos.Set (	finalPos.GetX() + m_ArrayPointsControl[i].GetX() * baseP,
						finalPos.GetY() + m_ArrayPointsControl[i].GetY() * baseP,
						finalPos.GetZ() + m_ArrayPointsControl[i].GetZ() * baseP );
	}

	return finalPos;
};

void BezierCurveNPoints::NormalizeArrayPointsPosition ( int in_MaxRecursiveLoop ){

	//Loop for each points.
	for ( int i=0 ; i<m_pointNumber; i++ ){
		NormalizeOnePointPosition ( i, in_MaxRecursiveLoop );
	}
};

void BezierCurveNPoints::NormalizeOnePointPosition ( int in_Index, int in_MaxRecursiveLoop ){

	//Compute the interval distance from first point.
	float	CurrentInterval = float ( in_Index ) * ( m_Length / ( m_pointNumber-1 ) );

	float	currentT = m_MoveStartRange;
	float	infPointDist = 0.0f;
	float	supPointDist = 0.0f;
	long	whileCounter = 0;

	Vector3D	supPointPos;
	Vector3D	infPointPos = m_ArrayPoints[0];
	Vector3D	distVector;

	//Find the good position and t.
	while ( supPointDist < CurrentInterval && whileCounter < in_MaxRecursiveLoop ){
		//Compute Sub T in loop.
		currentT = currentT + 1 / float(in_MaxRecursiveLoop) * m_MoveEndRange;
		//Update infPointDist.
		infPointDist = supPointDist;
		//Compute sup point position.
		if ( m_ArrayPointsControl.GetSize() == 4 ){
			supPointPos = ComputeBezierCurve4PointOnePoint ( currentT );
		}else if ( m_ArrayPointsControl.GetSize() == 8 ){
			supPointPos = ComputeBezierCurve8PointOnePoint ( currentT );
		}else{
			supPointPos = ComputeBezierCurveNPointOnePoint ( currentT );
		}
		//Compute the distance between sup and inf point.
		distVector.Sub ( supPointPos, infPointPos );
		//Compute the sup point distance.
		supPointDist = supPointDist + distVector.GetLength();
		//Update the inf pos distance.
		infPointPos = supPointPos;

		whileCounter +=1 ;
	}
	//Interpolated the result for more accurate result.
	float infToInter = CurrentInterval - infPointDist;
	float intToSup = supPointDist - infPointDist;
	float pourcent = 1.0f;

	if ( intToSup > 0.0f ){
		pourcent = infToInter / intToSup;
	}

	currentT = currentT - ( ( 1 - pourcent ) * ( 1 / float(in_MaxRecursiveLoop) * m_MoveEndRange ) );

	m_ArrayPoints[in_Index] = ComputeBezierCurveNPointOnePoint ( currentT );
};
