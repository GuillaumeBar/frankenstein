/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file FitBezierCurveNPoints.h
@author Guillaume Baratte
@date 2016-05-17
@brief Create a Fit Bezier Curve with N points with n Controler.
*/

#ifndef __FITBEZIERCURVENPOINTS__
#define __FITBEZIERCURVENPOINTS__

#include "DataArray.h"
#include "Vector3D.h"

class FitBezierCurveNPoints
{
	public:
		//CONTRUCTOR.
		/*! @brief Primary constructor. */
		FitBezierCurveNPoints ();

		//DESTRUCTOR.
		/*! @brief Destructor.*/
		~FitBezierCurveNPoints ();

		//CUSTOM METHODES.
		void ComputeCurve();

		/*! @brief Compute the additionnal tangent point for create sub bezier curves.*/
		void ComputeTangents();
		
		/*! @brief Compute the position of one point of the curve.
		*@param in_Index The index of the point to compute the t value.
		*/
		void ComputeOneFitPoint(int in_Index);

		/*! @brief Compute the position of all points of the curve.*/		
		void ComputeAllFitPoint();

		/*! @brief Compute the t value of one point of the curve.
		*@param in_Index The index of the point to compute the t value.
		*/
		void ComputeOneFitT(int in_Index);

		/*! @brief Compute the t value of all points of the curve.*/
		void ComputeAllFitT();

		/*! @brief Add the user tangent in the all point array to override the automatic tangents.*/
		void AddUserTangentsToArrayAllPoints();

		Vector3D ComputeBezierCurve4PointOnePoint(float in_T, DataArray<Vector3D>& in_PointPosition);

		/*! @brief Set the data of the fit bezier curve.
		*@param in_NumberPoints The number of points for the curve.
		*@param in_ArrayBasePoints The position of the controllers points.
		*@param in_FitStartRange The start range of the distribution.
		*@param in_FitEndRange The end range of the distribution.
		*@param in_FitMoveRange The offset of the distribution.
		*/
		void Set( 	int& in_NumberPoints,
					DataArray<Vector3D>& in_ArrayBasePoints,
					float& in_FitStartRange,
					float& in_FitEndRange,
					float& in_FitMoveRange,
					int& in_UseUserTangents,
					DataArray<Vector3D>& in_ArrayUserTangents);
		
		void Set( 	int& in_NumberPoints,
					DataArray<Vector3D>& in_ArrayBasePoints,
					float& in_FitStartRange,
					float& in_FitEndRange,
					float& in_FitMoveRange,
					int& in_UseUserTangents,
					float& in_inTangentLength,
					float& in_outTangentLength,
					DataArray<Vector3D>& in_ArrayUserTangents);

		/*! @brief Compute the length of the curve. */
		float ComputeFitCurveLength();

		/* @brief Return the array of the curve point position.*/
		DataArray<Vector3D> GetCurvePoints();
		DataArray<float> GetCurveSubLengths();
		DataArray<float> GetCurveTs();
		DataArray<Vector3D> GetCurveSubVectors();
		DataArray<Vector3D> GetCurveSubTangents();

		/* @brief Return the curve point position at index.*/
		Vector3D GetCurvePointsAt(int in_Index);

		float	GetTValueAt(int in_Index);

		float GetLengthAt(int in_Index);

		float GetLength();

	private:
		int 					m_NumberPoints;					/*! The number of point to compute.*/
		DataArray<Vector3D>		m_ArrayBasePoints;				/*! Array to store the base controller points.*/
		DataArray<float>		m_ArrayPointsT;					/*! Array to store the T value for each point on the curve.*/
		DataArray<Vector3D>		m_ArrayInterpolatedTangents;	/*! Array to store the interpolated tangent of each controller points.*/
		DataArray<Vector3D>		m_ArrayAllPoints;				/*! Array to store the base controller points and the tangents points.*/	
		DataArray<float>		m_ArraySubDistance;				/*! Array to store the distance between each controller points.*/
		DataArray<Vector3D>		m_ArraySubBezierController;		/*! Array to store the sub controller for sub bezier curve. */
		DataArray<Vector3D>		m_ArraySubTangents;
		DataArray<Vector3D>		m_ArrayUserTangents;			/*! Array to store the user tangents.*/
		int 					m_UseUserTangents;				/*! Active the use of the user tangents or auto tangents.*/
		float					m_FitStartRange;				/*! Define the t value of the start for curve point distribution.*/
		float					m_FitEndRange;					/*! Define the t value of the end for curve point distribution.*/
		float					m_FitMoveRange;					/*! Define the t value to move the curve point distribution.*/
		DataArray<Vector3D>		m_ArrayCurvePointsPosition;		/*! Array to store the position of the curve point.*/
		DataArray<float>		m_ArraySubLength; /*! Store the sub length of the point. */
		float					m_curveLength; /*! Store the length of the curve. */
		DataArray<Vector3D>		m_ArraySubVectors;

		float					m_inTangentLength;
		float					m_outTangentLength;

		static const int		m_MemoryBaseSize;
};


#endif