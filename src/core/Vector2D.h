/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Vector2D.h
@author Guillaume Baratte
@date 2016-03-27
@brief Define the vector2D class.
Help to manipulate the vector 2D data.
*/

#ifndef __VECTOR2D__
#define __VECTOR2D__

#include <math.h>

class Vector2D
{
	public:
		//METHODES.
		
		//CONSTRUCTOR.
		
		/*! @brief Primary constructor. */
		Vector2D ();
		
		/*! @brief Constructor with X et Y value.
		*@param in_fX Value for X.
		*@param in_fY Value for Y.
		*/
		Vector2D ( float in_fX, float in_fY );
		
		//DESTRUCTOR.
		/*! @brief Primary destructor. */
		~Vector2D ();
	
		//COMPARE OPERATOR.
		/*! @brief Define the equality operator.
		*@param in_VectorB The vector2D we want to compare.
		*@return True or False
		*/
		bool operator== ( Vector2D const& in_VectorB );
		
		/*! @brief Define the not equality operator.
		*@param in_VectorB The Vector2D we want to compare.
		*@return True or False.
		*/
		bool operator!= ( Vector2D const& in_VectorB );
		
		//OPERATIONS OPERATOR.
		
		/*! @brief Define the add operator.
		*@param in_VectorB Vector2D we want to add.
		*@return The result of the add.
		*/
		Vector2D operator+ ( Vector2D const& in_VectorB );
		
		/*! @brief Define the substract operator.
		*@param in_VectorB Vector2D we want to subtract.
		*@return The result of the substract.
		*/
		Vector2D operator- ( Vector2D const& in_VectorB );
		
		/*! @brief Define the multiply operator.
		*@param in_Float The value we want to multiply the vector.
		*@return The result of the multiply.
		*/
		Vector2D operator* ( float const& in_Float );
	
		//CUSTOM METHODES.
		/*! @brief Check if Vector2D are equal.
		@param in_VectorB The Vector2D we want to test.
		@return True or False.
		*/
		bool IsEqual ( Vector2D const& in_VectorB ) const;
		
		/*! @brief Add a Vector2D with the current Vector2D.
		*@param in_VectorB The Vector2D we want to add.
		*@return The result of the add in a new Vector2D.
		*/
		Vector2D Add ( Vector2D const& in_VectorB );
		
		/*! @brief Sub a Vector2D with the current Vector2D.
		*@param in_VectorB The Vector2D we want to sub.
		*@return The result of the sub in a new Vector2D.
		*/
		Vector2D Sub ( Vector2D const& in_VectorB );
		
		/*! @brief Multiply the current Vector2D by a float.
		*@param in_Float The value use to multiply the Vector2D.
		*@return The result of the multiply.
		*/
		Vector2D MulByFloat ( float const& in_Float );
		
		/*! @brief Calcul the length of the Vector2D.
		*@return The length of the Vector2D.
		*/
		float GetLength () const;
		
		/*! @brief Normalize a Vector2D in the current Vector2D.
		*@param in_VectorA The Vector2D we want to normalize.
		*@return True if we want normalize the Vector2D.
		*/
		bool Normalize ( Vector2D & in_VectorA );
	
		/*! @brief Calcul the dot product between two Vector2D.
		*@param in_VectorB The second Vector2D we want use for the dot product.
		*@return The result of the dot product.
		*/
		float DotProduct ( Vector2D const& in_VectorB ) const;
		
		/*! @brief Calcul the angle between two Vector2D.
		*@param in_VectorB The second Vector2D we want to use for calcul the angle.
		*@param in_Degrees True for the result in Degrees. False for the result in Radians.
		*@return The angle between the two Vector2D. Return -1 if one of the vector have a length equal to 0.
		*/
		float GetAngle ( Vector2D const& in_VectorB, bool in_Degrees ) const;
		
	
		//SET METHODES.
		
		/*! @brief Use for set the value x and y of the vector2D.
		*@param in_fX Value of X.
		*@param in_fY Value of Y.
		*/
		void Set ( float in_fX, float in_fY );
		
		/*! @brief Set the current Vector2D with the value of another Vector2D.
		*@param in_Vector2D Vector2D from copy the value X and Y.
		*/
		void SetFromVector2D ( Vector2D const& in_Vector2D );
		
		/*! @brief Use forset the value x of the vector2D.
		*@param in_Float Value of X.
		*/
		void SetX ( float in_Float );
		
		/*! @brief Use forset the value x of the vector2D.
		*@param in_Float Value of Y.
		*/		
		void SetY ( float in_Float );

		/*! @brief Use for get the value of X from the vector 2D.
		*@return The value of X.
		*/
		float GetX () const;
		
		/*! @brief Use for get the value of Y from the vector 2D.
		*@return The value of Y.
		*/		
		float GetY () const;

	private:
		// Attributs
		float m_x;	/*! Attribut for x. */
		float m_y;	/*! Attribut for y. */
};


#endif