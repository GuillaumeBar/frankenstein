/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Matrix3x3.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "Matrix3x3.h"

//CONSTRUCTORS.
Matrix3x3::Matrix3x3()
{
	m_Mat[0][0] = 1.0f;
	m_Mat[0][1] = 0.0f;
	m_Mat[0][2] = 0.0f;
	m_Mat[1][0] = 0.0f;
	m_Mat[1][1] = 1.0f;
	m_Mat[1][2] = 0.0f;
	m_Mat[2][0] = 0.0f;
	m_Mat[2][1] = 0.0f;
	m_Mat[2][2] = 1.0f;
};

Matrix3x3::Matrix3x3 ( 	float in_00, float in_01, float in_02,
						float in_10, float in_11, float in_12,
						float in_20, float in_21, float in_22 )
{
	m_Mat[0][0] = in_00;
	m_Mat[0][1] = in_01;
	m_Mat[0][2] = in_02;
	m_Mat[1][0] = in_10;
	m_Mat[1][1] = in_11;
	m_Mat[1][2] = in_12;
	m_Mat[2][0] = in_20;
	m_Mat[2][1] = in_21;
	m_Mat[2][2] = in_22;
};

Matrix3x3::Matrix3x3 ( Matrix3x3 const& in_Matrix )
{
	m_Mat[0][0] = in_Matrix.m_Mat[0][0];
	m_Mat[0][1] = in_Matrix.m_Mat[0][1];
	m_Mat[0][2] = in_Matrix.m_Mat[0][2];
	m_Mat[1][0] = in_Matrix.m_Mat[1][0];
	m_Mat[1][1] = in_Matrix.m_Mat[1][1];
	m_Mat[1][2] = in_Matrix.m_Mat[1][2];
	m_Mat[2][0] = in_Matrix.m_Mat[2][0];
	m_Mat[2][1] = in_Matrix.m_Mat[2][1];
	m_Mat[2][2] = in_Matrix.m_Mat[2][2];	
};

//DESTRUCTOR.
Matrix3x3::~Matrix3x3()
{

};

//OPERATIONS OPERATOR
bool Matrix3x3::operator== ( Matrix3x3 const& in_MatrixB ) const
{
	return IsEquals ( in_MatrixB );
}

bool Matrix3x3::operator!= ( Matrix3x3 const& in_MatrixB ) const
{
	return ! IsEquals ( in_MatrixB );
}

Matrix3x3& Matrix3x3::operator* ( Matrix3x3 const& in_MatrixB )
{
	return Mul ( *this, in_MatrixB );
}

//CUSTOM METHODES.
bool Matrix3x3::Invert ( Matrix3x3 const& in_MatrixB )
{
	float detM, invDetM;
	float det00, det01, det02;
	float det10, det11, det12;
	float det20, det21, det22;
	
	//Calculate the minor determinant.
	det00 = in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[2][2] - in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[2][1];
	det01 = in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[2][2] - in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[2][0];
	det02 = in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[2][1] - in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[2][0];
	
	//Matrix determinant.
	detM = in_MatrixB.m_Mat[0][0] * det00 - in_MatrixB.m_Mat[0][1] * det01 + in_MatrixB.m_Mat[0][2] * det02;
	
	//if determinant not equals to 0, the matrix have inverse.
	if ( detM != 0 )
	{
		//Invert the determinant.
		invDetM = 1 / detM;
	
		//Calculate the minor matrix determinant.
		det10 = in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[2][2] - in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[2][1];
		det11 = in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[2][2] - in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[2][0];
		det12 = in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[2][1] - in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[2][0];
		
		det20 = in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[1][2] - in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[1][1];
		det21 = in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][2] - in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[1][0];
		det22 = in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][1] - in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[1][0];

		//Update this matrix.
		m_Mat[0][0] = det00 * invDetM;
		m_Mat[0][1] = -det10 * invDetM;
		m_Mat[0][2] = det20 * invDetM;
		
		m_Mat[1][0] = -det01 * invDetM;
		m_Mat[1][1] = det11 * invDetM;
		m_Mat[1][2] = -det21 * invDetM;
		
		m_Mat[2][0] = det02 * invDetM;
		m_Mat[2][1] = -det12 * invDetM;
		m_Mat[2][2] = det22 * invDetM;
		
		return true;
	}
	else
	{
		return false;
	}
};

bool Matrix3x3::SelfInvert ()
{
	bool checkInvert = Invert( *this );
	
	return checkInvert;
}

void Matrix3x3::Transpose ( Matrix3x3 const& in_MatrixB )
{
	for ( int i=0; i<3; i++ )
	{
		for ( int j=0; j<3 ; j++ )
		{
			m_Mat[i][j] = in_MatrixB.m_Mat[j][i];
		}
	}
}

void Matrix3x3::SelfTranspose ()
{
	Transpose ( *this );
}

bool Matrix3x3::TransposeInverse (  Matrix3x3 const& in_MatrixB )
{
	bool checkInvert = Invert ( in_MatrixB );
	if ( checkInvert )
	{
		Transpose ( *this );
	}
	return checkInvert;
}

bool Matrix3x3::SelfTransposeInverse ()
{
	bool checkInvert = Invert ( *this );
	if( checkInvert )
	{
		Transpose ( *this );
	}
	return checkInvert;
}

bool Matrix3x3::IsEquals ( Matrix3x3 const& in_MatrixB ) const
{
	if ( 	m_Mat[0][0] == in_MatrixB.m_Mat [0][0] &&
			m_Mat[0][1] == in_MatrixB.m_Mat [0][1] &&
			m_Mat[0][2] == in_MatrixB.m_Mat [0][2] &&
			m_Mat[1][0] == in_MatrixB.m_Mat [1][0] &&
			m_Mat[1][1] == in_MatrixB.m_Mat [1][1] &&
			m_Mat[1][2] == in_MatrixB.m_Mat [1][2] &&
			m_Mat[2][0] == in_MatrixB.m_Mat [2][0] &&
			m_Mat[2][1] == in_MatrixB.m_Mat [2][1] &&
			m_Mat[2][2] == in_MatrixB.m_Mat [2][2] )
	{
		return true;
	}
	else
	{
		return false;
	}	
}

Matrix3x3& Matrix3x3::Mul ( Matrix3x3 const& in_MatrixA, Matrix3x3 const& in_MatrixB )
{
	for ( int i=0; i<3; i++ )
	{
		for ( int j=0; j<3; j++ )
		{
			m_Mat[i][j] = 	in_MatrixA.m_Mat[i][0] * in_MatrixB.m_Mat[0][j] +
							in_MatrixA.m_Mat[i][1] * in_MatrixB.m_Mat[1][j] +
							in_MatrixA.m_Mat[i][2] * in_MatrixB.m_Mat[2][j] ;
		}
	}
	
	return *this;
}

Matrix3x3& Matrix3x3::SelfMul ( Matrix3x3 const& in_MatrixB )
{
	return Mul ( *this, in_MatrixB );
}

//GET METHODES.
void Matrix3x3::Get ( 	float &io_00, float &io_01, float &io_02,
						float &io_10, float &io_11, float &io_12,
						float &io_20, float &io_21, float &io_22 ) const
{
	io_00 = m_Mat[0][0];
	io_01 = m_Mat[0][1];
	io_02 = m_Mat[0][2];
	io_10 = m_Mat[1][0];
	io_11 = m_Mat[1][1];
	io_12 = m_Mat[1][2];
	io_20 = m_Mat[2][0];
	io_21 = m_Mat[2][1];
	io_22 = m_Mat[2][2];	
};

float Matrix3x3::GetValue (  int in_Row, int in_Col ) const
{
	if ( in_Row >= 0 && in_Row <= 2 && in_Col >= 0 && in_Col <= 2 )
	{
		return m_Mat[in_Row][in_Col];
	}
	else
	{
		return 0.0f;
	}
};

void Matrix3x3::GetAxeX ( float &io_Xx, float &io_Xy, float &io_Xz ) const
{
	io_Xx = m_Mat[0][0];
	io_Xy = m_Mat[0][1];
	io_Xz = m_Mat[0][2];
};

void Matrix3x3::GetAxeY ( float &io_Yx, float &io_Yy, float &io_Yz ) const
{
	io_Yx = m_Mat[0][0];
	io_Yy = m_Mat[0][1];
	io_Yz = m_Mat[0][2];};

void Matrix3x3::GetAxeZ ( float &io_Zx, float &io_Zy, float &io_Zz ) const
{
	io_Zx = m_Mat[0][0];
	io_Zy = m_Mat[0][1];
	io_Zz = m_Mat[0][2];};

//SET METHODES.
void Matrix3x3::Set ( 	float in_00, float in_01, float in_02,
						float in_10, float in_11, float in_12,
						float in_20, float in_21, float in_22 )
{
	m_Mat[0][0] = in_00;
	m_Mat[0][1] = in_01;
	m_Mat[0][2] = in_02;
	m_Mat[1][0] = in_10;
	m_Mat[1][1] = in_11;
	m_Mat[1][2] = in_12;
	m_Mat[2][0] = in_20;
	m_Mat[2][1] = in_21;
	m_Mat[2][2] = in_22;	
};

void Matrix3x3::SetValue ( int in_Row, int in_Col, float in_Value )
{
	if ( in_Row < 3 && in_Row >= 0 && in_Col < 3 && in_Col >= 0 )
	{
		m_Mat[in_Row][in_Col] = in_Value;
	}
};

void Matrix3x3::SetFrom3x3Array ( float const in_Array[3][3] )
{
	m_Mat[0][0] = in_Array[0][0];
	m_Mat[0][1] = in_Array[0][1];
	m_Mat[0][2] = in_Array[0][2];
	m_Mat[1][0] = in_Array[1][0];
	m_Mat[1][1] = in_Array[1][1];
	m_Mat[1][2] = in_Array[1][2];
	m_Mat[2][0] = in_Array[2][0];
	m_Mat[2][1] = in_Array[2][1];
	m_Mat[2][2] = in_Array[2][2];	
};

void Matrix3x3::SetAxeX ( float const& in_Xx, float const& in_Xy, float const& in_Xz )
{
	m_Mat[0][0] = in_Xx;
	m_Mat[0][1] = in_Xy;
	m_Mat[0][2] = in_Xz;
}

void Matrix3x3::SetAxeY ( float const& in_Yx, float const& in_Yy, float const& in_Yz )
{
	m_Mat[1][0] = in_Yx;
	m_Mat[1][1] = in_Yy;
	m_Mat[1][2] = in_Yz;
}

void Matrix3x3::SetAxeZ ( float const& in_Zx, float const& in_Zy, float const& in_Zz )
{
	m_Mat[2][0] = in_Zx;
	m_Mat[2][1] = in_Zy;
	m_Mat[2][2] = in_Zz;
}

void Matrix3x3::ToEuler (float &out_X, float &out_Y, float &out_Z, int in_Radians)
{
	float x1, x2, y1, y2, z1, z2;

	float pi = 3.14159265359f;
	float epsilon = 0.9999998f;

	if ( m_Mat[0][2] < -epsilon )
	{
		out_X = 0.0f;
		out_Y = pi / 2.0f;
		out_Z = -atan2f(m_Mat[1][0], m_Mat[2][0]);
		
	}
	else if ( m_Mat[0][2] > epsilon )
	{
		out_X = 0.0f;
		out_Y = -pi / 2.0f;
		out_Z = atan2f(-m_Mat[1][0], -m_Mat[2][0]);
	}
	else
	{
		x1 = -asinf(m_Mat[0][2]);
		x2 = pi - x1;

		y1 = atan2f(m_Mat[1][2] / cosf(x1), m_Mat[2][2] / cosf(x1));
		y2 = atan2f(m_Mat[1][2] / cosf(x2), m_Mat[2][2] / cosf(x2));

		z1 = atan2f(m_Mat[0][1] / cosf(x1), m_Mat[0][0] / cosf(x1));
		z2 = atan2f(m_Mat[0][1] / cosf(x2), m_Mat[0][0] / cosf(x2));

		float absx1 = x1;
		float absy1 = y1;
		float absz1 = z1;
		float absx2 = x2;
		float absy2 = y2;
		float absz2 = z2;

		if(x1 < 0){absx1 = x1 * -1.0f;}
		if(y1 < 0){absy1 = y1 * -1.0f;}
		if(z1 < 0){absz1 = z1 * -1.0f;}
		if(x2 < 0){absx2 = x2 * -1.0f;}
		if(y2 < 0){absy2 = y2 * -1.0f;}
		if(z2 < 0){absz2 = z2 * -1.0f;}								

		if((absx1 + absy1 + absz1) <= (absx2 + absy2 + absz2))
		{
			out_X = y1;
			out_Y = x1;
			out_Z = z1;			
		}
		else
		{
			out_X = y2;
			out_Y = x2;
			out_Z = z2;			
		}
	}
}