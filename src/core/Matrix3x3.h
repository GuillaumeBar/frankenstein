/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Matrix3x3.h
@author Guillaume Baratte
@date 2016-03-27
@brief Define the matrix3x3 class.
Help to manipulate the matrix3x3 data.
*/

#ifndef __MATRIX3X3__
#define __MATRIX3X3__

#include <math.h>

class Matrix3x3
{
	public:
		//CONSTRUCTORS.
		/*! @brief Primary Constructor. */
		Matrix3x3 ();
		
		/*! @brief Init the matrix with a value fot each attributs.
		*@param in_00 Value for Row 0 Col 0.
		*@param in_01 Value for Row 0 Col 1.
		*@param in_02 Value for Row 0 Col 2.
		*@param in_10 Value for Row 1 Col 0.
		*@param in_11 Value for Row 1 Col 1.
		*@param in_12 Value for Row 1 Col 2.		
		*@param in_20 Value for Row 2 Col 0.
		*@param in_21 Value for Row 2 Col 1.
		*@param in_22 Value for Row 2 Col 2.		
		*/
		Matrix3x3 ( float in_00, float in_01, float in_02,
					float in_10, float in_11, float in_12,
					float in_20, float in_21, float in_22 );
		
		/*! @brief Init the matrix with another one for copy.
		*@param in_Matrix The matrix we use to init the new.
		*/
		Matrix3x3 ( Matrix3x3 const& in_Matrix );
		
		//DESTRUCTOR.
		/*! @brief Primary Destructor. */
		~Matrix3x3 ();
		
		//COMPARE OPERATORS.
		/*! @brief Overload the equality operator.
		*@param	in_MatrixB The matrix we want to compare.
		*@return True or False.
		*/
		bool operator== ( Matrix3x3 const& in_MatrixB ) const;
		
		/*! @brief Overload the not equality operator.
		*@param	in_MatrixB The matrix we want to compare.
		*@return True or False.
		*/		
		bool operator!= ( Matrix3x3 const& in_MatrixB ) const;
		
		//OPERATIONS OPERATORS.
		/*! @brief Overload the multiply operator.
		*@param in_MatrixB The matrix we want to multiply.
		*@return The result of the multiply.
		*/
		Matrix3x3& operator* ( Matrix3x3 const& in_MatrixB );
		
		//CUSTOM METHODES.
		/*! @brief Calcul the invert of a matrix in This.
		*@param in_MatrixB The Matrix we want to invert.
		*@return True if the matrix has been inverted.
		*/
		bool Invert ( Matrix3x3 const& in_MatrixB );
		
		/*! @brief Calcule the invert of this matrix.
		*@return True if the matrix has been inverted.
		*/
		bool SelfInvert ();
		
		/*! @brief Calcul the transpose of the inverse of a matrix in this.
		*@param in_MatrixB The matrix to transpose the inverse.
		*@return True if the matrix has been inverted.
		*/
		bool TransposeInverse ( Matrix3x3 const& in_MatrixB );
		
		/*! @brief Calcul the transpose of the inverse of this matrix.
		*@return True if the matrix has been inverted.
		*/
		bool SelfTransposeInverse ();
		
		/*! @brief Calcul the transpose of a matrix in this.
		*@param in_MatrixB The matrix to transpose.
		*/
		void Transpose ( Matrix3x3 const& in_MatrixB );
		
		/*! @brief Calcul the transpose of this.*/
		void SelfTranspose () ;
		
		/*! @brief Check if a matrix is equal of this.
		*@param in_MatrixB The matrix we want to test.
		*@return True or False.
		*/
		bool IsEquals ( Matrix3x3 const& in_MatrixB ) const;
		
		/*! @brief Multiply two matrix. MatrixA * MatrixB.
		*@param in_MatrixA The first matrix to multiply.
		*@param in_MatrixB The seconde matrix to multiply.
		*@return The result of the multiply.
		*/
		Matrix3x3& Mul ( Matrix3x3 const& in_MatrixA, Matrix3x3 const& in_MatrixB );
		
		/*! @brief Multiply a matrix with this. This * in_MatrixB.
		*@param in_MatrixB The seconde matrix to multiply.
		*@return The result of the multiply.
		*/
		Matrix3x3& SelfMul ( Matrix3x3 const& in_MatrixB );
		
		//GET METHODES.
		/*! @brief Get the component of the current matrix and return the value in the input variables.
		*@param io_00 Value of the Row 0 col 0.
		*@param io_01 Value of the Row 0 col 1.
		*@param io_02 Value of the Row 0 col 2.
		*@param io_10 Value of the Row 1 col 0.
		*@param io_11 Value of the Row 1 col 1.
		*@param io_12 Value of the Row 1 col 2.
		*@param io_20 Value of the Row 2 col 0.
		*@param io_21 Value of the Row 2 col 1.
		*@param io_22 Value of the Row 2 col 2.
		*/
		void Get ( 	float &io_00, float &io_01, float &io_02,
					float &io_10, float &io_11, float &io_12,
					float &io_20, float &io_21, float &io_22 ) const ;
			
		/*! @brief Get the value of a component at specify row and col.
		*@param in_Row The index of the Row.
		*@param in_Col The index of the Col.
		*@return The Value of the component.
		*/		
		float GetValue ( int in_Row, int in_Col ) const ;
		
		/*! @brief Get the value of the Axe X from the matrix.
		*@param io_Xx The value x of the Axe X.
		*@param io_Xy The value y of the Axe X.
		*@param io_Xz The value z of the Axe X.
		*/
		void GetAxeX ( float &io_Xx, float &io_Xy, float &io_Xz) const ;

		/*! @brief Get the value of the Axe Y from the matrix.
		*@param io_Yx The value x of the Axe Y.
		*@param io_Yy The value y of the Axe Y.
		*@param io_Yz The value z of the Axe Y.
		*/		
		void GetAxeY ( float &io_Yx, float &io_Yy, float &io_Yz) const ;

		/*! @brief Get the value of the Axe Z from the matrix.
		*@param io_Zx The value x of the Axe Z.
		*@param io_Zy The value y of the Axe Z.
		*@param io_Zz The value z of the Axe Z.
		*/		
		void GetAxeZ ( float &io_Zx, float &io_Zy, float &io_Zz ) const ;
		
		//SET METHODES.
		/*! @brief Set the matrix with the 9 components values.
		*@param in_00 Value for Row 0 Col 0.
		*@param in_01 Value for Row 0 Col 1.
		*@param in_02 Value for Row 0 Col 2.
		*@param in_10 Value for Row 1 Col 0.
		*@param in_11 Value for Row 1 Col 1.
		*@param in_12 Value for Row 1 Col 2.		
		*@param in_20 Value for Row 2 Col 0.
		*@param in_21 Value for Row 2 Col 1.
		*@param in_22 Value for Row 2 Col 2.
		*/
		void Set ( 	float in_00, float in_01, float in_02,
					float in_10, float in_11, float in_12,
					float in_20, float in_21, float in_22 );
		
		/*! @brief Set a value in the matrix with the row and col index.
		*@param in_Row The index of the row.
		*@param in_Col The index of the col.
		*@param in_Value The value of the component.
		*/
		void SetValue ( int in_Row, int in_Col, float in_Value );
		
		/*! @brief Set the self matrix with a 3x3 Array.
		*@param in_Array The 3x3 Array use to set this matrix.
		*/
		void SetFrom3x3Array ( float const in_Array[3][3] );
	
		/*! @brief Set the component corresponding to the Axe X.
		*@param in_Xx The value x of the Axe X.
		*@param in_Xy The value y of the Axe X.
		*@param in_Xz The value z of the Axe X.
		*/
		void SetAxeX ( float const& in_Xx, float const& in_Xy, float const& in_Xz );
		
		/*! @brief Set the component corresponding to the Axe Y .
		*@param in_Yx The value x of the Axe Y.
		*@param in_Yy The value y of the Axe Y.
		*@param in_Yz The value z of the Axe Y.
		*/		
		void SetAxeY ( float const& in_Yx, float const& in_Yy, float const& in_Yz );
		
		/*! @brief Set the component corresponding to the Axe Z.
		*@param in_Zx The value x of the Axe Z.
		*@param in_Zy The value y of the Axe Z.
		*@param in_Zz The value z of the Axe Z.
		*/		
		void SetAxeZ ( float const& in_Zx, float const& in_Zy, float const& in_Zz );		
	
		/*! @brief Return the rotatoin as euler angle.
		*@param out_X Return the angle on X (bank).
		*@param out_Y Return the angle on Y (heading).
		*@param	out_Z Return the angle on Z (attitude).
		*/
		void ToEuler ( float &out_X, float &out_Y, float &out_Z, int in_Radians);

	private:
		//ATTRIBUTES.
		float m_Mat [3][3]; /*! The matrix 3x3. */

};

#endif