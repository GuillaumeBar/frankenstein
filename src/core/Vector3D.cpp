/*
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Vector3D.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "Vector3D.h"

// Constructeur
Vector3D::Vector3D() : m_x(0.0f), m_y(0.0f), m_z(0.0f)
{

};

Vector3D::Vector3D( float in_x, float in_y, float in_z ) : m_x(0.0f), m_y(0.0f), m_z(0.0f)
{
	m_x = in_x;
	m_y = in_y;
	m_z = in_z;
};

// Destructeur
Vector3D::~Vector3D()
{

};

// Operateur de comparaison
bool Vector3D::operator==( Vector3D const& in_VectorB )
{
	return IsEqual(in_VectorB);
};

bool Vector3D::operator!=( Vector3D const& in_VectorB )
{
	if ( IsEqual(in_VectorB) )
		return false;
	else
		return true;
};

// Operateur mathematique
Vector3D& Vector3D::operator+ ( Vector3D const& in_VectorB )
{
	return SelfAdd ( in_VectorB );
};

Vector3D& Vector3D::operator- ( Vector3D const& in_VectorB )
{
	return SelfSub ( in_VectorB );
}

Vector3D& Vector3D::operator* ( float const& in_Float )
{
	return SelfMulByFloat( in_Float );
}

// Methode de comparaison

bool Vector3D::IsEqual ( Vector3D const& in_VectorB ) const
{
	//Test si le vecteur this est egal au vecteur B.
	return ( m_x == in_VectorB.m_x && m_y == in_VectorB.m_y && m_z == in_VectorB.m_z ); 

};

// Methode mathematique
Vector3D& Vector3D::SelfAdd ( Vector3D const& in_VectorB )
{
	/*
	__m256 vecA = _mm256_set_ps(m_x, m_y, m_z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	__m256 vecB = _mm256_set_ps(in_VectorB.m_x, in_VectorB.m_y, in_VectorB.m_z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	__m256 rslt = _mm256_add_ps(vecA, vecB);
	aValue = (float*)&rslt;

	m_x = aValue[7];
	m_y = aValue[6];
	m_z = aValue[5];
	*/
	
	m_x = m_x + in_VectorB.m_x;
	m_y = m_y + in_VectorB.m_y;
	m_z = m_z + in_VectorB.m_z;
	
	return *this;
};

Vector3D& Vector3D::SelfSub ( Vector3D const& in_VectorB )
{
	m_x = m_x - in_VectorB.m_x;
	m_y = m_y - in_VectorB.m_y;
	m_z = m_z - in_VectorB.m_z;

	return *this;
};

Vector3D& Vector3D::Add ( Vector3D const& in_VectorA , Vector3D const& in_VectorB )
{
	m_x = in_VectorA.m_x + in_VectorB.m_x ;
	m_y = in_VectorA.m_y + in_VectorB.m_y ;
	m_z = in_VectorA.m_z + in_VectorB.m_z ;

	return *this;
}

Vector3D& Vector3D::Sub ( Vector3D const& in_VectorA , Vector3D const& in_VectorB )
{
	m_x = in_VectorA.m_x - in_VectorB.m_x;
	m_y = in_VectorA.m_y - in_VectorB.m_y;
	m_z = in_VectorA.m_z - in_VectorB.m_z;

	return *this;
}

Vector3D& Vector3D::MulByFloat ( Vector3D const& in_VectorA, float const& in_Float )
{
	m_x = in_VectorA.m_x * in_Float;
	m_y = in_VectorA.m_y * in_Float;
	m_z = in_VectorA.m_z * in_Float;

	return *this;
}

Vector3D& Vector3D::SelfMulByFloat ( float const& in_Float )
{
	m_x = m_x * in_Float;
	m_y = m_y * in_Float;
	m_z = m_z * in_Float;

	return *this;
};

float Vector3D::GetLength () const
{
	return sqrt ( ( m_x * m_x ) + ( m_y * m_y ) + ( m_z * m_z ) );
};

void Vector3D::Normalize ( Vector3D &in_VectorA )
{
	float	vectorLength;

	vectorLength = sqrt ( ( in_VectorA.m_x * in_VectorA.m_x ) + ( in_VectorA.m_y * in_VectorA.m_y ) + ( in_VectorA.m_z * in_VectorA.m_z ) );

	m_x = in_VectorA.m_x / vectorLength;
	m_y = in_VectorA.m_y / vectorLength;
	m_z = in_VectorA.m_z / vectorLength;
}

void Vector3D::SelfNormalize ()
{
	float	vectorLength;

	vectorLength = GetLength();

	m_x = m_x / vectorLength;
	m_y = m_y / vectorLength;
	m_z = m_z / vectorLength;

};

float Vector3D::DotProduct ( Vector3D const& in_VectorB ) const
{
	float rtnResult;

	rtnResult = m_x * in_VectorB.m_x + m_y * in_VectorB.m_y + m_z * in_VectorB.m_z ;

	return rtnResult;
};

Vector3D& Vector3D::CrossProduct ( Vector3D const& in_VectorA, Vector3D const& in_VectorB )
{
	m_x = in_VectorA.m_y * in_VectorB.m_z - in_VectorA.m_z * in_VectorB.m_y;
	m_y = in_VectorA.m_z * in_VectorB.m_x - in_VectorA.m_x * in_VectorB.m_z;
	m_z = in_VectorA.m_x * in_VectorB.m_y - in_VectorA.m_y * in_VectorB.m_x;

	return *this;
};

float Vector3D::GetAngle ( Vector3D const& in_VectorB , bool in_Degrees ) const
{
	float	rtnResult;
	float	lengthVectorA, lengthVectorB;
	float	dotVector;

	lengthVectorA = GetLength();
	lengthVectorB = in_VectorB.GetLength();
	dotVector = DotProduct (in_VectorB );

	rtnResult = dotVector / ( lengthVectorA * lengthVectorB );

	if (in_Degrees)
		return rtnResult;
	else
		return acos(rtnResult);
};

// Methode Set
void Vector3D::Set ( float in_AxeX, float in_AxeY, float in_AxeZ )
{
	m_x = in_AxeX;
	m_y = in_AxeY;
	m_z = in_AxeZ;
};

void Vector3D::SetFromVector3D ( Vector3D const& in_Vector3 )
{
	m_x = in_Vector3.m_x;
	m_y = in_Vector3.m_y;
	m_z = in_Vector3.m_z;
}

void Vector3D::SetX ( float in_float )
{
	m_x = in_float;
};

void Vector3D::SetY ( float in_float )
{
	m_y = in_float;
};

void Vector3D::SetZ ( float in_float )
{
	m_z = in_float;
};

// Methode Get

float Vector3D::GetX () const
{
	return m_x;
};

float Vector3D::GetY () const
{
	return m_y;
};

float Vector3D::GetZ () const
{
	return m_z;
};

Vector3D& Vector3D::MulByMatrix3x3 ( Vector3D const& in_Vector, Matrix3x3 const& in_Matrix3x3 )
{
	float fX, fY, fZ;

	fX = in_Vector.m_x * in_Matrix3x3.GetValue(0,0) + in_Vector.m_y * in_Matrix3x3.GetValue(1,0) + in_Vector.m_z * in_Matrix3x3.GetValue(2,0) ;
	fY = in_Vector.m_x * in_Matrix3x3.GetValue(0,1) + in_Vector.m_y * in_Matrix3x3.GetValue(1,1) + in_Vector.m_z * in_Matrix3x3.GetValue(2,1) ;
	fZ = in_Vector.m_x * in_Matrix3x3.GetValue(0,2) + in_Vector.m_y * in_Matrix3x3.GetValue(1,2) + in_Vector.m_z * in_Matrix3x3.GetValue(2,2) ;
	
	m_x = fX;
	m_y = fY;
	m_z = fZ;

	return *this;
};

Vector3D& Vector3D::MulByMatrix4x4 ( Vector3D const& in_Vector, Matrix4x4 const& in_Matrix4x4 )
{
	float fX, fY, fZ, fW;

	fX = in_Vector.m_x * in_Matrix4x4.GetValue(0,0) + in_Vector.m_y * in_Matrix4x4.GetValue(1,0) + in_Vector.m_z * in_Matrix4x4.GetValue(2,0) + in_Matrix4x4.GetValue(3,0) ;
	fY = in_Vector.m_x * in_Matrix4x4.GetValue(0,1) + in_Vector.m_y * in_Matrix4x4.GetValue(1,1) + in_Vector.m_z * in_Matrix4x4.GetValue(2,1) + in_Matrix4x4.GetValue(3,1) ;
	fZ = in_Vector.m_x * in_Matrix4x4.GetValue(0,2) + in_Vector.m_y * in_Matrix4x4.GetValue(1,2) + in_Vector.m_z * in_Matrix4x4.GetValue(2,2) + in_Matrix4x4.GetValue(3,2) ;
	fW = in_Vector.m_x * in_Matrix4x4.GetValue(0,3) + in_Vector.m_y * in_Matrix4x4.GetValue(1,3) + in_Vector.m_z * in_Matrix4x4.GetValue(2,3) + in_Matrix4x4.GetValue(3,3) ;

	m_x = fX / fW;
	m_y = fY / fW;
	m_z = fZ / fW;

	return *this;
};

Vector3D& Vector3D::RotateByQuaternionFG (  Vector3D const& in_Vector, Quaternion const& in_Quaternion )
{
	float		tX,tY,tZ;
	float		rX,rY,rZ;

	//CrossProduct Quat Vector by In Vector
	rX = in_Quaternion.GetY() * in_Vector.m_z - in_Quaternion.GetZ() * in_Vector.m_y;
	rY = in_Quaternion.GetZ() * in_Vector.m_x - in_Quaternion.GetX() * in_Vector.m_z;
	rZ = in_Quaternion.GetX() * in_Vector.m_y - in_Quaternion.GetY() * in_Vector.m_x; 

	//Multiply r Vector By 2.
	tX = rX*2;
	tY = rY*2;
	tZ = rZ*2;

	//CrossProduct Quat Vector by t Vector.
	rX = in_Quaternion.GetY() * tZ - in_Quaternion.GetZ() * tY;
	rY = in_Quaternion.GetZ() * tX - in_Quaternion.GetX() * tZ;
	rZ = in_Quaternion.GetX() * tY - in_Quaternion.GetY() * tX; 

	//Multiply t Vector by W Quat.
	tX *= in_Quaternion.GetW();
	tY *= in_Quaternion.GetW();
	tZ *= in_Quaternion.GetW();

	//Compute the rotated vector.
	m_x = in_Vector.m_x + tX + rX;
	m_y = in_Vector.m_y + tY + rY;
	m_z = in_Vector.m_z + tZ + rZ;

	return *this;
};

Vector3D& Vector3D::SelfMulByMatrix3x3 ( Matrix3x3 const& in_Matrix3x3 )
{
	return MulByMatrix3x3 ( *this, in_Matrix3x3 );
};

Vector3D& Vector3D::SelfMulByMatrix4x4 ( Matrix4x4 const& in_Matrix4x4 )
{
	return MulByMatrix4x4 ( *this, in_Matrix4x4 );
};

Vector3D& Vector3D::SelfRotateByQuaternionFG ( Quaternion const& in_Quaternion )
{
	return RotateByQuaternionFG ( *this, in_Quaternion );
};