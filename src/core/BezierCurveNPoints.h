/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file BezierCurveNPoints.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Bezier Curve with N points with n Controler.
*/

#ifndef __BEZIERCURVENPOINTS__
#define __BEZIERCURVENPOINTS__

#include "DataArray.h"
#include "Vector3D.h"

class BezierCurveNPoints
{
	public:
		//CONTRUCTOR.
		/*! @brief Primary constructor. */
		BezierCurveNPoints ();

		/*! @brief Constructor with point number and point control.
		*@param in_PointNumber The number of points on the curve.
		*@param in_PointsControl The array with the position of the curve control points.
		*/
		BezierCurveNPoints ( int in_PointNumber, DataArray<Vector3D> const& in_PointsControl );

		//DESTRUCTOR.
		/*! @brief Destructor.*/
		~BezierCurveNPoints ();

		//CUSTOM METHODES.
		/*! @brief Normalize the position of all the point's curve.
		*@param in_MaxRecursiveLoop The number of recursion for the normalization.
		*/
		void NormalizeArrayPointsPosition ( int in_MaxRecursiveLoop );

		/*! @brief Normalize the position of one point of the curve.
		*@param in_Index The index of the point to normalize.
		*@param in_MaxRecursiveLoop The number of recursion for the normalization.
		*/
		void NormalizeOnePointPosition ( int in_Index, int in_MaxRecursiveLoop );

		/*! @brief Compute the position of all the point of the curve. */
		void ComputeArrayPointsPosition ();

		/*! @brief Compute the t value of one point of the curve.
		*@param in_Index The index of the point to compute the t value.
		*/
		void ComputeOneT ( int in_Index );

		/*! @brief Compute the t value of all the point of the curve. */
		void ComputeArrayT ();

		/*! @brief Compute the position of one point of the curve.
		*@param in_Index The index of the point to compute the t value.
		*/
		void ComputeOnePointPosition ( int in_Index );

		/*! @brief Compute the length of the curve. */
		void ComputeCurveLength ();

		/*! @brief Compute the value for start and end range distribution. */
		void ComputeStartEndRangeDistribution ();

		//SET METHODES.
		/*! @brief Set the class.
		*@param in_PointNumber The number of point on the curve.
		*@param in_PointsControl The array with the position of the control point.
		*@param in_StartRange The value of the start range. 0 to 1.
		*@param in_EndRange The value of the end range. 0 to 1.
		*@param in_MoveRange The value of the move range. 0 to 1.
		*/
		void Set ( int in_PointNumber, DataArray<Vector3D> const& in_PointsControl, float const& in_StartRange, float const& in_EndRange, float const& in_MoveRange );

		/*! @brief Set the number of point of the curve.
		*@param in_PointNumber The number of point on the curve.
		*/
		void SetPointsNumbers ( int in_PointNumber );

		/*! @brief Set the position of the control points.
		*@param in_PointsControl The array with the position of the control point.
		*/
		void SetPointsControl ( DataArray<Vector3D> const& in_PointsControl );

		/*! @brief Set the value of the range of the curve.
		*@param in_StartRange The value of the start range. 0 to 1.
		*@param in_EndRange The value of the end range. 0 to 1.
		*@param in_MoveRange The value of the move range. 0 to 1.
		*/
		void SetRange ( float const& in_StartRange, float const& in_EndRange, float const& in_MoveRange );

		//GET METHODES.
		/*! @brief Get the position of one point of the curve.
		*@param in_Index The index of the point.
		*@return The position of the point of the corresponding index. 
		*/
		Vector3D GetPointPositionAt ( int in_Index );

		/*! @brief Get the length of the curve.
		*@return The value of the length of the curve.
		*/
		float GetCurveLength () const;

		/*! @brief Compute the position of one point of the curve with given t value.
		* Use algorithm of n point control.
		*@param in_T The value t of the point.
		*@return The position of the point.
		*/
		Vector3D	ComputeBezierCurveNPointOnePoint ( float in_T );

		/*! @brief Compute the position of one point of the curve with given t value.
		* Use algorithm of 4 point control.
		*@param in_T The value t of the point.
		*@return The position of the point.
		*/
		Vector3D	ComputeBezierCurve4PointOnePoint ( float in_T );

		/*! @brief Compute the position of one point of the curve with given t value.
		* Use algorithm of 8 point control.
		*@param in_T The value t of the point.
		*@return The position of the point.
		*/
		Vector3D	ComputeBezierCurve8PointOnePoint ( float in_T );

		/*! @brief Compute the coeficient Binomial.
		*@param in_N The Value of n.
		*@param in_I The value of i.
		*@return The value of the coeficient binomial.
		*/
		float		ComputeCoeficientBinomial ( int in_N, int in_I );

		/*! @brief Compute the factortiel.
		*@param in_N The value of n.
		*@return The factoriel value.
		*/
		float		ComputeFactoriel ( int in_N );

		void 		ComputeCurve();

		DataArray<Vector3D> GetCurvePoints();
		DataArray<float> 	GetCurveLengths();
		DataArray<float> 	GetCurveTs();
		DataArray<Vector3D> GetCurveSubVectors();
		DataArray<Vector3D> GetCurveSubTangents();

	private:
		static const int		m_MemoryBaseSize;		/*! The size of the array by default. */
		int						m_pointNumber;			/*! The number of point that define the bezier curve.*/
		DataArray<Vector3D>		m_ArrayPointsControl;	/*! The Array of the control point. */
		float					m_Length;				/*! The length of the curve. */
		float					m_StartRange;			/*! The indice for start the point distribution on the curve.*/
		float					m_EndRange;				/*! The indice for end the point distribution on the curve.*/
		float					m_MoveRange;			/*! The indice for move the point distribution on the curve.*/
		float					m_MoveStartRange;		/*! The indice for the start range moved by the moverange value.*/
		float					m_MoveEndRange;			/*! The indice for the end range moved by the moverange value.*/
		DataArray<Vector3D>		m_ArrayPoints;			/*! The array for the position of all point that define the beizer curve. */
		DataArray<float>		m_ArrayT;				/*! The array for the T factor of all point that define the beizer curve. */
		DataArray<float>		m_ArrayLengths;
		DataArray<Vector3D>		m_ArraySubVectors;
		DataArray<Vector3D>		m_ArraySubTangents;
};


#endif