/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file DataArray.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a data array class.
It's help to manipulate array data.
*/

#ifndef __DATAARRAY__
#define __DATAARRAY__

template <class T>
class DataArray
{
	public:
		//CONSTRUCTOR.
		/*! @brief The primary contructor of the class. */
		DataArray ();

		/*! @brief Create the array with given size. */
		DataArray ( int in_Size );

		/*! @brief The copy constructor.
		*@param in_DataArray The Array we want to copy.
		*/
		DataArray ( DataArray const& in_DataArray );

		/*! @brief Create the Array with the given size and copy the same item in.
		*@param in_Size The size of the array.
		*@param in_Item The item we want to copy.
		*/
		DataArray ( int in_Size, T const& in_Item );

		//DESTRUCTOR.
		/*! @brief The destructor of the class. */
		~DataArray ();

		//OPERATORS.
		/*! @brief Overload the get item operator. 
		*@param in_Index The index we want to get.
		*@return The element at the index.
		*/
		T&	operator[] ( int in_Index );

		/*! @brief Overload the assignment operator.
		*@param in_DataArray The array we want to copy.
		*@return The Copied Array.
		*/
		DataArray& operator= ( DataArray const& in_DataArray );

		//CUSTOM METHODES.
		/*! @brief Define the size of the array and delete the contain.
		*@param	in_Size The size of the new array.
		*/
		void SetSizeAndClear ( int in_Size );

		/*! @brief Define a new size for the array.
		* If the new size is less than the old. The data at greater index is lost.
		*@param in_Size The new size of the array.
		*/
		void Resize ( int in_Size );

		/*! @brief Add a new item at the end of the array.
		*@param in_Item The item we want to add
		*/
		void Add (  T const& in_Item );

		/*! @brief Insert an item at the wanted index.
		*@param in_Index The index where we want to add the item.
		*@param in_Item The item we want to add.
		*@return True if the item has been added.
		*/
		bool InsertAt ( int in_Index, T const& in_Item );

		/*! @brief Replace the item the given index.
		*@param in_Index The index wehe wa want to replace item.
		*@in_Item The item that overwrite the old one.
		*@return True is success.
		*/
		bool ReplaceAt ( int in_Index, T const& in_Item );

		/*! @brief Check if the item exist in the array.
		*@param in_Item The item we want to check.
		*@return True if the item found.
		*/
		bool IsExist ( T const& in_Item );

		/*! @brief Remove a the item from the array.
		*@param in_Item The item we want to remove.
		*@return True if the item found and remove.
		*/
		bool Remove ( T const& in_Item );

		/*! @brief Get the size of the array.
		*@return The array size.
		*/
		int GetSize () const ;

		/*! @brief Clear the array and free memory. */
		void Clear ();

		/*! @brief Add a DataArray to this array at the end.
		*@param in_DataArray The Array we want to add.
		*/
		void AddDataArray ( DataArray const& in_DataArray );

	private:
		T	*m_Ptr;			/*! Point to the array adress. */
		int	m_Size;			/*! Size of the array. */
};

//////CPP IMPLEMENTATION////////

template <class T>
DataArray<T>::DataArray ()
{
	//Allocat the point.
	m_Ptr = 0;
	//Define the Size value.
	m_Size = 0;
};

template <class T>
DataArray<T>::DataArray ( DataArray const& in_DataArray )
{
	m_Size = in_DataArray.m_Size;
	m_Ptr = new T[m_Size];
	for ( int i=0; i<m_Size ; i++ )
	{	
		m_Ptr[i] = in_DataArray.m_Ptr[i];
	}
};

template <class T>
DataArray<T>::DataArray ( int in_Size )
{
	m_Size = in_Size;
	m_Ptr = new T[in_Size];
};

template <class T>
DataArray<T>::DataArray ( int in_Size, T const& in_Item )
{
	m_Size = in_Size;
	m_Ptr = new T[m_Size];
	for ( int i=0; i<m_Size; i++ )
	{
		m_Ptr[i] = in_Item;
	}
};

template <class T>
DataArray<T>::~DataArray()
{
	//Free the memory.
	delete [] m_Ptr;
};

template <class T>
T& DataArray<T>::operator[] ( int in_Index )
{
	return m_Ptr[in_Index];
};

template <class T>
DataArray<T>& DataArray<T>::operator= ( DataArray const& in_DataArray )
{
	if ( in_DataArray.m_Size == 0 )
	{
		Clear();
	}
	else
	{
		delete [] m_Ptr;
		m_Size = in_DataArray.m_Size;
		m_Ptr = new T[m_Size];
		for ( int i=0; i<m_Size; i++ )
		{
			m_Ptr[i] = in_DataArray.m_Ptr[i];
		}
	}

	return *this;
};

template <class T>

void DataArray<T>::Resize ( int in_Size )
{
	//Define the new Ptr.
	T *newPtr = new T[in_Size];

	//If the new size is greater than the older.
	if ( in_Size > m_Size )
	{
		//We loop with the old size to copy the data to the newer.
		for ( int i=0; i<m_Size; i++ )
		{
			newPtr[i] = m_Ptr[i];
		}
	}
	//If the new size is olwer than the older.
	else
	{
		//We loop with the new size to copy the data to the newer.
		for ( int i=0; i<in_Size; i++ )
		{
			newPtr[i] = m_Ptr[i];
		}
	}

	//Update the class data.
	m_Ptr = newPtr;
	m_Size = in_Size;

	//Delete the old array.
	delete [] newPtr;

}
template <class T>
void DataArray<T>::SetSizeAndClear ( int in_Size )
{
	delete [] m_Ptr;
	m_Size = in_Size;
	m_Ptr = new T[in_Size];
};

template <class T>
void DataArray<T>::Add ( T const& in_Item )
{
	//Check if the array is not null.
	bool checkCopy = false;
	if ( m_Size != 0 )
		checkCopy = true;

	//Add one to the size of the array.
	m_Size += 1;
	//Create a new point for the resized array.
	T *newPtr = new T[m_Size];
	if ( checkCopy )
	{
		//Copy the old array value.
		for ( int i=0; i<m_Size-1 ; i++ )
		{
			newPtr[i] = m_Ptr[i];
		}
	}

	//Add the new value in array.
	newPtr[m_Size-1] = in_Item;
	//Release old memory allocation.
	delete [] m_Ptr;
	//Retarget the class point to the new point.
	m_Ptr = newPtr;
};

template <class T>
bool DataArray<T>::InsertAt ( int in_Index, T const& in_Item )
{
	//Variable for the success of the insertion.
	bool success = false;
	//Test if the index is inside the array.
	if ( in_Index < m_Size )
	{
		//Update the new of the array.
		m_Size += 1;
		//Create the resized array.
		T *newPtr = new T[m_Size];
		//Variable for check when we add the item in the new array.
		bool checkAdded = false;
		for ( int i=0; i<m_Size; i++ )
		{
			//Test if we are at the good index to add the item.
			if ( i == in_Index )
			{
				checkAdded = true;
				newPtr[i] = in_Item;
			}
			else
			{
				if ( checkAdded )
				{
					newPtr[i] = m_Ptr[i-1];
				}
				else
				{
					newPtr[i] = m_Ptr[i];
				}
			}
		}

		delete [] m_Ptr;

		m_Ptr = newPtr;

		success = true;
	}

	return success;
};

template <class T>
bool DataArray<T>::ReplaceAt ( int in_Index, T const& in_Item )
{
	bool success = false;

	if ( in_Index < m_Size )
	{
		m_Ptr[in_Index] = in_Item;
		success = true;
	}

	return success;
};

template <class T>
bool DataArray<T>::Remove ( T const& in_Item )
{
	bool checkExist;
	//Test if the item exist in the Array.
	checkExist = IsExist ( in_Item );
	//If exist we remove it.
	if ( checkExist )
	{
		//Define the new point for the resize array.
		T *newPtr = new T[m_Size-1];

		//Variable for check when we need to skip the element to remove.
		bool findItem = false;
		//Copy old array elments without the item.
		for ( int i=0; i<m_Size; i++ )
		{
			//Check if the current element is the same that we want to remove.
			if ( m_Ptr[i] == in_Item )
			{
				findItem = true; 
			}
			else
			{
				if ( findItem )
				{
					newPtr[i-1] = m_Ptr[i];
				}
				else
				{
					newPtr[i] = m_Ptr[i];
				}
			}
		}

		//Release the old memory.
		delete [] m_Ptr;
		//Re Assign the class point.
		m_Ptr = newPtr;
		//Update the new size.
		m_Size -= 1;
	}

	return checkExist;
};

template <class T>
bool DataArray<T>::IsExist ( T const& in_Item )
{
	bool checkExist = false;

	if ( m_Size != 0 )
	{
		for ( int i=0; i<m_Size ; i++ )
		{
			if ( m_Ptr[i] == in_Item )
			{
				checkExist = true;
			}
		}
	}

	return checkExist;
};

template <class T>
int DataArray<T>::GetSize () const
{
	return m_Size;
};

template <class T>
void DataArray<T>::Clear()
{
	delete [] m_Ptr;
	m_Ptr = 0;
	m_Size = 0;
};

template <class T>
void DataArray<T>::AddDataArray ( DataArray const& in_DataArray )
{
	//Calcule the new array Size.
	int newArraySize = m_Size+in_DataArray.m_Size;
	//Create the new Array.
	T *newPtr = new T[newArraySize];
	//Copy the first array in the new array.
	for ( int i=0; i<m_Size; i++ )
	{
		newPtr[i] = m_Ptr[i];
	}
	//Copy the second array in the new array.
	for ( int i=m_Size; i< newArraySize; i++ )
	{
		newPtr[i] = in_DataArray.m_Ptr[i-m_Size];
	}
	//Clear the old point array.
	delete [] m_Ptr;
	//Update the point.
	m_Ptr = newPtr;
	//Update the Size.
	m_Size = newArraySize;
};

#endif