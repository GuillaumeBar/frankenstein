/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Vector2D.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "Vector2D.h"

//CONSTRUCTORS.
Vector2D::Vector2D() : m_x(0.0f), m_y(0.0f)
{

};

Vector2D::Vector2D ( float in_fX, float in_fY ) : m_x(0.0f), m_y(0.0f)
{
	m_x = in_fX;
	m_y = in_fY;
};

//DESTRUCTOR.
Vector2D::~Vector2D()
{

};

//COMPARE OPERATORS.
bool Vector2D::operator== ( Vector2D const& in_VectorB )
{
	return IsEqual(in_VectorB);
};

bool Vector2D::operator!= ( Vector2D const& in_VectorB )
{
	if ( IsEqual(in_VectorB) )
		return false;
	else
		return true;
};

//OPERATIONS OPERATORS.
Vector2D Vector2D::operator+ ( Vector2D const& in_VectorB )
{
	return Add( in_VectorB );
};

Vector2D Vector2D::operator- ( Vector2D const& in_VectorB )
{
	return Sub ( in_VectorB );
};

Vector2D Vector2D::operator* ( float const& in_Float ) 
{
	return MulByFloat( in_Float );
};

//COMPARE METHODES.
bool Vector2D::IsEqual ( Vector2D const& in_VectorB ) const
{
	return ( m_x == in_VectorB.m_x && m_y == in_VectorB.m_y );
}; 

//OPERATIONS METHODES.
Vector2D Vector2D::Add ( Vector2D const& in_VectorB )
{
	Vector2D 	rtnResult;
	float 		fX, fY;
	
	fX = m_x + in_VectorB.m_x;
	fY = m_y + in_VectorB.m_y;
	
	rtnResult.Set ( fX, fY );
	
	return rtnResult;
};

Vector2D Vector2D::Sub ( Vector2D const& in_VectorB )
{
	Vector2D 	rtnResult;
	float 		fX, fY;
	
	fX = m_x - in_VectorB.m_x;
	fY = m_y - in_VectorB.m_y;
	
	rtnResult.Set ( fX, fY );
	
	return rtnResult;	
};

Vector2D Vector2D::MulByFloat ( float const& in_Float )
{
	Vector2D 	rtnResult;
	float 		fX, fY;
	
	fX = m_x - in_Float;
	fY = m_y - in_Float;
	
	rtnResult.Set ( fX, fY );
	
	return rtnResult;
};

//CUSTOM METHODES.
float Vector2D::GetLength () const
{
	return sqrt ( ( m_x * m_x ) + ( m_y * m_y ) );
};

bool Vector2D::Normalize ( Vector2D & in_VectorA )
{
	float vectorLength;
	
	vectorLength = in_VectorA.GetLength();
	
	if ( vectorLength!= 0.0f )
	{
		m_x = m_x / vectorLength;
		m_y = m_y / vectorLength;
		return true;
	}
	else
	{
		return false;
	}
};

float Vector2D::DotProduct ( Vector2D const& in_VectorB ) const
{
	float rtnResult;
	
	rtnResult = m_x * in_VectorB.m_x + m_y * in_VectorB.m_y ;
	
	return rtnResult;
};

float Vector2D::GetAngle ( Vector2D const& in_VectorB, bool in_Degrees ) const
{
	float 	rtnResult;
	float	lengthVectorA, lengthVectorB;
	float 	dot;
	
	lengthVectorA = GetLength();
	lengthVectorB = in_VectorB.GetLength();
	dot = DotProduct ( in_VectorB );
	
	if ( lengthVectorA!=0.0f && lengthVectorB!=0.0f )
	{	
		rtnResult = dot / ( lengthVectorA * lengthVectorB );
		
		if ( in_Degrees )
			return acos(rtnResult);
		else
			return rtnResult;
	}
	else
	{
		return -1.0f;
	}
};

//SET METHODES.
void Vector2D::Set ( float in_fX, float in_fY )
{
	m_x = in_fX;
	m_y = in_fY;
};

void Vector2D::SetFromVector2D ( Vector2D const& in_Vector2D )
{
	m_x = in_Vector2D.m_x;
	m_y = in_Vector2D.m_y;
};

void Vector2D::SetX ( float in_Float )
{
	m_x = in_Float;
};

void Vector2D::SetY ( float in_Float )
{
	m_y = in_Float;
};

// GET METHODES.
float Vector2D::GetX () const
{
	return m_x;
}

float Vector2D::GetY () const
{
	return m_y;
}