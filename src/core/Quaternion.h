/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Quaternion.h
@author Guillaume Baratte
@date 2016-03-27
@brief Define the quaternion class.
Help to manipulate the quaternion data.
*/

#ifndef __QUATERNION__
#define __QUATERNION__

#include <math.h>
#include "Matrix3x3.h"

class Quaternion
{
	public:
		//CONSTRUCTORS.
		/*! @brief Primary constructor. */
		Quaternion ();

		/*! @brief Create Object with given components.
		*@param in_X The value of X for the Axe.
		*@param in_Y The value of Y for the Axe.
		*@param in_Z The value of Z for the Axe.
		*@param in_W The angle.
		*/
		Quaternion ( float const& in_X,
                        float const& in_Y,
                        float const& in_Z,
                        float const& in_W );

		/*! @brief Copy constructor.
		*@param in_Quat The Quaternion we want to copy.
		*/
		Quaternion ( Quaternion const& in_Quat );

		//DESTRUCTOR.
		/*! @brief Primary Destructor. */
		~Quaternion();

		//OPERATORS.
		/*! @brief Overload the equality operator.
		*@param in_Quat The Quaternion we want to test.
		*@return True if equals.
		*/
		bool operator== ( Quaternion const& in_Quat );

		/*! @brief Overload the not equality operator.
		*@param in_Quat The Quaternion we want to test.
		*@return True if not equals.
		*/
		bool operator!= ( Quaternion const& in_Quat );
		/*! @brief Overload the assign operator.
		*@param in_Quat The Quaternion to assign.
		*@return This.
		*/
		Quaternion& operator= ( Quaternion const& in_Quat );

		/*! @brief Overload The Addition and assign operator.
		*@param in_Quat The Quaternion we want to add.
		*@return The result of the addition.
		*/
		Quaternion& operator+= ( Quaternion const& in_Quat );

		/*! @brief Overload the Substract and assign operator.
		*@param in_Quat The Quaternion we want to Sub.
		*@return The result of the Substract.
		*/
		Quaternion& operator-= ( Quaternion const& in_Quat );

		/*! @brief Overload the Multiply and assign operator.
		*@param in_Quat The Quaternion we want to Multiply.
		*@return The result of the multiply.
		*/
		Quaternion& operator*= ( Quaternion const& in_Quat );

		//CUSTOM METHODES.
		/*! @brief Check if a Quaternion is equals to this.
		*@param in_Quat The Qauternion we want to test.
		*@return True if equals.
		*/
		bool Equals ( Quaternion const& in_Quat ) const;

		/*! @brief Addition the two Quaternions.
		*@param in_QuatA The first Quaternion.
		*@param in_QuatB The second Quaternion.
		*@return The Result of the addition.
		*/
		Quaternion& Add ( Quaternion const& in_QuatA,
                            Quaternion const& in_QuatB );

		/*! @brief Substract the two Quaternions.
		*@param in_QuatA The First Quaternion.
		*@param in_QuatB The Second Quaternion.
		*@return The Result of the subtract.
		*/
		Quaternion& Sub ( Quaternion const& in_QuatA,
                            Quaternion const& in_QuatB );

		/*! @brief Multiply the two Quaternions.
		*@param in_QuatA The first Quaternion.
		*@param in_QuatB The second Quaternion.
		*@return The result of the Multiply.
		*/
		Quaternion& Mul ( Quaternion const& in_QuatA,
                            Quaternion const& in_QuatB );

		/*! @brief Negate the given Quaternion in this.
		*@param in_Quat The quaternion we want to negate.
		*@return The result of the negate.
		*/
		Quaternion& Negate ( Quaternion const& in_Quat );

		/*! @brief Invert the given Quaternion in this.
		*@param in_Quat The Quaternion we want to invert.
		*return The result of the invert.
		*/
		Quaternion& Invert ( Quaternion const& in_Quat );

		/*! @brief Conjugate the given Quaternion in this.
		*@param in_Quat The Quaternion we want to conjugate.
		*@return The result of the conjugate.
		*/
		Quaternion& Conjugate ( Quaternion const& in_Quat );

		/*! @brief Normalize the given Quaternion in this.
		*@param in_Quat The Quaternion we want to normalize.
		*@return The result of the normalize.
		*/
		Quaternion& Normalize ( Quaternion const& in_Quat );

		/*! @brief Compute the Dot Product between a given Quaternion and this.
		*@param in_Quat The Quaternion to compute the dot product.
		*@return Result of the dot product.
		*/
		float DotProduct ( Quaternion const& in_Quat ) const;

		/*! @brief Compute a mix of two Quaternion at alpha.
		*@param in_QuatA The first Quaternion ( alpha = 0 );
		*@param in_QuatB The second Quaternion ( alpha = 1 );
		*@param in_Blend The alpha to mix the quaternion;
		*@param in_ReduceTo360 True for 360° solution, False for 720° solution. 
		*@return The result of the slerp.
		*/
		Quaternion& Slerp ( Quaternion const& in_QuatA,
                            Quaternion const& in_QuatB,
                            float const in_Blend,
                            bool const in_ReduceTo360 );

        /* @brief Compute a mix of two Quaternion with the blend value. Use the linear interpolation of the quaternion.
        *@param in_QuatA The first Quaternion ( alpha = 0 );
        *@param in_QuatB The second Quaternion ( alpha = 1 );
        *@param in_Blend The alpha to mix the quaternion;
        *@return The result of the lerp.
        */
        Quaternion Lerp (Quaternion const& in_QuatA,
                            Quaternion const& in_QuatB,
                            float const in_Blend);

		/*! @brief Addition a Quaternion with this.
		*@param in_Quat A Quaternion.
		*@return The result of the addition.
		*/
		Quaternion& SelfAdd ( Quaternion const& in_Quat );

		/*! @brief Subtract a Quaternion with this.
		*@param in_Quat A Quaternion.
		*@return The result of the subtract.
		*/
		Quaternion& SelfSub ( Quaternion const& in_Quat );

		/*! @brief Multiply a Quaternion with this.
		*@param in_Quat A Quaternion.
		*@return The Result of the Multiply.
		*/
		Quaternion& SelfMul ( Quaternion const& in_Quat );

		/*! @brief Negate this Quaternion.
		@return The result of the negate.
		*/
		Quaternion& SelfNegate ();

		/*! @brief Invert this Quaternion.
		@return The result of the invert.
		*/
		Quaternion& SelfInvert ();

		/*! @brief Conjugate this Quaternion.
		*@return The result of the conjugate.
		*/
		Quaternion& SelfConjugate();

		/*! @brief Normalize this Quaternion.
		*@return The result of the Normalize.
		*/
		Quaternion& SelfNormalize ();

		/*! @brief Convert the current Quaternion to Matrix 3x3.
		*@return The Matrix 3x3.
		*/
		Matrix3x3 ToMatrix3x3 () const;

		/*! @brief Convert the Matrix 3x3 to this Quaternion.
		*@param in_Matrix The matrix to convert.
		*@return The corresponding Quaternion.
		*/
		Quaternion& FromMatrix3x3 ( Matrix3x3 const& in_Matrix );

		/*! @brief Reciprocal square root approximation.
		*@param in_X The value with we compute the data.
		*@param The result.
		*/
		float ReciprocalSqrt (float in_X);

		//GET METHODES.

		/*! @brief Get the length of this quaternion.
		*@return The length of the Quaternion.
		*/
		float GetLength () const ; 

		/*! @brief Get the value x of the axe.
		*@return The value x of the axe.
		*/
		float GetX () const ;

		/*! @brief Get the value y of the axe.
		*@return The value y of the axe.
		*/
		float GetY () const ;

		/*! @brief Get the value z of the axe.
		*@return The value z of the axe.
		*/
		float GetZ () const ;

		/*! @brief Get the value w of the axe.
		*@return The value w of the axe.
		*/
		float GetW () const ;

		//SET METHODES.
		/*! @brief Set the quaternion with the value of each components.
		*@param in_X The value x of the axe.
		*@param in_Y The value y of the axe.
		*@param in_Z The value z of the axe.
		*@param in_W The value of the angle.
		*/
		void Set ( float const& in_X,
                    float const& in_Y,
                    float const& in_Z,
                    float const& in_W );

		/*! @brief Set the value x of the axe. */
		void SetX ( float const& in_X );

		/*! @brief Set the value y of the axe. */
		void SetY ( float const& in_Y );

		/*! @brief Set the value z of the axe. */
		void SetZ ( float const& in_Z );

		/*! @brief Set the value w of the angle. */
		void SetW ( float const& in_W );

		/*! @brief Set the quaternion with axis ( must to be normalized ) and angle ( in radians ).
		*@param in_X The x value of the axe.
		*@param in_Y The y value of the axe.
		*@param in_Z The z value of the axe.
		*@param	in_Angle The angle.
		*/
		void FromAxisAndAngle ( float const& in_X,
                                float const& in_Y,
                                float const& in_Z,
                                float const& in_Angle );

		/*! @brief Return the rotation as Euler.
		*@param out_X Return the angle on X (bank).
		*@param out_Y Return the angle on Y (heading).
		*@param out_Z Return the angle on Z (attitude).
		*/
		void ToEuler ( float &out_X,
                        float &out_Y,
                        float &out_Z );

	private:
		float m_x;	/*! Vector Axe X. */
		float m_y;	/*! Vector Axe Y. */
		float m_z;	/*! Vector Axe Z. */
		float m_w;	/*! Angle. */
};



#endif