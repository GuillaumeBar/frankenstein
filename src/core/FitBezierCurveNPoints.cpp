/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file FitBezierCurveNPoints.cpp
@author Guillaume Baratte
@date 2016-05-17
*/

#include "FitBezierCurveNPoints.h"
#include <iostream>
using namespace std;

const int FitBezierCurveNPoints::m_MemoryBaseSize = 500;

FitBezierCurveNPoints::FitBezierCurveNPoints(){	
	// Init the Fit Bezier Curve Data.
	m_NumberPoints = 1;
	m_ArrayBasePoints.SetSizeAndClear(4);
	m_ArraySubBezierController.SetSizeAndClear(4);
	m_ArrayInterpolatedTangents.SetSizeAndClear(4);
	m_ArrayAllPoints.SetSizeAndClear(10);
	m_ArraySubDistance.SetSizeAndClear(4);
	m_FitStartRange = 0.0f;
	m_FitEndRange = 1.0f;
	m_FitMoveRange = 0.0f;
	m_UseUserTangents = 0;
	m_curveLength = 0.0f;
	m_inTangentLength = 0.3333f;
	m_outTangentLength = 0.3333f;
	m_ArrayUserTangents.SetSizeAndClear(10);
	m_ArrayPointsT.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayCurvePointsPosition.SetSizeAndClear(m_MemoryBaseSize);
	m_ArraySubLength.SetSizeAndClear(m_MemoryBaseSize);
	m_ArraySubTangents.SetSizeAndClear(m_MemoryBaseSize);
	m_ArraySubVectors.SetSizeAndClear(m_MemoryBaseSize);
}

FitBezierCurveNPoints::~FitBezierCurveNPoints(){
}

void FitBezierCurveNPoints::Set(int& in_NumberPoints,
								DataArray<Vector3D>& in_ArrayBasePoints,
								float& in_FitStartRange,
								float& in_FitEndRange,
								float& in_FitMoveRange,
								int& in_UseUserTangents,
								DataArray<Vector3D>& in_ArrayUserTangents){

	// Init the Fit Bezier Curve Data.
	m_NumberPoints = in_NumberPoints;
	m_ArrayBasePoints = in_ArrayBasePoints;
	m_UseUserTangents = in_UseUserTangents;
	m_ArrayUserTangents = in_ArrayUserTangents;
	m_FitStartRange = in_FitStartRange;
	m_FitEndRange = in_FitEndRange;
	m_FitMoveRange = in_FitMoveRange;
	m_ArrayInterpolatedTangents.SetSizeAndClear(in_ArrayBasePoints.GetSize());
	m_ArrayAllPoints.SetSizeAndClear((in_ArrayBasePoints.GetSize()-2)*3+4);
	m_ArraySubDistance.SetSizeAndClear(in_ArrayBasePoints.GetSize());
	// Resize the array of computation if necessary.
	if(in_ArrayBasePoints.GetSize() > m_MemoryBaseSize){
		m_ArrayPointsT.SetSizeAndClear(m_NumberPoints);
		m_ArrayCurvePointsPosition.SetSizeAndClear(in_NumberPoints);
		m_ArraySubLength.SetSizeAndClear(in_NumberPoints);
		m_ArraySubVectors.SetSizeAndClear(m_MemoryBaseSize);
		m_ArraySubTangents.SetSizeAndClear(m_MemoryBaseSize);
	}
}

void FitBezierCurveNPoints::Set(int& in_NumberPoints,
								DataArray<Vector3D>& in_ArrayBasePoints,
								float& in_FitStartRange,
								float& in_FitEndRange,
								float& in_FitMoveRange,
								int& in_UseUserTangents,
								float& in_inTangentLength,
								float& in_outTangentLength,
								DataArray<Vector3D>& in_ArrayUserTangents){

	// Init the Fit Bezier Curve Data.
	m_NumberPoints = in_NumberPoints;
	m_ArrayBasePoints = in_ArrayBasePoints;
	m_UseUserTangents = in_UseUserTangents;
	m_ArrayUserTangents = in_ArrayUserTangents;
	m_FitStartRange = in_FitStartRange;
	m_FitEndRange = in_FitEndRange;
	m_FitMoveRange = in_FitMoveRange;
	m_inTangentLength = in_inTangentLength;
	m_outTangentLength = in_outTangentLength;
	m_ArrayInterpolatedTangents.SetSizeAndClear(in_ArrayBasePoints.GetSize());
	m_ArrayAllPoints.SetSizeAndClear((in_ArrayBasePoints.GetSize()-2)*3+4);
	m_ArraySubDistance.SetSizeAndClear(in_ArrayBasePoints.GetSize());
	// Resize the array of computation if necessary.
	if(in_ArrayBasePoints.GetSize() > m_MemoryBaseSize){
		m_ArrayPointsT.SetSizeAndClear(m_NumberPoints);
		m_ArrayCurvePointsPosition.SetSizeAndClear(in_NumberPoints);
		m_ArraySubLength.SetSizeAndClear(in_NumberPoints);
		m_ArraySubVectors.SetSizeAndClear(m_MemoryBaseSize);
		m_ArraySubTangents.SetSizeAndClear(m_MemoryBaseSize);
	}
}

void FitBezierCurveNPoints::ComputeCurve(){

	Vector3D distVector, prevPos, nextPos;
	Vector3D prevTan, nextTan;

	m_curveLength = 0.0f;
	m_ArraySubLength[0] = 0.0f;
	for(unsigned int i=0; i<m_NumberPoints; i++){
		// Compute the point t value of the curve.
		ComputeOneFitT(i);
		// Compute the point position of the curve.
		ComputeOneFitPoint(i);
		// Compute the length of the curve and sub vectors for computing tangents.
		if(i > 0){
			// Get the previous and the next point position.
			prevPos = GetCurvePointsAt(i-1);
			nextPos = GetCurvePointsAt(i);
			// Compute the in between vector.
			distVector = nextPos - prevPos;
			// Compute the legnth of the vector and add it to the length.
			m_curveLength += distVector.GetLength();
			// Store the sub curve length.
			m_ArraySubLength[i] = m_curveLength;
			// Store the sub vector.
			m_ArraySubVectors[i-1] = distVector;
		}
		// Compute the tangents.
		if(i == 2){
			// Compute the tangent for the first point.
			m_ArraySubTangents[0] = m_ArraySubVectors[0];
		}
		if(i > 1){
			// Compute the tangent fot the previous point.
			prevTan.MulByFloat(m_ArraySubVectors[i - 2], 0.5f);
			nextTan.MulByFloat(m_ArraySubVectors[i - 1], 0.5f);
			m_ArraySubTangents[i - 1].Add(prevTan, nextTan);
		}
		if(i == m_NumberPoints - 1){
			m_ArraySubTangents[i] = m_ArraySubTangents[i - 1];
		}
	}
}

void FitBezierCurveNPoints::ComputeAllFitPoint(){
	for(unsigned int i=0; i<m_NumberPoints; i++){
		ComputeOneFitPoint(i);
	}	
}

void FitBezierCurveNPoints::ComputeAllFitT(){
	for(unsigned int i=0; i<m_NumberPoints; i++){
		ComputeOneFitT(i);
	}		
}

void FitBezierCurveNPoints::AddUserTangentsToArrayAllPoints(){

	// Loop over the controller point.
	for(unsigned int iCon=0; iCon<m_ArrayBasePoints.GetSize(); iCon++){
		if(iCon == 0){
			m_ArrayAllPoints[iCon] = m_ArrayBasePoints[iCon];
			m_ArrayAllPoints[iCon+1] = m_ArrayUserTangents[iCon];
		}
		else if(iCon == m_ArrayBasePoints.GetSize() - 1){
			m_ArrayAllPoints[m_ArrayAllPoints.GetSize()-2] = m_ArrayUserTangents[m_ArrayUserTangents.GetSize()-1];
			m_ArrayAllPoints[m_ArrayAllPoints.GetSize()-1] = m_ArrayBasePoints[iCon];
		}else{
			m_ArrayAllPoints[iCon*3-1] = m_ArrayUserTangents[iCon*2-1];
			m_ArrayAllPoints[iCon*3] = m_ArrayBasePoints[iCon];
			m_ArrayAllPoints[iCon*3+1] = m_ArrayUserTangents[iCon*2];
		}
	}
}		

void FitBezierCurveNPoints::ComputeOneFitPoint ( int in_Index ){

	if(m_ArrayPointsT[in_Index] >= 1.0){
		m_ArrayCurvePointsPosition[in_Index] = m_ArrayAllPoints[m_ArrayAllPoints.GetSize()-1];
	}else{
		// Compute the scaled t with the number of sub bezier curve.
		float scaledT = m_ArrayPointsT[in_Index] * (float)(m_ArrayBasePoints.GetSize()-1);
		// Find the sub bezier curve index.
		int p0Index, p1Index, p2Index, p3Index;
		p0Index = floor(scaledT) * 3.0f;
		p1Index = p0Index + 1;
		p2Index = p1Index + 1;
		p3Index = p2Index + 1;				
		// Compute the sub bezier t value.
		float subT = fmod(scaledT, 1.0f);
		// Define the sub bezier control point.
		m_ArraySubBezierController[0] = m_ArrayAllPoints[p0Index];
		m_ArraySubBezierController[1] = m_ArrayAllPoints[p1Index];
		m_ArraySubBezierController[2] = m_ArrayAllPoints[p2Index];
		m_ArraySubBezierController[3] = m_ArrayAllPoints[p3Index];

		// Compute the point position on the sub bezier curve.
		m_ArrayCurvePointsPosition[in_Index] = ComputeBezierCurve4PointOnePoint(subT, m_ArraySubBezierController);
	}
}

void FitBezierCurveNPoints::ComputeOneFitT(int in_Index){
	float tFraction;
	//Compute the t fractoin.
	tFraction = float(in_Index) / float( m_NumberPoints - 1 );
	//Retarget the t Fraction value in Range m_MoveStartRange - m_MoveEndRange.
	m_ArrayPointsT[in_Index] = m_FitStartRange + tFraction * ( m_FitEndRange - m_FitStartRange );	
}

Vector3D FitBezierCurveNPoints::ComputeBezierCurve4PointOnePoint(float in_T, DataArray<Vector3D>& in_PointPosition){

	Vector3D			rsltPosition;
	DataArray<float>	arrayT(3);
	DataArray<float>	arrayInvT(3);
	DataArray<float>	arrayBase(4);

	// Compute t
	arrayT[0] = in_T;
	arrayT[1] = arrayT[0] * arrayT[0];
	arrayT[2] = arrayT[1] * arrayT[0];

	// Compute invT.
	arrayInvT[0] = 1 - arrayT[0];
	arrayInvT[1] = arrayInvT[0] * arrayInvT[0];
	arrayInvT[2] = arrayInvT[1] * arrayInvT[0];

	// Compute Base.
	arrayBase[0] = arrayInvT[2];
	arrayBase[1] = arrayT[0] * arrayInvT[1] * 3;
	arrayBase[2] = arrayT[1] * arrayInvT[0] * 3;
	arrayBase[3] = arrayT[2];

	//Compute the point position.
	for ( int i=0 ; i<4 ; i++ )
	{
		rsltPosition.Set (	rsltPosition.GetX() + in_PointPosition[i].GetX() * arrayBase[i],
							rsltPosition.GetY() + in_PointPosition[i].GetY() * arrayBase[i],
							rsltPosition.GetZ() + in_PointPosition[i].GetZ() * arrayBase[i] );
	}

	return rsltPosition;
}

void FitBezierCurveNPoints::ComputeTangents(){
	// Compute the interpolated tangent for each point and the distance.
	Vector3D tangentA, tangentB;
	float distA, distB, distSum, distAlpha;
	float scaleFactA, scaleFactB;
	int n = m_ArrayBasePoints.GetSize();
	for(unsigned int i=0; i<n; i++){
		// Compute the tangent A and B for each points.
		if(i == 0){
			tangentB.Sub(m_ArrayBasePoints[i+1], m_ArrayBasePoints[i]);
			tangentA = tangentB;
		}else if( i== n-1){
			tangentA.Sub(m_ArrayBasePoints[i], m_ArrayBasePoints[i-1]);
			tangentB = tangentA;			
		}else{
			tangentA.Sub(m_ArrayBasePoints[i], m_ArrayBasePoints[i-1]);
			tangentB.Sub(m_ArrayBasePoints[i+1], m_ArrayBasePoints[i]);
		}
		// Compute the distance A, B and Sum.
		distA = tangentA.GetLength();
		distB = tangentB.GetLength();
		distSum = distA + distB;
		// Store the distance B for later use.
		m_ArraySubDistance[i] = distB;
		// Compute the distance Alpha.
		distAlpha = distA / distSum;
		// Compute the scale factor for the tangent A and B.
		scaleFactA = (1 - distAlpha) / distA;
		scaleFactB = distAlpha / distA;
		// Scale the tangents.
		tangentA.SelfMulByFloat(scaleFactA);
		tangentB.SelfMulByFloat(scaleFactB);
		// Compute the interpolated tangent.
		m_ArrayInterpolatedTangents[i].Add(tangentA, tangentB);
	}

	// Fix the interpolated tangent for the start and end point.
	// Fix the start point.
	m_ArrayInterpolatedTangents[0].Sub(m_ArrayInterpolatedTangents[0], m_ArrayInterpolatedTangents[1]);
	m_ArrayInterpolatedTangents[0].SelfMulByFloat(2.0f);
	m_ArrayInterpolatedTangents[0].SelfAdd(m_ArrayInterpolatedTangents[1]);
	// Fix the end point.
	m_ArrayInterpolatedTangents[n-1].Sub(m_ArrayInterpolatedTangents[n-1], m_ArrayInterpolatedTangents[n-2]);
	m_ArrayInterpolatedTangents[n-1].SelfMulByFloat(2.0f);
	m_ArrayInterpolatedTangents[n-1].SelfAdd(m_ArrayInterpolatedTangents[n-2]);

	// Resize the array to store all the points for the curve computation.
	int allPointsSize = m_ArrayAllPoints.GetSize();

	// Compute all controller point.
	float distScale;
	int pIndex;
	// Loop over the base controller points array.
	for(unsigned int i=0; i<n; i++){
		if(i == 0){
			// Set the controller point at index 0 in all point array.
			m_ArrayAllPoints[i] = m_ArrayBasePoints[i];
			// Set the controller point at index 1 in all point array.
			distScale = m_ArraySubDistance[i] * m_outTangentLength;
			m_ArrayAllPoints[i+1] = m_ArrayInterpolatedTangents[i];
			m_ArrayAllPoints[i+1].SelfMulByFloat(distScale);
			m_ArrayAllPoints[i+1].SelfAdd(m_ArrayBasePoints[i]);
		}else if(i == n-1){
			// Set the controller point at index n-1 in all point array.
			m_ArrayAllPoints[allPointsSize-1] = m_ArrayBasePoints[i];
			// Set the controller point at index n-2 in all point array.
			distScale = -m_ArraySubDistance[i-1] * m_inTangentLength;
			m_ArrayAllPoints[allPointsSize-2] = m_ArrayInterpolatedTangents[i];
			m_ArrayAllPoints[allPointsSize-2].SelfMulByFloat(distScale);
			m_ArrayAllPoints[allPointsSize-2].SelfAdd(m_ArrayBasePoints[i]);
		}else{
			pIndex = i * 3;
			// Set the tangent A of the controller i.
			distScale =- m_ArraySubDistance[i-1] * m_inTangentLength;
			m_ArrayAllPoints[pIndex-1] = m_ArrayInterpolatedTangents[i];
			m_ArrayAllPoints[pIndex-1].SelfMulByFloat(distScale);
			m_ArrayAllPoints[pIndex-1].SelfAdd(m_ArrayBasePoints[i]);
			// Set the position of the controller i.
			m_ArrayAllPoints[pIndex] = m_ArrayBasePoints[i];
			// Set the tangent B of the controller i.
			distScale = m_ArraySubDistance[i] * m_outTangentLength;
			m_ArrayAllPoints[pIndex+1] = m_ArrayInterpolatedTangents[i];
			m_ArrayAllPoints[pIndex+1].SelfMulByFloat(distScale);
			m_ArrayAllPoints[pIndex+1].SelfAdd(m_ArrayBasePoints[i]);
		}
	}	
}

DataArray<Vector3D> FitBezierCurveNPoints::GetCurvePoints(){
	return m_ArrayCurvePointsPosition;
}

DataArray<float> FitBezierCurveNPoints::GetCurveSubLengths(){
	return m_ArraySubLength;
}

DataArray<float> FitBezierCurveNPoints::GetCurveTs(){
	return m_ArrayPointsT;
}

Vector3D FitBezierCurveNPoints::GetCurvePointsAt(int in_Index){
	return m_ArrayCurvePointsPosition[in_Index];
}

float FitBezierCurveNPoints::GetTValueAt(int in_Index){
	return m_ArrayPointsT[in_Index];
}

float FitBezierCurveNPoints::GetLengthAt(int in_Index){
	return m_ArraySubLength[in_Index];
}

float FitBezierCurveNPoints::GetLength(){
	return m_curveLength;
}

DataArray<Vector3D> FitBezierCurveNPoints::GetCurveSubVectors(){
	return m_ArraySubVectors;
}

DataArray<Vector3D> FitBezierCurveNPoints::GetCurveSubTangents(){
	return m_ArraySubTangents;
}