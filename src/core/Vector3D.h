/*
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
	\file Vector3D.h
	\author Guillaume Baratte
	\date 2016-03-27
	\brief Define the vector3D class.
	Help to manipulate the vector 3D data.
*/
/*!
	\page 1
	\section p1s1 Vector 3D

	The class Vector3D are use to manipulation vector in 3d .

	Vector3D : {x,y,z}

	The vector can be used in different case, for example:

	- To store the position of an object.
	- To define some force for particles or simulated objects.
	- To compute the distance between objects.
*/

#ifndef __VECTOR3D__
#define __VECTOR3D__

#include <math.h>
#include "Matrix3x3.h"
#include "Matrix4x4.h"
#include "Quaternion.h"
#include <xmmintrin.h>
#include <immintrin.h>

/*! @brief The Vector3D Class. */
class Vector3D
{
	public:
		// Methodes
		/*!
			\page 1
			\subsection p1s1s1 Create Instance

			~~~~{.c}
			#include "Vector3D.h"

			Vector3D pos;
			>> pos : {0.0, 0.0, 0.0}

			Vector3D pos = Vector3D(2.0, 1.0, 0.0);
			>> pos : {2.0, 1.0, 0.0}
			~~~~
		*/
		// Constructeur
		/*! @brief Constructeur principal. */
		Vector3D();
		
		/*! @brief Constructeur avec renseignement des valeurs x, y, z, du vecteur.
		* @param in_x La valeur de l'axe X.
		* @param in_y La valeur de l'axe Y.
		* @param in_z La valeur de l'axe Z.
		*/
		Vector3D( float in_x, float in_y, float in_z );

		// Destructeur
		/*! @brief Destructeur principal. */
		~Vector3D();

		// Operateur de comparaison
		/*! @brief Surcharge de l'operateur d'egalite.
		* @param in_VectorB Vecteur que l'on veut comparer.
		* @return Renvoie vrai ou faux.
		*/
		bool operator==	( Vector3D const& in_VectorB );

		/*! @brief Surcharge de l'operateur de difference.
		* @param in_VectorB Vecteur que l'on veut comparer.
		* @return Renvoie vrai ou faux.
		*/
		bool operator!=	( Vector3D const& in_VectorB );

		// Operateur arithmetiques

		/*! @brief Surcharge de l'operateur d'addition.
		*
		* Algorithme : \f$ \vec{N}=\vec{This}+\vec{B} \f$
		*
		* @param in_VectorB Vecteur que l'on veut ajouter.
		* @return Renvoie le resultat de l'addition dans un nouveau vecteur.
		*/
		Vector3D& operator+ ( Vector3D const& in_VectorB );

		/*! @brief Surcharge de l'operateur de soustraction.
		*
		* Algorithme : \f$ \vec{N}=\vec{This}-\vec{B} \f$
		*
		* @param in_VectorB Vecteur que l'on veut soustraire.
		* @return Renvoie le resultat de la soustraction dans un nouveau vecteur.
		*/	
		Vector3D& operator- ( Vector3D const& in_VectorB );

		/*! @brief Surcharge de l'operateur de multiplication par un float.
		* @param in_Float Valeur avec laquelle l'on veut multiplier le vecteur.
		* @return Renvoie le resultat de la multiplication dans un nouveau vecteur.
		*/	
		Vector3D& operator* ( float const& in_Float );


		//++++++++++++++++|
		//  SET METHODES  |
		//++++++++++++++++|
		/*!
			\page 1
			\subsection p1s1s2 Set Vector Value
			~~~~{.c}
			#include "Vector3D.h"

			Vector3D pos;

			// Set all valus.
			pos.Set(1.0, 2.0 1.0);
			>> pos : {1.0, 2.0, 1.0}

			// Set X value
			pos.SetX(3.5);
			>> pos : {3.5, 2.0 1.0}

			// Set Y value.
			pos.SetY(4.0);
			>> pos : {3.5, 4.0, 1.0}

			// Set Z value.
			pos.SetZ(2.0);
			>> pos : {3.5, 4.0, 2.0}

			// Set from an other Vector3D.
			Vector pos1;
			pos1.SetFromVector3D(pos);
			>> pos1 : {3.5, 4.0, 2.0}
			~~~~
		*/

		/*! @brief Permet de definir les valeur des axes X, Y, Z du vecteur.
		* @param in_AxeX La valeur de l'axe X.
		* @param in_AxeY La valeur de l'axe Y.
		* @param in_AxeZ La valeur de l'axe Z.
		*/
		void	Set ( float in_AxeX, float in_AxeY, float in_AxeZ );

		/*! @brief Permet de copier un vecteur.
		* @param in_Vector3 Le vecteur que l'on veut copier.
		*/
		void	SetFromVector3D ( Vector3D const& in_Vector3 );

		/*! @brief Permet de definir la valeur de X du vecteur.
		* @param in_float Le valeur que l'on veut assignee a X.
		*/
		void	SetX ( float in_float );

		/*! @brief Permet de definir la valeur de Y du vecteur.
		* @param in_float Le valeur que l'on veut assignee a Y.
		*/
		void	SetY ( float in_float );

		/*! @brief Permet de definir la valeur de Z du vecteur.
		* @param in_float Le valeur que l'on veut assignee a Z.
		*/
		void	SetZ ( float in_float );

		/**
			\page 1
			\subsection p1s1s3 Get Vector Values

			~~~~{.c}
			#include "Vector3D.h"

			Vector pos;

			// Set all values.
			pos.Set(1.0, 2.0 1.0);
			>> pos : {1.0, 2.0, 1.0}

			// Get X value.
			pos.GetX();
			>> 1.0

			// Get Y value.
			pos.GetY();
			>> 2.0

			// Get Z value.
			pos.GetZ();
			>> 1.0

			~~~~
		*/
		/*! @brief Permet de recupere la valeur de X du vecteur.
		* @return Renvoie la valeur de X.
		*/
		float	GetX () const;

		/*! @brief Permet de recupere la valeur de Y du vecteur.
		* @return Renvoie la valeur de Y.
		*/
		float	GetY () const;

		/*! @brief Permet de recupere la valeur de Z du vecteur.
		* @return Renvoie la valeur de Z.
		*/
		float	GetZ () const;

		/*! @brief Permet de tester si deux vecteurs sont egaux.
		* @param in_VectorB Le vecteur que l'on veut comparer.
		* @return Vrai ou faux si les vecteurs sont egaux ou non.
		*/
		bool IsEqual ( Vector3D const& in_VectorB ) const;

		/*! 
			\page 1
			\subsection p1s1s8 Length
			Get the length of the vector.
			~~~~{.c}
			// Example to compute distance between two position.
			#include "Vector3D.h"

			// Create position vectors.
			Vector3D pos1, pos2;

			// Set the position of the position vectors.
			pos1.Set(1.0, 2.0, 4.0);
			pos2.Set(3.0, 5.0, 7.0);

			// Create a vector to store the distance vector.
			Vector3D vDist;

			// Subtract the two position vector.
			vDist.Sub(pos2, pos1);

			// Get the length of the vector.
			float distance = vDist.GetLength();
			>> distance : 
			~~~~
		*/
		/*! @brief Permet de calculer le longeur du vector.
		@return La longeur du vecteur.
		*/
		float	GetLength () const;

		/*! @brief Permet de normalize le vecteur lui-meme.*/
		void	SelfNormalize ();

		/*! @brief Permet de normalizer un vecteur.
		* @param in_VectorA Le vecteur que l'on veut normaliser.
		*/
		void	Normalize( Vector3D & in_VectorA );

		/*! @brief Permet de calculer le dot product entre deux vecteurs.
		* @param in_VectorB Le deuxieme vecteur avec lequel l'on veut calculer le dot product.
		* @return La valeur du resultat du dot product.
		*/
		float	DotProduct ( Vector3D const& in_VectorB ) const;

		/*! @brief Permet de calculer le cross product entre deux vecteurs.
		* @param in_VectorA Le premier vecteur avec lequel l'on veut calculer le cross product.
		* @param in_VectorB Le deuxieme vecteur avec lequel l'on veut calculer le cross product.
		*/
		Vector3D& CrossProduct ( Vector3D const& in_VectorA, Vector3D const& in_VectorB );

		/*! @brief Permet de calculer l'angle entre deux vecteurs quelque soit la longeur des vecteurs.
		* @param in_VectorB Le vecteur avec lequel on veut calculer l'angle.
		* @param in_Degrees Permet de choisir le resultat en radians ou degrees. true->Radian. false->Degrees.
		* @return L'angle entre les deux vecteurs en radians ou en degrees.
		*/
		float	GetAngle ( Vector3D const& in_VectorB, bool in_Degrees ) const;

		/*! @brief Permet de calculer la somme de deux vecteurs.
		* @param in_VectorB Le deuxieme vecteur a additionner.
		* @return Le vecteur resultant de l'addition.
		*/
		Vector3D&	SelfAdd ( Vector3D const& in_VectorB );

		/*! @brief Permet de calculer d'un vecteur avec celui-ci.
		* @param in_VectorB Le deuxieme vecteur a soustraire.
		* @return Le vecteur resultant de la soustraction.
		*/
		Vector3D&	SelfSub ( Vector3D const& in_VectorB );

		/*! @brief Permet de multiplier un vecteur par un float.
		* @param in_Float Le nombre par lequel l'on veut multiplier le vecteur.
		* @return Le vecteur resultant de la multiplication par un float.
		*/
		Vector3D&	SelfMulByFloat ( float const& in_Float );

		/*! @brief Add two vector in this.
		*@param	in_VectorA The first vector to add.
		*@param in_VectorB The second vector to add.
		*@return The result of the add.
		*/
		Vector3D& Add ( Vector3D const& in_VectorA , Vector3D const& in_VectorB );

		/*! @brief Permet de calculer la soustraction entre deux vecteurs.
		*@param in_VectorA Le premier vecteur a soustraire.
		*@param in_VectorB Le deuxieme vecteur a soustraire.
		*@return Le vecteur resultant de la soustraction.
		*/
		Vector3D& Sub ( Vector3D const& in_VectorA , Vector3D const& in_VectorB );

		/*! @brief Multiply a vector by a float and retrun the result in this.
		*@param in_VectorA	The Vector we want to multiply.
		*@param in_Float	The value to multiply.
		*@return The result of the multiply.
		*/
		Vector3D& MulByFloat ( Vector3D const& in_VectorA, float const& in_Float );

		/*! @brief Multiply a Vector by a Matrix 3x3.
		*@param in_Vector The Vector for multiply.
		*@param in_Matrix3x3 The Matrix 3x3 for multiply.
		*@return The result of the multply.
		*/
		Vector3D& MulByMatrix3x3 ( Vector3D const& in_Vector, Matrix3x3 const& in_Matrix3x3 );

		/*! @brief Multiply a Vector by a Matrix 4x4.
		*@param in_Vector The Vector for multiply.
		*@param in_Matrix4x4 The Matrix 4x4 for multiply.
		*@return The result of the multply.
		*/
		Vector3D& MulByMatrix4x4 ( Vector3D const& in_Vector, Matrix4x4 const& in_Matrix4x4 );

		/*! @brief Multiply a Vector by a Quaternion.
		*@param in_Vector The Vector for multiply.
		*@param in_Quaternion The Quaternion for multiply.
		*@return The result of the multply.
		*/
		Vector3D& RotateByQuaternionFG ( Vector3D const& in_Vector, Quaternion const& in_Quaternion );

		/*! @brief Multiply this by a Matrix 3x3.
		*@param in_Matrix3x3 The Matrix 3x3 for multiply.
		*@return The result of the multply.
		*/
		Vector3D& SelfMulByMatrix3x3 ( Matrix3x3 const& in_Matrix3x3 );

		/*! @brief Multiply this by a Matrix 4x4.
		*@param in_Matrix4x4 The Matrix 4x4 for multiply.
		*@return The result of the multply.
		*/
		Vector3D& SelfMulByMatrix4x4 ( Matrix4x4 const& in_Matrix4x4 );

		/*! @brief Multiply this by a Quaternion.
		*@param in_Quaternion The Quaternion for multiply.
		*@return The result of the multply.
		*/
		Vector3D& SelfRotateByQuaternionFG ( Quaternion const& in_Quaternion );






	private:
		// Attributs
		float	m_x;	/*! Attribut de l'axe X.*/
		float	m_y;	/*! Attribut de l'axe Y.*/
		float	m_z;	/*! Attribut de l'axe Z.*/
		float*	aValue;
};

#endif
