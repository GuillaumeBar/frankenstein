/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Matrix4x4.h
@author Guillaume Baratte
@date 2016-03-27
@brief Define the matrix4x4 class.
Help to manipulate the matrix4x4 data.
*/

#ifndef __MATRIX4X4__
#define __MATRIX4X4__

#include <math.h>

class Matrix4x4
{
	public:
		//CONSTRUCTORS.
		/*! @brief Init the matrix. */
		Matrix4x4 ();

		/*! @brief Init the matrix with the value of each component.
		*@param in_00 Value of the row 0 and col 0.
		*@param in_01 Value of the row 0 and col 1.
		*@param in_02 Value of the row 0 and col 2.
		*@param in_03 Value of the row 0 and col 3.
		*@param in_10 Value of the row 1 and col 0.
		*@param in_11 Value of the row 1 and col 1.
		*@param in_12 Value of the row 1 and col 2.
		*@param in_13 Value of the row 1 and col 3.
		*@param in_20 Value of the row 2 and col 0.
		*@param in_21 Value of the row 2 and col 1.
		*@param in_22 Value of the row 2 and col 2.
		*@param in_23 Value of the row 2 and col 3.
		*@param in_30 Value of the row 3 and col 0.
		*@param in_31 Value of the row 3 and col 1.
		*@param in_32 Value of the row 3 and col 2.
		*@param in_33 Value of the row 3 and col 3.
		*/
		Matrix4x4 ( float in_00, float in_01, float in_02, float in_03,
					float in_10, float in_11, float in_12, float in_13,
					float in_20, float in_21, float in_22, float in_23,
					float in_30, float in_31, float in_32, float in_33 );

		//DESTRUCTOR.
		/*! @brief The destructor of the class.*/
		~Matrix4x4 ();

		//OPERATORS.
		/*! @brief Overload the equality operator.
		*@param in_MatrixB The matrix to compare.
		*@return True or False.
		*/
		bool operator== ( Matrix4x4 const& in_MatrixB ) const;

		/*! @brief Overload the not equality operator.
		*@param in_MatrixB The matrix to compare.
		*@return True or Flase.
		*/
		bool operator!= ( Matrix4x4 const& in_MatrixB ) const;

		/*! @brief Overload the multiply operator.
		*@param in_MatrixB The matrix we want to use the multiply.
		*@return The result of the multiply as matrix4x4.
		*/
		Matrix4x4& operator* ( Matrix4x4 const& in_MatrixB );

		//CUSTOM METHODES.
		/*! @brief Invert the given matrix to this.
		*@param in_MatrixB The matrix we want to invert.
		*@return True if the matrix have an inverse.
		*/
		bool Invert ( Matrix4x4 &in_MatrixB );

		/*! @brief Invert this matrix.
		*@return True if the matrix have an inverse.
		*/
		bool SelfInvert ();

		/*! @brief Transpose the given matrix to this.
		*@param in_MatrixB The matrix we want to transpose.
		*/
		void Transpose ( Matrix4x4 const& in_MatrixB );

		/*! @brief Transpose this matrix.*/
		void SelfTranspose ();

		/*! @brief Transpose the inverse of the giben matrix to this.
		*@param in_MatrixB The matrix we want to transpose the inverse.
		*@return True if the matrix have an inverse.
		*/
		bool TransposeInverse ( Matrix4x4 &in_MatrixB );

		/*! @brief Transpose the inverse of this.
		*@return True if the matrix have an inverse.
		*/
		bool SelfTransposeInverse ();

		/*! @brief Check if this is equals to the given matrix.
		*@param in_MatrixB The matix we want to test.
		*@return True or False.
		*/
		bool IsEquals ( Matrix4x4 const& in_MatrixB ) const;

		/*! @brief Multiply the two matrix A and B in this.
		*@param in_MatrixA The first matrix.
		*@param in_MatrixB The second matrix.
		*@return The result of the multiply.
		*/
		Matrix4x4& Mul ( Matrix4x4 const& in_MatrixA, Matrix4x4 const& in_MatrixB );

		/*! @brief Multiply this and matrix B.
		*@param in_MatrixB The matrix we want to multiply.
		*@return The result of the multiply.
		*/
		Matrix4x4& SelfMul ( Matrix4x4 const& in_MatrixB );

		//GET METHODES.
		/*! @brief Get each value of the matrix in arguments.
		*@param io_00 The Value at row 0 and col 0.
		*@param io_01 The Value at row 0 and col 1.
		*@param io_02 The Value at row 0 and col 2.
		*@param io_03 The Value at row 0 and col 3.
		*@param io_10 The Value at row 1 and col 0.
		*@param io_11 The Value at row 1 and col 1.
		*@param io_12 The Value at row 1 and col 2.
		*@param io_13 The Value at row 1 and col 3.
		*@param io_20 The Value at row 2 and col 0.
		*@param io_21 The Value at row 2 and col 1.
		*@param io_22 The Value at row 2 and col 2.
		*@param io_23 The Value at row 2 and col 3.
		*@param io_30 The Value at row 3 and col 0.
		*@param io_31 The Value at row 3 and col 1.
		*@param io_32 The Value at row 3 and col 2.
		*@param io_33 The Value at row 3 and col 3.
		*/
		void Get (	float &io_00, float &io_01, float &io_02, float &io_03,
					float &io_10, float &io_11, float &io_12, float &io_13,
					float &io_20, float &io_21, float &io_22, float &io_23,
					float &io_30, float &io_31, float &io_32, float &io_33 ) const ;

		/*! @brief Get the value at specific row and col.
		*@param in_Row The index of the row.
		*@param in_Col The index of the col.
		*@return The value at row and col.
		*/
		float GetValue ( int in_Row, int in_Col ) const ;

		/*! @brief Get the matrix3x3 of the matrix4x4.
		*@return The Matrix3x3
		*/
		void GetMatrix3x3 ( float &io_Xx, float &io_Xy, float &io_Xz,
							float &io_Yx, float &io_Yy, float &io_Yz,
							float &io_Zx, float &io_Zy, float &io_Zz ) const ;

		/*! @brief Get the Axe X of the matrix.
		*@return The Axe X.
		*/
		void GetAxeX ( float &io_Xx, float &io_Xy, float &io_Xz ) const ;

		/*! @brief Get the Axe Y of the matrix.
		*@return The Axe Y.
		*/
		void GetAxeY ( float &io_Yx, float &io_Yy, float &io_Yz ) const ;

		/*! @brief Get the AxeZ of the matrix.
		*@return The Axe Z.
		*/
		void GetAxeZ ( float &io_Zx, float &io_Zy, float &io_Zz ) const ;

		/*! @brief Get the Position of the matrix.
		*@return The position in the arguments.
		*/
		void GetPosition ( float &io_Px, float &io_Py, float &io_Pz) const ;

		/*! @brief Get the scale value of the matrix.
		*@return The scale in the arguments.
		*/
		void GetScale ( float &io_AxeX, float &io_AxeY, float &io_AxeZ );

		//SET METHODES.
		/*! @brief Set the matrix with the value of each component.
		*@param in_00 Value of the row 0 and col 0.
		*@param in_01 Value of the row 0 and col 1.
		*@param in_02 Value of the row 0 and col 2.
		*@param in_03 Value of the row 0 and col 3.
		*@param in_10 Value of the row 1 and col 0.
		*@param in_11 Value of the row 1 and col 1.
		*@param in_12 Value of the row 1 and col 2.
		*@param in_13 Value of the row 1 and col 3.
		*@param in_20 Value of the row 2 and col 0.
		*@param in_21 Value of the row 2 and col 1.
		*@param in_22 Value of the row 2 and col 2.
		*@param in_23 Value of the row 2 and col 3.
		*@param in_30 Value of the row 3 and col 0.
		*@param in_31 Value of the row 3 and col 1.
		*@param in_32 Value of the row 3 and col 2.
		*@param in_33 Value of the row 3 and col 3.
		*/
		void Set ( float in_00, float in_01, float in_02, float in_03,
					float in_10, float in_11, float in_12, float in_13,
					float in_20, float in_21, float in_22, float in_23,
					float in_30, float in_31, float in_32, float in_33 );

		/*! @brief Set a value in the matrix at specific row and col.
		*@param in_Row The index of the row.
		*@param in_Col The index of the col.
		*@param in_Value The value of the component.
		*/
		void SetValue ( int in_Row, int in_Col, float in_Value );

		/*! @brief Set the matrix with an Array[4][4] of float.
		*@param in_Array The Array we want to use.
		*/
		void SetFrom4x4Array ( float const in_Array[4][4] );

		/*! @brief Set the axe X of the matrix.
		*@param in_AxeX The vector represent the axe X.
		*/
		void SetAxeX ( float const& in_Xx, float const& in_Xy, float const& in_Xz );

		/*! @brief Set the axe Y of the matrix.
		*@param in_AxeY The vector represent the axe Y.
		*/
		void SetAxeY ( float const& in_Yx, float const& in_Yy, float const& in_Yz );

		/*! @brief Set the axe Z of the matrix.
		*@param in_AxeZ The vector represent the axe Z.
		*/
		void SetAxeZ ( float const& in_Zx, float const& in_Zy, float const& in_Zz );

		/*! @brief Set the position of the matrix.
		*@param in_Position The vector represent the position.
		*/
		void SetPosition ( float const& in_Px, float const& in_Py, float const& in_Pz );

		/*! @brief Set the rotation of the matrix.
		*@param in_Matrix3x3 The matrix represent the rotation.
		*/
		void SetFromMatrix3x3 ( float const& in_Xx, float const& in_Xy, float const& in_Xz,
								float const& in_Yx, float const& in_Yy, float const& in_Yz,
								float const& in_Zx, float const& in_Zy, float const& in_Zz );

		/*! @brief Set the scaling of the matrix.
		*@param in_AxeX The scale value on local axe X.
		*@param in_AxeY The scale value on local axe Y.
		*@param in_AxeZ The scale value on local axe Z.
		*/
		void SetScale ( float const& in_AxeX, float const& in_AxeY, float const& in_AxeZ );

		/*! @brief Multiply the current scale of the matrix by the scale axes.
		*@param in_AxeX The scale to multiply of axe X.
		*@param in_AxeY The scale to multiply of axe Y.
		*@param in_AxeZ The scale to multiply of axe Z.
		*/
		void AddScale ( float const& in_AxeX, float const& in_AxeY, float const& in_AxeZ );

		/*! @brief Get the sub matrix3x3 from the matrix4x4 for the calcul of the determinant.
		*@param in_Row	The index of the row we want to delete.
		*@param in_Col	The index of the col we want to delete.
		*@return The Matrix3x3 without the row and col.
		*/
		void GetSubMat ( int in_Row, int in_Col );

		/*! @brief Calculate the determinant for matrix3x3.
		*@param in_Matix3x3 The matrix for the calcul.
		*@return The determinant of the matrix.
		*/
		float GetDetFromMatri3x3 () const ;

	private:

		float m_Mat[4][4];		/*! The matrix 4x4. */
		float m_SubMat[3][3];	/*! The matrix 3x3 for determinant calcule. */

};

#endif