/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Matrix4x4.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "Matrix4x4.h"

//CONSTRUCTORS.
Matrix4x4::Matrix4x4 ()
{
	m_Mat[0][0] = 1.0f;
	m_Mat[0][1] = 0.0f;
	m_Mat[0][2] = 0.0f;
	m_Mat[0][3] = 0.0f;
	m_Mat[1][0] = 0.0f;
	m_Mat[1][1] = 1.0f;
	m_Mat[1][2] = 0.0f;
	m_Mat[1][3] = 0.0f;
	m_Mat[2][0] = 0.0f;
	m_Mat[2][1] = 0.0f;
	m_Mat[2][2] = 1.0f;
	m_Mat[2][3] = 0.0f;
	m_Mat[3][0] = 0.0f;
	m_Mat[3][1] = 0.0f;
	m_Mat[3][2] = 0.0f;
	m_Mat[3][3] = 1.0f;
};

Matrix4x4::Matrix4x4 (	float in_00, float in_01, float in_02, float in_03,
						float in_10, float in_11, float in_12, float in_13,
						float in_20, float in_21, float in_22, float in_23,
						float in_30, float in_31, float in_32, float in_33 )
{
	m_Mat[0][0] = in_00;
	m_Mat[0][1] = in_01;
	m_Mat[0][2] = in_02;
	m_Mat[0][3] = in_03;
	m_Mat[1][0] = in_10;
	m_Mat[1][1] = in_11;
	m_Mat[1][2] = in_12;
	m_Mat[1][3] = in_13;
	m_Mat[2][0] = in_20;
	m_Mat[2][1] = in_21;
	m_Mat[2][2] = in_22;
	m_Mat[2][3] = in_23;
	m_Mat[3][0] = in_30;
	m_Mat[3][1] = in_31;
	m_Mat[3][2] = in_32;
	m_Mat[3][3] = in_33;
};


//DESTRUCTOR.
Matrix4x4::~Matrix4x4()
{

};

//OPERATIONS OPERATORS.
bool Matrix4x4::operator== ( Matrix4x4 const& in_MatrixB ) const
{
	return IsEquals ( in_MatrixB );
};

bool Matrix4x4::operator!= ( Matrix4x4 const& in_MatrixB ) const
{
	return ! IsEquals ( in_MatrixB );
};

Matrix4x4& Matrix4x4::operator* ( Matrix4x4 const& in_MatrixB )
{
	return Mul ( *this, in_MatrixB );
};

//CUSTOM METHODES.
Matrix4x4& Matrix4x4::SelfMul ( Matrix4x4 const& in_MatrixB )
{
	return Mul( *this, in_MatrixB );
};

Matrix4x4& Matrix4x4::Mul ( Matrix4x4 const& in_MatrixA, Matrix4x4 const& in_MatrixB )
{
	float tempMat[4][4];
	for ( int i=0; i<4; i++ )
	{
		for ( int j=0; j<4; j++ )
		{
			tempMat[i][j] =	in_MatrixA.m_Mat[i][0] * in_MatrixB.m_Mat[0][j]+
							in_MatrixA.m_Mat[i][1] * in_MatrixB.m_Mat[1][j]+
							in_MatrixA.m_Mat[i][2] * in_MatrixB.m_Mat[2][j]+
							in_MatrixA.m_Mat[i][3] * in_MatrixB.m_Mat[3][j] ;
		}
	}

	SetFrom4x4Array(tempMat);
	return *this;
};

bool Matrix4x4::IsEquals ( Matrix4x4 const& in_MatrixB ) const
{
	bool isEquals = true;

	for ( int i=0; i<4; i++ )
	{
		for ( int j=0; j<4; j++ )
		{
			if ( m_Mat[i][j] != in_MatrixB.m_Mat[i][j] )
			{
				isEquals = false;
				break;
			}
		}
	}

	return isEquals;
};


bool Matrix4x4::SelfTransposeInverse ()
{
	bool checkExist = Invert ( *this );

	if ( checkExist )
	{
		Transpose ( *this );
	}

	return checkExist;
};


bool Matrix4x4::TransposeInverse ( Matrix4x4 &in_MatrixB )
{
	bool checkExist = Invert ( in_MatrixB );

	if ( checkExist )
	{
		Transpose ( *this );
	}

	return checkExist;
};

void Matrix4x4::SelfTranspose ()
{
	Transpose ( *this );
};

void Matrix4x4::Transpose ( Matrix4x4 const& in_MatrixB )
{
	for ( int i=0; i<4; i++)
	{
		for ( int j=0; j<4; j++)
		{
			m_Mat[i][j] = in_MatrixB.m_Mat[j][i];
		}
	}
};

bool Matrix4x4::SelfInvert ()
{
	return Invert ( *this );
};

bool Matrix4x4::Invert(Matrix4x4 &in_MatrixB)
{
	float	tmp[12];
	float	src[16];
	float	dst[16];
	float	det;

	//Transpose the input matrix.
	for(int i=0; i<4; i++)
	{
		for(int j=0; j<4; j++)
		{
			src[(i*4)+j] = in_MatrixB.m_Mat[j][i];
		}
	}

	//Calcule pairs for the first 8 elements (cofactors).
	tmp[0]  = src[10] * src[15];
	tmp[1]  = src[11] * src[14];
	tmp[2]  = src[9]  * src[15];
	tmp[3]  = src[11] * src[13];
	tmp[4]  = src[9]  * src[14];
	tmp[5]  = src[10] * src[13];
	tmp[6]  = src[8]  * src[15];
	tmp[7]  = src[11] * src[12];
	tmp[8]  = src[8]  * src[14];
	tmp[9]  = src[10] * src[12];
	tmp[10] = src[8]  * src[13];
	tmp[11] = src[9]  * src[12];

	//Calcule first 8 elements (cofactors).
	dst[0] = tmp[0]*src[5] + tmp[3]*src[6] + tmp[4]*src[7];
	dst[0] -= tmp[1]*src[5] + tmp[2]*src[6] + tmp[5]*src[7];
	dst[1] = tmp[1]*src[4] + tmp[6]*src[6] + tmp[9]*src[7];
	dst[1] -= tmp[0]*src[4] + tmp[7]*src[6] + tmp[8]*src[7];
	dst[2] = tmp[2]*src[4] + tmp[7]*src[5] + tmp[10]*src[7];
	dst[2] -= tmp[3]*src[4] + tmp[6]*src[5] + tmp[11]*src[7];
	dst[3] = tmp[5]*src[4] + tmp[8]*src[5] + tmp[11]*src[6];
	dst[3] -= tmp[4]*src[4] + tmp[9]*src[5] + tmp[10]*src[6];
	dst[4] = tmp[1]*src[1] + tmp[2]*src[2] + tmp[5]*src[3];
	dst[4] -= tmp[0]*src[1] + tmp[3]*src[2] + tmp[4]*src[3];
	dst[5] = tmp[0]*src[0] + tmp[7]*src[2] + tmp[8]*src[3];
	dst[5] -= tmp[1]*src[0] + tmp[6]*src[2] + tmp[9]*src[3];
	dst[6] = tmp[3]*src[0] + tmp[6]*src[1] + tmp[11]*src[3];
	dst[6] -= tmp[2]*src[0] + tmp[7]*src[1] + tmp[10]*src[3];
	dst[7] = tmp[4]*src[0] + tmp[9]*src[1] + tmp[10]*src[2];
	dst[7] -= tmp[5]*src[0] + tmp[8]*src[1] + tmp[11]*src[2];

	/* calculate pairs for second 8 elements (cofactors) */
	tmp[0] = src[2]*src[7];
	tmp[1] = src[3]*src[6];
	tmp[2] = src[1]*src[7];
	tmp[3] = src[3]*src[5];
	tmp[4] = src[1]*src[6];
	tmp[5] = src[2]*src[5];
	tmp[6] = src[0]*src[7];
	tmp[7] = src[3]*src[4];
	tmp[8] = src[0]*src[6];
	tmp[9] = src[2]*src[4];
	tmp[10] = src[0]*src[5];
	tmp[11] = src[1]*src[4];

	/* calculate second 8 elements (cofactors) */
	dst[8] = tmp[0]*src[13] + tmp[3]*src[14] + tmp[4]*src[15];
	dst[8] -= tmp[1]*src[13] + tmp[2]*src[14] + tmp[5]*src[15];
	dst[9] = tmp[1]*src[12] + tmp[6]*src[14] + tmp[9]*src[15];
	dst[9] -= tmp[0]*src[12] + tmp[7]*src[14] + tmp[8]*src[15];
	dst[10] = tmp[2]*src[12] + tmp[7]*src[13] + tmp[10]*src[15];
	dst[10]-= tmp[3]*src[12] + tmp[6]*src[13] + tmp[11]*src[15];
	dst[11] = tmp[5]*src[12] + tmp[8]*src[13] + tmp[11]*src[14];
	dst[11]-= tmp[4]*src[12] + tmp[9]*src[13] + tmp[10]*src[14];
	dst[12] = tmp[2]*src[10] + tmp[5]*src[11] + tmp[1]*src[9];
	dst[12]-= tmp[4]*src[11] + tmp[0]*src[9] + tmp[3]*src[10];
	dst[13] = tmp[8]*src[11] + tmp[0]*src[8] + tmp[7]*src[10];
	dst[13]-= tmp[6]*src[10] + tmp[9]*src[11] + tmp[1]*src[8];
	dst[14] = tmp[6]*src[9] + tmp[11]*src[11] + tmp[3]*src[8];
	dst[14]-= tmp[10]*src[11] + tmp[2]*src[8] + tmp[7]*src[9];
	dst[15] = tmp[10]*src[10] + tmp[4]*src[8] + tmp[9]*src[9];
	dst[15]-= tmp[8]*src[9] + tmp[11]*src[10] + tmp[5]*src[8];
	/* calculate determinant */
	det=src[0]*dst[0]+src[1]*dst[1]+src[2]*dst[2]+src[3]*dst[3];
	/* calculate matrix inverse */
	det = 1/det;

	for (int i=0; i<4; i++)
	{
		for (int j=0; j<4; j++)
		{
			m_Mat[i][j] = dst[(i*4)+j] * det;
		}
	}

	return true;
}
/*
bool Matrix4x4::Invert(Matrix4x4 &in_MatrixB)
{
	float invMat[4][4];
	float detM;

	invMat[0][0] =	in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[2][2] * in_MatrixB.m_Mat[3][3] -
					in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[2][3] * in_MatrixB.m_Mat[3][2] -
					in_MatrixB.m_Mat[2][1] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[3][3] +
					in_MatrixB.m_Mat[2][1] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[3][2] +
					in_MatrixB.m_Mat[3][1] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[2][3] -
					in_MatrixB.m_Mat[3][1] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[2][2];

	invMat[1][0] = -in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[2][2] * in_MatrixB.m_Mat[3][3] +
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[2][3] * in_MatrixB.m_Mat[3][2] +
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[3][3] -
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[3][2] -
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[2][3] +
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[2][2];

	invMat[2][0] =  in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[2][1] * in_MatrixB.m_Mat[3][3] -
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[2][3] * in_MatrixB.m_Mat[3][1] -
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[3][3] +
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[3][1] +
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[2][3] -
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[2][1];

	invMat[3][0] = -in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[2][1] * in_MatrixB.m_Mat[3][2] +
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[2][2] * in_MatrixB.m_Mat[3][1] +
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[3][2] -
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[3][1] -
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[2][2] +
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[2][1];

	invMat[0][1] = -in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[2][2] * in_MatrixB.m_Mat[3][3] +
					in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[2][3] * in_MatrixB.m_Mat[3][2] +
					in_MatrixB.m_Mat[2][1] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[3][3] -
					in_MatrixB.m_Mat[2][1] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[3][2] -
					in_MatrixB.m_Mat[3][1] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[2][3] +
					in_MatrixB.m_Mat[3][1] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[2][2];

	invMat[1][1] =  in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[2][2] * in_MatrixB.m_Mat[3][3] -
					in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[2][3] * in_MatrixB.m_Mat[3][2] -
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[3][3] +
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[3][2] +
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[2][3] -
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[2][2];

	invMat[2][1] = -in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[2][1] * in_MatrixB.m_Mat[3][3] +
					in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[2][3] * in_MatrixB.m_Mat[3][1] +
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[3][3] -
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[3][1] -
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[2][3] +
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[2][1];

	invMat[3][1] =  in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[2][1] * in_MatrixB.m_Mat[3][2] -
					in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[2][2] * in_MatrixB.m_Mat[3][1] -
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[3][2] +
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[3][1] +
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[2][2] -
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[2][1];

	invMat[0][2] =  in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[3][3] -
					in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[3][2] -
					in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[3][3] +
					in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[3][2] +
					in_MatrixB.m_Mat[3][1] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[1][3] -
					in_MatrixB.m_Mat[3][1] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[1][2];

	invMat[1][2] = -in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[3][3] +
					in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[3][2] +
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[3][3] -
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[3][2] -
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[1][3] +
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[1][2];

	invMat[2][2] =  in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[3][3] -
					in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[3][1] -
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[3][3] +
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[3][1] +
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[1][3] -
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[1][1];

	invMat[3][2] = -in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[3][2] +
					in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[3][1] +
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[3][2] -
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[3][1] -
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[1][2] +
					in_MatrixB.m_Mat[3][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[1][1];

	invMat[0][3] = -in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[2][3] +
					in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[2][2] +
					in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[2][3] -
					in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[2][2] -
					in_MatrixB.m_Mat[2][1] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[1][3] +
					in_MatrixB.m_Mat[2][1] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[1][2];

	invMat[1][3] =  in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[2][3] -
					in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[2][2] -
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[2][3] +
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[2][2] +
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[1][3] -
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[1][2];

	invMat[2][3] = -in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[2][3] +
					in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][3] * in_MatrixB.m_Mat[2][1] +
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[2][3] -
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[2][1] -
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[1][3] +
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][3] * in_MatrixB.m_Mat[1][1];

	invMat[3][3] =  in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][1] * in_MatrixB.m_Mat[2][2] - 
					in_MatrixB.m_Mat[0][0] * in_MatrixB.m_Mat[1][2] * in_MatrixB.m_Mat[2][1] -
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[2][2] +
					in_MatrixB.m_Mat[1][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[2][1] +
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][1] * in_MatrixB.m_Mat[1][2] -
					in_MatrixB.m_Mat[2][0] * in_MatrixB.m_Mat[0][2] * in_MatrixB.m_Mat[1][1];

	detM = in_MatrixB.m_Mat[0][0] * invMat[0][0] + in_MatrixB.m_Mat[0][1] * invMat[1][0] + in_MatrixB.m_Mat[0][2] * invMat[2][0]  + in_MatrixB.m_Mat[0][3] * invMat[3][0];

	if (detM == 0)
	{
		return false;
	}
	else
	{
		detM = 1.0f / detM;

		for (int i=0; i<4; i++)
		{
			for (int j=0; j<4; j++)
			{
				m_Mat[i][j] = invMat[i][j] * detM;
			}
		}
		return true;
	}
}
*/
/*
bool Matrix4x4::Invert ( Matrix4x4 &in_MatrixB )
{
	float		detM = 0.0f;
	float		invDet;
	float		det[4][4];

	//Calculate the determinant.
	//Loop on the first row of the matrix.
	for ( int j=0; j<4; j++ )
	{
		//Get the local sub matrix 3x3.
		in_MatrixB.GetSubMat ( 0, j );
		//Calculate the sub matrix determinant.
		det[0][j] = in_MatrixB.GetDetFromMatri3x3();
		//Use Co factor rule to calculate the matrix 4x4 determinant.
		detM += in_MatrixB.m_Mat[0][j] * pow(-1.0f,(1+(j+1))) * det[0][j];
	}
	
	//Test if the determinant is not equals to zero.
	if( detM != 0.0f )
	{
		//Calcule all the determinant and adjoint.
		for ( int i=1; i<4; i++ )
		{
			for ( int j=1; j<4; j++ )
			{
				//Get the local sub matrix.
				in_MatrixB.GetSubMat ( i, j );
				//Calculate the sub matrix determinant.
				det[i][j] = in_MatrixB.GetDetFromMatri3x3();
				//Adjoint the determinant.
				det[i][j] = det[i][j] * pow(-1.0f,((i+1)+(j+1)));
			}
		}

		invDet = 1 / detM;

		//Upadte this matrix and Transpose.
		for ( int i=0; i<4; i++ )
		{
			for ( int j=0; j<4; j++ )
			{
				m_Mat[i][j] = det[j][i] * invDet;
			}
		}

		return true;
	}
	else
	{
		return false;
	}
};
*/
void Matrix4x4::GetSubMat ( int in_Row, int in_Col )
{
	int			l,m;

	for ( int r=0; r<4; r++ )
	{
		for ( int c=0; c<4; c++)
		{
			if ( r != in_Row && c != in_Col )
			{
				r > in_Row ? l = r-1 : l = r ;
				c > in_Col ? m = c-1 : m = c ;

				m_SubMat[l][m] = m_Mat[r][c];
			}
		}
	}

};

float Matrix4x4::GetDetFromMatri3x3 () const
{
	float rtnDet;
	float diag0, diag1, diag2;
	float diag3, diag4, diag5;

	diag0 = m_SubMat[0][0] * m_SubMat[1][1] * m_SubMat[2][2];
	diag1 = m_SubMat[0][1] * m_SubMat[1][2] * m_SubMat[2][0];
	diag2 = m_SubMat[0][2] * m_SubMat[1][0] * m_SubMat[2][1];
	diag3 = m_SubMat[0][2] * m_SubMat[1][1] * m_SubMat[2][0];
	diag4 = m_SubMat[0][0] * m_SubMat[1][2] * m_SubMat[2][1];
	diag5 = m_SubMat[0][1] * m_SubMat[1][0] * m_SubMat[2][2];

	rtnDet = diag0 + diag1 + diag2 - diag3 - diag4 - diag5;

	return rtnDet;
};


//GET METHODES.
void Matrix4x4::Get (	float &io_00, float &io_01, float &io_02, float &io_03,
						float &io_10, float &io_11, float &io_12, float &io_13,
						float &io_20, float &io_21, float &io_22, float &io_23,
						float &io_30, float &io_31, float &io_32, float &io_33 ) const
{
	io_00 = m_Mat[0][0];
	io_01 = m_Mat[0][1];
	io_02 = m_Mat[0][2];
	io_03 = m_Mat[0][3];
	io_10 = m_Mat[1][0];
	io_11 = m_Mat[1][1];
	io_12 = m_Mat[1][2];
	io_13 = m_Mat[1][3];
	io_20 = m_Mat[2][0];
	io_21 = m_Mat[2][1];
	io_22 = m_Mat[2][2];
	io_23 = m_Mat[2][3];
	io_30 = m_Mat[3][0];
	io_31 = m_Mat[3][1];
	io_32 = m_Mat[3][2];
	io_33 = m_Mat[3][3];
};

float Matrix4x4::GetValue ( int in_Row, int in_Col ) const
{
	if ( in_Row >=0 && in_Row<4 && in_Col >=0 && in_Col<4 )
	{
		return m_Mat[in_Row][in_Col];
	}
	else
	{
		return 0.0f;
	}
};

void Matrix4x4::GetMatrix3x3 (	float &io_Xx, float &io_Xy, float &io_Xz,
								float &io_Yx, float &io_Yy, float &io_Yz,
								float &io_Zx, float &io_Zy, float &io_Zz ) const
{
	io_Xx = m_Mat[0][0];
	io_Xy = m_Mat[0][1];
	io_Xz = m_Mat[0][2];
	io_Yx = m_Mat[1][0];
	io_Yy = m_Mat[1][1];
	io_Yz = m_Mat[1][2];
	io_Zx = m_Mat[2][0];
	io_Zy = m_Mat[2][1];
	io_Zz = m_Mat[2][2];
};

void Matrix4x4::GetAxeX ( float &io_Xx, float &io_Xy, float &io_Xz ) const
{
	io_Xx = m_Mat[0][0];
	io_Xy = m_Mat[0][1];
	io_Xz = m_Mat[0][2];
};

void Matrix4x4::GetAxeY ( float &io_Yx, float &io_Yy, float &io_Yz ) const
{
	io_Yx = m_Mat[1][0];
	io_Yy = m_Mat[1][1];
	io_Yz = m_Mat[1][2];
}

void Matrix4x4::GetAxeZ ( float &io_Zx, float &io_Zy, float &io_Zz ) const
{
	io_Zx = m_Mat[2][0];
	io_Zy = m_Mat[2][1];
	io_Zz = m_Mat[2][2];
}

void Matrix4x4::GetPosition ( float &io_Px, float &io_Py, float &io_Pz ) const
{
	io_Px = m_Mat[3][0];
	io_Py = m_Mat[3][1];
	io_Pz = m_Mat[3][2];
}

void Matrix4x4::GetScale ( float &io_AxeX, float &io_AxeY, float &io_AxeZ )
{
	io_AxeX = sqrt( (m_Mat[0][0] * m_Mat[0][0]) + (m_Mat[0][1] * m_Mat[0][1]) + (m_Mat[0][2] * m_Mat[0][2]) );
	io_AxeY = sqrt( (m_Mat[1][0] * m_Mat[1][0]) + (m_Mat[1][1] * m_Mat[1][1]) + (m_Mat[1][2] * m_Mat[1][2]) );
	io_AxeZ = sqrt( (m_Mat[2][0] * m_Mat[2][0]) + (m_Mat[2][1] * m_Mat[2][1]) + (m_Mat[2][2] * m_Mat[2][2]) );
}

//SET METHODES.
void Matrix4x4::Set (	float in_00, float in_01, float in_02, float in_03,
						float in_10, float in_11, float in_12, float in_13,
						float in_20, float in_21, float in_22, float in_23,
						float in_30, float in_31, float in_32, float in_33 )
{
	m_Mat[0][0] = in_00;
	m_Mat[0][1] = in_01;
	m_Mat[0][2] = in_02;
	m_Mat[0][3] = in_03;
	m_Mat[1][0] = in_10;
	m_Mat[1][1] = in_11;
	m_Mat[1][2] = in_12;
	m_Mat[1][3] = in_13;
	m_Mat[2][0] = in_20;
	m_Mat[2][1] = in_21;
	m_Mat[2][2] = in_22;
	m_Mat[2][3] = in_23;
	m_Mat[3][0] = in_30;
	m_Mat[3][1] = in_31;
	m_Mat[3][2] = in_32;
	m_Mat[3][3] = in_33;
};

void Matrix4x4::SetValue ( int in_Row, int in_Col, float in_Value )
{
	if ( in_Row >= 0 && in_Row < 4 && in_Col >=0 && in_Col < 4 )
	{
		m_Mat[in_Row][in_Col] = in_Value;
	}
};

void Matrix4x4::SetFrom4x4Array (  float const in_Array[4][4] )
{
	for ( int i=0; i<4; i++ )
	{
		for ( int j=0; j<4; j++ )
		{
			m_Mat[i][j] = in_Array[i][j];
		}
	}
};

void Matrix4x4::SetAxeX ( float const& in_Xx, float const& in_Xy, float const& in_Xz )
{
	m_Mat[0][0] = in_Xx;
	m_Mat[0][1] = in_Xy;
	m_Mat[0][2] = in_Xz;
};

void Matrix4x4::SetAxeY ( float const& in_Yx, float const& in_Yy, float const& in_Yz )
{
	m_Mat[1][0] = in_Yx;
	m_Mat[1][1] = in_Yy;
	m_Mat[1][2] = in_Yz;
};

void Matrix4x4::SetAxeZ ( float const& in_Zx, float const& in_Zy, float const& in_Zz  )
{
	m_Mat[2][0] = in_Zx;
	m_Mat[2][1] = in_Zy;
	m_Mat[2][2] = in_Zz;
};

void Matrix4x4::SetPosition ( float const& in_Px, float const& in_Py, float const& in_Pz )
{
	m_Mat[3][0] = in_Px;
	m_Mat[3][1] = in_Py;
	m_Mat[3][2] = in_Pz;
};

void Matrix4x4::SetFromMatrix3x3 (	float const& in_Xx, float const& in_Xy, float const& in_Xz,
									float const& in_Yx, float const& in_Yy, float const& in_Yz,
									float const& in_Zx, float const& in_Zy, float const& in_Zz )
{
	m_Mat[0][0] = in_Xx;
	m_Mat[0][1] = in_Xy;
	m_Mat[0][2] = in_Xz;

	m_Mat[1][0] = in_Yx;
	m_Mat[1][1] = in_Yy;
	m_Mat[1][2] = in_Yz;

	m_Mat[2][0] = in_Zx;
	m_Mat[2][1] = in_Zy;
	m_Mat[2][2] = in_Zz;	
};

void Matrix4x4::SetScale ( float const& in_AxeX, float const& in_AxeY, float const& in_AxeZ )
{
	//Compute the lenght of each axes.
	float normAxeX, normAxeY, normAxeZ;
	normAxeX = sqrt( (m_Mat[0][0] * m_Mat[0][0]) + (m_Mat[0][1] * m_Mat[0][1]) + (m_Mat[0][2] * m_Mat[0][2]) );
	normAxeY = sqrt( (m_Mat[1][0] * m_Mat[1][0]) + (m_Mat[1][1] * m_Mat[1][1]) + (m_Mat[1][2] * m_Mat[1][2]) );
	normAxeZ = sqrt( (m_Mat[2][0] * m_Mat[2][0]) + (m_Mat[2][1] * m_Mat[2][1]) + (m_Mat[2][2] * m_Mat[2][2]) );

	//Normalize the matrix axes.
	m_Mat[0][0] /= normAxeX;
	m_Mat[0][1] /= normAxeX;
	m_Mat[0][2] /= normAxeX;

	m_Mat[1][0] /= normAxeY;
	m_Mat[1][1] /= normAxeY;
	m_Mat[1][2] /= normAxeY;

	m_Mat[2][0] /= normAxeZ;
	m_Mat[2][1] /= normAxeZ;
	m_Mat[2][2] /= normAxeZ;

	//Scale the axes and update the matrix.
	m_Mat[0][0] *= in_AxeX;
	m_Mat[0][1] *= in_AxeX;
	m_Mat[0][2] *= in_AxeX;

	m_Mat[1][0] *= in_AxeY;
	m_Mat[1][1] *= in_AxeY;
	m_Mat[1][2] *= in_AxeY;

	m_Mat[2][0] *= in_AxeZ;
	m_Mat[2][1] *= in_AxeZ;
	m_Mat[2][2] *= in_AxeZ;
}

void Matrix4x4::AddScale ( float const& in_AxeX, float const& in_AxeY, float const& in_AxeZ )
{
	m_Mat[0][0] *= in_AxeX;
	m_Mat[0][1] *= in_AxeX;
	m_Mat[0][2] *= in_AxeX;

	m_Mat[1][0] *= in_AxeY;
	m_Mat[1][1] *= in_AxeY;
	m_Mat[1][2] *= in_AxeY;

	m_Mat[2][0] *= in_AxeZ;
	m_Mat[2][1] *= in_AxeZ;
	m_Mat[2][2] *= in_AxeZ;
}