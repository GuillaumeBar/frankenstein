/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Matrix4_ToQuat.h
@author Guillaume Baratte
@date 2016-05-28
@brief Extract the quaternion rotation from a 4x4 matrix..
*/

#ifndef _fkn_Matrix4_ToQuat_
#define _fkn_Matrix4_ToQuat_

#include "../fkn_Maya_Tools.h"

class fkn_Matrix4_ToQuat : public MPxNode
{
	public:
		//Constructor.
							fkn_Matrix4_ToQuat();
		//Destructor.
		virtual				~fkn_Matrix4_ToQuat();

		//Maya Methode.
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		static void*		creator();
		static MStatus		initialize();

	public:
		static MTypeId id;

		static MObject m_MatrixA; 	/* The input matrix4x4.*/

		static MObject out_Quat; 	/* The output Quaternion.*/
		static MObject out_QuatX;	/* The output Quaternion.x. */
		static MObject out_QuatY;	/* The output Quaternion.y. */
		static MObject out_QuatZ;	/* The output Quaternion.z. */
		static MObject out_QuatW;	/* The output Quaternion.w. */
};

#endif