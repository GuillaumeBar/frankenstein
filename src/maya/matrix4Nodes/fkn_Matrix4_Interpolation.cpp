/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Matrix4_Interpolation.cpp
@author Guillaume Baratte
@date 2016-05-28
*/

#include "fkn_Matrix4_Interpolation.h"


// Unique plugins ID.
MTypeId fkn_Matrix4_Interpolation::id( fkn_Matrix4_InterpolationID );

MObject	fkn_Matrix4_Interpolation::m_MatrixA;
MObject	fkn_Matrix4_Interpolation::m_MatrixB;
MObject	fkn_Matrix4_Interpolation::m_Blend;

MObject	fkn_Matrix4_Interpolation::out_Matrix;

//Constructor.
fkn_Matrix4_Interpolation::fkn_Matrix4_Interpolation(){

};

//Destructor.
fkn_Matrix4_Interpolation::~fkn_Matrix4_Interpolation(){

};

//Maya Creator.
void* fkn_Matrix4_Interpolation::creator(){

	return new fkn_Matrix4_Interpolation();
};

//Maya Initialize.
MStatus fkn_Matrix4_Interpolation::initialize(){

	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_MatrixA = mAttr.create("MatrixA", "MatrixA");
	mAttr.setStorable(true);
	stat = addAttribute(m_MatrixA);

	m_MatrixB = mAttr.create("MatrixB", "MatrixB");
	mAttr.setStorable(true);
	stat = addAttribute(m_MatrixB);

	m_Blend = nAttr.create("Blend", "Blend", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setSoftMax(1.0f);
	nAttr.setSoftMin(0.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_Blend);

	//Define the output attribut.
	out_Matrix = mAttr.create("Result", "Result");
	mAttr.setWritable(false);
	mAttr.setStorable(false);
	stat = addAttribute( out_Matrix );

	// Define the link between the input and output.
	stat = attributeAffects( m_Blend, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_MatrixA, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_MatrixB, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);


	return MS::kSuccess;
};

MStatus fkn_Matrix4_Interpolation::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MObject	thisNode = thisMObject();

	MDataHandle		matrixAData = in_Data.inputValue(m_MatrixA, &returnStatus);
	MMatrix			matrixA = matrixAData.asMatrix();

	MDataHandle		matrixBData = in_Data.inputValue(m_MatrixB, &returnStatus);
	MMatrix			matrixB = matrixBData.asMatrix();

	MDataHandle		blendData = in_Data.inputValue(m_Blend, &returnStatus);
	float			blend = blendData.asFloat();

	if (returnStatus == MS::kSuccess){
		if ( in_Plug == out_Matrix ){

			MDataHandle	outMatrix = in_Data.outputValue(out_Matrix, &returnStatus);
			
			// Convert Maya Data to Frankenstein.
			Matrix4x4		fknMatrixA, fknMatrixB, fknResultMat;
			for ( int i=0; i<4; i++ ){
				for ( int j=0; j<4; j++){
					fknMatrixA.SetValue(i, j, (float)matrixA(i, j));
					fknMatrixB.SetValue(i, j, (float)matrixB(i, j));
				}
			};

			// Compute the blend.
			BlendMatrix4x4 blendMat(fknMatrixA, fknMatrixB, blend);
			blendMat.ComputeBlend();
			fknResultMat = blendMat.GetResultMatrix();

			// Convert Frankenstein data to Maya.
			MMatrix		rtnMat;
			for ( int i=0; i<4; i++ ){
				for ( int j=0; j<4; j++){
					rtnMat(i, j) = fknResultMat.GetValue(i, j);
				}
			};

			// Update the output data.
			outMatrix.set(rtnMat);
			outMatrix.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Matrix4_Interpolation cannot get value.\n" );
	}

	return returnStatus;
};