/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Matrix4_ToVector3.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Matrix4_ToVector3.h"

// Unique plugins ID.
MTypeId fkn_Matrix4_ToVector3::id( fkn_Matrix_MatrixToVector3_ID );

MObject	fkn_Matrix4_ToVector3::m_MatrixA;

MObject	fkn_Matrix4_ToVector3::out_AxeX;
MObject	fkn_Matrix4_ToVector3::out_AxeXX;
MObject	fkn_Matrix4_ToVector3::out_AxeXY;
MObject	fkn_Matrix4_ToVector3::out_AxeXZ;

MObject	fkn_Matrix4_ToVector3::out_AxeY;
MObject	fkn_Matrix4_ToVector3::out_AxeYX;
MObject	fkn_Matrix4_ToVector3::out_AxeYY;
MObject	fkn_Matrix4_ToVector3::out_AxeYZ;

MObject	fkn_Matrix4_ToVector3::out_AxeZ;
MObject	fkn_Matrix4_ToVector3::out_AxeZX;
MObject	fkn_Matrix4_ToVector3::out_AxeZY;
MObject	fkn_Matrix4_ToVector3::out_AxeZZ;

MObject	fkn_Matrix4_ToVector3::out_Position;
MObject	fkn_Matrix4_ToVector3::out_PositionX;
MObject	fkn_Matrix4_ToVector3::out_PositionY;
MObject	fkn_Matrix4_ToVector3::out_PositionZ;

//Constructor.
fkn_Matrix4_ToVector3::fkn_Matrix4_ToVector3(){

};

//Destructor.
fkn_Matrix4_ToVector3::~fkn_Matrix4_ToVector3(){

};

//Maya Creator.
void* fkn_Matrix4_ToVector3::creator(){

	return new fkn_Matrix4_ToVector3();
};

//Maya Initialize.
MStatus fkn_Matrix4_ToVector3::initialize(){

	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_MatrixA = mAttr.create("MatrixA", "MatA");
	mAttr.setStorable(true);
	stat = addAttribute(m_MatrixA);

	//Define the output attribut.
	out_AxeXX = nAttr.create("AxeXX", "AXX", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_AxeXY = nAttr.create("AxeXY", "AXY", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_AxeXZ = nAttr.create("AxeXZ", "AXZ", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_AxeX = nAttr.create("AxeX", "AX",out_AxeXX, out_AxeXY, out_AxeXZ);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute(out_AxeX);

	out_AxeYX = nAttr.create("AxeYX", "AYX", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_AxeYY = nAttr.create("AxeYY", "AYY", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_AxeYZ = nAttr.create("AxeYZ", "AYZ", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_AxeY = nAttr.create("AxeY", "AY", out_AxeYX, out_AxeYY, out_AxeYZ);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute(out_AxeY);

	out_AxeZX = nAttr.create("AxeZX", "AZX", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_AxeZY = nAttr.create("AxeZY", "AZY", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_AxeZZ = nAttr.create("AxeZZ", "AZZ", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_AxeZ = nAttr.create("AxeZ", "AZ", out_AxeZX, out_AxeZY, out_AxeZZ);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute(out_AxeZ);

	out_PositionX = nAttr.create("PositionX", "PosX", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_PositionY = nAttr.create("PositionY", "PosY", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_PositionZ = nAttr.create("PositionZ", "PosZ", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_Position = nAttr.create("Position", "Pos", out_PositionX, out_PositionY, out_PositionZ);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute(out_Position);

	// Define the link between the input and output.
	stat = attributeAffects( m_MatrixA, out_AxeX);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_MatrixA, out_AxeY);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_MatrixA, out_AxeZ);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_MatrixA, out_Position);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Matrix4_ToVector3::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		matrixAData = in_Data.inputValue(m_MatrixA, &returnStatus);
	MMatrix			matrixA = matrixAData.asMatrix();

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_AxeX || in_Plug == out_AxeY || in_Plug == out_AxeZ || in_Plug == out_Position){

			MDataHandle	outAxeX = in_Data.outputValue(out_AxeX, &returnStatus);
			MDataHandle outAxeY = in_Data.outputValue(out_AxeY, &returnStatus);
			MDataHandle outAxeZ = in_Data.outputValue(out_AxeZ, &returnStatus);
			MDataHandle outPosition = in_Data.outputValue(out_Position, &returnStatus);

			Matrix4x4 fknMat;

			for ( int i=0; i<4; i++){

				for ( int j=0; j<4 ; j++){

					fknMat.SetValue(i, j, (float)matrixA(i, j));
				}
			}

			outAxeX.set(fknMat.GetValue(0,0), fknMat.GetValue(0,1), fknMat.GetValue(0,2));
			outAxeX.setClean();

			outAxeY.set(fknMat.GetValue(1,0), fknMat.GetValue(1,1), fknMat.GetValue(1,2));
			outAxeY.setClean();

			outAxeZ.set(fknMat.GetValue(2,0), fknMat.GetValue(2,1), fknMat.GetValue(2,2));
			outAxeZ.setClean();

			outPosition.set(fknMat.GetValue(3,0), fknMat.GetValue(3,1), fknMat.GetValue(3,2));
			outPosition.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};