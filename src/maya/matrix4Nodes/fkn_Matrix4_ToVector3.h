/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Matrix4_ToVector3.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Matrix4 To Vector3 node for maya.
*/

#ifndef _fkn_Matrix4_ToVector3_
#define _fkn_Matrix4_ToVector3_

#include "../fkn_Maya_Tools.h"

class fkn_Matrix4_ToVector3 : public MPxNode
{
	public:
		//Constructor.
							fkn_Matrix4_ToVector3();
		//Destructor.
		virtual				~fkn_Matrix4_ToVector3();

		//Maya Methode.
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		static void*		creator();
		static MStatus		initialize();

	public:
		static	MTypeId		id;

		static MObject m_MatrixA;

		static MObject out_AxeX;
		static MObject out_AxeXX;
		static MObject out_AxeXY;
		static MObject out_AxeXZ;

		static MObject out_AxeY;
		static MObject out_AxeYX;
		static MObject out_AxeYY;
		static MObject out_AxeYZ;

		static MObject out_AxeZ;
		static MObject out_AxeZX;
		static MObject out_AxeZY;
		static MObject out_AxeZZ;

		static MObject out_Position;
		static MObject out_PositionX;
		static MObject out_PositionY;
		static MObject out_PositionZ;
};

#endif