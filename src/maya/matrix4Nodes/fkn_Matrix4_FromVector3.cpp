/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Matrix4_FromVector3.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Matrix4_FromVector3.h"


// Unique plugins ID.
MTypeId fkn_Matrix4_FromVector3::id( fkn_Matrix_Vector3ToMatrix_ID );

MObject	fkn_Matrix4_FromVector3::m_AxeX;
MObject	fkn_Matrix4_FromVector3::m_AxeXX;
MObject	fkn_Matrix4_FromVector3::m_AxeXY;
MObject	fkn_Matrix4_FromVector3::m_AxeXZ;

MObject	fkn_Matrix4_FromVector3::m_AxeY;
MObject	fkn_Matrix4_FromVector3::m_AxeYX;
MObject	fkn_Matrix4_FromVector3::m_AxeYY;
MObject	fkn_Matrix4_FromVector3::m_AxeYZ;

MObject	fkn_Matrix4_FromVector3::m_AxeZ;
MObject	fkn_Matrix4_FromVector3::m_AxeZX;
MObject	fkn_Matrix4_FromVector3::m_AxeZY;
MObject	fkn_Matrix4_FromVector3::m_AxeZZ;

MObject	fkn_Matrix4_FromVector3::m_Position;
MObject	fkn_Matrix4_FromVector3::m_PositionX;
MObject	fkn_Matrix4_FromVector3::m_PositionY;
MObject	fkn_Matrix4_FromVector3::m_PositionZ;

MObject	fkn_Matrix4_FromVector3::out_Matrix;

//Constructor.
fkn_Matrix4_FromVector3::fkn_Matrix4_FromVector3(){

};

//Destructor.
fkn_Matrix4_FromVector3::~fkn_Matrix4_FromVector3(){

};

//Maya Creator.
void* fkn_Matrix4_FromVector3::creator(){

	return new fkn_Matrix4_FromVector3();
};

//Maya Initialize.
MStatus fkn_Matrix4_FromVector3::initialize(){

	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_AxeXX = nAttr.create("AxeXX", "AXX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_AxeXY = nAttr.create("AxeXY", "AXY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_AxeXZ = nAttr.create("AxeXZ", "AXZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_AxeX = nAttr.create("AxeX", "AX", m_AxeXX, m_AxeXY, m_AxeXZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_AxeX);

	m_AxeYX = nAttr.create("AxeYX", "AYX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_AxeYY = nAttr.create("AxeYY", "AYY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_AxeYZ = nAttr.create("AxeYZ", "AYZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_AxeY = nAttr.create("AxeY", "AY", m_AxeYX, m_AxeYY, m_AxeYZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_AxeY);

	m_AxeZX = nAttr.create("AxeZX", "AZX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_AxeZY = nAttr.create("AxeZY", "AZY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_AxeZZ = nAttr.create("AxeZZ", "AZZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_AxeZ = nAttr.create("AxeZ", "AZ", m_AxeZX, m_AxeZY, m_AxeZZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_AxeZ);

	m_PositionX = nAttr.create("PositionX", "PosX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_PositionY = nAttr.create("PositionY", "PosY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_PositionZ = nAttr.create("PositionZ", "PosZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Position = nAttr.create("Position", "Pos", m_PositionX, m_PositionY, m_PositionZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_Position);

	//Define the output attribut.
	out_Matrix = mAttr.create("Matrix", "Mat");
	mAttr.setWritable(false);
	mAttr.setStorable(false);
	stat = addAttribute(out_Matrix);

	// Define the link between the input and output.
	stat = attributeAffects( m_AxeX, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_AxeY, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_AxeZ, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Position, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Matrix4_FromVector3::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		axeXData = in_Data.inputValue(m_AxeX, &returnStatus);
	MVector			axeX = axeXData.asFloatVector();
	MDataHandle		axeYData = in_Data.inputValue(m_AxeY, &returnStatus);
	MVector			axeY = axeYData.asFloatVector();
	MDataHandle		axeZData = in_Data.inputValue(m_AxeZ, &returnStatus);
	MVector			axeZ = axeZData.asFloatVector();
	MDataHandle		positionData = in_Data.inputValue(m_Position, &returnStatus);
	MVector			position = positionData.asFloatVector();

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Matrix ){

			MDataHandle	outMatrix = in_Data.outputValue(out_Matrix, &returnStatus);

			MMatrix	rtnMat;

			rtnMat(0,0) = axeX.x;
			rtnMat(0,1) = axeX.y;
			rtnMat(0,2) = axeX.z;
			rtnMat(0,3) = 0.0f;
			rtnMat(1,0) = axeY.x;
			rtnMat(1,1) = axeY.y;
			rtnMat(1,2) = axeY.z;
			rtnMat(1,3) = 0.0f;
			rtnMat(2,0) = axeZ.x;
			rtnMat(2,1) = axeZ.y;
			rtnMat(2,2) = axeZ.z;
			rtnMat(2,3) = 0.0f;
			rtnMat(3,0) = position.x;
			rtnMat(3,1) = position.y;
			rtnMat(3,2) = position.z;
			rtnMat(3,3) = 1.0f;

			outMatrix.set(rtnMat);

			outMatrix.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};