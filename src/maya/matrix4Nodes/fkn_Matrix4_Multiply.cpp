/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Matrix4_Multiply.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Matrix4_Multiply.h"


// Unique plugins ID.
MTypeId fkn_Matrix4_Multiply::id( fkn_Matrix_Multiply_ID );

MObject	fkn_Matrix4_Multiply::m_MatrixA;
MObject	fkn_Matrix4_Multiply::m_MatrixB;

MObject	fkn_Matrix4_Multiply::out_Matrix;


//Constructor.
fkn_Matrix4_Multiply::fkn_Matrix4_Multiply(){

};

//Destructor.
fkn_Matrix4_Multiply::~fkn_Matrix4_Multiply(){

};

//Maya Creator.
void* fkn_Matrix4_Multiply::creator(){

	return new fkn_Matrix4_Multiply();
};

//Maya Initialize.
MStatus fkn_Matrix4_Multiply::initialize(){

	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_MatrixA = mAttr.create("MatrixA", "MatA");
	mAttr.setStorable(true);
	stat = addAttribute(m_MatrixA);

	m_MatrixB = mAttr.create("MatrixB", "MatB");
	mAttr.setStorable(true);
	stat = addAttribute(m_MatrixB);

	//Define the output attribut.
	out_Matrix = mAttr.create("Result", "Rlt");
	mAttr.setWritable(false);
	mAttr.setStorable(false);
	stat = addAttribute( out_Matrix );

	// Define the link between the input and output.
	stat = attributeAffects( m_MatrixA, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_MatrixB, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Matrix4_Multiply::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MObject	thisNode = thisMObject();

	MDataHandle		matrixAData = in_Data.inputValue(m_MatrixA, &returnStatus);
	MMatrix			matrixA = matrixAData.asMatrix();

	MDataHandle		matrixBData = in_Data.inputValue(m_MatrixB, &returnStatus);
	MMatrix			matrixB = matrixBData.asMatrix();

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Matrix ){

			MDataHandle	outMatrix = in_Data.outputValue(out_Matrix, &returnStatus);
			Matrix4x4	fknMatrixA, fknMatrixB;
			MMatrix		rtnMat;
			

			for ( int i=0; i<4; i++ ){

				for ( int j=0; j<4; j++){

					fknMatrixA.SetValue(i, j, (float)matrixA(i, j));
					fknMatrixB.SetValue(i, j, (float)matrixB(i, j));
				}
			};

			fknMatrixA.SelfMul(fknMatrixB);

			for ( int i=0; i<4; i++ ){

				for ( int j=0; j<4; j++){

					rtnMat(i, j) = fknMatrixA.GetValue(i, j);
				}
			};

			outMatrix.set(rtnMat);

			outMatrix.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};