/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Matrix4_Interpolation.h
@author Guillaume Baratte
@date 2016-05-28
@brief Create a Matrix4 Interpolation node for maya.
*/

#ifndef _fkn_Matrix4_Interpolation_
#define _fkn_Matrix4_Interpolation_

#include "../fkn_Maya_Tools.h"

class fkn_Matrix4_Interpolation : public MPxNode
{
	public:
		//Constructor.
							fkn_Matrix4_Interpolation();
		//Destructor.
		virtual				~fkn_Matrix4_Interpolation();

		//Maya Methode.
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		static void*		creator();
		static MStatus		initialize();

	public:
		static	MTypeId		id;

		static MObject m_MatrixA;
		static MObject m_MatrixB;
		static MObject m_Blend;
		
		static MObject out_Matrix;
};

#endif