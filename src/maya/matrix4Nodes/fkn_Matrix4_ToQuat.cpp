/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Matrix4_ToQuat.cpp
@author Guillaume Baratte
@date 2016-05-28
*/

#include "fkn_Matrix4_ToQuat.h"

// Unique plugins ID.
MTypeId fkn_Matrix4_ToQuat::id(fkn_Matrix4_ToQuatID);

MObject	fkn_Matrix4_ToQuat::m_MatrixA;

MObject	fkn_Matrix4_ToQuat::out_Quat;
MObject	fkn_Matrix4_ToQuat::out_QuatX;
MObject	fkn_Matrix4_ToQuat::out_QuatY;
MObject	fkn_Matrix4_ToQuat::out_QuatZ;
MObject	fkn_Matrix4_ToQuat::out_QuatW;

//Constructor.
fkn_Matrix4_ToQuat::fkn_Matrix4_ToQuat(){

};

//Destructor.
fkn_Matrix4_ToQuat::~fkn_Matrix4_ToQuat(){

};

//Maya Creator.
void* fkn_Matrix4_ToQuat::creator(){

	return new fkn_Matrix4_ToQuat();
};

//Maya Initialize.
MStatus fkn_Matrix4_ToQuat::initialize(){

	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnCompoundAttribute	cAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_MatrixA = mAttr.create("MatrixA", "MatA");
	mAttr.setStorable(true);
	stat = addAttribute(m_MatrixA);

	//Define the output attribut.
	//Define the output attribut.
	out_QuatX = nAttr.create("QuaternionX", "QuatX", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatY = nAttr.create("QuaternionY", "QuatY", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatZ = nAttr.create("QuaternionZ", "QuatZ", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatW = nAttr.create("QuaternionW", "QuatW", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_Quat = cAttr.create("Quaternion", "Quat");
	cAttr.addChild(out_QuatX);
	cAttr.addChild(out_QuatY);
	cAttr.addChild(out_QuatZ);
	cAttr.addChild(out_QuatW);
	cAttr.setStorable(false);
	cAttr.setWritable(false);
	stat = addAttribute(out_Quat);

	// Define the link between the input and output.
	stat = attributeAffects( m_MatrixA, out_Quat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Matrix4_ToQuat::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		matrixAData = in_Data.inputValue(m_MatrixA, &returnStatus);
	MMatrix			matrixA = matrixAData.asMatrix();

	if (returnStatus == MS::kSuccess){
		if ( in_Plug == out_Quat){
			// Convert Maya data to Frankenstein.
			// Extract the rotation as Matrix3x3.
			Matrix3x3 	ExtractRotation;
			for ( int iCol=0; iCol<3; iCol++){
				for ( int iRow=0; iRow<3 ; iRow++){
					ExtractRotation.SetValue(iCol, iRow, (float)matrixA(iCol, iRow));
				}
			}
			// Conver the matrix 3x3 to Quaternion.
			Quaternion  resultQuat;
			resultQuat.FromMatrix3x3(ExtractRotation);
			// Update the ouput quaternion.
			MObject		thisNode = thisMObject();
			MPlug 		quatPlugOut(thisNode, out_Quat);
			quatPlugOut.child(0, &returnStatus).setValue(resultQuat.GetX());
			quatPlugOut.child(1, &returnStatus).setValue(resultQuat.GetY());
			quatPlugOut.child(2, &returnStatus).setValue(resultQuat.GetZ());
			quatPlugOut.child(3, &returnStatus).setValue(resultQuat.GetW());
			in_Data.setClean(in_Plug);
		}
		else{
			return MS::kUnknownParameter;
		}
	}
	else{
		MGlobal::displayError( "Node fkn_Matrix4_ToQuat cannot get value.\n" );
	}

	return returnStatus;
};