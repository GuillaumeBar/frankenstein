/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Matrix4_Switch.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Matrix4_Switch.h"


// Unique plugins ID.
MTypeId fkn_Matrix4_Switch::id( fkn_Matrix_Switch_ID );

MObject fkn_Matrix4_Switch::m_Switch;
MObject	fkn_Matrix4_Switch::m_MatrixA;
MObject	fkn_Matrix4_Switch::m_MatrixB;

MObject	fkn_Matrix4_Switch::out_Matrix;


//Constructor.
fkn_Matrix4_Switch::fkn_Matrix4_Switch(){

};

//Destructor.
fkn_Matrix4_Switch::~fkn_Matrix4_Switch(){

};

//Maya Creator.
void* fkn_Matrix4_Switch::creator(){

	return new fkn_Matrix4_Switch();
};

//Maya Initialize.
MStatus fkn_Matrix4_Switch::initialize(){

	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_Switch = nAttr.create("Switch", "Swt", MFnNumericData::kBoolean);
	nAttr.setStorable(true);
	stat = addAttribute(m_Switch);

	m_MatrixA = mAttr.create("MatrixA", "MatA");
	mAttr.setStorable(true);
	stat = addAttribute(m_MatrixA);

	m_MatrixB = mAttr.create("MatrixB", "MatB");
	mAttr.setStorable(true);
	stat = addAttribute(m_MatrixB);

	//Define the output attribut.
	out_Matrix = mAttr.create("Result", "Rlt");
	mAttr.setWritable(false);
	mAttr.setStorable(false);
	stat = addAttribute( out_Matrix );

	// Define the link between the input and output.
	stat = attributeAffects( m_Switch, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_MatrixA, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_MatrixB, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Matrix4_Switch::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		switchData = in_Data.inputValue(m_Switch, &returnStatus);
	MDataHandle		matrixAData = in_Data.inputValue(m_MatrixA, &returnStatus);
	MDataHandle		matrixBData = in_Data.inputValue(m_MatrixB, &returnStatus);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Matrix ){

			MDataHandle	outMatrix = in_Data.outputValue(out_Matrix, &returnStatus);
			MMatrix		rtnMat(matrixAData.asMatrix());

			if ( switchData.asBool() ){

				rtnMat = matrixBData.asMatrix();
			}

			outMatrix.set(rtnMat);

			outMatrix.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};