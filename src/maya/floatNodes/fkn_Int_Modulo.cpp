/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Int_Modulo.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Int_Modulo.h"


// Unique plugins ID.
MTypeId fkn_Int_Modulo::id( fkn_Int_Modulo_ID );

MObject	fkn_Int_Modulo::m_IntX;
MObject fkn_Int_Modulo::m_IntBy;

MObject	fkn_Int_Modulo::out_Int;

//Constructor.
fkn_Int_Modulo::fkn_Int_Modulo(){

};

//Destructor.
fkn_Int_Modulo::~fkn_Int_Modulo(){

};

//Maya Creator.
void* fkn_Int_Modulo::creator(){

	return new fkn_Int_Modulo();
};

//Maya Initialize.
MStatus fkn_Int_Modulo::initialize(){

	MFnNumericAttribute		nAttr;
	MFnEnumAttribute		eAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_IntX = nAttr.create("FloatX", "fltX", MFnNumericData::kInt);
	nAttr.setStorable(true);
	stat = addAttribute(m_IntX);

	m_IntBy = nAttr.create("FloatBy", "fltBy", MFnNumericData::kInt);
	nAttr.setStorable(true);
	stat = addAttribute(m_IntBy);

	//Define the output attribut.
	out_Int = nAttr.create("Result", "Rlt", MFnNumericData::kInt);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Int );

	// Define the link between the input and output.
	stat = attributeAffects( m_IntX, out_Int);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_IntBy, out_Int);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Int_Modulo::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		intXData = in_Data.inputValue(m_IntX, &returnStatus);
	MDataHandle		intByData = in_Data.inputValue(m_IntBy, &returnStatus);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Int ){

			MDataHandle	outFloat = in_Data.outputValue(out_Int, &returnStatus);

			int	rtnInt = intXData.asInt() % intByData.asInt();

			outFloat.set(rtnInt);

			outFloat.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};