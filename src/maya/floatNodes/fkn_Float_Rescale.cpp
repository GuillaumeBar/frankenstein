/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Float_Rescale.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Float_Rescale.h"


// Unique plugins ID.
MTypeId fkn_Float_Rescale::id( fkn_Float_Rescale_ID );

MObject	fkn_Float_Rescale::m_FloatX;
MObject fkn_Float_Rescale::m_StartMax;
MObject fkn_Float_Rescale::m_StartMin;
MObject fkn_Float_Rescale::m_EndMax;
MObject fkn_Float_Rescale::m_EndMin;
MObject fkn_Float_Rescale::m_Clamp;

MObject	fkn_Float_Rescale::out_Float;

//Constructor.
fkn_Float_Rescale::fkn_Float_Rescale(){

};

//Destructor.
fkn_Float_Rescale::~fkn_Float_Rescale(){

};

//Maya Creator.
void* fkn_Float_Rescale::creator(){

	return new fkn_Float_Rescale();
};

//Maya Initialize.
MStatus fkn_Float_Rescale::initialize(){

	MFnNumericAttribute		nAttr;
	MFnEnumAttribute		eAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_FloatX = nAttr.create("FloatX", "FloatX", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_FloatX);

	m_StartMax = nAttr.create("StartMax", "StartMax", MFnNumericData::kFloat);
	nAttr.setDefault(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_StartMax);

	m_StartMin = nAttr.create("StartMin", "StartMin", MFnNumericData::kFloat);
	nAttr.setDefault(0.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_StartMin);

	m_EndMax = nAttr.create("EndMax", "EndMax", MFnNumericData::kFloat);
	nAttr.setDefault(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_EndMax);

	m_EndMin = nAttr.create("EndMin", "EndMin", MFnNumericData::kFloat);
	nAttr.setDefault(0.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_EndMin);

	m_Clamp = nAttr.create("Clamp", "Clamp", MFnNumericData::kBoolean);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_Clamp);

	//Define the output attribut.
	out_Float = nAttr.create("Result", "Result", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Float );

	// Define the link between the input and output.
	stat = attributeAffects( m_FloatX, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_StartMax, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_StartMin, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_EndMax, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_EndMin, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Clamp, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Float_Rescale::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		floatXData = in_Data.inputValue(m_FloatX, &returnStatus);
	float			floatX = floatXData.asFloat();
	MDataHandle		startMaxData = in_Data.inputValue(m_StartMax, &returnStatus);
	float			startMax = startMaxData.asFloat();
	MDataHandle		startMinData = in_Data.inputValue(m_StartMin, &returnStatus);
	float			startMin = startMinData.asFloat();
	MDataHandle		endMaxData = in_Data.inputValue(m_EndMax, &returnStatus);
	float			endMax = endMaxData.asFloat();
	MDataHandle		endMinData = in_Data.inputValue(m_EndMin, &returnStatus);
	float			endMin = endMinData.asFloat();
	MDataHandle		clampData = in_Data.inputValue(m_Clamp, &returnStatus);
	bool			clamp = clampData.asBool();

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Float ){

			MDataHandle	outFloat = in_Data.outputValue(out_Float, &returnStatus);

			float startRange = startMax-startMin;

			float	rtnFloat = 0.0f;
				
			if ( startRange != 0.0f ){

				rtnFloat = (floatX-startMin)/startRange*(endMax-endMin)+endMin;

				if ( clamp ){

					if ( endMin < endMax ){

						if ( rtnFloat < endMin ){

							rtnFloat = endMin;
						}
						else if ( rtnFloat > endMax ){

							rtnFloat = endMax;
						}
					}
					else{

						if ( rtnFloat > endMin ){

								rtnFloat = endMin;
						}
						else if ( rtnFloat < endMax ){

							rtnFloat = endMax;
						}
					}
				}
			}

			outFloat.set(rtnFloat);

			outFloat.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};