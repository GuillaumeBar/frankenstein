/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Float_Compare.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Float_Compare.h"


// Unique plugins ID.
MTypeId fkn_Float_Compare::id( fkn_Float_Compare_ID );

MObject	fkn_Float_Compare::m_CompareType;
MObject fkn_Float_Compare::m_Epsilon;

MObject	fkn_Float_Compare::m_FloatA;
MObject	fkn_Float_Compare::m_FloatB;


MObject	fkn_Float_Compare::out_Bool;

//Constructor.
fkn_Float_Compare::fkn_Float_Compare(){

};

//Destructor.
fkn_Float_Compare::~fkn_Float_Compare(){

};

//Maya Creator.
void* fkn_Float_Compare::creator(){

	return new fkn_Float_Compare();
};

//Maya Initialize.
MStatus fkn_Float_Compare::initialize(){

	MFnNumericAttribute		nAttr;
	MFnEnumAttribute		eAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_CompareType = eAttr.create("Type", "Type");
	eAttr.addField("==", 0);
	eAttr.addField(">", 1);
	eAttr.addField(">=", 2);
	eAttr.addField("<", 3);
	eAttr.addField("<=", 4);
	eAttr.addField("!=", 5);
	eAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_CompareType);

	m_Epsilon = nAttr.create("Epsilon", "Epsilon", MFnNumericData::kFloat);
	nAttr.setDefault(0.001f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_Epsilon);

	m_FloatA = nAttr.create("FloatA", "FloatA", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_FloatA);

	m_FloatB = nAttr.create("FloatB", "FloatB", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_FloatB);

	//Define the output attribut.
	out_Bool = nAttr.create("Result", "Result", MFnNumericData::kBoolean);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Bool );

	// Define the link between the input and output.
	stat = attributeAffects( m_CompareType, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Epsilon, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_FloatA, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_FloatB, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Float_Compare::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle	floatAData = in_Data.inputValue(m_FloatA, &returnStatus);
	MDataHandle	floatBData = in_Data.inputValue(m_FloatB, &returnStatus);
	MDataHandle	compareTypeData = in_Data.inputValue(m_CompareType, &returnStatus);
	MDataHandle	epsilonData = in_Data.inputValue(m_Epsilon, &returnStatus);	

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Bool ){

			MDataHandle	outBool = in_Data.outputValue(out_Bool, &returnStatus);

			int			compareType = compareTypeData.asShort();
			float		epsilon = epsilonData.asFloat();
			bool		rtnBool = false;

			//Compute the input vector length.
			float		floatA = floatAData.asFloat();
			float		floatB = floatBData.asFloat();
			//Compute min max length with epsilon.
			float		minFloatA = floatA - epsilon;
			float		maxFloatA = floatA + epsilon;

			if ( compareType == 0 && floatB < maxFloatA && floatB > minFloatA ){

				rtnBool = true;
			}
			else if ( compareType == 1 && floatB > maxFloatA ){

				rtnBool = true;
			}
			else if ( compareType == 2 && floatB >= maxFloatA ){

				rtnBool = true;
			}
			else if ( compareType == 3 && floatB < minFloatA ){

				rtnBool = true;
			}
			else if ( compareType == 4 && floatB <= minFloatA ){

				rtnBool = true;
			}
			else if ( compareType == 5 && (floatB < minFloatA || floatB > maxFloatA) ){

				rtnBool = true;
			}

			outBool.set(rtnBool);

			outBool.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};