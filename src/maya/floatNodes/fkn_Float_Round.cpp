/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Float_Round.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Float_Round.h"


// Unique plugins ID.
MTypeId fkn_Float_Round::id( fkn_Float_Round_ID );

MObject	fkn_Float_Round::m_SelectType;
MObject	fkn_Float_Round::m_FloatX;

MObject	fkn_Float_Round::out_Int;

//Constructor.
fkn_Float_Round::fkn_Float_Round(){

};

//Destructor.
fkn_Float_Round::~fkn_Float_Round(){

};

//Maya Creator.
void* fkn_Float_Round::creator(){

	return new fkn_Float_Round();
};

//Maya Initialize.
MStatus fkn_Float_Round::initialize(){

	MFnNumericAttribute		nAttr;
	MFnEnumAttribute		eAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_SelectType = eAttr.create("SelectType", "selT");
	eAttr.addField("Round", 0);
	eAttr.addField("Floor", 1);
	eAttr.addField("Ceil", 2);
	eAttr.setStorable(true);
	stat = addAttribute(m_SelectType);

	m_FloatX = nAttr.create("FloatX", "fltX", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	stat = addAttribute(m_FloatX);

	//Define the output attribut.
	out_Int = nAttr.create("Result", "Rlt", MFnNumericData::kInt);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Int );

	// Define the link between the input and output.
	stat = attributeAffects( m_SelectType, out_Int);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_FloatX, out_Int);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Float_Round::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		stretchTypeData = in_Data.inputValue(m_SelectType, &returnStatus);
	int				stretchType = stretchTypeData.asShort();
	MDataHandle		floatXData = in_Data.inputValue(m_FloatX, &returnStatus);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Int ){

			MDataHandle	outFloat = in_Data.outputValue(out_Int, &returnStatus);

			int	rtnInt = 0;

			if ( stretchType == 0 ){

				rtnInt = (int)floorf(floatXData.asFloat()+0.5f);
			}
			else if ( stretchType == 1){

				rtnInt =  (int)floorf(floatXData.asFloat());
			}
			else{

				rtnInt = (int)ceilf(floatXData.asFloat());
			}

			outFloat.set(rtnInt);

			outFloat.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};