/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Float_SquareRoot.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Float_SquareRoot.h"


// Unique plugins ID.
MTypeId fkn_Float_SquareRoot::id( fkn_Float_Sqrt_ID );

MObject	fkn_Float_SquareRoot::m_FloatA;

MObject	fkn_Float_SquareRoot::out_Float;

//Constructor.
fkn_Float_SquareRoot::fkn_Float_SquareRoot(){

};

//Destructor.
fkn_Float_SquareRoot::~fkn_Float_SquareRoot(){

};

//Maya Creator.
void* fkn_Float_SquareRoot::creator(){

	return new fkn_Float_SquareRoot();
};

//Maya Initialize.
MStatus fkn_Float_SquareRoot::initialize(){

	MFnNumericAttribute		nAttr;
	MFnEnumAttribute		eAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_FloatA = nAttr.create("FloatA", "fltA", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	stat = addAttribute(m_FloatA);

	//Define the output attribut.
	out_Float = nAttr.create("Result", "Rlt", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Float );

	// Define the link between the input and output.
	stat = attributeAffects( m_FloatA, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Float_SquareRoot::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		floatAData = in_Data.inputValue(m_FloatA, &returnStatus);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Float ){

			MDataHandle	outFloat = in_Data.outputValue(out_Float, &returnStatus);

			float	rtnFloat = sqrtf(floatAData.asFloat());

			outFloat.set(rtnFloat);

			outFloat.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};