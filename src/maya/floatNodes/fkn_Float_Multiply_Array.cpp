/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Float_Multiply_Array.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Float_Multiply_Array.h"


// Unique plugins ID.
MTypeId fkn_Float_Multiply_Array::id( 0x80053 );

MObject	fkn_Float_Multiply_Array::m_aFloat;

MObject	fkn_Float_Multiply_Array::out_Float;

//Constructor.
fkn_Float_Multiply_Array::fkn_Float_Multiply_Array(){

};

//Destructor.
fkn_Float_Multiply_Array::~fkn_Float_Multiply_Array(){

};

//Maya Creator.
void* fkn_Float_Multiply_Array::creator(){

	return new fkn_Float_Multiply_Array();
};

//Maya Initialize.
MStatus fkn_Float_Multiply_Array::initialize(){

	MFnNumericAttribute		nAttr;
	MFnEnumAttribute		eAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_aFloat = nAttr.create("Floats", "Floats", MFnNumericData::kFloat);
	nAttr.setArray(true);
	nAttr.setStorable(true);
	stat = addAttribute(m_aFloat);

	//Define the output attribut.
	out_Float = nAttr.create("Result", "Result", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Float );

	// Define the link between the input and output.
	stat = attributeAffects( m_aFloat, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Float_Multiply_Array::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MArrayDataHandle	afloatsData = in_Data.inputArrayValue(m_aFloat, &returnStatus);
	afloatsData.jumpToElement(0);
	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Float ){

			MDataHandle	outFloat = in_Data.outputValue(out_Float, &returnStatus);
			float result = 0.0f;

			for (unsigned int i=0; i<afloatsData.elementCount(); i++ ){
				result *= afloatsData.inputValue().asFloat();
				afloatsData.next();
			}

			outFloat.set(result);

			outFloat.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};