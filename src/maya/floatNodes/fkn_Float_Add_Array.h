/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Float_AddArray.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Float Add Array node for maya.
*/

#ifndef _fkn_Float_Add_Array_
#define _fkn_Float_Add_Array_

#include "../fkn_Maya_Tools.h"

/*! @brief Maya Class for Add N vector.

*/
class fkn_Float_Add_Array : public MPxNode
{
	public:
		/*! @bief The constructor.*/
		fkn_Float_Add_Array();
		/*! @brief The destructor. */
		virtual				~fkn_Float_Add_Array();

		//Maya Methode.
		/*! @brief The Maya compute function. 
		@param[in] in_Plug The input connection to the node.
		@param[in] in_Data The data block of the node.
		@return Success.
		*/
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		/*! @biref The Maya creator function. */
		static void*		creator();
		/*! @brief The Maya initialize function. */
		static MStatus		initialize();

	public:
		static	MTypeId		id; /*! Contain the maya unique id.*/

		static MObject m_Float; /*! The inpuy float. */
		static MObject m_aFloat; /*! The input Array of float. */

		static MObject out_Float; /*! The ouput result.*/
};

#endif