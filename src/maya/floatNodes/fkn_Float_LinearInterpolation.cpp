/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Float_LinearInterpolation.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Float_LinearInterpolation.h"


// Unique plugins ID.
MTypeId fkn_Float_LinearInterpolation::id( fkn_Float_LinearInterpolation_ID );

MObject	fkn_Float_LinearInterpolation::m_FloatA;
MObject fkn_Float_LinearInterpolation::m_FloatB;
MObject fkn_Float_LinearInterpolation::m_Blend;

MObject	fkn_Float_LinearInterpolation::out_Float;

//Constructor.
fkn_Float_LinearInterpolation::fkn_Float_LinearInterpolation(){

};

//Destructor.
fkn_Float_LinearInterpolation::~fkn_Float_LinearInterpolation(){

};

//Maya Creator.
void* fkn_Float_LinearInterpolation::creator(){

	return new fkn_Float_LinearInterpolation();
};

//Maya Initialize.
MStatus fkn_Float_LinearInterpolation::initialize(){

	MFnNumericAttribute		nAttr;
	MFnEnumAttribute		eAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_FloatA = nAttr.create("FloatA", "FloatA", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_FloatA);

	m_FloatB = nAttr.create("FloatB", "FloatB", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_FloatB);

	m_Blend = nAttr.create("Blend", "Blend", MFnNumericData::kFloat);
	nAttr.setMax(1.0f);
	nAttr.setMin(0.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_Blend);

	//Define the output attribut.
	out_Float = nAttr.create("Result", "Result", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Float );

	// Define the link between the input and output.
	stat = attributeAffects( m_FloatA, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_FloatB, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Blend, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Float_LinearInterpolation::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		floatAData = in_Data.inputValue(m_FloatA, &returnStatus);
	float			floatA = floatAData.asFloat();
	MDataHandle		floatBData = in_Data.inputValue(m_FloatB, &returnStatus);
	float			floatB = floatBData.asFloat();
	MDataHandle		blendData = in_Data.inputValue(m_Blend, &returnStatus);
	float			blend = blendData.asFloat();

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Float ){

			MDataHandle	outFloat = in_Data.outputValue(out_Float, &returnStatus);

			float	rtnFloat = 0.0f;

			if ( blend < 0.0f ){

				blend = 0.0f;
			}
			else if ( blend > 1.0f ){

				blend = 1.0f;
			}

			rtnFloat = floatA * ( 1.0f - blend ) + floatB * blend;

			outFloat.set(rtnFloat);

			outFloat.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};