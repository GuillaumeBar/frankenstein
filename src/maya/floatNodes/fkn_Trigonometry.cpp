/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Trigonometry.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Trigonometry.h"


// Unique plugins ID.
MTypeId fkn_Trigonometry::id( 0x80016 );

MObject	fkn_Trigonometry::m_SelectType;
MObject	fkn_Trigonometry::m_FloatX;
MObject fkn_Trigonometry::m_FloatY;

MObject	fkn_Trigonometry::out_Float;


//Constructor.
fkn_Trigonometry::fkn_Trigonometry(){

};

//Destructor.
fkn_Trigonometry::~fkn_Trigonometry(){

};

//Maya Creator.
void* fkn_Trigonometry::creator(){

	return new fkn_Trigonometry();
};

//Maya Initialize.
MStatus fkn_Trigonometry::initialize(){

	MFnNumericAttribute		nAttr;
	MFnEnumAttribute		eAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_SelectType = eAttr.create("SelectType", "selT");
	eAttr.addField("ArcCos", 0);
	eAttr.addField("ArcSin", 1);
	eAttr.addField("ArcTan", 2);
	eAttr.addField("Cos", 3);
	eAttr.addField("Sin", 4);
	eAttr.addField("Tan", 5);
	eAttr.addField("ArcTan2", 6);
	eAttr.setStorable(true);
	stat = addAttribute(m_SelectType);

	m_FloatX = nAttr.create("FloatX", "fltX", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	stat = addAttribute(m_FloatX);

	m_FloatY = nAttr.create("FloatY", "fltY", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	stat = addAttribute(m_FloatY);

	//Define the output attribut.
	out_Float = nAttr.create("Result", "Rlt", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Float );

	// Define the link between the input and output.
	stat = attributeAffects( m_SelectType, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_FloatX, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_FloatY, out_Float);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Trigonometry::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		selectTypeData = in_Data.inputValue(m_SelectType, &returnStatus);
	MDataHandle		floatXData = in_Data.inputValue(m_FloatX, &returnStatus);
	MDataHandle		floatYData = in_Data.inputValue(m_FloatY, &returnStatus);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Float ){

			MDataHandle	outVector3 = in_Data.outputValue(out_Float, &returnStatus);
			float	inFloatX = floatXData.asFloat();
			float	inFloatY = floatYData.asFloat();
			int	selectType = selectTypeData.asShort();
			float	rtnFloat = 0.0f;

			if ( selectType == 0 ){

				rtnFloat = acosf(inFloatX);
			}
			else if ( selectType == 1){

				rtnFloat = asinf(inFloatX);
			}
			else if ( selectType == 2){

				rtnFloat = atanf(inFloatX);
			}
			else if ( selectType == 3){

				rtnFloat = cosf(inFloatX);
			}
			else if ( selectType == 4){

				rtnFloat = sinf(inFloatX);
			}
			else if ( selectType == 5){

				rtnFloat = tanf(inFloatX);
			}
			else if ( selectType == 6){

				rtnFloat = atan2f(inFloatX, inFloatY);
			}

			outVector3.set(rtnFloat);

			outVector3.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};