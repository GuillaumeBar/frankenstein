/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Maya_Tools.h
@author Guillaume Baratte
@date 2016-03-27
@brief Define the include for maya plugin.
*/

#ifndef _fkn_Maya_ToolsNode_
#define _fkn_Maya_ToolsNode_

// Add Maya Headers.
#include <maya/MPxNode.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MMatrix.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MArrayDataBuilder.h>
#include <maya/MGlobal.h>
#include <maya/MPoint.h>
#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MRampAttribute.h>
#include <maya/MStatus.h>
#include <maya/MEulerRotation.h>
#include <maya/MQuaternion.h>
#include <maya/MAngle.h>
#include <maya/MFnCompoundAttribute.h>
#include <math.h>
#include <maya/MFnNurbsSurface.h>
#include <maya/MFnMesh.h>
#include <maya/MTypes.h>

// Add Frankenstein Headers.
#include "../core/BezierCurveNPoints.h"
#include "../core/DataArray.h"
#include "../core/Matrix3x3.h"
#include "../core/Matrix4x4.h"
#include "../core/Quaternion.h"
#include "../core/Vector2D.h"
#include "../core/Vector3D.h"
#include "../rig/SplineMember.h"
#include "../rig/SplineMember2.h"
#include "../rig/Ik3BonesMember.h"
#include "../rig/Ik2BonesMember.h"
#include "../rig/SRT_Constraint.h"
#include "../rig/BlendMatrix4x4.h"
#include "../rig/BoneRollsConstraint.h"
#include "../rig/CompressionCompensator.h"

// Add SIMD headers.
#include <xmmintrin.h>
//#include <immintrin.h>
#include <iostream>

//Maya node ID.
static MTypeId fkn_SplineMember2Node_ID(0x80123);
//static MTypeId fkn_surfaceConstraintNode_ID(0x80123);
static MTypeId fkn_surfaceConstraintNode_ID(0x80122);
static MTypeId fkn_VertexConstraintNode_ID(0x80121);
static MTypeId fkn_Bool_Invert_ID(0x80120);
static MTypeId fkn_Vector3_SelectCase_ID(0x80119);
static MTypeId fkn_Float_SelectCase_ID(0x80118);
static MTypeId fkn_Bool_SelectCase_ID(0x80117);
static MTypeId fkn_Bool_Or_Array_ID(0x80116);
static MTypeId fkn_Bool_And_Array_ID(0x80115);
static MTypeId fkn_SpaceSwitchDynamicCnsID(0x80114);
static MTypeId fkn_SpaceSwitchCnsID(0x80113);
static MTypeId fkn_CompressionCompensatorID(0x80112);
static MTypeId fkn_BoneRollsConstraintID(0x80111);
static MTypeId fkn_Ik2BonesMemberNodeID(0x80110);
static MTypeId fkn_Matrix4_InterpolationID(0x80109);
static MTypeId fkn_Matrix4_ToQuatID(0x80108);
static MTypeId fkn_ConvertToLocalSpace_ID(0x80107);
static MTypeId fkn_BlendRotCns_ID(0x80105);
static MTypeId fkn_ChainOnNurbsSurfaceCns_ID(0x80103);
static MTypeId fkn_SRT_Constraint_ID(0x80102);
static MTypeId fkn_Float_Add_ID(0x80017);
static MTypeId fkn_Float_AddArray_ID(0x80052);
static MTypeId fkn_Float_Multiply_ID(0x80019);
static MTypeId fkn_Float_MultiplyArray_ID(0x80053);
static MTypeId fkn_Float_Divide_ID(0x80020);
static MTypeId fkn_Float_Sub_ID(0x80018);
static MTypeId fkn_Float_LinearInterpolation_ID(0x80033);
static MTypeId fkn_Float_Sqrt_ID(0x80021);
static MTypeId fkn_Float_Pow_ID(0x80022);
static MTypeId fkn_Float_Log_ID(0x80024);
static MTypeId fkn_Float_Exp_ID(0x80025);
static MTypeId fkn_Trigonometry_ID(0x80016);
static MTypeId fkn_Float_Round_ID(0x80026);
static MTypeId fkn_Float_Negate_ID(0x80027);
static MTypeId fkn_Float_Clamp_ID(0x80029);
static MTypeId fkn_Float_SoftClamp_ID();
static MTypeId fkn_Float_Rescale_ID(0x80030);
static MTypeId fkn_Float_Abs_ID(0x80028);
static MTypeId fkn_Float_Compare_ID(0x80031);
static MTypeId fkn_Float_Switch_ID(0x80032);
static MTypeId fkn_Float_FilterByRamp_ID(0x80017);
static MTypeId fkn_Int_Modulo_ID(0x80023);
static MTypeId fkn_Matrix_Multiply_ID(0x80034);
static MTypeId fkn_Matrix_Invert_ID(0x80035);
static MTypeId fkn_Matrix_Transpose_ID(0x80036);
static MTypeId fkn_Matrix_TransposeInverse_ID(0x80037);
static MTypeId fkn_Matrix_Switch_ID(0x80040);
static MTypeId fkn_Matrix_Vector3ToMatrix_ID(0x80039);
static MTypeId fkn_Matrix_MatrixToVector3_ID(0x80038);
static MTypeId fkn_Vector3_Add_ID(0x80005);
static MTypeId fkn_Vector3_Sub_ID(0x80006);
static MTypeId fkn_Vector3_MultiplyByFloat_ID(0x80007);
static MTypeId fkn_Vector3_MultiplyByMatrix_ID(0x80013);
static MTypeId fkn_Vector3_RotateByQuaternion_ID(0x80012);
static MTypeId fkn_Vector3_Normalize_ID(0x80011);
static MTypeId fkn_Vector3_Length_ID(0x80008);
static MTypeId fkn_Vector3_Dot_ID(0x80010);
static MTypeId fkn_Vector3_Cross_ID(0x80009);
static MTypeId fkn_Vector3_Compare_ID(0x80014);
static MTypeId fkn_Vector3_Switch_ID(0x80015);
static MTypeId fkn_Quaternion_Add_ID(0x80041);
static MTypeId fkn_Quaternion_AddArray_ID();
static MTypeId fkn_Quaternion_Multiply_ID(0x80042);
static MTypeId fkn_Quaternion_ToEuler_ID(0x80051);
static MTypeId fkn_Quaternion_AxisAndAngle_ID(0x80050);
static MTypeId fkn_Quaternion_Slerp_ID(0x80049);
static MTypeId fkn_Quaternion_Conjugate_ID(0x80044);
static MTypeId fkn_Quaternion_Invert_ID(0x80045);
static MTypeId fkn_Quaternion_Normalize_ID(0x80046);
static MTypeId fkn_Quaternion_Sub_ID(0x80043);
static MTypeId fkn_Quaternion_Product_ID(0x80048);
static MTypeId fkn_Quaternion_Negate_ID(0x80047);



#endif
