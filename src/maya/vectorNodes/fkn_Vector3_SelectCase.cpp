/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Vector3_SelectCase.cpp
@author Guillaume Baratte
@date 2016-10-31
*/

#include "fkn_Vector3_SelectCase.h"

// Unique plugins ID.
MTypeId fkn_Vector3_SelectCase::id( fkn_Vector3_SelectCase_ID );

MObject	fkn_Vector3_SelectCase::m_switchValue;

MObject	fkn_Vector3_SelectCase::m_defaultVectorX;
MObject	fkn_Vector3_SelectCase::m_defaultVectorY;
MObject	fkn_Vector3_SelectCase::m_defaultVectorZ;
MObject	fkn_Vector3_SelectCase::m_defaultVector;

MObject	fkn_Vector3_SelectCase::m_arrayVectorX;
MObject	fkn_Vector3_SelectCase::m_arrayVectorY;
MObject	fkn_Vector3_SelectCase::m_arrayVectorZ;
MObject	fkn_Vector3_SelectCase::m_arrayVector;

MObject	fkn_Vector3_SelectCase::out_VectorX;
MObject	fkn_Vector3_SelectCase::out_VectorY;
MObject	fkn_Vector3_SelectCase::out_VectorZ;
MObject	fkn_Vector3_SelectCase::out_Vector;

//Constructor.
fkn_Vector3_SelectCase::fkn_Vector3_SelectCase(){

};

//Destructor.
fkn_Vector3_SelectCase::~fkn_Vector3_SelectCase(){

};

//Maya Creator.
void* fkn_Vector3_SelectCase::creator(){

	return new fkn_Vector3_SelectCase();
};

//Maya Initialize.
MStatus fkn_Vector3_SelectCase::initialize(){

	MFnNumericAttribute		nAttr;

	MStatus					stat;

	// Define the input attribute.
	m_switchValue = nAttr.create("switchValue", "switchValue",
									MFnNumericData::kInt);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_switchValue);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_defaultVectorX = nAttr.create("defaultVectorX", "defaultVectorX",
										MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_defaultVectorY = nAttr.create("defaultVectorY", "defaultVectorY",
										MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_defaultVectorZ = nAttr.create("defaultVectorZ", "defaultVectorZ",
										MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_defaultVector = nAttr.create("defaultVector", "defaultVector",
										m_defaultVectorX,
										m_defaultVectorY,
										m_defaultVectorZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_defaultVector);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_arrayVectorX = nAttr.create("arrayVectorX", "arrayVectorX",
										MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_arrayVectorY = nAttr.create("arrayVectorY", "arrayVectorY",
										MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_arrayVectorZ = nAttr.create("arrayVectorZ", "arrayVectorZ",
										MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_arrayVector = nAttr.create("arrayVector", "arrayVector",
										m_arrayVectorX,
										m_arrayVectorY,
										m_arrayVectorZ);
	nAttr.setStorable(true);
	nAttr.setArray(true);
	stat = addAttribute(m_arrayVector);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attributs.
	out_VectorX = nAttr.create("VectorX", "VectorX", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_VectorY = nAttr.create("VectorY", "VectorY", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_VectorZ = nAttr.create("VectorZ", "VectorZ", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_Vector = nAttr.create("Result", "Rlt", out_VectorX, out_VectorY, out_VectorZ);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Vector );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_switchValue, out_Vector);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_defaultVector, out_Vector);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_arrayVector, out_Vector);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Vector3_SelectCase::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MArrayDataHandle	aVectorData = in_Data.inputArrayValue(m_arrayVector, &returnStatus);

	MDataHandle switchValueData = in_Data.inputValue(m_switchValue, &returnStatus);
	int switchValue = switchValueData.asShort();

	MDataHandle defautlVectorData = in_Data.inputValue(m_defaultVector, &returnStatus);
	MFloatVector defautlVector = defautlVectorData.asFloatVector();

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Vector ){

			MDataHandle	outVector = in_Data.outputValue(out_Vector, &returnStatus);
			MFloatVector result = defautlVector;

			unsigned int arrayCount = aVectorData.elementCount();

			if ( switchValue >= 0 && switchValue < arrayCount){
				aVectorData.jumpToElement(switchValue);
				result = aVectorData.inputValue(&returnStatus).asFloatVector();
			}

			outVector.set(result.x, result.y, result.z);

			outVector.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_SelectCase cannot get value.\n" );
	}

	return returnStatus;
};