/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Vector3_MultiplyByFloat.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Vector3_MultiplyByFloat.h"


// Unique plugins ID.
MTypeId     fkn_Vector3_MultiplyByFloat::id( fkn_Vector3_MultiplyByFloat_ID );

MObject	fkn_Vector3_MultiplyByFloat::m_Vector3A;
MObject	fkn_Vector3_MultiplyByFloat::m_Vector3AX;
MObject	fkn_Vector3_MultiplyByFloat::m_Vector3AY;
MObject	fkn_Vector3_MultiplyByFloat::m_Vector3AZ;

MObject		fkn_Vector3_MultiplyByFloat::m_Float;

MObject		fkn_Vector3_MultiplyByFloat::out_Vector3;

//Constructor.
fkn_Vector3_MultiplyByFloat::fkn_Vector3_MultiplyByFloat(){

};

//Destructor.
fkn_Vector3_MultiplyByFloat::~fkn_Vector3_MultiplyByFloat(){

};

//Maya Creator.
void* fkn_Vector3_MultiplyByFloat::creator(){

	return new fkn_Vector3_MultiplyByFloat();
};

//Maya Initialize.
MStatus fkn_Vector3_MultiplyByFloat::initialize(){

	MFnNumericAttribute		nAttr;
	
	MStatus					stat;

	// Define and add the input attribute to he node.
	m_Vector3AX = nAttr.create("VectorAX", "VecAX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3AY = nAttr.create("VectorAY", "VecAY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3AZ = nAttr.create("VectorAZ", "VecAZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3A = nAttr.create("VectorA", "VecA", m_Vector3AX, m_Vector3AY, m_Vector3AZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_Vector3A);

	m_Float = nAttr.create("Float", "Flt", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	stat = addAttribute(m_Float);

	//Define the output attribut.

	out_Vector3 = nAttr.create("Result", "Rlt", MFnNumericData::k3Float);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Vector3 );


	// Define the link between the input and output.
	stat = attributeAffects( m_Vector3A, out_Vector3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Float, out_Vector3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Vector3_MultiplyByFloat::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		vector3AData = in_Data.inputValue(m_Vector3A, &returnStatus);

	MDataHandle		floatData = in_Data.inputValue(m_Float, &returnStatus);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Vector3 ){

			MDataHandle	outVector3 = in_Data.outputValue(out_Vector3, &returnStatus);
			
			Vector3D	fknVectorA(vector3AData.asFloatVector().x, vector3AData.asFloatVector().y, vector3AData.asFloatVector().z);

			fknVectorA.SelfMulByFloat(floatData.asFloat());

			outVector3.set(fknVectorA.GetX(), fknVectorA.GetY(), fknVectorA.GetZ());

			outVector3.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};