/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Vector3_Compare.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Vector3_Compare.h"


// Unique plugins ID.
MTypeId fkn_Vector3_Compare::id( fkn_Vector3_Compare_ID );

MObject	fkn_Vector3_Compare::m_CompareType;
MObject fkn_Vector3_Compare::m_Epsilon;

MObject	fkn_Vector3_Compare::m_Vector3A;
MObject	fkn_Vector3_Compare::m_Vector3AX;
MObject	fkn_Vector3_Compare::m_Vector3AY;
MObject	fkn_Vector3_Compare::m_Vector3AZ;

MObject	fkn_Vector3_Compare::m_Vector3B;
MObject	fkn_Vector3_Compare::m_Vector3BX;
MObject	fkn_Vector3_Compare::m_Vector3BY;
MObject	fkn_Vector3_Compare::m_Vector3BZ;

MObject	fkn_Vector3_Compare::out_Bool;

//Constructor.
fkn_Vector3_Compare::fkn_Vector3_Compare(){

};

//Destructor.
fkn_Vector3_Compare::~fkn_Vector3_Compare(){

};

//Maya Creator.
void* fkn_Vector3_Compare::creator(){

	return new fkn_Vector3_Compare();
};

//Maya Initialize.
MStatus fkn_Vector3_Compare::initialize(){

	MFnNumericAttribute		nAttr;
	MFnEnumAttribute		eAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_CompareType = eAttr.create("Type", "Type");
	eAttr.addField("==", 0);
	eAttr.addField(">", 1);
	eAttr.addField(">=", 2);
	eAttr.addField("<", 3);
	eAttr.addField("<=", 4);
	eAttr.addField("!=", 5);
	eAttr.setStorable(true);
	stat = addAttribute(m_CompareType);

	m_Epsilon = nAttr.create("Epsilon", "epsi", MFnNumericData::kFloat);
	nAttr.setDefault(0.001f);
	nAttr.setStorable(true);
	stat = addAttribute(m_Epsilon);

	m_Vector3AX = nAttr.create("VectorAX", "VecAX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3AY = nAttr.create("VectorAY", "VecAY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3AZ = nAttr.create("VectorAZ", "VecAZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3A = nAttr.create("VectorA", "VecA", m_Vector3AX, m_Vector3AY, m_Vector3AZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_Vector3A);

	m_Vector3BX = nAttr.create("VectorBX", "VecBX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3BY = nAttr.create("VectorBY", "VecBY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3BZ = nAttr.create("VectorBZ", "VecBZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3B = nAttr.create("VectorB", "VecB", m_Vector3BX, m_Vector3BY, m_Vector3BZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_Vector3B);

	//Define the output attribut.
	out_Bool = nAttr.create("Result", "Rlt", MFnNumericData::kBoolean);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Bool );

	// Define the link between the input and output.
	stat = attributeAffects( m_CompareType, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Epsilon, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Vector3A, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Vector3B, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Vector3_Compare::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle	vector3AData = in_Data.inputValue(m_Vector3A, &returnStatus);
	MDataHandle	vector3BData = in_Data.inputValue(m_Vector3B, &returnStatus);
	MDataHandle	compareTypeData = in_Data.inputValue(m_CompareType, &returnStatus);
	MDataHandle	epsilonData = in_Data.inputValue(m_Epsilon, &returnStatus);	

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Bool ){

			MDataHandle	outBool = in_Data.outputValue(out_Bool, &returnStatus);
			
			Vector3D	fknVectorA(vector3AData.asFloatVector().x, vector3AData.asFloatVector().y, vector3AData.asFloatVector().z);
			Vector3D	fknVectorB(vector3BData.asFloatVector().x, vector3BData.asFloatVector().y, vector3BData.asFloatVector().z);
			int			compareType = compareTypeData.asShort();
			float		epsilon = epsilonData.asFloat();
			bool		rtnBool = false;

			//Compute the input vector length.
			float		vectorAL = fknVectorA.GetLength();
			float		vectorBL = fknVectorB.GetLength();
			//Compute min max length with epsilon.
			float		minVectorAL = vectorAL - epsilon;
			float		maxVectorAL = vectorAL + epsilon;

			if ( compareType == 0 && vectorBL < maxVectorAL && vectorBL > minVectorAL ){

				rtnBool = true;
			}
			else if ( compareType == 1 && vectorBL > maxVectorAL ){

				rtnBool = true;
			}
			else if ( compareType == 2 && vectorBL >= maxVectorAL ){

				rtnBool = true;
			}
			else if ( compareType == 3 && vectorBL < minVectorAL ){

				rtnBool = true;
			}
			else if ( compareType == 4 && vectorBL <= minVectorAL ){

				rtnBool = true;
			}
			else if ( compareType == 5 && (vectorBL < minVectorAL || vectorBL > maxVectorAL) ){

				rtnBool = true;
			}

			outBool.set(rtnBool);

			outBool.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};