/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Vector3_RotateByQuaternion.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Vector3 Rotate By Quaternion node for maya.
*/

#ifndef _fkn_Vector3_RotateByQuaternion_
#define _fkn_Vector3_RotateByQuaternion_

#include "../fkn_Maya_Tools.h"

class fkn_Vector3_RotateByQuaternion : public MPxNode
{
	public:
		//Constructor.
							fkn_Vector3_RotateByQuaternion();
		//Destructor.
		virtual				~fkn_Vector3_RotateByQuaternion();

		//Maya Methode.
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		static void*		creator();
		static MStatus		initialize();

	public:
		static	MTypeId		id;

		static MObject m_Vector3A;
		static MObject m_Vector3AX;
		static MObject m_Vector3AY;
		static MObject m_Vector3AZ;

		static MObject m_Quat;
		static MObject m_QuatX;
		static MObject m_QuatY;
		static MObject m_QuatZ;
		static MObject m_QuatW;

		static MObject out_Vector3;
		static MObject out_Vector3X;
		static MObject out_Vector3Y;
		static MObject out_Vector3Z;
};

#endif