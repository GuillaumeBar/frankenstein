/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Vector3_SelectCase.h
@author Guillaume Baratte
@date 2016-10-31
@brief Create a Boolean And Array node for maya.
*/

#ifndef _fkn_Vector3_SelectCase_
#define _fkn_Vector3_SelectCase_

#include "../fkn_Maya_Tools.h"


class fkn_Vector3_SelectCase : public MPxNode
{
	public:
		/*! @bief The constructor.*/
		fkn_Vector3_SelectCase();
		/*! @brief The destructor. */
		virtual	~fkn_Vector3_SelectCase();

		//Maya Methode.
		/*! @brief The Maya compute function. 
		@param[in] in_Plug The input connection to the node.
		@param[in] in_Data The data block of the node.
		@return Success.
		*/
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		/*! @biref The Maya creator function. */
		static void*		creator();
		/*! @brief The Maya initialize function. */
		static MStatus		initialize();

	public:
		static MTypeId id;

		static MObject m_switchValue;
		static MObject m_defaultVectorX;
		static MObject m_defaultVectorY;
		static MObject m_defaultVectorZ;
		static MObject m_defaultVector;

		static MObject m_arrayVectorX;
		static MObject m_arrayVectorY;
		static MObject m_arrayVectorZ;
		static MObject m_arrayVector;


		static MObject out_VectorX;
		static MObject out_VectorY;
		static MObject out_VectorZ;
		static MObject out_Vector;
};

#endif