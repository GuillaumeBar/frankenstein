/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Vector3_Sub.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Vector3_Sub.h"

// Unique plugins ID.
MTypeId     fkn_Vector3_Sub::id( fkn_Vector3_Sub_ID );

MObject	fkn_Vector3_Sub::m_Vector3A;
MObject	fkn_Vector3_Sub::m_Vector3AX;
MObject	fkn_Vector3_Sub::m_Vector3AY;
MObject	fkn_Vector3_Sub::m_Vector3AZ;

MObject	fkn_Vector3_Sub::m_Vector3B;
MObject	fkn_Vector3_Sub::m_Vector3BX;
MObject	fkn_Vector3_Sub::m_Vector3BY;
MObject	fkn_Vector3_Sub::m_Vector3BZ;

MObject	fkn_Vector3_Sub::out_Vector3;
MObject	fkn_Vector3_Sub::out_Vector3X;
MObject	fkn_Vector3_Sub::out_Vector3Y;
MObject	fkn_Vector3_Sub::out_Vector3Z;

//Constructor.
fkn_Vector3_Sub::fkn_Vector3_Sub(){

};

//Destructor.
fkn_Vector3_Sub::~fkn_Vector3_Sub(){

};

//Maya Creator.
void* fkn_Vector3_Sub::creator(){

	return new fkn_Vector3_Sub();
};

//Maya Initialize.
MStatus fkn_Vector3_Sub::initialize(){

	MFnNumericAttribute		nAttr;
	
	MStatus					stat;

	// Define and add the input attribute to he node.
	m_Vector3AX = nAttr.create("VectorAX", "VecAX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3AY = nAttr.create("VectorAY", "VecAY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3AZ = nAttr.create("VectorAZ", "VecAZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3A = nAttr.create("VectorA", "VecA", m_Vector3AX, m_Vector3AY, m_Vector3AZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_Vector3A);

	m_Vector3BX = nAttr.create("VectorBX", "VecBX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3BY = nAttr.create("VectorBY", "VecBY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3BZ = nAttr.create("VectorBZ", "VecBZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3B = nAttr.create("VectorB", "VecB", m_Vector3BX, m_Vector3BY, m_Vector3BZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_Vector3B);

	//Define the output attribut.

	out_Vector3X = nAttr.create("ResultX", "rltX", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_Vector3Y = nAttr.create("ResultY", "rltY", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_Vector3Z = nAttr.create("ResultZ", "rltZ", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_Vector3 = nAttr.create("Result", "Rlt", out_Vector3X, out_Vector3Y, out_Vector3Z);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Vector3 );


	// Define the link between the input and output.
	stat = attributeAffects( m_Vector3A, out_Vector3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Vector3B, out_Vector3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Vector3_Sub::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		vector3AData = in_Data.inputValue(m_Vector3A, &returnStatus);

	MDataHandle		vector3BData = in_Data.inputValue(m_Vector3B, &returnStatus);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Vector3 ){

			MDataHandle	outVector3 = in_Data.outputValue(out_Vector3, &returnStatus);
			
			Vector3D	fknVectorA(vector3AData.asFloatVector().x, vector3AData.asFloatVector().y, vector3AData.asFloatVector().z);
			Vector3D	fknVectorB(vector3BData.asFloatVector().x, vector3BData.asFloatVector().y, vector3BData.asFloatVector().z);

			fknVectorA.SelfSub(fknVectorB);

			outVector3.set(fknVectorA.GetX(), fknVectorA.GetY(), fknVectorA.GetZ());

			outVector3.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};