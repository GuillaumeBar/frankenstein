/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Vector3_Add.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Vector3_Add.h"


// Unique plugins ID.
MTypeId fkn_Vector3_Add::id( fkn_Vector3_Add_ID );

MObject	fkn_Vector3_Add::m_Vector3A;
MObject	fkn_Vector3_Add::m_Vector3AX;
MObject	fkn_Vector3_Add::m_Vector3AY;
MObject	fkn_Vector3_Add::m_Vector3AZ;

MObject	fkn_Vector3_Add::m_Vector3B;
MObject	fkn_Vector3_Add::m_Vector3BX;
MObject	fkn_Vector3_Add::m_Vector3BY;
MObject	fkn_Vector3_Add::m_Vector3BZ;

MObject	fkn_Vector3_Add::out_Vector3;
MObject	fkn_Vector3_Add::out_Vector3X;
MObject	fkn_Vector3_Add::out_Vector3Y;
MObject	fkn_Vector3_Add::out_Vector3Z;

//Constructor.
fkn_Vector3_Add::fkn_Vector3_Add(){

};

//Destructor.
fkn_Vector3_Add::~fkn_Vector3_Add(){

};

//Maya Creator.
void* fkn_Vector3_Add::creator(){

	return new fkn_Vector3_Add();
};

//Maya Initialize.
MStatus fkn_Vector3_Add::initialize(){

	MFnNumericAttribute		nAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_Vector3AX = nAttr.create("VectorAX", "VecAX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3AY = nAttr.create("VectorAY", "VecAY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3AZ = nAttr.create("VectorAZ", "VecAZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3A = nAttr.create("VectorA", "VecA", m_Vector3AX, m_Vector3AY, m_Vector3AZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_Vector3A);

	m_Vector3BX = nAttr.create("VectorBX", "VecBX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3BY = nAttr.create("VectorBY", "VecBY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3BZ = nAttr.create("VectorBZ", "VecBZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3B = nAttr.create("VectorB", "VecB", m_Vector3BX, m_Vector3BY, m_Vector3BZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_Vector3B);

	//Define the output attribut.

	out_Vector3X = nAttr.create("ResultX", "rltX", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_Vector3Y = nAttr.create("ResultY", "rltY", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_Vector3Z = nAttr.create("ResultZ", "rltZ", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_Vector3 = nAttr.create("Result", "Rlt", out_Vector3X, out_Vector3Y, out_Vector3Z);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Vector3 );

	// Define the link between the input and output.
	stat = attributeAffects( m_Vector3A, out_Vector3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Vector3B, out_Vector3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Vector3_Add::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MObject	thisNode = thisMObject();

	MDataHandle		vector3AData = in_Data.inputValue(m_Vector3A, &returnStatus);
	MFloatVector	vector3A = vector3AData.asFloatVector();

	MDataHandle		vector3BData = in_Data.inputValue(m_Vector3B, &returnStatus);
	MFloatVector	vector3B = vector3BData.asFloatVector();

	MFloatVector rtn;

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Vector3 ){

			MDataHandle	outVector3 = in_Data.outputValue(out_Vector3, &returnStatus);

			
			//FKN CLASSIQUE.
			Vector3D	fknVectorA(vector3A.x, vector3A.y, vector3A.z);
			Vector3D	fknVectorB(vector3B.x, vector3B.y, vector3B.z);

			fknVectorA.SelfAdd(fknVectorB);

			outVector3.set(fknVectorA.GetX(), fknVectorA.GetY(), fknVectorA.GetZ());
			
			/*
			//MAYA CLASSIQUE.
			vector3A += vector3B;
			outVector3.set(vector3A.x, vector3A.y, vector3A.z);
			*/
			/*
			//SIMD
			__m256 vectorA = _mm256_set_ps(vector3A.x, vector3A.y, vector3A.z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
			__m256 vectorB = _mm256_set_ps(vector3B.x, vector3B.y, vector3B.z, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);

			__m256 result = _mm256_add_ps(vectorA, vectorB);

			float* values = (float*)&result;
			/*
			printf("%f, %f, %f, %f, %f, %f, %f, %f\n",
					values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[7] );
			
			outVector3.set(values[7], values[6], values[5]);
			*/
			//_mm_free(value);
			
			outVector3.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};