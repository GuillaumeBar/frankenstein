/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Vector3_MultiplyByMatrix.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Vector3_MultiplyByMatrix.h"


// Unique plugins ID.
MTypeId     fkn_Vector3_MultiplyByMatrix::id( fkn_Vector3_MultiplyByMatrix_ID );

MObject	fkn_Vector3_MultiplyByMatrix::m_Vector3A;
MObject	fkn_Vector3_MultiplyByMatrix::m_Vector3AX;
MObject	fkn_Vector3_MultiplyByMatrix::m_Vector3AY;
MObject	fkn_Vector3_MultiplyByMatrix::m_Vector3AZ;

MObject	fkn_Vector3_MultiplyByMatrix::m_Matrix;

MObject	fkn_Vector3_MultiplyByMatrix::out_Vector3;
MObject	fkn_Vector3_MultiplyByMatrix::out_Vector3X;
MObject	fkn_Vector3_MultiplyByMatrix::out_Vector3Y;
MObject	fkn_Vector3_MultiplyByMatrix::out_Vector3Z;


//Constructor.
fkn_Vector3_MultiplyByMatrix::fkn_Vector3_MultiplyByMatrix(){

};

//Destructor.
fkn_Vector3_MultiplyByMatrix::~fkn_Vector3_MultiplyByMatrix(){

};

//Maya Creator.
void* fkn_Vector3_MultiplyByMatrix::creator(){

	return new fkn_Vector3_MultiplyByMatrix();
};

//Maya Initialize.
MStatus fkn_Vector3_MultiplyByMatrix::initialize(){

	MFnNumericAttribute		nAttr;
	MFnCompoundAttribute	cAttr;
	MFnMatrixAttribute		mAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 
	m_Vector3AX = nAttr.create("VectorAX", "VecAX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3AY = nAttr.create("VectorAY", "VecAY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3AZ = nAttr.create("VectorAZ", "VecAZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector3A = nAttr.create("VectorA", "VecA", m_Vector3AX, m_Vector3AY, m_Vector3AZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_Vector3A);

	m_Matrix = mAttr.create("Matrix", "mat");
	mAttr.setStorable(true);
	stat = addAttribute(m_Matrix);

	//Define the output attribut.
	out_Vector3X = nAttr.create("ResultX", "rltX", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_Vector3Y = nAttr.create("ResultY", "rltY", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_Vector3Z = nAttr.create("ResultZ", "rltZ", MFnNumericData::kFloat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);

	out_Vector3 = nAttr.create("Result", "Rlt", out_Vector3X, out_Vector3Y, out_Vector3Z);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Vector3 );

	// Define the link between the input and output.
	stat = attributeAffects( m_Vector3A, out_Vector3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Matrix, out_Vector3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Vector3_MultiplyByMatrix::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		vector3AData = in_Data.inputValue(m_Vector3A, &returnStatus);
	MDataHandle		matrixData = in_Data.inputValue(m_Matrix, &returnStatus);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Vector3 ){

			MDataHandle	outVector3 = in_Data.outputValue(out_Vector3, &returnStatus);
			
			Vector3D	fknVectorA(vector3AData.asFloatVector().x, vector3AData.asFloatVector().y, vector3AData.asFloatVector().z);
			Matrix4x4	fknMatrix(	matrixData.asMatrix()(0,0), matrixData.asMatrix()(0,1), matrixData.asMatrix()(0,2), matrixData.asMatrix()(0,3),
									matrixData.asMatrix()(1,0), matrixData.asMatrix()(1,1), matrixData.asMatrix()(1,2), matrixData.asMatrix()(1,3),
									matrixData.asMatrix()(2,0), matrixData.asMatrix()(2,1), matrixData.asMatrix()(2,2), matrixData.asMatrix()(2,3),
									matrixData.asMatrix()(3,0), matrixData.asMatrix()(3,1), matrixData.asMatrix()(3,2), matrixData.asMatrix()(3,3) ); 

			fknVectorA.SelfMulByMatrix4x4(fknMatrix);

			outVector3.set(fknVectorA.GetX(), fknVectorA.GetY(), fknVectorA.GetZ());

			outVector3.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};