/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Quat_DotProduct.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Quaternion Dot Product node for maya.
*/

#ifndef _fkn_Quat_DotProduct_
#define _fkn_Quat_DotProduct_

#include "../fkn_Maya_Tools.h"

class fkn_Quat_DotProduct : public MPxNode
{
	public:
		//Constructor.
							fkn_Quat_DotProduct();
		//Destructor.
		virtual				~fkn_Quat_DotProduct();

		//Maya Methode.
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		static void*		creator();
		static MStatus		initialize();

	public:
		static	MTypeId		id;

		static MObject m_QuatA;
		static MObject m_QuatAX;
		static MObject m_QuatAY;
		static MObject m_QuatAZ;
		static MObject m_QuatAW;

		static MObject m_QuatB;
		static MObject m_QuatBX;
		static MObject m_QuatBY;
		static MObject m_QuatBZ;
		static MObject m_QuatBW;

		static MObject out_Angle;
};

#endif