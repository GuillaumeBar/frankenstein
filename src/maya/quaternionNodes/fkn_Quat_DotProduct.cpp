/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Quat_DotProduct.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Quat_DotProduct.h"


// Unique plugins ID.
MTypeId fkn_Quat_DotProduct::id( fkn_Quaternion_Product_ID );

MObject	fkn_Quat_DotProduct::m_QuatA;
MObject	fkn_Quat_DotProduct::m_QuatAX;
MObject	fkn_Quat_DotProduct::m_QuatAY;
MObject	fkn_Quat_DotProduct::m_QuatAZ;
MObject	fkn_Quat_DotProduct::m_QuatAW;

MObject	fkn_Quat_DotProduct::m_QuatB;
MObject	fkn_Quat_DotProduct::m_QuatBX;
MObject	fkn_Quat_DotProduct::m_QuatBY;
MObject	fkn_Quat_DotProduct::m_QuatBZ;
MObject	fkn_Quat_DotProduct::m_QuatBW;

MObject	fkn_Quat_DotProduct::out_Angle;

//Constructor.
fkn_Quat_DotProduct::fkn_Quat_DotProduct(){

};

//Destructor.
fkn_Quat_DotProduct::~fkn_Quat_DotProduct(){

};

//Maya Creator.
void* fkn_Quat_DotProduct::creator(){

	return new fkn_Quat_DotProduct();
};

//Maya Initialize.
MStatus fkn_Quat_DotProduct::initialize(){

	MFnNumericAttribute		nAttr;
	MFnCompoundAttribute	cAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_QuatAX = nAttr.create("QuaternionAX", "QuatAX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAY = nAttr.create("QuaternionAY", "QuatAY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAZ = nAttr.create("QuaternionAZ", "QuatAZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAW = nAttr.create("QuaternionAW", "QuatAW", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatA = cAttr.create("QuaternionA", "QuatA");
	cAttr.addChild(m_QuatAX);
	cAttr.addChild(m_QuatAY);
	cAttr.addChild(m_QuatAZ);
	cAttr.addChild(m_QuatAW);
	cAttr.setStorable(true);
	stat = addAttribute(m_QuatA);

	m_QuatBX = nAttr.create("QuaternionBX", "QuatBX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatBY = nAttr.create("QuaternionBY", "QuatBY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatBZ = nAttr.create("QuaternionBZ", "QuatBZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatBW = nAttr.create("QuaternionBW", "QuatBW", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatB = cAttr.create("QuaternionB", "QuatB");
	cAttr.addChild(m_QuatBX);
	cAttr.addChild(m_QuatBY);
	cAttr.addChild(m_QuatBZ);
	cAttr.addChild(m_QuatBW);
	cAttr.setStorable(true);
	stat = addAttribute(m_QuatB);

	//Define the output attribut.
	out_Angle = nAttr.create("Angle", "Ang", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	stat = addAttribute(out_Angle);

	// Define the link between the input and output.
	stat = attributeAffects( m_QuatA, out_Angle);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_QuatB, out_Angle);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Quat_DotProduct::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MObject			thisNode = thisMObject();
	MPlug			quatPlugA(thisNode, m_QuatA);
	float			quatAX, quatAY, quatAZ, quatAW;

	quatPlugA.child(0, &returnStatus).getValue(quatAX);
	quatPlugA.child(1, &returnStatus).getValue(quatAY);
	quatPlugA.child(2, &returnStatus).getValue(quatAZ);
	quatPlugA.child(3, &returnStatus).getValue(quatAW);

	MPlug			quatPlugB(thisNode, m_QuatB);
	float			quatBX, quatBY, quatBZ, quatBW;

	quatPlugB.child(0, &returnStatus).getValue(quatBX);
	quatPlugB.child(1, &returnStatus).getValue(quatBY);
	quatPlugB.child(2, &returnStatus).getValue(quatBZ);
	quatPlugB.child(3, &returnStatus).getValue(quatBW);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Angle ){

			MDataHandle	outAngle = in_Data.outputValue(out_Angle, &returnStatus);
			
			Quaternion fknQuatA(quatAX, quatAY, quatAZ, quatAW);
			Quaternion fknQuatB(quatBX, quatBY, quatBZ, quatBW);

			float rtnAngle = fknQuatA.DotProduct(fknQuatB);

			outAngle.set(rtnAngle);

			outAngle.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};