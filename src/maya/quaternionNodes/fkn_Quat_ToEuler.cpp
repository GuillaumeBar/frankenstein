/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Quat_ToEuler.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Quat_ToEuler.h"


// Unique plugins ID.
MTypeId fkn_Quat_ToEuler::id( fkn_Quaternion_ToEuler_ID );

MObject	fkn_Quat_ToEuler::m_QuatA;
MObject	fkn_Quat_ToEuler::m_QuatAX;
MObject	fkn_Quat_ToEuler::m_QuatAY;
MObject	fkn_Quat_ToEuler::m_QuatAZ;
MObject	fkn_Quat_ToEuler::m_QuatAW;

MObject	fkn_Quat_ToEuler::out_Euler;
MObject	fkn_Quat_ToEuler::out_EulerX;
MObject	fkn_Quat_ToEuler::out_EulerY;
MObject	fkn_Quat_ToEuler::out_EulerZ;

//Constructor.
fkn_Quat_ToEuler::fkn_Quat_ToEuler(){

};

//Destructor.
fkn_Quat_ToEuler::~fkn_Quat_ToEuler(){

};

//Maya Creator.
void* fkn_Quat_ToEuler::creator(){

	return new fkn_Quat_ToEuler();
};

//Maya Initialize.
MStatus fkn_Quat_ToEuler::initialize(){

	MFnNumericAttribute		nAttr;
	MFnCompoundAttribute	cAttr;
	MFnUnitAttribute		uAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_QuatAX = nAttr.create("QuaternionAX", "QuatAX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAY = nAttr.create("QuaternionAY", "QuatAY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAZ = nAttr.create("QuaternionAZ", "QuatAZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAW = nAttr.create("QuaternionAW", "QuatAW", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatA = cAttr.create("QuaternionA", "QuatA");
	cAttr.addChild(m_QuatAX);
	cAttr.addChild(m_QuatAY);
	cAttr.addChild(m_QuatAZ);
	cAttr.addChild(m_QuatAW);
	cAttr.setStorable(true);
	stat = addAttribute(m_QuatA);

	//Define the output attribut.
	out_EulerX = uAttr.create("EulerX", "EulX", MFnUnitAttribute::kAngle);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_EulerY = uAttr.create("EulerY", "EulY", MFnUnitAttribute::kAngle);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_EulerZ = uAttr.create("EulerZ", "EulZ", MFnUnitAttribute::kAngle);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_Euler = nAttr.create("Euler", "Eul", out_EulerX, out_EulerY, out_EulerZ);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	stat = addAttribute(out_Euler);

	// Define the link between the input and output.
	stat = attributeAffects( m_QuatA, out_Euler);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Quat_ToEuler::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MObject			thisNode = thisMObject();
	MPlug			quatPlugA(thisNode, m_QuatA);
	float			quatAX, quatAY, quatAZ, quatAW;

	quatPlugA.child(0, &returnStatus).getValue(quatAX);
	quatPlugA.child(1, &returnStatus).getValue(quatAY);
	quatPlugA.child(2, &returnStatus).getValue(quatAZ);
	quatPlugA.child(3, &returnStatus).getValue(quatAW);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Euler ){

			MDataHandle	outEuler = in_Data.outputValue(out_Euler, &returnStatus);
			
			Quaternion 	fknQuatA(quatAX, quatAY, quatAZ, quatAW);
			Matrix3x3 	fknRot;
			float aX, aY, aZ;

			fknRot = fknQuatA.ToMatrix3x3();
			fknRot.ToEuler(aX, aY, aZ,1);
			MVector eulerRot(aX, aY, aZ);

			outEuler.set(eulerRot);

			outEuler.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};