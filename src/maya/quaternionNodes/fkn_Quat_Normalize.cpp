/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Quat_Normalize.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Quat_Normalize.h"


// Unique plugins ID.
MTypeId fkn_Quat_Normalize::id( fkn_Quaternion_Normalize_ID );

MObject	fkn_Quat_Normalize::m_QuatA;
MObject	fkn_Quat_Normalize::m_QuatAX;
MObject	fkn_Quat_Normalize::m_QuatAY;
MObject	fkn_Quat_Normalize::m_QuatAZ;
MObject	fkn_Quat_Normalize::m_QuatAW;

MObject	fkn_Quat_Normalize::out_Quat;
MObject	fkn_Quat_Normalize::out_QuatX;
MObject	fkn_Quat_Normalize::out_QuatY;
MObject	fkn_Quat_Normalize::out_QuatZ;
MObject	fkn_Quat_Normalize::out_QuatW;

//Constructor.
fkn_Quat_Normalize::fkn_Quat_Normalize(){

};

//Destructor.
fkn_Quat_Normalize::~fkn_Quat_Normalize(){

};

//Maya Creator.
void* fkn_Quat_Normalize::creator(){

	return new fkn_Quat_Normalize();
};

//Maya Initialize.
MStatus fkn_Quat_Normalize::initialize(){

	MFnNumericAttribute		nAttr;
	MFnCompoundAttribute	cAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_QuatAX = nAttr.create("QuaternionAX", "QuatAX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAY = nAttr.create("QuaternionAY", "QuatAY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAZ = nAttr.create("QuaternionAZ", "QuatAZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAW = nAttr.create("QuaternionAW", "QuatAW", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatA = cAttr.create("QuaternionA", "QuatA");
	cAttr.addChild(m_QuatAX);
	cAttr.addChild(m_QuatAY);
	cAttr.addChild(m_QuatAZ);
	cAttr.addChild(m_QuatAW);
	cAttr.setStorable(true);
	stat = addAttribute(m_QuatA);

	//Define the output attribut.
	out_QuatX = nAttr.create("QuaternionX", "QuatX", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatY = nAttr.create("QuaternionY", "QuatY", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatZ = nAttr.create("QuaternionZ", "QuatZ", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatW = nAttr.create("QuaternionW", "QuatW", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_Quat = cAttr.create("Quaternion", "Quat");
	cAttr.addChild(out_QuatX);
	cAttr.addChild(out_QuatY);
	cAttr.addChild(out_QuatZ);
	cAttr.addChild(out_QuatW);
	cAttr.setStorable(false);
	cAttr.setWritable(false);
	stat = addAttribute(out_Quat);

	// Define the link between the input and output.
	stat = attributeAffects( m_QuatA, out_Quat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Quat_Normalize::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MObject			thisNode = thisMObject();
	MPlug			quatPlugA(thisNode, m_QuatA);
	float			quatAX, quatAY, quatAZ, quatAW;

	quatPlugA.child(0, &returnStatus).getValue(quatAX);
	quatPlugA.child(1, &returnStatus).getValue(quatAY);
	quatPlugA.child(2, &returnStatus).getValue(quatAZ);
	quatPlugA.child(3, &returnStatus).getValue(quatAW);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Quat ){

			MDataHandle	outQuat = in_Data.outputValue(out_Quat, &returnStatus);
			
			Quaternion fknQuatA(quatAX, quatAY, quatAZ, quatAW);

			fknQuatA.SelfNormalize();

			MPlug			quatPlugOut(thisNode, out_Quat);

			quatPlugOut.child(0, &returnStatus).setValue(fknQuatA.GetX());
			quatPlugOut.child(1, &returnStatus).setValue(fknQuatA.GetY());
			quatPlugOut.child(2, &returnStatus).setValue(fknQuatA.GetZ());
			quatPlugOut.child(3, &returnStatus).setValue(fknQuatA.GetW());

			in_Data.setClean(in_Plug);
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};