/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Quat_FromAxisAndAngle.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Quaternion From Axis And Angle node for maya.
*/

#ifndef _fkn_Quat_FromAxisAndAngle_
#define _fkn_Quat_FromAxisAndAngle_

#include "../fkn_Maya_Tools.h"

class fkn_Quat_FromAxisAndAngle : public MPxNode
{
	public:
		//Constructor.
							fkn_Quat_FromAxisAndAngle();
		//Destructor.
		virtual				~fkn_Quat_FromAxisAndAngle();

		//Maya Methode.
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		static void*		creator();
		static MStatus		initialize();

	public:
		static	MTypeId		id;

		static MObject m_Angle;;

		static MObject m_Vector;
		static MObject m_VectorX;
		static MObject m_VectorY;
		static MObject m_VectorZ;

		static MObject out_Quat;
		static MObject out_QuatX;
		static MObject out_QuatY;
		static MObject out_QuatZ;
		static MObject out_QuatW;
};

#endif