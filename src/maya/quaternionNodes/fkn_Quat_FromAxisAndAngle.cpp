/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Quat_FromAxisAndAngle.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Quat_FromAxisAndAngle.h"


// Unique plugins ID.
MTypeId fkn_Quat_FromAxisAndAngle::id( fkn_Quaternion_AxisAndAngle_ID );

MObject	fkn_Quat_FromAxisAndAngle::m_Angle;

MObject	fkn_Quat_FromAxisAndAngle::m_Vector;
MObject	fkn_Quat_FromAxisAndAngle::m_VectorX;
MObject	fkn_Quat_FromAxisAndAngle::m_VectorY;
MObject	fkn_Quat_FromAxisAndAngle::m_VectorZ;

MObject	fkn_Quat_FromAxisAndAngle::out_Quat;
MObject	fkn_Quat_FromAxisAndAngle::out_QuatX;
MObject	fkn_Quat_FromAxisAndAngle::out_QuatY;
MObject	fkn_Quat_FromAxisAndAngle::out_QuatZ;
MObject	fkn_Quat_FromAxisAndAngle::out_QuatW;

//Constructor.
fkn_Quat_FromAxisAndAngle::fkn_Quat_FromAxisAndAngle(){

};

//Destructor.
fkn_Quat_FromAxisAndAngle::~fkn_Quat_FromAxisAndAngle(){

};

//Maya Creator.
void* fkn_Quat_FromAxisAndAngle::creator(){

	return new fkn_Quat_FromAxisAndAngle();
};

//Maya Initialize.
MStatus fkn_Quat_FromAxisAndAngle::initialize(){

	MFnNumericAttribute		nAttr;
	MFnCompoundAttribute	cAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_Angle = nAttr.create("Angle", "Ang", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	stat = addAttribute(m_Angle);

	m_VectorX = nAttr.create("VectorX", "VecX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_VectorY = nAttr.create("VectorY", "VecY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_VectorZ = nAttr.create("VectorZ", "VecZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_Vector = nAttr.create("Vector", "Vec", m_VectorX, m_VectorY, m_VectorZ);
	nAttr.setStorable(true);
	stat = addAttribute(m_Vector);

	//Define the output attribut.
	out_QuatX = nAttr.create("QuaternionX", "QuatX", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatY = nAttr.create("QuaternionY", "QuatY", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatZ = nAttr.create("QuaternionZ", "QuatZ", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatW = nAttr.create("QuaternionW", "QuatW", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_Quat = cAttr.create("Quaternion", "Quat");
	cAttr.addChild(out_QuatX);
	cAttr.addChild(out_QuatY);
	cAttr.addChild(out_QuatZ);
	cAttr.addChild(out_QuatW);
	cAttr.setStorable(false);
	cAttr.setWritable(false);
	stat = addAttribute(out_Quat);

	// Define the link between the input and output.
	stat = attributeAffects( m_Angle, out_Quat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Vector, out_Quat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Quat_FromAxisAndAngle::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MObject			thisNode = thisMObject();
	MDataHandle	angleData = in_Data.inputValue(m_Angle, &returnStatus);
	MDataHandle vectorData = in_Data.inputValue(m_Vector, &returnStatus);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Quat ){

			MDataHandle	outQuat = in_Data.outputValue(out_Quat, &returnStatus);
			
			Quaternion rtnQuat;

			rtnQuat.FromAxisAndAngle(vectorData.asFloatVector().x, vectorData.asFloatVector().y, vectorData.asFloatVector().z, angleData.asFloat());

			MPlug			quatPlugOut(thisNode, out_Quat);

			quatPlugOut.child(0, &returnStatus).setValue(rtnQuat.GetX());
			quatPlugOut.child(1, &returnStatus).setValue(rtnQuat.GetY());
			quatPlugOut.child(2, &returnStatus).setValue(rtnQuat.GetZ());
			quatPlugOut.child(3, &returnStatus).setValue(rtnQuat.GetW());

			in_Data.setClean(in_Plug);
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};