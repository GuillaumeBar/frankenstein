/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Quat_Slerp.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Quat_Slerp.h"


// Unique plugins ID.
MTypeId fkn_Quat_Slerp::id( fkn_Quaternion_Slerp_ID );


MObject fkn_Quat_Slerp::m_Blend;
MObject	fkn_Quat_Slerp::m_QuatA;
MObject	fkn_Quat_Slerp::m_QuatAX;
MObject	fkn_Quat_Slerp::m_QuatAY;
MObject	fkn_Quat_Slerp::m_QuatAZ;
MObject	fkn_Quat_Slerp::m_QuatAW;

MObject	fkn_Quat_Slerp::m_QuatB;
MObject	fkn_Quat_Slerp::m_QuatBX;
MObject	fkn_Quat_Slerp::m_QuatBY;
MObject	fkn_Quat_Slerp::m_QuatBZ;
MObject	fkn_Quat_Slerp::m_QuatBW;

MObject	fkn_Quat_Slerp::out_Quat;
MObject	fkn_Quat_Slerp::out_QuatX;
MObject	fkn_Quat_Slerp::out_QuatY;
MObject	fkn_Quat_Slerp::out_QuatZ;
MObject	fkn_Quat_Slerp::out_QuatW;

//Constructor.
fkn_Quat_Slerp::fkn_Quat_Slerp(){

};

//Destructor.
fkn_Quat_Slerp::~fkn_Quat_Slerp(){

};

//Maya Creator.
void* fkn_Quat_Slerp::creator(){

	return new fkn_Quat_Slerp();
};

//Maya Initialize.
MStatus fkn_Quat_Slerp::initialize(){

	MFnNumericAttribute		nAttr;
	MFnCompoundAttribute	cAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_Blend = nAttr.create("Blend", "Bld", MFnNumericData::kFloat);
	nAttr.setStorable(true);
	stat = addAttribute(m_Blend);

	m_QuatAX = nAttr.create("QuaternionAX", "QuatAX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAY = nAttr.create("QuaternionAY", "QuatAY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAZ = nAttr.create("QuaternionAZ", "QuatAZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatAW = nAttr.create("QuaternionAW", "QuatAW", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatA = cAttr.create("QuaternionA", "QuatA");
	cAttr.addChild(m_QuatAX);
	cAttr.addChild(m_QuatAY);
	cAttr.addChild(m_QuatAZ);
	cAttr.addChild(m_QuatAW);
	cAttr.setStorable(true);
	stat = addAttribute(m_QuatA);

	m_QuatBX = nAttr.create("QuaternionBX", "QuatBX", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatBY = nAttr.create("QuaternionBY", "QuatBY", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatBZ = nAttr.create("QuaternionBZ", "QuatBZ", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatBW = nAttr.create("QuaternionBW", "QuatBW", MFnNumericData::kFloat);
	nAttr.setStorable(true);

	m_QuatB = cAttr.create("QuaternionB", "QuatB");
	cAttr.addChild(m_QuatBX);
	cAttr.addChild(m_QuatBY);
	cAttr.addChild(m_QuatBZ);
	cAttr.addChild(m_QuatBW);
	cAttr.setStorable(true);
	stat = addAttribute(m_QuatB);

	//Define the output attribut.
	out_QuatX = nAttr.create("QuaternionX", "QuatX", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatY = nAttr.create("QuaternionY", "QuatY", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatZ = nAttr.create("QuaternionZ", "QuatZ", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_QuatW = nAttr.create("QuaternionW", "QuatW", MFnNumericData::kFloat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_Quat = cAttr.create("Quaternion", "Quat");
	cAttr.addChild(out_QuatX);
	cAttr.addChild(out_QuatY);
	cAttr.addChild(out_QuatZ);
	cAttr.addChild(out_QuatW);
	cAttr.setStorable(false);
	cAttr.setWritable(false);
	stat = addAttribute(out_Quat);

	// Define the link between the input and output.
	stat = attributeAffects( m_QuatA, out_Quat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_QuatB, out_Quat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_Blend, out_Quat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	
	return MS::kSuccess;
};

MStatus fkn_Quat_Slerp::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MObject			thisNode = thisMObject();
	MDataHandle		blendData = in_Data.inputValue(m_Blend, &returnStatus);

	MPlug			quatPlugA(thisNode, m_QuatA);
	float			quatAX, quatAY, quatAZ, quatAW;

	quatPlugA.child(0, &returnStatus).getValue(quatAX);
	quatPlugA.child(1, &returnStatus).getValue(quatAY);
	quatPlugA.child(2, &returnStatus).getValue(quatAZ);
	quatPlugA.child(3, &returnStatus).getValue(quatAW);

	MPlug			quatPlugB(thisNode, m_QuatB);
	float			quatBX, quatBY, quatBZ, quatBW;

	quatPlugB.child(0, &returnStatus).getValue(quatBX);
	quatPlugB.child(1, &returnStatus).getValue(quatBY);
	quatPlugB.child(2, &returnStatus).getValue(quatBZ);
	quatPlugB.child(3, &returnStatus).getValue(quatBW);

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Quat ){

			MDataHandle	outQuat = in_Data.outputValue(out_Quat, &returnStatus);
			
			Quaternion fknQuatA(quatAX, quatAY, quatAZ, quatAW);
			Quaternion fknQuatB(quatBX, quatBY, quatBZ, quatBW);
			Quaternion rtnQuat;

			rtnQuat.Slerp ( fknQuatA, fknQuatB, blendData.asFloat(), true);

			MPlug			quatPlugOut(thisNode, out_Quat);

			quatPlugOut.child(0, &returnStatus).setValue(rtnQuat.GetX());
			quatPlugOut.child(1, &returnStatus).setValue(rtnQuat.GetY());
			quatPlugOut.child(2, &returnStatus).setValue(rtnQuat.GetZ());
			quatPlugOut.child(3, &returnStatus).setValue(rtnQuat.GetW());

			in_Data.setClean(in_Plug);
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};