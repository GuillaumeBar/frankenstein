/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Bool_Or_Array.cpp
@author Guillaume Baratte
@date 2016-10-31
*/

#include "fkn_Bool_Or_Array.h"

// Unique plugins ID.
MTypeId fkn_Bool_Or_Array::id( fkn_Bool_Or_Array_ID );

MObject	fkn_Bool_Or_Array::m_aBools;

MObject	fkn_Bool_Or_Array::out_Bool;

//Constructor.
fkn_Bool_Or_Array::fkn_Bool_Or_Array(){

};

//Destructor.
fkn_Bool_Or_Array::~fkn_Bool_Or_Array(){

};

//Maya Creator.
void* fkn_Bool_Or_Array::creator(){

	return new fkn_Bool_Or_Array();
};

//Maya Initialize.
MStatus fkn_Bool_Or_Array::initialize(){

	MFnNumericAttribute		nAttr;
	MFnEnumAttribute		eAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_aBools = nAttr.create("Bools", "Bools", MFnNumericData::kBoolean);
	nAttr.setArray(true);
	nAttr.setStorable(true);
	stat = addAttribute(m_aBools);

	//Define the output attribut.
	out_Bool = nAttr.create("Result", "Result", MFnNumericData::kBoolean);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Bool );

	// Define the link between the input and output.
	stat = attributeAffects( m_aBools, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Bool_Or_Array::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MArrayDataHandle	aboolData = in_Data.inputArrayValue(m_aBools, &returnStatus);
	aboolData.jumpToElement(0);
	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Bool ){

			MDataHandle	outBool = in_Data.outputValue(out_Bool, &returnStatus);
			bool result = false;

			for (unsigned int i=0; i<aboolData.elementCount(); i++ ){
				if( aboolData.inputValue().asBool() == true){
					result = true;
					break;
				}

				aboolData.next();
			}

			outBool.set(result);

			outBool.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Bool_Or_Array cannot get value.\n" );
	}

	return returnStatus;
};