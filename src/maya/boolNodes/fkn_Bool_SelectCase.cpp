/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Bool_Or_Array.cpp
@author Guillaume Baratte
@date 2016-10-31
*/

#include "fkn_Bool_SelectCase.h"

// Unique plugins ID.
MTypeId fkn_Bool_SelectCase::id( fkn_Bool_SelectCase_ID );

MObject	fkn_Bool_SelectCase::m_switchValue;
MObject	fkn_Bool_SelectCase::m_defaultValue;
MObject	fkn_Bool_SelectCase::m_baseBool;
MObject	fkn_Bool_SelectCase::m_aValues;

MObject	fkn_Bool_SelectCase::out_Bool;

//Constructor.
fkn_Bool_SelectCase::fkn_Bool_SelectCase(){

};

//Destructor.
fkn_Bool_SelectCase::~fkn_Bool_SelectCase(){

};

//Maya Creator.
void* fkn_Bool_SelectCase::creator(){

	return new fkn_Bool_SelectCase();
};

//Maya Initialize.
MStatus fkn_Bool_SelectCase::initialize(){

	MFnNumericAttribute		nAttr;
	MFnCompoundAttribute	cAttr;
	MFnEnumAttribute		eAttr;

	MStatus					stat;

	// Define the input attribute.
	m_switchValue = nAttr.create("switchValue", "switchValue", MFnNumericData::kInt);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_switchValue);

	m_defaultValue = nAttr.create("defaultValue", "defaultValue", MFnNumericData::kInt);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_defaultValue);

	m_baseBool = nAttr.create("bool", "bool", MFnNumericData::kInt, -1000);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);

	// Define the input attribute.
	m_aValues = cAttr.create("arrayValue", "arrayValue");
	cAttr.addChild(m_baseBool);
	cAttr.setStorable(true);
	cAttr.setKeyable(true);
	cAttr.setArray(true);
	stat = addAttribute(m_aValues);

	//Define the output attributs.
	out_Bool = nAttr.create("Result", "Result", MFnNumericData::kInt);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Bool );

	// Define the link between the input and output.
	stat = attributeAffects( m_switchValue, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_defaultValue, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_aValues, out_Bool);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Bool_SelectCase::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MArrayDataHandle	aboolData = in_Data.inputArrayValue(m_aValues, &returnStatus);

	MDataHandle switchValueData = in_Data.inputValue(m_switchValue, &returnStatus);
	int switchValue = switchValueData.asShort();

	MDataHandle defautlValueData = in_Data.inputValue(m_defaultValue, &returnStatus);
	int defaultValue = defautlValueData.asShort();

	if (returnStatus == MS::kSuccess){

		if ( in_Plug == out_Bool ){

			MDataHandle	outBool = in_Data.outputValue(out_Bool, &returnStatus);
			int result = defaultValue;

			unsigned int arrayCount = aboolData.elementCount();

			if ( switchValue >= 0 && switchValue < arrayCount){
				aboolData.jumpToElement(switchValue);
				result = aboolData.inputValue(&returnStatus).child(m_baseBool).asShort();
			}

			outBool.set(result);

			outBool.setClean();
		}
		else{

			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Bool_SelectCase cannot get value.\n" );
	}

	return returnStatus;
};