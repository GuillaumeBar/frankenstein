/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file pluginMain.cpp
@author Guillaume Baratte
@date 2016-03-27
*/


#include "vectorNodes/fkn_Vector3_Add.h"
#include "vectorNodes/fkn_Vector3_Sub.h"
#include "vectorNodes/fkn_Vector3_MultiplyByFloat.h"
#include "vectorNodes/fkn_Vector3_Length.h"
#include "vectorNodes/fkn_Vector3_CrossProduct.h"
#include "vectorNodes/fkn_Vector3_DotProduct.h"
#include "vectorNodes/fkn_Vector3_Normalize.h"
#include "vectorNodes/fkn_Vector3_RotateByQuaternion.h"
#include "vectorNodes/fkn_Vector3_MultiplyByMatrix.h"
#include "vectorNodes/fkn_Vector3_Compare.h"
#include "vectorNodes/fkn_Vector3_Switch.h"
#include "vectorNodes/fkn_Vector3_SelectCase.h"

#include "floatNodes/fkn_Trigonometry.h"
#include "floatNodes/fkn_Float_Add.h"
#include "floatNodes/fkn_Float_Add_Array.h"
#include "floatNodes/fkn_Float_Sub.h"
#include "floatNodes/fkn_Float_Multiply.h"
#include "floatNodes/fkn_Float_Divide.h"
#include "floatNodes/fkn_Float_SquareRoot.h"
#include "floatNodes/fkn_Float_Pow.h"
#include "floatNodes/fkn_Int_Modulo.h"
#include "floatNodes/fkn_Float_Log.h"
#include "floatNodes/fkn_Float_Exponent.h"
#include "floatNodes/fkn_Float_Round.h"
#include "floatNodes/fkn_Float_Negate.h"
#include "floatNodes/fkn_Float_Abs.h"
#include "floatNodes/fkn_Float_Clamp.h"
#include "floatNodes/fkn_Float_Rescale.h"
#include "floatNodes/fkn_Float_Compare.h"
#include "floatNodes/fkn_Float_Switch.h"
#include "floatNodes/fkn_Float_LinearInterpolation.h"
#include "floatNodes/fkn_Float_SelectCase.h"

#include "matrix4Nodes/fkn_Matrix4_Multiply.h"
#include "matrix4Nodes/fkn_Matrix4_Invert.h"
#include "matrix4Nodes/fkn_Matrix4_Transpose.h"
#include "matrix4Nodes/fkn_Matrix4_TransposeInverse.h"
#include "matrix4Nodes/fkn_Matrix4_ToVector3.h"
#include "matrix4Nodes/fkn_Matrix4_FromVector3.h"
#include "matrix4Nodes/fkn_Matrix4_Switch.h"
#include "matrix4Nodes/fkn_Matrix4_ToQuat.h"
#include "matrix4Nodes/fkn_Matrix4_Interpolation.h"

#include "quaternionNodes/fkn_Quat_Add.h"
#include "quaternionNodes/fkn_Quat_Multiply.h"
#include "quaternionNodes/fkn_Quat_Sub.h"
#include "quaternionNodes/fkn_Quat_Conjugate.h"
#include "quaternionNodes/fkn_Quat_Invert.h"
#include "quaternionNodes/fkn_Quat_Normalize.h"
#include "quaternionNodes/fkn_Quat_Negate.h"
#include "quaternionNodes/fkn_Quat_Slerp.h"
#include "quaternionNodes/fkn_Quat_DotProduct.h"
#include "quaternionNodes/fkn_Quat_FromAxisAndAngle.h"
#include "quaternionNodes/fkn_Quat_ToEuler.h"

#include "rigNodes/fkn_SplineMemberNode.h"
#include "rigNodes/fkn_Ik3BonesMemberNode.h"
#include "rigNodes/fkn_IkPlanConstraint.h"
#include "rigNodes/fkn_SRT_Constraint.h"
#include "rigNodes/fkn_ChainOnNurbsSurfaceCns.h"
#include "rigNodes/fkn_BlendRotCns.h"
#include "rigNodes/fkn_ConvertToLocalSpace.h"
#include "rigNodes/fkn_Ik2BonesMemberNode.h"
#include "rigNodes/fkn_BoneRollsConstraint.h"
#include "rigNodes/fkn_CompressionCompensator.h"
#include "rigNodes/fkn_SpaceSwitchCns.h"
#include "rigNodes/fkn_SpaceSwitchDynamicCns.h"
#include "rigNodes/fkn_vertexConstraintNode.h"
#include "rigNodes/fkn_surfaceConstraintNode.h"
#include "rigNodes/fkn_SplineMember2Node.h"

#include "boolNodes/fkn_Bool_And_Array.h"
#include "boolNodes/fkn_Bool_Or_Array.h"
#include "boolNodes/fkn_Bool_SelectCase.h"
#include "boolNodes/fkn_Bool_Invert.h"

#include <maya/MFnPlugin.h>

MStatus initializePlugin( MObject obj )
//
//	Description:
//		this method is called when the plug-in is loaded into Maya.  It 
//		registers all of the services that this plug-in provides with 
//		Maya.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{ 
	cout << "Frankenstein Tools | Loaded." << endl;
	cout << "Frankenstein Tools | v1.11.004" << endl;
	cout << "Frankenstein Tools | Build 2017.05.05" << endl;

	MStatus   status;
	MFnPlugin plugin( obj, "Guillaume Baratte", "1.0", "Any");

	const MString	plusCategory = "Utility";

	status = plugin.registerNode( "fkn_SplineMemberNode",
									fkn_SplineMemberNode::id,
									fkn_SplineMemberNode::creator,
									fkn_SplineMemberNode::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Vector3_Add",
									fkn_Vector3_Add::id,
									fkn_Vector3_Add::creator,
									fkn_Vector3_Add::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Vector3_Sub",
									fkn_Vector3_Sub::id,
									fkn_Vector3_Sub::creator,
									fkn_Vector3_Sub::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Vector3_MultiplyByFloat",
									fkn_Vector3_MultiplyByFloat::id,
									fkn_Vector3_MultiplyByFloat::creator,
									fkn_Vector3_MultiplyByFloat::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Vector3_Length",
									fkn_Vector3_Length::id,
									fkn_Vector3_Length::creator,
									fkn_Vector3_Length::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Vector3_CrossProduct",
									fkn_Vector3_CrossProduct::id,
									fkn_Vector3_CrossProduct::creator,
									fkn_Vector3_CrossProduct::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Vector3_DotProduct",
									fkn_Vector3_DotProduct::id,
									fkn_Vector3_DotProduct::creator,
									fkn_Vector3_DotProduct::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Vector3_Normalize",
									fkn_Vector3_Normalize::id,
									fkn_Vector3_Normalize::creator,
									fkn_Vector3_Normalize::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Vector3_RotateByQuaternion",
									fkn_Vector3_RotateByQuaternion::id,
									fkn_Vector3_RotateByQuaternion::creator,
									fkn_Vector3_RotateByQuaternion::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Vector3_MultiplyByMatrix",
									fkn_Vector3_MultiplyByMatrix::id,
									fkn_Vector3_MultiplyByMatrix::creator,
									fkn_Vector3_MultiplyByMatrix::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Vector3_Compare",
									fkn_Vector3_Compare::id,
									fkn_Vector3_Compare::creator,
									fkn_Vector3_Compare::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Vector3_Switch",
									fkn_Vector3_Switch::id,
									fkn_Vector3_Switch::creator,
									fkn_Vector3_Switch::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Int_Modulo",
									fkn_Int_Modulo::id,
									fkn_Int_Modulo::creator,
									fkn_Int_Modulo::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Trigonometry",
									fkn_Trigonometry::id,
									fkn_Trigonometry::creator,
									fkn_Trigonometry::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Add",
									fkn_Float_Add::id,
									fkn_Float_Add::creator,
									fkn_Float_Add::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Add_Array",
									fkn_Float_Add_Array::id,
									fkn_Float_Add_Array::creator,
									fkn_Float_Add_Array::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Sub",
									fkn_Float_Sub::id,
									fkn_Float_Sub::creator,
									fkn_Float_Sub::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Multiply",
									fkn_Float_Multiply::id,
									fkn_Float_Multiply::creator,
									fkn_Float_Multiply::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Divide",
									fkn_Float_Divide::id,
									fkn_Float_Divide::creator,
									fkn_Float_Divide::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_SquareRoot",
									fkn_Float_SquareRoot::id,
									fkn_Float_SquareRoot::creator,
									fkn_Float_SquareRoot::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Pow",
									fkn_Float_Pow::id,
									fkn_Float_Pow::creator,
									fkn_Float_Pow::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Log",
									fkn_Float_Log::id,
									fkn_Float_Log::creator,
									fkn_Float_Log::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Exponent",
									fkn_Float_Exponent::id,
									fkn_Float_Exponent::creator,
									fkn_Float_Exponent::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Round",
									fkn_Float_Round::id,
									fkn_Float_Round::creator,
									fkn_Float_Round::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Negate",
									fkn_Float_Negate::id,
									fkn_Float_Negate::creator,
									fkn_Float_Negate::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Abs",
									fkn_Float_Abs::id,
									fkn_Float_Abs::creator,
									fkn_Float_Abs::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Clamp",
									fkn_Float_Clamp::id,
									fkn_Float_Clamp::creator,
									fkn_Float_Clamp::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Rescale",
									fkn_Float_Rescale::id,
									fkn_Float_Rescale::creator,
									fkn_Float_Rescale::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Compare",
									fkn_Float_Compare::id,
									fkn_Float_Compare::creator,
									fkn_Float_Compare::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_Switch",
									fkn_Float_Switch::id,
									fkn_Float_Switch::creator,
									fkn_Float_Switch::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Float_LinearInterpolation",
									fkn_Float_LinearInterpolation::id,
									fkn_Float_LinearInterpolation::creator,
									fkn_Float_LinearInterpolation::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Matrix4_Multiply",
									fkn_Matrix4_Multiply::id,
									fkn_Matrix4_Multiply::creator,
									fkn_Matrix4_Multiply::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Matrix4_Invert",
									fkn_Matrix4_Invert::id,
									fkn_Matrix4_Invert::creator,
									fkn_Matrix4_Invert::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Matrix4_Transpose",
									fkn_Matrix4_Transpose::id,
									fkn_Matrix4_Transpose::creator,
									fkn_Matrix4_Transpose::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Matrix4_TransposeInverse",
									fkn_Matrix4_TransposeInverse::id,
									fkn_Matrix4_TransposeInverse::creator,
									fkn_Matrix4_TransposeInverse::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Matrix4_ToVector3",
									fkn_Matrix4_ToVector3::id,
									fkn_Matrix4_ToVector3::creator,
									fkn_Matrix4_ToVector3::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Matrix4_FromVector3",
									fkn_Matrix4_FromVector3::id,
									fkn_Matrix4_FromVector3::creator,
									fkn_Matrix4_FromVector3::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Matrix4_Switch",
									fkn_Matrix4_Switch::id,
									fkn_Matrix4_Switch::creator,
									fkn_Matrix4_Switch::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Quat_Add",
									fkn_Quat_Add::id,
									fkn_Quat_Add::creator,
									fkn_Quat_Add::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Quat_Multiply",
									fkn_Quat_Multiply::id,
									fkn_Quat_Multiply::creator,
									fkn_Quat_Multiply::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Quat_Sub",
									fkn_Quat_Sub::id,
									fkn_Quat_Sub::creator,
									fkn_Quat_Sub::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Quat_Conjugate",
									fkn_Quat_Conjugate::id,
									fkn_Quat_Conjugate::creator,
									fkn_Quat_Conjugate::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Quat_Invert",
									fkn_Quat_Invert::id,
									fkn_Quat_Invert::creator,
									fkn_Quat_Invert::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Quat_Normalize",
									fkn_Quat_Normalize::id,
									fkn_Quat_Normalize::creator,
									fkn_Quat_Normalize::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Quat_Negate",
									fkn_Quat_Negate::id,
									fkn_Quat_Negate::creator,
									fkn_Quat_Negate::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Quat_Slerp",
									fkn_Quat_Slerp::id,
									fkn_Quat_Slerp::creator,
									fkn_Quat_Slerp::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Quat_DotProduct",
									fkn_Quat_DotProduct::id,
									fkn_Quat_DotProduct::creator,
									fkn_Quat_DotProduct::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Quat_FromAxisAndAngle",
									fkn_Quat_FromAxisAndAngle::id,
									fkn_Quat_FromAxisAndAngle::creator,
									fkn_Quat_FromAxisAndAngle::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode("fkn_Quat_ToEuler",
									fkn_Quat_ToEuler::id,
									fkn_Quat_ToEuler::creator,
									fkn_Quat_ToEuler::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_Ik3BonesMemberNode",
									fkn_Ik3BonesMemberNode::id,
									fkn_Ik3BonesMemberNode::creator,
									fkn_Ik3BonesMemberNode::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_IkPlanConstraint",
									fkn_IkPlanConstraint::id,
									fkn_IkPlanConstraint::creator,
									fkn_IkPlanConstraint::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_SRT_Constraint",
									fkn_SRT_Constraint::id,
									fkn_SRT_Constraint::creator,
									fkn_SRT_Constraint::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_ChainOnNurbsSurfaceCns",
									fkn_ChainOnNurbsSurfaceCns::id,
									fkn_ChainOnNurbsSurfaceCns::creator,
									fkn_ChainOnNurbsSurfaceCns::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_BlendRotCns",
									fkn_BlendRotCns::id,
									fkn_BlendRotCns::creator,
									fkn_BlendRotCns::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_ConvertToLocalSpace",
									fkn_ConvertToLocalSpace::id,
									fkn_ConvertToLocalSpace::creator,
									fkn_ConvertToLocalSpace::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_Matrix4_ToQuat",
									fkn_Matrix4_ToQuat::id,
									fkn_Matrix4_ToQuat::creator,
									fkn_Matrix4_ToQuat::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_Matrix4_Interpolation",
									fkn_Matrix4_Interpolation::id,
									fkn_Matrix4_Interpolation::creator,
									fkn_Matrix4_Interpolation::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_Ik2BonesMemberNode",
									fkn_Ik2BonesMemberNode::id,
									fkn_Ik2BonesMemberNode::creator,
									fkn_Ik2BonesMemberNode::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_BoneRollsConstraint",
									fkn_BoneRollsConstraint::id,
									fkn_BoneRollsConstraint::creator,
									fkn_BoneRollsConstraint::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_CompressionCompensator",
									fkn_CompressionCompensator::id,
									fkn_CompressionCompensator::creator,
									fkn_CompressionCompensator::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_SpaceSwitchCns",
									fkn_SpaceSwitchCns::id,
									fkn_SpaceSwitchCns::creator,
									fkn_SpaceSwitchCns::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_SpaceSwitchDynamicCns",
									fkn_SpaceSwitchDynamicCns::id,
									fkn_SpaceSwitchDynamicCns::creator,
									fkn_SpaceSwitchDynamicCns::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_Bool_And_Array",
									fkn_Bool_And_Array::id,
									fkn_Bool_And_Array::creator,
									fkn_Bool_And_Array::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_Bool_Or_Array",
									fkn_Bool_Or_Array::id,
									fkn_Bool_Or_Array::creator,
									fkn_Bool_Or_Array::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_Bool_SelectCase",
									fkn_Bool_SelectCase::id,
									fkn_Bool_SelectCase::creator,
									fkn_Bool_SelectCase::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_Float_SelectCase",
									fkn_Float_SelectCase::id,
									fkn_Float_SelectCase::creator,
									fkn_Float_SelectCase::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_Vector3_SelectCase",
									fkn_Vector3_SelectCase::id,
									fkn_Vector3_SelectCase::creator,
									fkn_Vector3_SelectCase::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_Bool_Invert",
									fkn_Bool_Invert::id,
									fkn_Bool_Invert::creator,
									fkn_Bool_Invert::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_VertexConstraintNode",
									fkn_VertexConstraintNode::id,
									fkn_VertexConstraintNode::creator,
									fkn_VertexConstraintNode::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_surfaceConstraintNode",
									fkn_surfaceConstraintNode::id,
									fkn_surfaceConstraintNode::creator,
									fkn_surfaceConstraintNode::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	status = plugin.registerNode( "fkn_SplineMember2Node",
									fkn_SplineMember2Node::id,
									fkn_SplineMember2Node::creator,
									fkn_SplineMember2Node::initialize,
									MPxNode::kDependNode,
									&plusCategory );
	
	if (!status) {
		status.perror("registerNode");
		return status;
	}

	return status;
}

MStatus uninitializePlugin( MObject obj)
//
//	Description:
//		this method is called when the plug-in is unloaded from Maya. It 
//		deregisters all of the services that it was providing.
//
//	Arguments:
//		obj - a handle to the plug-in object (use MFnPlugin to access it)
//
{
	MStatus   status;
	MFnPlugin plugin( obj );

	status = plugin.deregisterNode( fkn_SplineMemberNode::id );
	status = plugin.deregisterNode( fkn_Vector3_Add::id );
	status = plugin.deregisterNode( fkn_Vector3_Sub::id );
	status = plugin.deregisterNode( fkn_Vector3_MultiplyByFloat::id );
	status = plugin.deregisterNode( fkn_Vector3_Length::id );
	status = plugin.deregisterNode( fkn_Vector3_CrossProduct::id );
	status = plugin.deregisterNode( fkn_Vector3_DotProduct::id );
	status = plugin.deregisterNode( fkn_Vector3_Normalize::id );
	status = plugin.deregisterNode( fkn_Vector3_RotateByQuaternion::id );
	status = plugin.deregisterNode( fkn_Vector3_MultiplyByMatrix::id );
	status = plugin.deregisterNode( fkn_Vector3_Compare::id );
	status = plugin.deregisterNode( fkn_Vector3_Switch::id );
	status = plugin.deregisterNode( fkn_Int_Modulo::id );
	status = plugin.deregisterNode( fkn_Trigonometry::id );
	status = plugin.deregisterNode( fkn_Float_Add::id );
	status = plugin.deregisterNode( fkn_Float_Add_Array::id );
	status = plugin.deregisterNode( fkn_Float_Sub::id );
	status = plugin.deregisterNode( fkn_Float_Multiply::id );
	status = plugin.deregisterNode( fkn_Float_Divide::id );
	status = plugin.deregisterNode( fkn_Float_SquareRoot::id );
	status = plugin.deregisterNode( fkn_Float_Pow::id );
	status = plugin.deregisterNode( fkn_Float_Log::id );
	status = plugin.deregisterNode( fkn_Float_Exponent::id );
	status = plugin.deregisterNode( fkn_Float_Round::id );
	status = plugin.deregisterNode( fkn_Float_Negate::id );
	status = plugin.deregisterNode( fkn_Float_Abs::id );
	status = plugin.deregisterNode( fkn_Float_Clamp::id );
	status = plugin.deregisterNode( fkn_Float_Rescale::id );
	status = plugin.deregisterNode( fkn_Float_Compare::id );
	status = plugin.deregisterNode( fkn_Float_Switch::id );
	status = plugin.deregisterNode( fkn_Float_LinearInterpolation::id );
	status = plugin.deregisterNode( fkn_Matrix4_Multiply::id );
	status = plugin.deregisterNode( fkn_Matrix4_Invert::id );
	status = plugin.deregisterNode( fkn_Matrix4_Transpose::id );
	status = plugin.deregisterNode( fkn_Matrix4_TransposeInverse::id );
	status = plugin.deregisterNode( fkn_Matrix4_ToVector3::id );
	status = plugin.deregisterNode( fkn_Matrix4_FromVector3::id );
	status = plugin.deregisterNode( fkn_Matrix4_Switch::id );
	status = plugin.deregisterNode( fkn_Quat_Add::id );
	status = plugin.deregisterNode( fkn_Quat_Multiply::id );
	status = plugin.deregisterNode( fkn_Quat_Sub::id );
	status = plugin.deregisterNode( fkn_Quat_Conjugate::id );
	status = plugin.deregisterNode( fkn_Quat_Invert::id );
	status = plugin.deregisterNode( fkn_Quat_Normalize::id );
	status = plugin.deregisterNode( fkn_Quat_Negate::id );
	status = plugin.deregisterNode( fkn_Quat_Slerp::id );
	status = plugin.deregisterNode( fkn_Quat_DotProduct::id );
	status = plugin.deregisterNode( fkn_Quat_FromAxisAndAngle::id );
	status = plugin.deregisterNode( fkn_Quat_ToEuler::id );
	status = plugin.deregisterNode( fkn_Ik3BonesMemberNode::id );
	status = plugin.deregisterNode( fkn_IkPlanConstraint::id );
	status = plugin.deregisterNode( fkn_SRT_Constraint::id );
	status = plugin.deregisterNode( fkn_ChainOnNurbsSurfaceCns::id );
	status = plugin.deregisterNode( fkn_BlendRotCns::id );
	status = plugin.deregisterNode( fkn_ConvertToLocalSpace::id );
	status = plugin.deregisterNode( fkn_Matrix4_ToQuat::id );
	status = plugin.deregisterNode( fkn_Matrix4_Interpolation::id );
	status = plugin.deregisterNode( fkn_Ik2BonesMemberNode::id );
	status = plugin.deregisterNode( fkn_BoneRollsConstraint::id );
	status = plugin.deregisterNode( fkn_CompressionCompensator::id );
	status = plugin.deregisterNode( fkn_SpaceSwitchCns::id );
	status = plugin.deregisterNode( fkn_SpaceSwitchDynamicCns::id );
	status = plugin.deregisterNode( fkn_Bool_And_Array::id );
	status = plugin.deregisterNode( fkn_Bool_Or_Array::id );
	status = plugin.deregisterNode( fkn_Bool_SelectCase::id );
	status = plugin.deregisterNode( fkn_Float_SelectCase::id );
	status = plugin.deregisterNode( fkn_Vector3_SelectCase::id );
	status = plugin.deregisterNode( fkn_Bool_Invert::id );
	status = plugin.deregisterNode( fkn_VertexConstraintNode::id );
	status = plugin.deregisterNode( fkn_surfaceConstraintNode::id );
	status = plugin.deregisterNode( fkn_SplineMember2Node::id );

	if (!status) {
		status.perror("deregisterNode");
		return status;
	}

	return status;
}
