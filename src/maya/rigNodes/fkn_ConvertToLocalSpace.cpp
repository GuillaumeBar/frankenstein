/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_ConvertToLocalSpace.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_ConvertToLocalSpace.h"

// Unique plugins ID.
MTypeId fkn_ConvertToLocalSpace::id( fkn_ConvertToLocalSpace_ID );

// Input Attributes.
MObject	fkn_ConvertToLocalSpace::m_GlobalSpaceMatrix;
MObject	fkn_ConvertToLocalSpace::m_InvertParentMatrix;

//Out Attributes.
MObject	fkn_ConvertToLocalSpace::out_Position;
MObject	fkn_ConvertToLocalSpace::out_PosX;
MObject	fkn_ConvertToLocalSpace::out_PosY;
MObject	fkn_ConvertToLocalSpace::out_PosZ;
MObject	fkn_ConvertToLocalSpace::out_Rotation;
MObject	fkn_ConvertToLocalSpace::out_RotX;
MObject	fkn_ConvertToLocalSpace::out_RotY;
MObject	fkn_ConvertToLocalSpace::out_RotZ;
MObject	fkn_ConvertToLocalSpace::out_Scale;
MObject	fkn_ConvertToLocalSpace::out_SclX;
MObject	fkn_ConvertToLocalSpace::out_SclY;
MObject	fkn_ConvertToLocalSpace::out_SclZ;

//Constructor.
fkn_ConvertToLocalSpace::fkn_ConvertToLocalSpace(){

};

//Destructor.
fkn_ConvertToLocalSpace::~fkn_ConvertToLocalSpace(){

};

//Maya Creator.
void* fkn_ConvertToLocalSpace::creator(){
	return new fkn_ConvertToLocalSpace();
};

//Maya Initialize.
MStatus fkn_ConvertToLocalSpace::initialize(){

	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_GlobalSpaceMatrix = mAttr.create("GlobalSpaceMatrix", "GlobalSpaceMatrix");
	mAttr.setStorable(true);
	stat = addAttribute(m_GlobalSpaceMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_InvertParentMatrix = mAttr.create("ParentInvertMatrix", "ParentInvertMatrix");
	mAttr.setStorable(true);
	stat = addAttribute(m_InvertParentMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_PosX = nAttr.create("OutPosX", "OutPosX", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_PosY = nAttr.create("OutPosY", "OutPosY", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_PosZ = nAttr.create("OutPosZ", "OutPosZ", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_Position = nAttr.create("Position", "Position", out_PosX, out_PosY, out_PosZ, &stat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Position );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_RotX = uAttr.create("OutRotX", "OutRotX", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_RotY = uAttr.create("OutRotY", "OutRotY", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_RotZ = uAttr.create("OutRotZ", "OutRotZ", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_Rotation = nAttr.create("Rotation", "Rotation", out_RotX, out_RotY, out_RotZ, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	stat = addAttribute( out_Rotation );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_SclX = nAttr.create("OutSclX", "OutSclX", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_SclY = nAttr.create("OutSclY", "OutSclY", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_SclZ = nAttr.create("OutSclZ", "OutSclZ", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_Scale = nAttr.create("scale", "Scale", out_SclX, out_SclY, out_SclZ, &stat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	stat = addAttribute( out_Scale );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_GlobalSpaceMatrix, out_Position);
	stat = attributeAffects( m_GlobalSpaceMatrix, out_Rotation);
	stat = attributeAffects( m_GlobalSpaceMatrix, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_InvertParentMatrix, out_Position);
	stat = attributeAffects( m_InvertParentMatrix, out_Rotation);
	stat = attributeAffects( m_InvertParentMatrix, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};


MStatus fkn_ConvertToLocalSpace::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.	
	MDataHandle		globalSpaceMatrixData = in_Data.inputValue(m_GlobalSpaceMatrix, &returnStatus);
	MMatrix			globalSpaceMatrix = globalSpaceMatrixData.asMatrix();

	MDataHandle		invertParentMatrixData = in_Data.inputValue(m_InvertParentMatrix, &returnStatus);
	MMatrix			invertParentMatrix = invertParentMatrixData.asMatrix();

	if ( returnStatus == MS::kSuccess ){	

		if (in_Plug == out_Position || in_Plug == out_PosX || in_Plug == out_PosY || in_Plug == out_PosZ || in_Plug == out_Rotation || in_Plug == out_RotX || in_Plug == out_RotY || in_Plug == out_RotZ || in_Plug == out_SclX || in_Plug == out_SclY || in_Plug == out_SclZ || in_Plug == out_Scale){
			// Convert Data to fkn.
			Matrix4x4 fknGloblaMat, fknInvParMat;
			for(unsigned int iRow=0; iRow<4; iRow++){
				for(unsigned int iCol=0; iCol<4; iCol++){
					fknGloblaMat.SetValue(iRow, iCol, globalSpaceMatrix(iRow, iCol));
					fknInvParMat.SetValue(iRow, iCol, invertParentMatrix(iRow, iCol));
				}
			}

			// Convert to local matrix.
			Matrix4x4 finaleMat;
			finaleMat.Mul(fknGloblaMat, fknInvParMat);

			// Extract the scale.
			float sclX, sclY, sclZ;
			finaleMat.GetScale(sclX, sclY, sclZ);
			
			// Extract the rotation.
			Matrix3x3 extractRot;
			float sclFactor;
			for(unsigned int iRow=0; iRow<3; iRow++){
				for(unsigned int iCol=0; iCol<3; iCol++){
					// Remove the scale.
					if(iRow == 0){
						sclFactor = sclX;
					}else if(iRow == 1){
						sclFactor = sclY;
					}else{
						sclFactor = sclZ;
					}
					extractRot.SetValue(iRow, iCol, finaleMat.GetValue(iRow, iCol)/sclFactor);
				}
			}

			// Convert Rotation to Maya
			float eulX, eulY, eulZ;
			extractRot.ToEuler(eulX, eulY, eulZ, 1);
			MVector eulerRot(eulX, eulY, eulZ);

			//Set the Maya output data.
			MDataHandle	outPos = in_Data.outputValue(out_Position, &returnStatus);
			outPos.set(finaleMat.GetValue(3, 0), finaleMat.GetValue(3, 1), finaleMat.GetValue(3, 2));
			outPos.setClean();

			MDataHandle	outRot = in_Data.outputValue(out_Rotation, &returnStatus);
			outRot.set(eulerRot);
			outRot.setClean();

			MDataHandle	outScl = in_Data.outputValue(out_Scale, &returnStatus);
			outScl.set(sclX, sclY, sclZ);
			outScl.setClean();

		}else{
			return MS::kUnknownParameter;
		}
	}else{
		MGlobal::displayError( "Node fkn_SplineMember cannot get value.\n" );
	}

	return returnStatus;
};