/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_surfaceConstraintNode.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_surfaceConstraintNode.h"


// Unique plugins ID.
MTypeId     fkn_surfaceConstraintNode::id( fkn_surfaceConstraintNode_ID );

// Input Attributes.
MObject		fkn_surfaceConstraintNode::m_mesh;
MObject		fkn_surfaceConstraintNode::m_arraySurfaceData;
MObject 	fkn_surfaceConstraintNode::m_faceID;
MObject 	fkn_surfaceConstraintNode::m_faceU;
MObject 	fkn_surfaceConstraintNode::m_faceV;
MObject		fkn_surfaceConstraintNode::m_vertexIDX;
MObject		fkn_surfaceConstraintNode::m_vertexIDZ;

//Out Attributes.
MObject		fkn_surfaceConstraintNode::m_outPosX;
MObject		fkn_surfaceConstraintNode::m_outPosY;
MObject		fkn_surfaceConstraintNode::m_outPosZ;
MObject		fkn_surfaceConstraintNode::m_outArrayPos;
MObject		fkn_surfaceConstraintNode::m_outRotX;
MObject		fkn_surfaceConstraintNode::m_outRotY;
MObject		fkn_surfaceConstraintNode::m_outRotZ;
MObject		fkn_surfaceConstraintNode::m_outArrayEuler;


//Constructor.
fkn_surfaceConstraintNode::fkn_surfaceConstraintNode()
{

};

//Destructor.
fkn_surfaceConstraintNode::~fkn_surfaceConstraintNode()
{

};

//Maya Creator.
void* fkn_surfaceConstraintNode::creator()
{
	return new fkn_surfaceConstraintNode();
};

//Maya Initialize.
MStatus fkn_surfaceConstraintNode::initialize()
{
	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;
	MFnCompoundAttribute	cAttr;
	MFnTypedAttribute		tAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_mesh = tAttr.create("inMesh", "inMesh", MFnData::kMesh);
	stat = addAttribute(m_mesh);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_faceID = nAttr.create("FaceID", "FaceID", MFnNumericData::kInt, -1, &stat );
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_faceID);

	m_faceU = nAttr.create("FaceU", "FaceU", MFnNumericData::kFloat, 50.0f, &stat );
	nAttr.setMin(0.0f);
	nAttr.setMax(100.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_faceU);

	m_faceV = nAttr.create("FaceV", "FaceV", MFnNumericData::kFloat, 50.0f, &stat );
	nAttr.setMin(0.0f);
	nAttr.setMax(100.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_faceV);

	m_vertexIDX = nAttr.create("VertexIDX", "VertexIDX", MFnNumericData::kInt, -1, &stat );
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_vertexIDX);

	m_vertexIDZ = nAttr.create("VertexIDZ", "VertexIDZ", MFnNumericData::kInt, -1, &stat );
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_vertexIDZ);

	m_arraySurfaceData = cAttr.create("ArraySurfaceData", "ArraySurfaceData");
	cAttr.addChild(m_faceID);
	cAttr.addChild(m_faceU);
	cAttr.addChild(m_faceV);
	cAttr.addChild(m_vertexIDX);
	cAttr.addChild(m_vertexIDZ);
	cAttr.setStorable(true);
	cAttr.setKeyable(true);
	cAttr.setArray(true);
	stat = addAttribute(m_arraySurfaceData);

	//Define the output attribut.
	m_outPosX = nAttr.create("PositionX", "PositionX", MFnNumericData::kDouble, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	m_outPosY = nAttr.create("PositionY", "PositionY", MFnNumericData::kDouble, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	m_outPosZ = nAttr.create("PositionZ", "PositionZ", MFnNumericData::kDouble, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	m_outArrayPos = nAttr.create("ArrayPosition", "ArrayPosition", m_outPosX,
																		m_outPosY,
																		m_outPosZ, &stat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	nAttr.setArray(true);
	nAttr.setUsesArrayDataBuilder(true);
	stat = addAttribute( m_outArrayPos );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_outRotX = uAttr.create("RotationX", "RotationX", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	m_outRotY = uAttr.create("RotationY", "RotationY", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	m_outRotZ = uAttr.create("RotationZ", "RotationZ", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	m_outArrayEuler = nAttr.create("ArrayRotation", "ArrayRotation", m_outRotX,
																		m_outRotY,
																		m_outRotZ, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	nAttr.setArray(true);
	nAttr.setUsesArrayDataBuilder(true);
	stat = addAttribute( m_outArrayEuler );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_mesh, m_outArrayPos);
	stat = attributeAffects( m_mesh, m_outArrayEuler);

	stat = attributeAffects( m_arraySurfaceData, m_outArrayPos);
	stat = attributeAffects( m_arraySurfaceData, m_outArrayEuler);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_surfaceConstraintNode::compute(const MPlug& in_Plug, MDataBlock& in_Data)
{
	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	// Get the in mesh geometry data.
	MDataHandle inMeshData = in_Data.inputValue(m_mesh, &returnStatus);
	MObject		inMesh = inMeshData.asMesh();
	MFnMesh		meshFn(inMesh, &returnStatus);

	// Get the array input for vertex data.
	MArrayDataHandle aSurfaceDataData = in_Data.inputArrayValue(m_arraySurfaceData, &returnStatus);

	if ( returnStatus == MS::kSuccess ){

		// Check if the output port are plugged.
		if (in_Plug == m_outArrayPos || in_Plug == m_outArrayEuler){

			// Get the number of input face Data.
			unsigned int arrayCount = aSurfaceDataData.elementCount();

			// Get the position ouput array builder.
			MArrayDataBuilder	outArrayPosition ( m_outArrayPos, arrayCount, &returnStatus );
			MDataHandle			outSetPosition;
			// Get the rotation ouput array builder.
			MArrayDataBuilder	outArrayRotation ( m_outArrayEuler, arrayCount, &returnStatus);
			MDataHandle			outSetRotation;

			MVector				toEuler, toPosition;
			MPoint				pPos, posX, posZ;
			MVector				axisX, axisY, axisZ;
			MMatrix 			finalTransformation;
			MEulerRotation		finalEulerRotation;
			int iFaceID, iVertexIDX, iVertexIDZ, numberVertices, numberFaces;
			float faceU, faceV, invFaceU, invFaceV, addUV;
			float p0w, p1w, p2w, p3w, cumulw;
			MPoint p0, p1, p2, p3;
			MIntArray listIDs;

			// Loop over the input vertex data array.
			for ( int iData=0; iData<arrayCount; iData++ )
			{
				// Get the vertex ID data.
				aSurfaceDataData.jumpToElement(iData);
				iFaceID = aSurfaceDataData.inputValue(&returnStatus).child(m_faceID).asShort();
				faceU = aSurfaceDataData.inputValue(&returnStatus).child(m_faceU).asFloat();
				faceV = aSurfaceDataData.inputValue(&returnStatus).child(m_faceV).asFloat();
				invFaceU = 100.0f - faceU;
				invFaceV = 100.0f - faceV;
				iVertexIDX = aSurfaceDataData.inputValue(&returnStatus).child(m_vertexIDX).asShort();
				iVertexIDZ = aSurfaceDataData.inputValue(&returnStatus).child(m_vertexIDZ).asShort();

				// Get the number of face of the in mesh.
				numberFaces = meshFn.numPolygons();
				numberVertices = meshFn.numVertices();

				if( iFaceID > -1 && iFaceID < numberFaces){
					// Get the number of point of the face.
					meshFn.getPolygonVertices(iFaceID, listIDs);
					// Check if the number is equal to 4.
					if(listIDs.length() == 4){
						addUV = faceU + faceV;
						if( addUV > 100.0f){
							p1w = faceU * invFaceV * 0.01f;
							p2w = (faceU * faceV - invFaceU * invFaceV) * 0.01f;
							p3w = invFaceU * faceV * 0.01f;

							cumulw = p1w + p2w + p3w;

							meshFn.getPoint(listIDs[1], p1, MSpace::kWorld);
							meshFn.getPoint(listIDs[2], p2, MSpace::kWorld);
							meshFn.getPoint(listIDs[3], p3, MSpace::kWorld);

							pPos = p1 * (p1w / cumulw)  + p2 * (p2w / cumulw) + p3 * (p3w / cumulw);
						}else{
							p0w = (invFaceU * invFaceV - faceU * faceV )* 0.01f;
							p1w = faceU * invFaceV * 0.01f;
							p3w = invFaceU * faceV * 0.01f;

							cumulw = p0w + p1w + p3w;

							meshFn.getPoint(listIDs[0], p0, MSpace::kWorld);
							meshFn.getPoint(listIDs[1], p1, MSpace::kWorld);
							meshFn.getPoint(listIDs[3], p3, MSpace::kWorld);

							pPos = p0 * (p0w / cumulw) + p1 * (p1w / cumulw) + p3 * (p3w / cumulw);
						}
					}else{
						pPos.x = 0.0f;
						pPos.y = 0.0f;
						pPos.z = 0.0f;
					}

					if(iVertexIDX > -1 && iVertexIDX < numberVertices && iVertexIDZ > -1 && iVertexIDZ < numberVertices){
						// Check if we need to compute the rotation base on the X and Z reference vertex.
						// Get the posY, posZ position.
						meshFn.getPoint(iVertexIDX, posX, MSpace::kWorld);
						meshFn.getPoint(iVertexIDZ, posZ, MSpace::kWorld);

						// Compute the axisY, axisZ.
						axisX = posX - pPos;
						axisZ = posZ - pPos;
						axisY = axisZ ^ axisX;
						axisZ = axisX ^ axisY;

						// Normalize the axis.
						axisX.normalize();
						axisY.normalize();
						axisZ.normalize();

						// Convert to matrix.
						finalTransformation(0,0) = axisX.x;
						finalTransformation(0,1) = axisX.y;
						finalTransformation(0,2) = axisX.z;
						finalTransformation(0,3) = 0.0;

						finalTransformation(1,0) = axisY.x;
						finalTransformation(1,1) = axisY.y;
						finalTransformation(1,2) = axisY.z;
						finalTransformation(1,3) = 0.0;

						finalTransformation(2,0) = axisZ.x;
						finalTransformation(2,1) = axisZ.y;
						finalTransformation(2,2) = axisZ.z;
						finalTransformation(2,3) = 0.0;

						finalTransformation(3,0) = pPos.x;
						finalTransformation(3,1) = pPos.y;
						finalTransformation(3,2) = pPos.z;
						finalTransformation(3,3) = 1.0;

						// Extract the Euler vector.
						finalEulerRotation = finalTransformation;
						toEuler = finalEulerRotation.asVector();

					}else if(iVertexIDX > -1 && iVertexIDX < numberVertices && iVertexIDZ < 0){
						// Check if we need to compute the rotation base on the X and vertex normal.
						// Get the reference point X.
						meshFn.getPoint(iVertexIDX, posX, MSpace::kWorld);
						// Get the vertex normal.
						meshFn.getPolygonNormal(iFaceID, axisY, MSpace::kWorld);
						// Compute the axisY, axisZ.
						axisX = posX - pPos;
						axisZ = axisX ^ axisY;
						axisX = axisY ^ axisZ;
						// Normalize the axis.
						axisX.normalize();
						axisY.normalize();
						axisZ.normalize();
						// Convert to matrix.
						finalTransformation(0,0) = axisX.x;
						finalTransformation(0,1) = axisX.y;
						finalTransformation(0,2) = axisX.z;
						finalTransformation(0,3) = 0.0;

						finalTransformation(1,0) = axisY.x;
						finalTransformation(1,1) = axisY.y;
						finalTransformation(1,2) = axisY.z;
						finalTransformation(1,3) = 0.0;

						finalTransformation(2,0) = axisZ.x;
						finalTransformation(2,1) = axisZ.y;
						finalTransformation(2,2) = axisZ.z;
						finalTransformation(2,3) = 0.0;

						finalTransformation(3,0) = pPos.x;
						finalTransformation(3,1) = pPos.y;
						finalTransformation(3,2) = pPos.z;
						finalTransformation(3,3) = 1.0;
						// Extract the Euler vector.
						finalEulerRotation = finalTransformation;
						toEuler = finalEulerRotation.asVector();
					}
					else{
					toEuler.x = 0.0;
					toEuler.y = 0.0;
					toEuler.z = 0.0;
					}
				}

				//Get Position
				outSetPosition = outArrayPosition.addElement(iData);
				outSetPosition.set(pPos.x,
									pPos.y,
									pPos.z);

				//Get Rotation.
				outSetRotation = outArrayRotation.addElement(iData);
				outSetRotation.set(toEuler);
			}

			MArrayDataHandle arrayOutPosition = in_Data.outputArrayValue(m_outArrayPos, &returnStatus);
			arrayOutPosition.set ( outArrayPosition );
			arrayOutPosition.setAllClean();
			
			MArrayDataHandle arrayOutRotation = in_Data.outputArrayValue(m_outArrayEuler, &returnStatus);
			arrayOutRotation.set ( outArrayRotation );
			arrayOutRotation.setAllClean();
		}
		else
		{
			return MS::kUnknownParameter;
		}
	}
	else
	{
		MGlobal::displayError( "Node fkn_VertexConstraintNode cannot get value.\n" );
	}

	return returnStatus;
};