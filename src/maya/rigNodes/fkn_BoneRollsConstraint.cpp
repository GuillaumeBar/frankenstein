/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_BoneRollsConstraint.cpp
@author Guillaume Baratte
@date 2016-05-31
*/

#include "fkn_BoneRollsConstraint.h"
#include <iostream>
using namespace std;


// Unique plugins ID.
MTypeId     fkn_BoneRollsConstraint::id( fkn_BoneRollsConstraintID );

// Input Attributes.
MObject		fkn_BoneRollsConstraint::m_RollsNumber;
MObject		fkn_BoneRollsConstraint::m_AxeAlign;
MObject		fkn_BoneRollsConstraint::m_Symmetrize;
MObject		fkn_BoneRollsConstraint::m_Bone1Matrix;
MObject		fkn_BoneRollsConstraint::m_Bone2Matrix;
MObject		fkn_BoneRollsConstraint::m_StartDistribution;
MObject		fkn_BoneRollsConstraint::m_EndDistribution;
MObject		fkn_BoneRollsConstraint::m_StartRollsOffset;
MObject		fkn_BoneRollsConstraint::m_EndRollsOffset;
MObject		fkn_BoneRollsConstraint::m_ArrayRollsBlend;

//Out Attributes.
MObject		fkn_BoneRollsConstraint::out_ArrayRollsMatrix;

//Constructor.
fkn_BoneRollsConstraint::fkn_BoneRollsConstraint(){

};

//Destructor.
fkn_BoneRollsConstraint::~fkn_BoneRollsConstraint(){

};

//Maya Creator.
void* fkn_BoneRollsConstraint::creator(){
	return new fkn_BoneRollsConstraint();
};

//Help Function.
void fkn_BoneRollsConstraint::ConvertMatrix4x4ToMMatrix(Matrix4x4& in_Matrix4x4, MMatrix& out_MMatrix){
	for(unsigned int iRow=0; iRow<4; iRow++){
		for(unsigned int iCol=0; iCol<4; iCol++){
			out_MMatrix(iRow, iCol) = (double)in_Matrix4x4.GetValue(iRow,iCol);
		}
	}
};

void fkn_BoneRollsConstraint::ConvertMMatrixToMatrix4x4(MMatrix& in_MMatrix, Matrix4x4& out_Matrix4x4){
	for(unsigned int iRow=0; iRow<4; iRow++){
		for(unsigned int iCol=0; iCol<4; iCol++){
			out_Matrix4x4.SetValue(iRow, iCol, (float)in_MMatrix(iRow,iCol));
		}
	}
};

//Maya Initialize.
MStatus fkn_BoneRollsConstraint::initialize(){

	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 
	m_RollsNumber = nAttr.create("NumberRoll", "NumberRoll", MFnNumericData::kInt, 3, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	stat = addAttribute(m_RollsNumber);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_AxeAlign = eAttr.create("AxeAlign", "AxeAlign", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Axe X", 0);
	eAttr.addField("Axe Y", 1);
	eAttr.addField("Axe Z", 2);
	eAttr.addField("Axe -X", 3);
	eAttr.addField("Axe -Y", 4);
	eAttr.addField("Axe -Z", 5);

	eAttr.setStorable(true);
	stat = addAttribute(m_AxeAlign);
	CHECK_MSTATUS_AND_RETURN_IT(stat)

	m_Symmetrize = eAttr.create("SymBonesMatrix", "SymBonesMatrix", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No Symmetry", 0);
	eAttr.addField("Symmetry", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_Symmetrize);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_StartDistribution = nAttr.create("StartDistribution", "StartDistribution", MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_StartDistribution);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	
	m_EndDistribution = nAttr.create("EndDistribution", "EndDistribution", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_EndDistribution);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_StartRollsOffset = nAttr.create("StartRollOffset", "StartRollOffset", MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(-360.0f);
	nAttr.setMax(360.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_StartRollsOffset);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_EndRollsOffset = nAttr.create("EndRollOffset", "EndRollOffset", MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(-360.0f);
	nAttr.setMax(360.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_EndRollsOffset);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayRollsBlend = rAttr.createCurveRamp("FilterRotationBlend", "FilterRotationBlend", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_ArrayRollsBlend);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_Bone1Matrix = mAttr.create("Bone1WorldMatrix", "Bone1WorldMatrix");
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	mAttr.setKeyable(false);
	mAttr.setStorable(false);
	stat = addAttribute(m_Bone1Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_Bone2Matrix = mAttr.create("Bone2WorldMatrix", "Bone2WorldMatrix");
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	mAttr.setKeyable(false);
	mAttr.setStorable(false);
	stat = addAttribute(m_Bone2Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_ArrayRollsMatrix = mAttr.create("OutRollWorldMatrix", "OutRollWorldMatrix");
	mAttr.setStorable(false);
	mAttr.setWritable(false);
	mAttr.setArray(true);
	mAttr.setUsesArrayDataBuilder(true);
	stat = addAttribute( out_ArrayRollsMatrix );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_RollsNumber, out_ArrayRollsMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_AxeAlign, out_ArrayRollsMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_Symmetrize, out_ArrayRollsMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_StartDistribution, out_ArrayRollsMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_EndDistribution, out_ArrayRollsMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_StartRollsOffset, out_ArrayRollsMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_EndRollsOffset, out_ArrayRollsMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_ArrayRollsBlend, out_ArrayRollsMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_Bone1Matrix, out_ArrayRollsMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_Bone2Matrix, out_ArrayRollsMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_BoneRollsConstraint::compute(const MPlug& in_Plug, MDataBlock& in_Data){
	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		rollNumberData = in_Data.inputValue(m_RollsNumber, &returnStatus);
	int rollNumber = rollNumberData.asInt();
			
	MDataHandle		axeAlignData = in_Data.inputValue(m_AxeAlign, &returnStatus);
	int axeAlign = axeAlignData.asShort();

	MDataHandle		symmetrizeData = in_Data.inputValue(m_Symmetrize, &returnStatus);
	int symmetrize = symmetrizeData.asShort();

	MDataHandle		startDistData = in_Data.inputValue(m_StartDistribution, &returnStatus);
	float startDist = startDistData.asFloat();

	MDataHandle		endDistData = in_Data.inputValue(m_EndDistribution, &returnStatus);
	float endDist = endDistData.asFloat();

	MDataHandle		startOffsetData = in_Data.inputValue(m_StartRollsOffset, &returnStatus);
	float startOffset = startOffsetData.asFloat();

	MDataHandle		endOffsetData = in_Data.inputValue(m_EndRollsOffset, &returnStatus);
	float endOffset = endOffsetData.asFloat();

	MObject			thisNode = thisMObject();
	MRampAttribute	filterAxeAlignData ( thisNode, m_ArrayRollsBlend, &returnStatus );

	MDataHandle		bone1MatrixData = in_Data.inputValue(m_Bone1Matrix, &returnStatus);
	MMatrix			bone1Matrix = bone1MatrixData.asMatrix();

	MDataHandle		bone2MatrixData = in_Data.inputValue(m_Bone2Matrix, &returnStatus);
	MMatrix			bone2Matrix = bone2MatrixData.asMatrix();

	if ( returnStatus == MS::kSuccess ){
		//Convert Maya Data to Frankenstein Data.
		Matrix4x4 fknBone1, fknBone2;
		DataArray<float> fknRotFilter(rollNumber);
		// Convert the bone's matrix.
		ConvertMMatrixToMatrix4x4(bone1Matrix, fknBone1);
		ConvertMMatrixToMatrix4x4(bone2Matrix, fknBone2);
		// Convert the rotation filter.
		float curvePos, currentFilter;
		for(int iRol=0; iRol<rollNumber; iRol++){
			curvePos = (float)iRol / (float)(rollNumber-1);
			filterAxeAlignData.getValueAtPosition(curvePos, currentFilter, &returnStatus);
			fknRotFilter[iRol] = currentFilter;
		}
		
		//Init the frankenstein BoneRollsConstraint.
		BoneRollsConstraint	fknBoneRollsCns(rollNumber, fknBone1, fknBone2, startDist,
											endDist, startOffset, endOffset, fknRotFilter, axeAlign, symmetrize);

		//Compute the frankenstein BoneRollsConstraint.
		fknBoneRollsCns.Compute();
		
		if (in_Plug == out_ArrayRollsMatrix){

			MArrayDataBuilder	outArrayRollMatrix ( out_ArrayRollsMatrix, rollNumber, &returnStatus );
			Matrix4x4			fknMat;
			MMatrix				mayaMat;
			MDataHandle			outSetMatrix;

			// Loop over the roll.
			for(int iRol=0; iRol<rollNumber; iRol++){
				// Get the current roll matrix.
				fknMat = fknBoneRollsCns.GetAt(iRol);
				ConvertMatrix4x4ToMMatrix(fknMat, mayaMat);
				outSetMatrix = outArrayRollMatrix.addElement(iRol);
				outSetMatrix.set (mayaMat);
			}

			// Update the output matrix maya array.
			MArrayDataHandle arrayOutRollMatrix = in_Data.outputArrayValue(out_ArrayRollsMatrix, &returnStatus);
			arrayOutRollMatrix.set ( outArrayRollMatrix );
			arrayOutRollMatrix.setAllClean();
		}else{
			return MS::kUnknownParameter;
		}
	}else{
		MGlobal::displayError( "Node fkn_BoneRollsConstraint cannot get value.\n" );
	}

	return returnStatus;
};