/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_ChainOnNurbsSurfaceCns.cpp
@author Guillaume Baratte
@date 2016-04-28
*/

#include "fkn_ChainOnNurbsSurfaceCns.h"


// Unique plugins ID.
MTypeId     fkn_ChainOnNurbsSurfaceCns::id( fkn_ChainOnNurbsSurfaceCns_ID );

// Input Attributes.
MObject		fkn_ChainOnNurbsSurfaceCns::m_NurbsSurface;
MObject		fkn_ChainOnNurbsSurfaceCns::m_RefOrientMat1;
MObject		fkn_ChainOnNurbsSurfaceCns::m_RefOrientMat2;
MObject 	fkn_ChainOnNurbsSurfaceCns::m_RefOrientBlend;
MObject		fkn_ChainOnNurbsSurfaceCns::m_CoordUP0;
MObject		fkn_ChainOnNurbsSurfaceCns::m_CoordVP0;
MObject		fkn_ChainOnNurbsSurfaceCns::m_CoordUP1;
MObject		fkn_ChainOnNurbsSurfaceCns::m_CoordVP1;
MObject		fkn_ChainOnNurbsSurfaceCns::m_CoordUP2;
MObject		fkn_ChainOnNurbsSurfaceCns::m_CoordVP2;
MObject		fkn_ChainOnNurbsSurfaceCns::m_CoordUP3;
MObject		fkn_ChainOnNurbsSurfaceCns::m_CoordVP3;

//Out Attributes.
MObject		fkn_ChainOnNurbsSurfaceCns::out_Mat0;
MObject		fkn_ChainOnNurbsSurfaceCns::out_Mat1;
MObject		fkn_ChainOnNurbsSurfaceCns::out_Mat2;
MObject		fkn_ChainOnNurbsSurfaceCns::out_Mat3;

//Constructor.
fkn_ChainOnNurbsSurfaceCns::fkn_ChainOnNurbsSurfaceCns()
{

};

//Destructor.
fkn_ChainOnNurbsSurfaceCns::~fkn_ChainOnNurbsSurfaceCns()
{

};

//Maya Creator.
void* fkn_ChainOnNurbsSurfaceCns::creator()
{
	return new fkn_ChainOnNurbsSurfaceCns();
};

//Maya Initialize.
MStatus fkn_ChainOnNurbsSurfaceCns::initialize()
{
	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;
	MFnTypedAttribute		tAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 
	m_NurbsSurface = tAttr.create("NurbsSurface", "NurbsSurface", MFnData::kNurbsSurface);
	stat = addAttribute(m_NurbsSurface);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_RefOrientMat1 = mAttr.create("RefOrientationMat1", "RefOrientationMat1");
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	stat = addAttribute(m_RefOrientMat1);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	
	m_RefOrientMat2 = mAttr.create("RefOrientationMat2", "RefOrientationMat2");
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	stat = addAttribute(m_RefOrientMat2);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_RefOrientBlend = nAttr.create("RefOrientationBlend", "RefOrientationBlend", MFnNumericData::kFloat, 0);
	nAttr.setSoftMax(1);
	nAttr.setMin(0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_RefOrientBlend);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_CoordUP0 = nAttr.create("P0UCoord", "P0UCoord", MFnNumericData::kFloat, 0);
	nAttr.setSoftMax(1);
	nAttr.setMin(0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_CoordUP0);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_CoordVP0 = nAttr.create("P0VCoord", "P0VCoord", MFnNumericData::kFloat, 0);
	nAttr.setSoftMax(1);
	nAttr.setMin(0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_CoordVP0);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_CoordUP1 = nAttr.create("P1UCoord", "P1UCoord", MFnNumericData::kFloat, 0);
	nAttr.setSoftMax(1);
	nAttr.setMin(0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_CoordUP1);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_CoordVP1 = nAttr.create("P1VCoord", "P1VCoord", MFnNumericData::kFloat, 0);
	nAttr.setSoftMax(1);
	nAttr.setMin(0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_CoordVP1);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_CoordUP2 = nAttr.create("P2UCoord", "P2UCoord", MFnNumericData::kFloat, 0);
	nAttr.setSoftMax(1);
	nAttr.setMin(0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_CoordUP2);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_CoordVP2 = nAttr.create("P2VCoord", "P2VCoord", MFnNumericData::kFloat, 0);
	nAttr.setSoftMax(1);
	nAttr.setMin(0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_CoordVP2);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_CoordUP3 = nAttr.create("P3UCoord", "P3UCoord", MFnNumericData::kFloat, 0);
	nAttr.setSoftMax(1);
	nAttr.setMin(0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_CoordUP3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_CoordVP3 = nAttr.create("P3VCoord", "P3VCoord", MFnNumericData::kFloat, 0);
	nAttr.setSoftMax(1);
	nAttr.setMin(0);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_CoordVP3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_Mat0 = mAttr.create("OutMat0", "OutMat0");
	mAttr.setWritable(false);
	mAttr.setStorable(false);
	stat = addAttribute(out_Mat0);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_Mat1 = mAttr.create("OutMat1", "OutMat1");
	mAttr.setWritable(false);
	mAttr.setStorable(false);
	stat = addAttribute(out_Mat1);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_Mat2 = mAttr.create("OutMat2", "OutMat2");
	mAttr.setWritable(false);
	mAttr.setStorable(false);
	stat = addAttribute(out_Mat2);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_Mat3 = mAttr.create("OutMat3", "OutMat3");
	mAttr.setWritable(false);
	mAttr.setStorable(false);
	stat = addAttribute(out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_NurbsSurface, out_Mat0);
	stat = attributeAffects( m_NurbsSurface, out_Mat1);
	stat = attributeAffects( m_NurbsSurface, out_Mat2);
	stat = attributeAffects( m_NurbsSurface, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_RefOrientMat1, out_Mat0);
	stat = attributeAffects( m_RefOrientMat1, out_Mat1);
	stat = attributeAffects( m_RefOrientMat1, out_Mat2);
	stat = attributeAffects( m_RefOrientMat1, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_RefOrientMat2, out_Mat0);
	stat = attributeAffects( m_RefOrientMat2, out_Mat1);
	stat = attributeAffects( m_RefOrientMat2, out_Mat2);
	stat = attributeAffects( m_RefOrientMat2, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_RefOrientBlend, out_Mat0);
	stat = attributeAffects( m_RefOrientBlend, out_Mat1);
	stat = attributeAffects( m_RefOrientBlend, out_Mat2);
	stat = attributeAffects( m_RefOrientBlend, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_CoordUP0, out_Mat0);
	stat = attributeAffects( m_CoordUP0, out_Mat1);
	stat = attributeAffects( m_CoordUP0, out_Mat2);
	stat = attributeAffects( m_CoordUP0, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_CoordVP0, out_Mat0);
	stat = attributeAffects( m_CoordVP0, out_Mat1);
	stat = attributeAffects( m_CoordVP0, out_Mat2);
	stat = attributeAffects( m_CoordVP0, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_CoordUP1, out_Mat0);
	stat = attributeAffects( m_CoordUP1, out_Mat1);
	stat = attributeAffects( m_CoordUP1, out_Mat2);
	stat = attributeAffects( m_CoordUP1, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_CoordVP1, out_Mat0);
	stat = attributeAffects( m_CoordVP1, out_Mat1);
	stat = attributeAffects( m_CoordVP1, out_Mat2);
	stat = attributeAffects( m_CoordVP1, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_CoordUP2, out_Mat0);
	stat = attributeAffects( m_CoordUP2, out_Mat1);
	stat = attributeAffects( m_CoordUP2, out_Mat2);
	stat = attributeAffects( m_CoordUP2, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_CoordVP2, out_Mat0);
	stat = attributeAffects( m_CoordVP2, out_Mat1);
	stat = attributeAffects( m_CoordVP2, out_Mat2);
	stat = attributeAffects( m_CoordVP2, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_CoordUP3, out_Mat0);
	stat = attributeAffects( m_CoordUP3, out_Mat1);
	stat = attributeAffects( m_CoordUP3, out_Mat2);
	stat = attributeAffects( m_CoordUP3, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_CoordVP3, out_Mat0);
	stat = attributeAffects( m_CoordVP3, out_Mat1);
	stat = attributeAffects( m_CoordVP3, out_Mat2);
	stat = attributeAffects( m_CoordVP3, out_Mat3);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_ChainOnNurbsSurfaceCns::compute(const MPlug& in_Plug, MDataBlock& in_Data)
{
	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle nurbsSurfaceData = in_Data.inputValue(m_NurbsSurface, &returnStatus);
	MObject surf = nurbsSurfaceData.asNurbsSurface();
	MFnNurbsSurface surFn(surf, &returnStatus);

	MDataHandle	refOrientMat1Data = in_Data.inputValue(m_RefOrientMat1, &returnStatus);
	MMatrix refOrientMat1 = refOrientMat1Data.asMatrix();

	MDataHandle	refOrientMat2Data = in_Data.inputValue(m_RefOrientMat2, &returnStatus);
	MMatrix refOrientMat2 = refOrientMat2Data.asMatrix();

	MDataHandle	refOrientBlendData = in_Data.inputValue(m_RefOrientBlend, &returnStatus);
	float refOrientBlend = refOrientBlendData.asFloat();

	MDataHandle	coorUP0Data = in_Data.inputValue(m_CoordUP0, &returnStatus);
	float coorUP0 = coorUP0Data.asFloat();

	MDataHandle	coorVP0Data = in_Data.inputValue(m_CoordVP0, &returnStatus);
	float coorVP0 = coorVP0Data.asFloat();

	MDataHandle	coorUP1Data = in_Data.inputValue(m_CoordUP1, &returnStatus);
	float coorUP1 = coorUP1Data.asFloat();

	MDataHandle	coorVP1Data = in_Data.inputValue(m_CoordVP1, &returnStatus);
	float coorVP1 = coorVP1Data.asFloat();

	MDataHandle	coorUP2Data = in_Data.inputValue(m_CoordUP2, &returnStatus);
	float coorUP2 = coorUP2Data.asFloat();

	MDataHandle	coorVP2Data = in_Data.inputValue(m_CoordVP2, &returnStatus);
	float coorVP2 = coorVP2Data.asFloat();

	MDataHandle	coorUP3Data = in_Data.inputValue(m_CoordUP3, &returnStatus);
	float coorUP3 = coorUP3Data.asFloat();

	MDataHandle	coorVP3Data = in_Data.inputValue(m_CoordVP3, &returnStatus);
	float coorVP3 = coorVP3Data.asFloat();

	if ( returnStatus == MS::kSuccess ){

		if (in_Plug == out_Mat0 || in_Plug == out_Mat1 || in_Plug == out_Mat2 || in_Plug == out_Mat3){

			Matrix4x4 fknP0Mat, fknP1Mat, fknP2Mat, fknP3Mat;
			Vector3D fknP0Pos, fknP1Pos, fknP2Pos, fknP3Pos;
			Vector3D fknP0N, fknP1N, fknP2N, fknP3N;

			// Get the axe Y of the reference orientation matrix.
			Vector3D refAxeY1(refOrientMat1(1,0), refOrientMat1(1,1), refOrientMat1(1,2));
			Vector3D refAxeY2(refOrientMat2(1,0), refOrientMat2(1,1), refOrientMat2(1,2));
			// Compute the interpolated axe Y.
			Vector3D interAxeY;
			float invrefOrientBlend = 1.0f - refOrientBlend;
			refAxeY1.SelfMulByFloat(invrefOrientBlend);
			refAxeY2.SelfMulByFloat(refOrientBlend);
			interAxeY.Add(refAxeY1, refAxeY2);

			// Get the position for the controlers.
			MPoint p0Pos, p1Pos, p2Pos, p3Pos;
			surFn.getPointAtParam(coorUP0, coorVP0, p0Pos, MSpace::kWorld);
			surFn.getPointAtParam(coorUP1, coorVP1, p1Pos, MSpace::kWorld);
			surFn.getPointAtParam(coorUP2, coorVP2, p2Pos, MSpace::kWorld);
			surFn.getPointAtParam(coorUP3, coorVP3, p3Pos, MSpace::kWorld);

			// Get the normal for the controllers.
			MVector p0N, p1N, p2N, p3N;
			p0N = surFn.normal(coorUP0, coorVP0, MSpace::kWorld);
			p1N = surFn.normal(coorUP1, coorVP1, MSpace::kWorld);
			p2N = surFn.normal(coorUP2, coorVP2, MSpace::kWorld);
			p3N = surFn.normal(coorUP3, coorVP3, MSpace::kWorld);

			// Convert the surface point to fkn vector.
			fknP0Pos.Set(p0Pos.x, p0Pos.y, p0Pos.z);
			fknP1Pos.Set(p1Pos.x, p1Pos.y, p1Pos.z);
			fknP2Pos.Set(p2Pos.x, p2Pos.y, p2Pos.z);
			fknP3Pos.Set(p3Pos.x, p3Pos.y, p3Pos.z);
			// Convert the surface normal to fkn vector.
			fknP0N.Set(p0N.x, p0N.y, p0N.z);
			fknP1N.Set(p1N.x, p1N.y, p1N.z);
			fknP2N.Set(p2N.x, p2N.y, p2N.z);
			fknP3N.Set(p3N.x, p3N.y, p3N.z);
			// Compute the out matrix axe.
			Vector3D p0AxeX, p0AxeY, p0AxeZ;
			Vector3D p1AxeX, p1AxeY, p1AxeZ;
			Vector3D p2AxeX, p2AxeY, p2AxeZ;
			Vector3D p3AxeX, p3AxeY, p3AxeZ;
			// Compute the axe Z.
			p0AxeZ.Sub(fknP1Pos, fknP0Pos);
			p1AxeZ.Sub(fknP2Pos, fknP1Pos);
			p2AxeZ.Sub(fknP3Pos, fknP2Pos);
			p3AxeZ.Sub(fknP3Pos, fknP2Pos);
			p0AxeZ.SelfNormalize();
			p1AxeZ.SelfNormalize();
			p2AxeZ.SelfNormalize();
			p3AxeZ.SelfNormalize();
			// Compute the axe X.
			p0AxeX.CrossProduct(fknP0N, p0AxeZ);
			p1AxeX.CrossProduct(fknP1N, p1AxeZ);
			p2AxeX.CrossProduct(fknP2N, p2AxeZ);
			p3AxeX.CrossProduct(fknP3N, p3AxeZ);
			p0AxeX.SelfNormalize();
			p1AxeX.SelfNormalize();
			p2AxeX.SelfNormalize();
			p3AxeX.SelfNormalize();
			// Compute the axe Y.
			p0AxeY.CrossProduct(p0AxeZ, p0AxeX);
			p1AxeY.CrossProduct(p1AxeZ, p1AxeX);
			p2AxeY.CrossProduct(p2AxeZ, p2AxeX);
			p3AxeY.CrossProduct(p3AxeZ, p3AxeX);
			p0AxeY.SelfNormalize();
			p1AxeY.SelfNormalize();
			p2AxeY.SelfNormalize();
			p3AxeY.SelfNormalize();
			// Reconstruct the final matrix.
			fknP0Mat.Set(	p0AxeX.GetX(), p0AxeX.GetY(), p0AxeX.GetZ(), 0.0f,
							p0AxeY.GetX(), p0AxeY.GetY(), p0AxeY.GetZ(), 0.0f,
							p0AxeZ.GetX(), p0AxeZ.GetY(), p0AxeZ.GetZ(), 0.0f,
							fknP0Pos.GetX(), fknP0Pos.GetY(), fknP0Pos.GetZ(), 1.0f);
			fknP1Mat.Set(	p1AxeX.GetX(), p1AxeX.GetY(), p1AxeX.GetZ(), 0.0f,
							p1AxeY.GetX(), p1AxeY.GetY(), p1AxeY.GetZ(), 0.0f,
							p1AxeZ.GetX(), p1AxeZ.GetY(), p1AxeZ.GetZ(), 0.0f,
							fknP1Pos.GetX(), fknP1Pos.GetY(), fknP1Pos.GetZ(), 1.0f);
			fknP2Mat.Set(	p2AxeX.GetX(), p2AxeX.GetY(), p2AxeX.GetZ(), 0.0f,
							p2AxeY.GetX(), p2AxeY.GetY(), p2AxeY.GetZ(), 0.0f,
							p2AxeZ.GetX(), p2AxeZ.GetY(), p2AxeZ.GetZ(), 0.0f,
							fknP2Pos.GetX(), fknP2Pos.GetY(), fknP2Pos.GetZ(), 1.0f);
			fknP3Mat.Set(	p3AxeX.GetX(), p3AxeX.GetY(), p3AxeX.GetZ(), 0.0f,
							p3AxeY.GetX(), p3AxeY.GetY(), p3AxeY.GetZ(), 0.0f,
							p3AxeZ.GetX(), p3AxeZ.GetY(), p3AxeZ.GetZ(), 0.0f,
							fknP3Pos.GetX(), fknP3Pos.GetY(), fknP3Pos.GetZ(), 1.0f);

			// Convert the fkn matrix to the maya matrix.
			MMatrix P0Mat, P1Mat, P2Mat, P3Mat;
			for(unsigned int iRow=0; iRow<4; iRow++){
				for(unsigned int iCol=0; iCol<4; iCol++){
					P0Mat(iRow, iCol) = (double)fknP0Mat.GetValue(iRow,iCol);
					P1Mat(iRow, iCol) = (double)fknP1Mat.GetValue(iRow,iCol);
					P2Mat(iRow, iCol) = (double)fknP2Mat.GetValue(iRow,iCol);
					P3Mat(iRow, iCol) = (double)fknP3Mat.GetValue(iRow,iCol);
				}
			}			

			// Update the ouput data.
			MDataHandle outMat0 = in_Data.outputValue(out_Mat0, &returnStatus);
			outMat0.set ( P0Mat );
			outMat0.setClean();

			MDataHandle outMat1 = in_Data.outputValue(out_Mat1, &returnStatus);
			outMat1.set ( P1Mat );
			outMat1.setClean();

			MDataHandle outMat2 = in_Data.outputValue(out_Mat2, &returnStatus);
			outMat2.set ( P2Mat );
			outMat2.setClean();

			MDataHandle outMat3 = in_Data.outputValue(out_Mat3, &returnStatus);
			outMat3.set ( P3Mat );
			outMat3.setClean();			

		}else{

			return MS::kUnknownParameter;
		}
	}else{

		MGlobal::displayError( "Node fkn_SplineMember cannot get value.\n" );
	}

	return returnStatus;
};