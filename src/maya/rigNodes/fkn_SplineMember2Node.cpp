/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_SplineMemberNode2.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_SplineMember2Node.h"


// Unique plugins ID.
MTypeId     fkn_SplineMember2Node::id( fkn_SplineMember2Node_ID );

// Input Attributes.
MObject		fkn_SplineMember2Node::m_ArrayControllers;
MObject 	fkn_SplineMember2Node::m_ArrayUserTangents;
MObject 	fkn_SplineMember2Node::m_NumberDeformers;
MObject 	fkn_SplineMember2Node::m_CurveResolution;
MObject 	fkn_SplineMember2Node::m_PositionType;
MObject 	fkn_SplineMember2Node::m_StartRange;
MObject 	fkn_SplineMember2Node::m_EndRange;
MObject 	fkn_SplineMember2Node::m_MoveRange;
MObject 	fkn_SplineMember2Node::m_RestLength;
MObject 	fkn_SplineMember2Node::m_StretchType;
MObject 	fkn_SplineMember2Node::m_MaxStretch;
MObject 	fkn_SplineMember2Node::m_MinStretch;
MObject 	fkn_SplineMember2Node::m_CurveType;
MObject 	fkn_SplineMember2Node::m_InterpolatedOutPosition;
MObject 	fkn_SplineMember2Node::m_rotationType;
MObject 	fkn_SplineMember2Node::m_scaleType;
MObject 	fkn_SplineMember2Node::m_alignType;
MObject 	fkn_SplineMember2Node::m_useAdditiveRotation;
MObject 	fkn_SplineMember2Node::m_addStartRotation;
MObject 	fkn_SplineMember2Node::m_addEndRotation;
MObject 	fkn_SplineMember2Node::m_inverseCurve;
MObject 	fkn_SplineMember2Node::m_globalScale;
MObject 	fkn_SplineMember2Node::m_compressionMaxScaleAxis1;
MObject 	fkn_SplineMember2Node::m_compressionMaxScaleAxis2;
MObject 	fkn_SplineMember2Node::m_stretchMaxScaleAxis1;
MObject 	fkn_SplineMember2Node::m_stretchMaxScaleAxis2;
MObject 	fkn_SplineMember2Node::m_blendToLock;
MObject 	fkn_SplineMember2Node::m_blendToLimited;
MObject 	fkn_SplineMember2Node::m_useUserTangents;
MObject 	fkn_SplineMember2Node::m_inTangentLength;
MObject 	fkn_SplineMember2Node::m_outTangentLength;
MObject 	fkn_SplineMember2Node::m_useLimitedSoftStretch;
MObject 	fkn_SplineMember2Node::m_stretchStartSoft;
MObject 	fkn_SplineMember2Node::m_compressionStartSoft;

MObject 	fkn_SplineMember2Node::m_ArrayPointDistribution;
MObject 	fkn_SplineMember2Node::m_ArrayPointRotation;
MObject 	fkn_SplineMember2Node::m_ArrayPointFollowTangent;
MObject 	fkn_SplineMember2Node::m_ArrayPointScale;
MObject 	fkn_SplineMember2Node::m_ArrayPointScaleCompressionAxis1;
MObject 	fkn_SplineMember2Node::m_ArrayPointScaleCompressionAxis2;
MObject 	fkn_SplineMember2Node::m_ArrayPointScaleStretchAxis1;
MObject 	fkn_SplineMember2Node::m_ArrayPointScaleStretchAxis2;

//Out Attributes.
MObject		fkn_SplineMember2Node::out_ArrayDefPosition;
MObject		fkn_SplineMember2Node::out_RotX;
MObject		fkn_SplineMember2Node::out_RotY;
MObject		fkn_SplineMember2Node::out_RotZ;
MObject		fkn_SplineMember2Node::out_ArrayDefRotEuler;
MObject		fkn_SplineMember2Node::out_ArrayDefScale;
MObject 	fkn_SplineMember2Node::out_localCurveLength;
MObject 	fkn_SplineMember2Node::out_globalCurveLength;

//Constructor.
fkn_SplineMember2Node::fkn_SplineMember2Node()
{

};

//Destructor.
fkn_SplineMember2Node::~fkn_SplineMember2Node()
{

};

//Maya Creator.
void* fkn_SplineMember2Node::creator()
{
	return new fkn_SplineMember2Node();
};


//Maya Initialize.
MStatus fkn_SplineMember2Node::initialize()
{
	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 
	m_NumberDeformers = nAttr.create("numberDeformer", "numberDeformer",
										MFnNumericData::kInt, 10, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(100);
	nAttr.setMin(3);
	nAttr.setStorable(true);
	stat = addAttribute(m_NumberDeformers);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_StartRange = nAttr.create("startRange", "startRange",
										MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_StartRange);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_EndRange = nAttr.create("endRange", "endRange",
										MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_EndRange);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_MoveRange = nAttr.create("moveRange", "moveRange",
										MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(-1.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_MoveRange);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayPointDistribution = rAttr.createCurveRamp("pointDistribution", "pointDistribution", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_ArrayPointDistribution);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_InterpolatedOutPosition = eAttr.create("outInterpolation", "outInterpolation", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No", 0);
	eAttr.addField("Active", 1);
	eAttr.setStorable(true);
	eAttr.setKeyable(true);
	stat = addAttribute(m_InterpolatedOutPosition);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_StretchType = eAttr.create("stretchType", "stretchType", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Free", 0);
	eAttr.addField("Limited", 1);
	eAttr.addField("Lock", 2);
	eAttr.addField("Blend", 3);
	eAttr.setStorable(true);
	eAttr.setKeyable(true);
	stat = addAttribute(m_StretchType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_blendToLock = nAttr.create("blendToLock", "blendToLock",
										MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_blendToLock);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_blendToLimited = nAttr.create("blendToLimited", "blendToLimited",
										MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_blendToLimited);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_MaxStretch = nAttr.create("maxStretch", "maxStretch",
										MFnNumericData::kFloat, 1.2f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_MaxStretch);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_MinStretch = nAttr.create("minStretch", "minStretch",
										MFnNumericData::kFloat, 0.9f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_MinStretch);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_useLimitedSoftStretch = eAttr.create("useLimitedSoftStretch", "useLimitedSoftStretch", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Hard", 0);
	eAttr.addField("Soft", 1);
	eAttr.setStorable(true);
	eAttr.setKeyable(true);
	stat = addAttribute(m_useLimitedSoftStretch);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_stretchStartSoft = nAttr.create("stretchStartSoft", "stretchStartSoft",
										MFnNumericData::kFloat, 0.5f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_stretchStartSoft);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_compressionStartSoft = nAttr.create("compressionStartSoft", "compressionStartSoft",
										MFnNumericData::kFloat, 0.5f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_compressionStartSoft);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_RestLength = nAttr.create("restLenght", "restLenght",
										MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_RestLength);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_globalScale = nAttr.create("globalScale", "globalScale",
										MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_globalScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat)

	m_compressionMaxScaleAxis1 = nAttr.create("compressionScaleAxis1", "compressionScaleAxis1",
										MFnNumericData::kFloat, 2.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_compressionMaxScaleAxis1);
	CHECK_MSTATUS_AND_RETURN_IT(stat)

	m_compressionMaxScaleAxis2 = nAttr.create("compressionScaleAxis2", "compressionScaleAxis2",
										MFnNumericData::kFloat, 2.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_compressionMaxScaleAxis2);
	CHECK_MSTATUS_AND_RETURN_IT(stat)

	m_stretchMaxScaleAxis1 = nAttr.create("stretchScaleAxis1", "stretchScaleAxis1",
										MFnNumericData::kFloat, 0.5f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_stretchMaxScaleAxis1);
	CHECK_MSTATUS_AND_RETURN_IT(stat)

	m_stretchMaxScaleAxis2 = nAttr.create("stretchScaleAxis2", "stretchScaleAxis2",
										MFnNumericData::kFloat, 0.5f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_stretchMaxScaleAxis2);
	CHECK_MSTATUS_AND_RETURN_IT(stat)

	m_CurveType = eAttr.create("curveType", "curveType", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Bezier", 0);
	eAttr.addField("Fit Bezier", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_CurveType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_useUserTangents = eAttr.create("useUserTangents", "useUserTangents", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Auto", 0);
	eAttr.addField("User", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_useUserTangents);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_inTangentLength = nAttr.create("inTangentLength", "inTangentLength",
										MFnNumericData::kFloat, 0.333f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_inTangentLength);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_outTangentLength = nAttr.create("outTangentLength", "outTangentLength",
										MFnNumericData::kFloat, 0.333f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_outTangentLength);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_PositionType = eAttr.create("positionType", "positionType", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Cubic", 0);
	eAttr.addField("Normalized", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_PositionType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_inverseCurve = eAttr.create("inverseCurveTangent", "inverseCurveTangent", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Positive", 0);
	eAttr.addField("Negative", 1);
	eAttr.setStorable(true);
	eAttr.setKeyable(true);
	stat = addAttribute(m_inverseCurve);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_CurveResolution = nAttr.create("curveResolution", "curveResolution",
										MFnNumericData::kInt, 50, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(100);
	nAttr.setMin(3);
	nAttr.setStorable(true);
	stat = addAttribute(m_CurveResolution);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_rotationType = eAttr.create("rotationType", "rotationType", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("First to Last", 0);
	eAttr.addField("All", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_rotationType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_scaleType = eAttr.create("scaleType", "scaleType", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("None", 0);
	eAttr.addField("Stretch", 1);
	eAttr.addField("First to Last", 2);
	eAttr.addField("All", 3);
	eAttr.setStorable(true);
	stat = addAttribute(m_scaleType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_alignType = eAttr.create("alignType", "alignType", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Axis X", 0);
	eAttr.addField("Axis Y", 1);
	eAttr.addField("Axis Z", 2);
	eAttr.setStorable(true);
	stat = addAttribute(m_alignType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_useAdditiveRotation = eAttr.create("useAdditiveRotation", "useAdditiveRotation", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No", 0);
	eAttr.addField("Active", 1);
	eAttr.setStorable(true);
	eAttr.setKeyable(true);
	stat = addAttribute(m_useAdditiveRotation);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_addStartRotation = nAttr.create("addStartAngle", "addStartAngle",
										MFnNumericData::kFloat, 0.0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(-720.0f);
	nAttr.setMax(720.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_addStartRotation);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_addEndRotation = nAttr.create("addEndAngle", "addEndAngle",
										MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(-720.0f);
	nAttr.setMax(720.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_addEndRotation);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayPointRotation = rAttr.createCurveRamp("rotationDistribution",
													"rotationDistribution", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_ArrayPointRotation);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayPointFollowTangent = rAttr.createCurveRamp("followTangentDistribution",
														"followTangentDistribution", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_ArrayPointFollowTangent);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayPointScale = rAttr.createCurveRamp("scaleDistribution",
														"scaleDistribution", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_ArrayPointScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayPointScaleCompressionAxis1 = rAttr.createCurveRamp("scaleCompressionCurveAxis1",
															"scaleCompressionCurveAxis1", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_ArrayPointScaleCompressionAxis1);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayPointScaleCompressionAxis2 = rAttr.createCurveRamp("scaleCompressionCurveAxis2",
															"scaleCompressionCurveAxis2", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_ArrayPointScaleCompressionAxis2);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayPointScaleStretchAxis1 = rAttr.createCurveRamp("scaleStretchCurveAxis1",
														"scaleStretchCurveAxis1", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_ArrayPointScaleStretchAxis1);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayPointScaleStretchAxis2 = rAttr.createCurveRamp("scaleStretchCurveAxis2",
														"scaleStretchCurveAxis2", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_ArrayPointScaleStretchAxis2);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayControllers = mAttr.create("controllersIK", "controllersIK");
	mAttr.setArray(true);
	mAttr.setKeyable(false);
	stat = addAttribute( m_ArrayControllers );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayUserTangents = mAttr.create("userTangents", "userTangents");
	mAttr.setArray(true);
	mAttr.setKeyable(false);
	stat = addAttribute( m_ArrayUserTangents );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_ArrayDefPosition = nAttr.create("position", "position",
											MFnNumericData::k3Float, 0.0f, &stat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	nAttr.setArray(true);
	nAttr.setUsesArrayDataBuilder(true);
	stat = addAttribute( out_ArrayDefPosition );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_RotX = uAttr.create("rotationX", "rotationX",
								MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_RotY = uAttr.create("rotationY", "rotationY",
								MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_RotZ = uAttr.create("rotationZ", "rotationZ",
								MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_ArrayDefRotEuler = nAttr.create("rotation", "rotation",
											out_RotX, out_RotY, out_RotZ, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	nAttr.setArray(true);
	nAttr.setUsesArrayDataBuilder(true);
	stat = addAttribute( out_ArrayDefRotEuler );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_ArrayDefScale = nAttr.create("scale", "scale",
										MFnNumericData::k3Float, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	nAttr.setArray(true);
	nAttr.setUsesArrayDataBuilder(true);
	stat = addAttribute( out_ArrayDefScale );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_localCurveLength = nAttr.create("localCurveLength", "localCurveLength",
									MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	stat = addAttribute( out_localCurveLength );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_globalCurveLength = nAttr.create("globalCurveLength", "globalCurveLength",
									MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	stat = addAttribute( out_globalCurveLength );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_NumberDeformers, out_ArrayDefPosition);
	stat = attributeAffects( m_NumberDeformers, out_ArrayDefRotEuler);
	stat = attributeAffects( m_NumberDeformers, out_ArrayDefScale);

	stat = attributeAffects( m_CurveResolution, out_ArrayDefPosition);
	stat = attributeAffects( m_CurveResolution, out_ArrayDefRotEuler);
	stat = attributeAffects( m_CurveResolution, out_ArrayDefScale);

	stat = attributeAffects( m_PositionType, out_ArrayDefPosition);
	stat = attributeAffects( m_PositionType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_PositionType, out_ArrayDefScale);

	stat = attributeAffects( m_StartRange, out_ArrayDefPosition);
	stat = attributeAffects( m_StartRange, out_ArrayDefRotEuler);
	stat = attributeAffects( m_StartRange, out_ArrayDefScale);
	stat = attributeAffects( m_StartRange, out_localCurveLength);

	stat = attributeAffects( m_EndRange, out_ArrayDefPosition);
	stat = attributeAffects( m_EndRange, out_ArrayDefRotEuler);
	stat = attributeAffects( m_EndRange, out_ArrayDefScale);
	stat = attributeAffects( m_EndRange, out_localCurveLength);

	stat = attributeAffects( m_MoveRange, out_ArrayDefPosition);
	stat = attributeAffects( m_MoveRange, out_ArrayDefRotEuler);
	stat = attributeAffects( m_MoveRange, out_ArrayDefScale);

	stat = attributeAffects( m_ArrayControllers, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayControllers, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayControllers, out_ArrayDefScale);
	stat = attributeAffects( m_ArrayControllers, out_localCurveLength);
	stat = attributeAffects( m_ArrayControllers, out_globalCurveLength);

	stat = attributeAffects( m_StretchType, out_ArrayDefPosition);
	stat = attributeAffects( m_StretchType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_StretchType, out_ArrayDefScale);
	stat = attributeAffects( m_StretchType, out_localCurveLength);

	stat = attributeAffects( m_RestLength, out_ArrayDefPosition);
	stat = attributeAffects( m_RestLength, out_ArrayDefRotEuler);
	stat = attributeAffects( m_RestLength, out_ArrayDefScale);
	stat = attributeAffects( m_RestLength, out_localCurveLength);

	stat = attributeAffects( m_MaxStretch, out_ArrayDefPosition);
	stat = attributeAffects( m_MaxStretch, out_ArrayDefRotEuler);
	stat = attributeAffects( m_MaxStretch, out_ArrayDefScale);
	stat = attributeAffects( m_MaxStretch, out_localCurveLength);

	stat = attributeAffects( m_MinStretch, out_ArrayDefPosition);
	stat = attributeAffects( m_MinStretch, out_ArrayDefRotEuler);
	stat = attributeAffects( m_MinStretch, out_ArrayDefScale);
	stat = attributeAffects( m_MinStretch, out_localCurveLength);

	stat = attributeAffects( m_CurveType, out_ArrayDefPosition);
	stat = attributeAffects( m_CurveType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_CurveType, out_ArrayDefScale);
	stat = attributeAffects( m_CurveType, out_localCurveLength);

	stat = attributeAffects( m_InterpolatedOutPosition, out_ArrayDefPosition);
	stat = attributeAffects( m_InterpolatedOutPosition, out_ArrayDefRotEuler);
	stat = attributeAffects( m_InterpolatedOutPosition, out_ArrayDefScale);
	stat = attributeAffects( m_InterpolatedOutPosition, out_localCurveLength);

	stat = attributeAffects( m_ArrayPointDistribution, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayPointDistribution, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayPointDistribution, out_ArrayDefScale);
	stat = attributeAffects( m_ArrayPointDistribution, out_localCurveLength);

	stat = attributeAffects( m_rotationType, out_ArrayDefPosition);
	stat = attributeAffects( m_rotationType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_rotationType, out_ArrayDefScale);
	stat = attributeAffects( m_rotationType, out_localCurveLength);

	stat = attributeAffects( m_scaleType, out_ArrayDefPosition);
	stat = attributeAffects( m_scaleType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_scaleType, out_ArrayDefScale);
	stat = attributeAffects( m_scaleType, out_localCurveLength);

	stat = attributeAffects( m_alignType, out_ArrayDefPosition);
	stat = attributeAffects( m_alignType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_alignType, out_ArrayDefScale);
	stat = attributeAffects( m_alignType, out_localCurveLength);

	stat = attributeAffects( m_useAdditiveRotation, out_ArrayDefPosition);
	stat = attributeAffects( m_useAdditiveRotation, out_ArrayDefRotEuler);
	stat = attributeAffects( m_useAdditiveRotation, out_ArrayDefScale);
	stat = attributeAffects( m_useAdditiveRotation, out_localCurveLength);

	stat = attributeAffects( m_addStartRotation, out_ArrayDefPosition);
	stat = attributeAffects( m_addStartRotation, out_ArrayDefRotEuler);
	stat = attributeAffects( m_addStartRotation, out_ArrayDefScale);
	stat = attributeAffects( m_addStartRotation, out_localCurveLength);

	stat = attributeAffects( m_addEndRotation, out_ArrayDefPosition);
	stat = attributeAffects( m_addEndRotation, out_ArrayDefRotEuler);
	stat = attributeAffects( m_addEndRotation, out_ArrayDefScale);
	stat = attributeAffects( m_addEndRotation, out_localCurveLength);

	stat = attributeAffects( m_ArrayPointRotation, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayPointRotation, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayPointRotation, out_ArrayDefScale);
	stat = attributeAffects( m_ArrayPointRotation, out_localCurveLength);

	stat = attributeAffects( m_ArrayPointFollowTangent, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayPointFollowTangent, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayPointFollowTangent, out_ArrayDefScale);
	stat = attributeAffects( m_ArrayPointFollowTangent, out_localCurveLength);

	stat = attributeAffects( m_inverseCurve, out_ArrayDefPosition);
	stat = attributeAffects( m_inverseCurve, out_ArrayDefRotEuler);
	stat = attributeAffects( m_inverseCurve, out_ArrayDefScale);
	stat = attributeAffects( m_inverseCurve, out_localCurveLength);

	stat = attributeAffects( m_globalScale, out_ArrayDefPosition);
	stat = attributeAffects( m_globalScale, out_ArrayDefRotEuler);
	stat = attributeAffects( m_globalScale, out_ArrayDefScale);
	stat = attributeAffects( m_globalScale, out_localCurveLength);

	stat = attributeAffects( m_compressionMaxScaleAxis1, out_ArrayDefPosition);
	stat = attributeAffects( m_compressionMaxScaleAxis1, out_ArrayDefRotEuler);
	stat = attributeAffects( m_compressionMaxScaleAxis1, out_ArrayDefScale);
	stat = attributeAffects( m_compressionMaxScaleAxis1, out_localCurveLength);

	stat = attributeAffects( m_compressionMaxScaleAxis2, out_ArrayDefPosition);
	stat = attributeAffects( m_compressionMaxScaleAxis2, out_ArrayDefRotEuler);
	stat = attributeAffects( m_compressionMaxScaleAxis2, out_ArrayDefScale);
	stat = attributeAffects( m_compressionMaxScaleAxis2, out_localCurveLength);

	stat = attributeAffects( m_stretchMaxScaleAxis1, out_ArrayDefPosition);
	stat = attributeAffects( m_stretchMaxScaleAxis1, out_ArrayDefRotEuler);
	stat = attributeAffects( m_stretchMaxScaleAxis1, out_ArrayDefScale);
	stat = attributeAffects( m_stretchMaxScaleAxis1, out_localCurveLength);

	stat = attributeAffects( m_stretchMaxScaleAxis2, out_ArrayDefPosition);
	stat = attributeAffects( m_stretchMaxScaleAxis2, out_ArrayDefRotEuler);
	stat = attributeAffects( m_stretchMaxScaleAxis2, out_ArrayDefScale);
	stat = attributeAffects( m_stretchMaxScaleAxis2, out_localCurveLength);

	stat = attributeAffects( m_ArrayPointScale, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayPointScale, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayPointScale, out_ArrayDefScale);
	stat = attributeAffects( m_ArrayPointScale, out_localCurveLength);

	stat = attributeAffects( m_ArrayPointScaleCompressionAxis1, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayPointScaleCompressionAxis1, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayPointScaleCompressionAxis1, out_ArrayDefScale);
	stat = attributeAffects( m_ArrayPointScaleCompressionAxis1, out_localCurveLength);

	stat = attributeAffects( m_ArrayPointScaleCompressionAxis2, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayPointScaleCompressionAxis2, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayPointScaleCompressionAxis2, out_ArrayDefScale);
	stat = attributeAffects( m_ArrayPointScaleCompressionAxis2, out_localCurveLength);

	stat = attributeAffects( m_ArrayPointScaleStretchAxis1, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayPointScaleStretchAxis1, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayPointScaleStretchAxis1, out_ArrayDefScale);
	stat = attributeAffects( m_ArrayPointScaleStretchAxis1, out_localCurveLength);

	stat = attributeAffects( m_ArrayPointScaleStretchAxis2, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayPointScaleStretchAxis2, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayPointScaleStretchAxis2, out_ArrayDefScale);
	stat = attributeAffects( m_ArrayPointScaleStretchAxis2, out_localCurveLength);

	stat = attributeAffects( m_blendToLock, out_ArrayDefPosition);
	stat = attributeAffects( m_blendToLock, out_ArrayDefRotEuler);
	stat = attributeAffects( m_blendToLock, out_ArrayDefScale);
	stat = attributeAffects( m_blendToLock, out_localCurveLength);

	stat = attributeAffects( m_blendToLimited, out_ArrayDefPosition);
	stat = attributeAffects( m_blendToLimited, out_ArrayDefRotEuler);
	stat = attributeAffects( m_blendToLimited, out_ArrayDefScale);
	stat = attributeAffects( m_blendToLimited, out_localCurveLength);

	stat = attributeAffects( m_ArrayUserTangents, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayUserTangents, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayUserTangents, out_ArrayDefScale);
	stat = attributeAffects( m_ArrayUserTangents, out_localCurveLength);

	stat = attributeAffects( m_useUserTangents, out_ArrayDefPosition);
	stat = attributeAffects( m_useUserTangents, out_ArrayDefRotEuler);
	stat = attributeAffects( m_useUserTangents, out_ArrayDefScale);
	stat = attributeAffects( m_useUserTangents, out_localCurveLength);

	stat = attributeAffects( m_inTangentLength, out_ArrayDefPosition);
	stat = attributeAffects( m_inTangentLength, out_ArrayDefRotEuler);
	stat = attributeAffects( m_inTangentLength, out_ArrayDefScale);
	stat = attributeAffects( m_inTangentLength, out_localCurveLength);

	stat = attributeAffects( m_outTangentLength, out_ArrayDefPosition);
	stat = attributeAffects( m_outTangentLength, out_ArrayDefRotEuler);
	stat = attributeAffects( m_outTangentLength, out_ArrayDefScale);
	stat = attributeAffects( m_outTangentLength, out_localCurveLength);

	stat = attributeAffects( m_useLimitedSoftStretch, out_ArrayDefPosition);
	stat = attributeAffects( m_useLimitedSoftStretch, out_ArrayDefRotEuler);
	stat = attributeAffects( m_useLimitedSoftStretch, out_ArrayDefScale);
	stat = attributeAffects( m_useLimitedSoftStretch, out_localCurveLength);

	stat = attributeAffects( m_stretchStartSoft, out_ArrayDefPosition);
	stat = attributeAffects( m_stretchStartSoft, out_ArrayDefRotEuler);
	stat = attributeAffects( m_stretchStartSoft, out_ArrayDefScale);
	stat = attributeAffects( m_stretchStartSoft, out_localCurveLength);

	stat = attributeAffects( m_compressionStartSoft, out_ArrayDefPosition);
	stat = attributeAffects( m_compressionStartSoft, out_ArrayDefRotEuler);
	stat = attributeAffects( m_compressionStartSoft, out_ArrayDefScale);
	stat = attributeAffects( m_compressionStartSoft, out_localCurveLength);


	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_SplineMember2Node::compute(const MPlug& in_Plug, MDataBlock& in_Data)
{
	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	// Get the number of deformer.
	MDataHandle		numberDeformersData = in_Data.inputValue(m_NumberDeformers, &returnStatus);
	int numberDeformers = numberDeformersData.asInt();
	// Get the curve resolution.
	MDataHandle		curveResolutionData = in_Data.inputValue(m_CurveResolution, &returnStatus);
	int curveResolution = curveResolutionData.asInt();
	// Get the position type.
	MDataHandle		positionTypeData = in_Data.inputValue(m_PositionType, &returnStatus);
	int positionType = positionTypeData.asShort();
	// Get the curve start range.
	MDataHandle		startRangeData = in_Data.inputValue(m_StartRange, &returnStatus);
	float startRange = startRangeData.asFloat();
	// Get the in tangent length
	MDataHandle		inTangentLengthData = in_Data.inputValue(m_inTangentLength, &returnStatus);
	float inTangentLength = inTangentLengthData.asFloat();
	// Get the out tangent length
	MDataHandle		outTangentLengthData = in_Data.inputValue(m_outTangentLength, &returnStatus);
	float outTangentLength = outTangentLengthData.asFloat();	// Get the curve end range.
	MDataHandle		endRangeData = in_Data.inputValue(m_EndRange, &returnStatus);
	float endRange = endRangeData.asFloat();
	// Get the curve move range.
	MDataHandle		moveRangeData = in_Data.inputValue(m_MoveRange, &returnStatus);
	float moveRange = moveRangeData.asFloat();
	// Get the stretch type.
	MDataHandle		stretchTypeData = in_Data.inputValue(m_StretchType, &returnStatus);
	int stretchType = stretchTypeData.asShort();
	// Get the use limited soft stretch.
	MDataHandle		useLimitedSoftStretchData = in_Data.inputValue(m_useLimitedSoftStretch, &returnStatus);
	int useLimitedSoftStretch = useLimitedSoftStretchData.asShort();
	// Get the stretch start soft.
	MDataHandle		stretchStartSoftData = in_Data.inputValue(m_stretchStartSoft, &returnStatus);
	float stretchStartSoft = stretchStartSoftData.asFloat();
	// Get the compression start soft.
	MDataHandle		compressionStartSoftData = in_Data.inputValue(m_compressionStartSoft, &returnStatus);
	float compressionStartSoft = compressionStartSoftData.asFloat();
	// Get the blend to lock.
	MDataHandle		blendToLockData = in_Data.inputValue(m_blendToLock, &returnStatus);
	float blendToLock = blendToLockData.asFloat();
	// Get the blend to limited.
	MDataHandle		blendToLimitedData = in_Data.inputValue(m_blendToLimited, &returnStatus);
	float blendToLimited = blendToLimitedData.asFloat();
	// Get the curve rest length.
	MDataHandle		restLengthData = in_Data.inputValue(m_RestLength, &returnStatus);
	float restLength = restLengthData.asFloat();
	// Get the global scale.
	MDataHandle		globalScaleData = in_Data.inputValue(m_globalScale, &returnStatus);
	float globalScale = globalScaleData.asFloat();
	// Get the compression Scale of the axis 1.
	MDataHandle		compressionMaxScaleAxis1Data = in_Data.inputValue(m_compressionMaxScaleAxis1, &returnStatus);
	float compressionMaxScaleAxis1 = compressionMaxScaleAxis1Data.asFloat();
	// Get the compression Scale of the axis 2.
	MDataHandle		compressionMaxScaleAxis2Data = in_Data.inputValue(m_compressionMaxScaleAxis2, &returnStatus);
	float compressionMaxScaleAxis2 = compressionMaxScaleAxis2Data.asFloat();
	// Get the stretch Scale of the axis 1.
	MDataHandle		stretchMaxScaleAxis1Data = in_Data.inputValue(m_stretchMaxScaleAxis1, &returnStatus);
	float stretchMaxScaleAxis1 = stretchMaxScaleAxis1Data.asFloat();
	// Get the stretch Scale of the axis 2.
	MDataHandle		stretchMaxScaleAxis2Data = in_Data.inputValue(m_stretchMaxScaleAxis2, &returnStatus);
	float stretchMaxScaleAxis2 = stretchMaxScaleAxis2Data.asFloat();
	// Get the max stretch multiplier.
	MDataHandle		maxStretchData = in_Data.inputValue(m_MaxStretch, &returnStatus);
	float maxStretch = maxStretchData.asFloat();
	// Get the min stretch multiplier.
	MDataHandle		minStretchData = in_Data.inputValue(m_MinStretch, &returnStatus);
	float minStretch = minStretchData.asFloat();
	// Get the curve data type.
	MDataHandle		curveTypeData = in_Data.inputValue(m_CurveType, &returnStatus);
	int curveType = curveTypeData.asShort();
	// Get the use user tangent.
	MDataHandle		useUserTangentsData = in_Data.inputValue(m_useUserTangents, &returnStatus);
	int useUserTangents = useUserTangentsData.asShort();
	// Get the rotation type.
	MDataHandle		rotationTypeData = in_Data.inputValue(m_rotationType, &returnStatus);
	int rotationType = rotationTypeData.asShort();
	// Get the scale type.
	MDataHandle		scaleTypeData = in_Data.inputValue(m_scaleType, &returnStatus);
	int scaleType = scaleTypeData.asShort();
	// Get the alignemnt type.
	MDataHandle		alignTypeData = in_Data.inputValue(m_alignType, &returnStatus);
	int alignType = alignTypeData.asShort();
	// Get the inverse curve.
	MDataHandle		inverseCurveData = in_Data.inputValue(m_inverseCurve, &returnStatus);
	int inverseCurve = inverseCurveData.asShort();
	// Get the use additive rotation.
	MDataHandle		useAdditiveRotationData = in_Data.inputValue(m_useAdditiveRotation,
																	&returnStatus);
	int useAdditiveRotation = useAdditiveRotationData.asShort();
	// Get the start additive rotation angle.
	MDataHandle		addStartAngleData = in_Data.inputValue(m_addStartRotation, &returnStatus);
	float addStartAngle = addStartAngleData.asFloat();
	// Get the end additive rotation angle.
	MDataHandle		addEndAngleData = in_Data.inputValue(m_addEndRotation, &returnStatus);
	float addEndAngle = addEndAngleData.asFloat();
	// Get the out interpolation.
	MDataHandle		outInterpolationData = in_Data.inputValue(m_InterpolatedOutPosition, &returnStatus);
	int outInterpolation = outInterpolationData.asShort();
	// Get the array of curve's controller.
	MArrayDataHandle arrayControllers = in_Data.inputArrayValue(m_ArrayControllers, &returnStatus);
	// Get the array user tangents.
	MArrayDataHandle arrayUserTangents = in_Data.inputArrayValue(m_ArrayUserTangents, &returnStatus);


	// Get the ramp attirbute.
	MObject			thisNode = thisMObject();
	// Get the point distribution ramp.
	MRampAttribute	pointDistributionData ( thisNode, m_ArrayPointDistribution, &returnStatus );
	// Get the rotation ramp.
	MRampAttribute	rotationDistributionData ( thisNode, m_ArrayPointRotation, &returnStatus );
	// Get the follow tangent ramp.
	MRampAttribute	followTangentDistributionData ( thisNode, m_ArrayPointFollowTangent, &returnStatus );
	// Get the scale ramp.
	MRampAttribute	scaleDistributionData ( thisNode, m_ArrayPointScale, &returnStatus );
	// Get the compression axis 1 ramp.
	MRampAttribute	scaleCompressionCurveAxis1Data ( thisNode, m_ArrayPointScaleCompressionAxis1, &returnStatus );
	// Get the compression axis 2 ramp.
	MRampAttribute	scaleCompressionCurveAxis2Data ( thisNode, m_ArrayPointScaleCompressionAxis2, &returnStatus );
	// Get the stretch axis 1 ramp.
	MRampAttribute	scaleStretchCurveAxis1Data ( thisNode, m_ArrayPointScaleStretchAxis1, &returnStatus );
	// Get the stretch axis 2 ramp.
	MRampAttribute	scaleStretchCurveAxis2Data ( thisNode, m_ArrayPointScaleStretchAxis2, &returnStatus );

	// If get all user attribute is ok.
	if ( returnStatus == MS::kSuccess )
	{

		//Convert Maya Data to Frankenstein Data.
		int nbControllers = arrayControllers.elementCount();
		int nbUserTangents = arrayUserTangents.elementCount();
		arrayControllers.jumpToElement(0);
		arrayUserTangents.jumpToElement(0);

		DataArray<Matrix4x4>	listControllersIK(nbControllers);
		DataArray<Matrix4x4>	listUserTangents(nbUserTangents);

		//Convert the controler transformation matrix.
		MMatrix					currentController;
		for ( int iCon=0; iCon<nbControllers; iCon++ )
		{
			currentController = arrayControllers.inputValue().asMatrix();
			for ( int iRow=0; iRow<4; iRow++)
			{
				for ( int iCol=0; iCol<4; iCol++ )
				{
					listControllersIK[iCon].SetValue(iRow, iCol, 
														(float)currentController(iRow, iCol));
				}
			}

			if ( iCon < nbControllers )
			{
				arrayControllers.next();
			}
		}

		for ( int iTan=0; iTan<nbUserTangents; iTan++ )
		{
			currentController = arrayUserTangents.inputValue().asMatrix();
			for ( int iRow=0; iRow<4; iRow++)
			{
				for ( int iCol=0; iCol<4; iCol++ )
				{
					listUserTangents[iTan].SetValue(iRow, iCol, 
														(float)currentController(iRow, iCol));
				}
			}

			if ( iTan < nbUserTangents )
			{
				arrayUserTangents.next();
			}
		}

		// Convert ramp to array data.
		DataArray<float>		pointDistribution(numberDeformers);
		DataArray<float>		rotationDistribution(numberDeformers);
		DataArray<float>		followTangentDistribution(numberDeformers);
		DataArray<float>		scaleDistribution(numberDeformers);
		DataArray<float>		scaleCompressionCurveAxis1(numberDeformers);
		DataArray<float>		scaleCompressionCurveAxis2(numberDeformers);
		DataArray<float>		scaleStretchCurveAxis1(numberDeformers);
		DataArray<float>		scaleStretchCurveAxis2(numberDeformers);

		//Convert the curves filter.
		float curvePos, pointDist, rotDist, followDist, scaleDist, compCurveAxis1, compCurveAxis2, stretchCurveAxis1, stretchCurveAxis2;

		for ( int iDef=0; iDef<numberDeformers; iDef++ )
		{
			curvePos = (float)iDef / (float)(numberDeformers-1);

			pointDistributionData.getValueAtPosition(curvePos, pointDist, &returnStatus);
			rotationDistributionData.getValueAtPosition(curvePos, rotDist, &returnStatus);
			followTangentDistributionData.getValueAtPosition(curvePos, followDist, &returnStatus);
			scaleDistributionData.getValueAtPosition(curvePos, scaleDist, &returnStatus);
			scaleCompressionCurveAxis1Data.getValueAtPosition(curvePos, compCurveAxis1, &returnStatus);
			scaleCompressionCurveAxis2Data.getValueAtPosition(curvePos, compCurveAxis2, &returnStatus);
			scaleStretchCurveAxis1Data.getValueAtPosition(curvePos, stretchCurveAxis1, &returnStatus);
			scaleStretchCurveAxis2Data.getValueAtPosition(curvePos, stretchCurveAxis2, &returnStatus);

			pointDistribution[iDef] = pointDist;
			rotationDistribution[iDef] = rotDist;
			followTangentDistribution[iDef] = followDist;
			scaleDistribution[iDef] = scaleDist;
			scaleCompressionCurveAxis1[iDef] = compCurveAxis1;
			scaleCompressionCurveAxis2[iDef] = compCurveAxis2;
			scaleStretchCurveAxis1[iDef] = stretchCurveAxis1;
			scaleStretchCurveAxis2[iDef] = stretchCurveAxis2;
		}

		//Init the frankenstein splineMember.
		SplineMember2	fknSplineMember;
		fknSplineMember.Set (numberDeformers,
								listControllersIK,
								listUserTangents,
								curveResolution,
								positionType,
								startRange,
								endRange,
								moveRange,
								restLength,
								stretchType,
								maxStretch,
								minStretch,
								curveType,
								outInterpolation,
								rotationType,
								scaleType,
								alignType,
								useAdditiveRotation,
								addStartAngle,
								addEndAngle,
								inverseCurve,
								globalScale,
								compressionMaxScaleAxis1,
								compressionMaxScaleAxis2,
								stretchMaxScaleAxis1,
								stretchMaxScaleAxis2,
								blendToLock,
								blendToLimited,
								useUserTangents,
								inTangentLength,
								outTangentLength,
								useLimitedSoftStretch,
								stretchStartSoft,
								compressionStartSoft,
								pointDistribution,
								rotationDistribution,
								followTangentDistribution,
								scaleDistribution,
								scaleCompressionCurveAxis1,
								scaleCompressionCurveAxis2,
								scaleStretchCurveAxis1,
								scaleStretchCurveAxis2);

		//Compute the frankenstein SplineMember.
		fknSplineMember.computeCurve();

		if (in_Plug == out_ArrayDefPosition || in_Plug == out_ArrayDefRotEuler || in_Plug == out_ArrayDefScale)
		{
			//Convert Frankenstein Data to Maya Data.
			MArrayDataBuilder	outArrayDefPosition ( out_ArrayDefPosition, numberDeformers, &returnStatus );
			Vector3D			fromFknPosition;
			MDataHandle			outSetPosition;
			
			MArrayDataBuilder	outArrayDefEuler ( out_ArrayDefRotEuler, numberDeformers, &returnStatus);
			Vector3D			fromFknEuler;
			MVector				toEuler;
			MDataHandle			outSetEuler;
			
			MArrayDataBuilder	outArrayDefScale ( out_ArrayDefScale, numberDeformers, &returnStatus);
			Vector3D			fromFknScale;
			MDataHandle			outSetScale;
			
			for ( int iDef=0; iDef<numberDeformers; iDef++ )
			{
				//Get Position
				fromFknPosition = fknSplineMember.getPositionAt(iDef);
				outSetPosition = outArrayDefPosition.addElement(iDef);
				outSetPosition.set(fromFknPosition.GetX(), fromFknPosition.GetY(), fromFknPosition.GetZ());
				//Get Rotation.
				fromFknEuler = fknSplineMember.getEulerAt(iDef);
				outSetEuler = outArrayDefEuler.addElement(iDef);
				toEuler.x = fromFknEuler.GetX(); 
				toEuler.y = fromFknEuler.GetY(); 
				toEuler.z = fromFknEuler.GetZ(); 
				outSetEuler.set(toEuler);
				//Get Scale.
				fromFknScale = fknSplineMember.getScaleAt(iDef);
				outSetScale = outArrayDefScale.addElement(iDef);
				outSetScale.set (fromFknScale.GetX(), fromFknScale.GetY(), fromFknScale.GetZ());
			}

			// Update the out array of deformer position.
			MArrayDataHandle arrayOutDefPosition = in_Data.outputArrayValue(out_ArrayDefPosition, &returnStatus);
			arrayOutDefPosition.set ( outArrayDefPosition );
			arrayOutDefPosition.setAllClean();
			
			// Update the out array of deformer rotation.
			MArrayDataHandle arrayOutDefEuler = in_Data.outputArrayValue(out_ArrayDefRotEuler, &returnStatus);
			arrayOutDefEuler.set ( outArrayDefEuler );
			arrayOutDefEuler.setAllClean();
			
			// Update the out array of deformer scale.
			MArrayDataHandle arrayOutDefScale = in_Data.outputArrayValue(out_ArrayDefScale, &returnStatus);
			arrayOutDefScale.set ( outArrayDefScale );
			arrayOutDefScale.setAllClean();

			// Update the out local curve length.
			MDataHandle outLocalCurveLength = in_Data.outputValue(out_localCurveLength, &returnStatus);
			outLocalCurveLength.set(fknSplineMember.getLocalCurveLength());
			outLocalCurveLength.setClean();

			// Update the out global curve length.
			MDataHandle outGlobalCurveLength = in_Data.outputValue(out_globalCurveLength, &returnStatus);
			outGlobalCurveLength.set(fknSplineMember.getGlobalCurveLength());
			outGlobalCurveLength.setClean();
		}
		else
		{
			return MS::kUnknownParameter;
		}

		if(in_Plug == out_localCurveLength){

		}

		if(in_Plug == out_globalCurveLength){

		}
	}
	else
	{
		MGlobal::displayError( "Node fkn_SplineMember2Node cannot get value.\n" );
	}



	return returnStatus;
};