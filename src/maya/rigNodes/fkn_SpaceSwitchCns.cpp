/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_BlendRotCns.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_SpaceSwitchCns.h"

// Unique plugins ID.
MTypeId fkn_SpaceSwitchCns::id( fkn_SpaceSwitchCnsID );

// Input Attributes.
MObject	fkn_SpaceSwitchCns::m_SpaceSwitch;
MObject	fkn_SpaceSwitchCns::m_ArraySpaces;
MObject	fkn_SpaceSwitchCns::m_SpaceMatrix;
MObject	fkn_SpaceSwitchCns::m_SpaceOffsetMatrix;

//Out Attributes.
MObject	fkn_SpaceSwitchCns::out_Matrix;


//Constructor.
fkn_SpaceSwitchCns::fkn_SpaceSwitchCns(){

};

//Destructor.
fkn_SpaceSwitchCns::~fkn_SpaceSwitchCns(){

};

//Maya Creator.
void* fkn_SpaceSwitchCns::creator(){
	return new fkn_SpaceSwitchCns();
};

//Maya Initialize.
MStatus fkn_SpaceSwitchCns::initialize(){

	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;
	MFnCompoundAttribute	cAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 
	m_SpaceSwitch = nAttr.create("SwitchSpace", "SwitchSpace", MFnNumericData::kInt, 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_SpaceSwitch);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_SpaceMatrix = mAttr.create("SpaceMatrix", "SpaceMatrix");
	mAttr.setStorable(true);
	cAttr.setKeyable(true);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_SpaceOffsetMatrix = mAttr.create("Offset", "Offset");
	mAttr.setStorable(true);
	cAttr.setKeyable(true);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArraySpaces = cAttr.create("Spaces", "Spaces");
	cAttr.addChild(m_SpaceMatrix);
	cAttr.addChild(m_SpaceOffsetMatrix);
	cAttr.setArray(true);
	cAttr.setKeyable(true);
	cAttr.setStorable(true);
	stat = addAttribute(m_ArraySpaces);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_Matrix = mAttr.create("Result", "Result");
	stat = addAttribute(out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_SpaceSwitch, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_ArraySpaces, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_SpaceMatrix, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_SpaceOffsetMatrix, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};


MStatus fkn_SpaceSwitchCns::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		spaceSwitchData = in_Data.inputValue(m_SpaceSwitch, &returnStatus);
	unsigned int spaceSwitch = spaceSwitchData.asShort();

	MArrayDataHandle arraySpaces = in_Data.inputArrayValue(m_ArraySpaces, &returnStatus);

	if ( returnStatus == MS::kSuccess ){

		if (in_Plug == out_Matrix ){

			// Get the count of input spaces.
			unsigned int spaceCount = arraySpaces.elementCount();
			// Check if the spaceSwitch value is not greater than the space count.
			if (spaceSwitch < spaceCount){
				
				// Get the space.
				arraySpaces.jumpToElement(spaceSwitch);

				// Get the space matrix.
				MDataHandle spaceMatrixData = arraySpaces.inputValue(&returnStatus).child(m_SpaceMatrix);
				MMatrix spaceMatrix = spaceMatrixData.asMatrix();

				// Get the space offset matrix.
				MDataHandle spaceOffsetMatrixData = arraySpaces.inputValue(&returnStatus).child(m_SpaceOffsetMatrix);
				MMatrix spaceOffsetMatrix = spaceOffsetMatrixData.asMatrix();

				// Multiply the space and the offset.
				MMatrix finalMatrix = spaceOffsetMatrix * spaceMatrix;

				//Set the Maya output data.
				MDataHandle	outMatrix = in_Data.outputValue(out_Matrix, &returnStatus);
				outMatrix.set(finalMatrix);
				outMatrix.setClean();
			}
		}else{
			return MS::kUnknownParameter;
		}
	}else{
		MGlobal::displayError( "Node fkn_SpaceSwitchCns cannot get value.\n" );
	}

	return returnStatus;
};