/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_SRT_Constraint.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a SRT Constraint node for maya.
*/

#ifndef _fkn_SRT_Constraint_
#define _fkn_SRT_Constraint_

#include "../fkn_Maya_Tools.h"

/*! @brief Maya Class for Add two vector.

*/
class fkn_SRT_Constraint : public MPxNode
{
	public:
		/*! @bief The constructor.*/
		fkn_SRT_Constraint();
		/*! @brief The destructor. */
		virtual	~fkn_SRT_Constraint();

		//Maya Methode.
		/*! @brief The Maya compute function. 
		@param[in] in_Plug The input connection to the node.
		@param[in] in_Data The data block of the node.
		@return Success.
		*/
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		/*! @biref The Maya creator function. */
		static void*		creator();
		/*! @brief The Maya initialize function. */
		static MStatus		initialize();

	public:
		static	MTypeId		id; /*! Contain the maya unique id.*/

		static MObject m_UseOffset; /*! Use the offset matrix. */
		static MObject m_LocalTransform; /*! Return the driven matrix in local space. */
		static MObject m_DriverMatrix; /*! The world matrix of the driver object. */
		static MObject m_ParentMatrix; /*! The world matrix of the parent object. */

		static MObject m_OffsetMatrix00;
		static MObject m_OffsetMatrix01;
		static MObject m_OffsetMatrix02;

		static MObject m_OffsetMatrix10;
		static MObject m_OffsetMatrix11;
		static MObject m_OffsetMatrix12;

		static MObject m_OffsetMatrix20;
		static MObject m_OffsetMatrix21;
		static MObject m_OffsetMatrix22;

		static MObject m_OffsetMatrix30;
		static MObject m_OffsetMatrix31;
		static MObject m_OffsetMatrix32;

		static MObject out_Translation; /*! The out translation of the driven object. */
		static MObject out_TranslationX;
		static MObject out_TranslationY;
		static MObject out_TranslationZ;

		static MObject out_Rotation; /*! The out rotation of the driven object. */
		static MObject out_RotationX;
		static MObject out_RotationY;
		static MObject out_RotationZ;

		static MObject out_Scale; /*! The out scale of the driven object. */
		static MObject out_ScaleX;
		static MObject out_ScaleY;
		static MObject out_ScaleZ;						
};

#endif