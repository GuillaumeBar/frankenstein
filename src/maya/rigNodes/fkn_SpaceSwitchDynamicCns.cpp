/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_BlendRotCns.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_SpaceSwitchDynamicCns.h"

// Unique plugins ID.
MTypeId fkn_SpaceSwitchDynamicCns::id( fkn_SpaceSwitchDynamicCnsID );

// Input Attributes.
MObject	fkn_SpaceSwitchDynamicCns::m_SpaceSwitch;
MObject	fkn_SpaceSwitchDynamicCns::m_ArraySpaces;
MObject	fkn_SpaceSwitchDynamicCns::m_00;
MObject	fkn_SpaceSwitchDynamicCns::m_01;
MObject	fkn_SpaceSwitchDynamicCns::m_02;
MObject	fkn_SpaceSwitchDynamicCns::m_03;
MObject	fkn_SpaceSwitchDynamicCns::m_10;
MObject	fkn_SpaceSwitchDynamicCns::m_11;
MObject	fkn_SpaceSwitchDynamicCns::m_12;
MObject	fkn_SpaceSwitchDynamicCns::m_13;
MObject	fkn_SpaceSwitchDynamicCns::m_20;
MObject	fkn_SpaceSwitchDynamicCns::m_21;
MObject	fkn_SpaceSwitchDynamicCns::m_22;
MObject	fkn_SpaceSwitchDynamicCns::m_23;
MObject	fkn_SpaceSwitchDynamicCns::m_30;
MObject	fkn_SpaceSwitchDynamicCns::m_31;
MObject	fkn_SpaceSwitchDynamicCns::m_32;
MObject	fkn_SpaceSwitchDynamicCns::m_33;

//Out Attributes.
MObject	fkn_SpaceSwitchDynamicCns::out_Matrix;


//Constructor.
fkn_SpaceSwitchDynamicCns::fkn_SpaceSwitchDynamicCns(){

};

//Destructor.
fkn_SpaceSwitchDynamicCns::~fkn_SpaceSwitchDynamicCns(){

};

//Maya Creator.
void* fkn_SpaceSwitchDynamicCns::creator(){
	return new fkn_SpaceSwitchDynamicCns();
};

//Maya Initialize.
MStatus fkn_SpaceSwitchDynamicCns::initialize(){

	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;
	MFnCompoundAttribute	cAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 
	m_SpaceSwitch = nAttr.create("SwitchSpace", "SwitchSpace", MFnNumericData::kInt, 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_SpaceSwitch);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArraySpaces = mAttr.create("SpaceMatrix", "SpaceMatrix");
	mAttr.setStorable(true);
	mAttr.setKeyable(true);
	mAttr.setArray(true);
	stat = addAttribute(m_ArraySpaces);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_00 = nAttr.create("offset_00", "offset_00", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_00);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_01 = nAttr.create("offset_01", "offset_01", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_01);	
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_02 = nAttr.create("offset_02", "offset_02", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_02);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_03 = nAttr.create("offset_03", "offset_03", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_03);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_10 = nAttr.create("offset_10", "offset_10", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_10);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_11 = nAttr.create("offset_11", "offset_11", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_11);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_12 = nAttr.create("offset_12", "offset_12", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_12);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_13 = nAttr.create("offset_13", "offset_13", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_13);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_20 = nAttr.create("offset_20", "offset_20", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_20);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_21 = nAttr.create("offset_21", "offset_21", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_21);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_22 = nAttr.create("offset_22", "offset_22", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_22);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_23 = nAttr.create("offset_23", "offset_23", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_23);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_30 = nAttr.create("offset_30", "offset_30", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_30);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_31 = nAttr.create("offset_31", "offset_31", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_31);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_32 = nAttr.create("offset_32", "offset_32", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_32);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_33 = nAttr.create("offset_33", "offset_33", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_33);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_Matrix = mAttr.create("Result", "Result");
	stat = addAttribute(out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_SpaceSwitch, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_ArraySpaces, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_00, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_01, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_02, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_03, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_10, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_11, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_12, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_13, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_20, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_21, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_22, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_23, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_30, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_31, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_32, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_33, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};


MStatus fkn_SpaceSwitchDynamicCns::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		spaceSwitchData = in_Data.inputValue(m_SpaceSwitch, &returnStatus);
	unsigned int spaceSwitch = spaceSwitchData.asShort();

	MArrayDataHandle arraySpaces = in_Data.inputArrayValue(m_ArraySpaces, &returnStatus);

	MDataHandle m00Data = in_Data.inputValue(m_00, &returnStatus);
	float m00 = m00Data.asFloat();

	MDataHandle m01Data = in_Data.inputValue(m_01, &returnStatus);
	float m01 = m01Data.asFloat();

	MDataHandle m02Data = in_Data.inputValue(m_02, &returnStatus);
	float m02 = m02Data.asFloat();

	MDataHandle m03Data = in_Data.inputValue(m_03, &returnStatus);
	float m03 = m03Data.asFloat();

	MDataHandle m10Data = in_Data.inputValue(m_10, &returnStatus);
	float m10 = m10Data.asFloat();

	MDataHandle m11Data = in_Data.inputValue(m_11, &returnStatus);
	float m11 = m11Data.asFloat();

	MDataHandle m12Data = in_Data.inputValue(m_12, &returnStatus);
	float m12 = m12Data.asFloat();

	MDataHandle m13Data = in_Data.inputValue(m_13, &returnStatus);
	float m13 = m13Data.asFloat();

	MDataHandle m20Data = in_Data.inputValue(m_20, &returnStatus);
	float m20 = m20Data.asFloat();

	MDataHandle m21Data = in_Data.inputValue(m_21, &returnStatus);
	float m21 = m21Data.asFloat();

	MDataHandle m22Data = in_Data.inputValue(m_22, &returnStatus);
	float m22 = m22Data.asFloat();

	MDataHandle m23Data = in_Data.inputValue(m_23, &returnStatus);
	float m23 = m23Data.asFloat();

	MDataHandle m30Data = in_Data.inputValue(m_30, &returnStatus);
	float m30 = m30Data.asFloat();

	MDataHandle m31Data = in_Data.inputValue(m_31, &returnStatus);
	float m31 = m31Data.asFloat();

	MDataHandle m32Data = in_Data.inputValue(m_32, &returnStatus);
	float m32 = m32Data.asFloat();

	MDataHandle m33Data = in_Data.inputValue(m_33, &returnStatus);
	float m33 = m33Data.asFloat();

	if ( returnStatus == MS::kSuccess ){

		if (in_Plug == out_Matrix ){

			// Get the count of input spaces.
			unsigned int spaceCount = arraySpaces.elementCount();
			// Check if the spaceSwitch value is not greater than the space count.
			if (spaceSwitch < spaceCount){
				
				// Get the space.
				arraySpaces.jumpToElement(spaceSwitch);

				// Get the space matrix.
				MMatrix spaceMatrix = arraySpaces.inputValue().asMatrix();

				// Get the offset matrix.
				MMatrix offsetMatrix;
				offsetMatrix(0,0) = m00;
				offsetMatrix(0,1) = m01;
				offsetMatrix(0,2) = m02;
				offsetMatrix(0,3) = m03;
				offsetMatrix(1,0) = m10;
				offsetMatrix(1,1) = m11;
				offsetMatrix(1,2) = m12;
				offsetMatrix(1,3) = m13;
				offsetMatrix(2,0) = m20;
				offsetMatrix(2,1) = m21;
				offsetMatrix(2,2) = m22;
				offsetMatrix(2,3) = m23;
				offsetMatrix(3,0) = m30;
				offsetMatrix(3,1) = m31;
				offsetMatrix(3,2) = m32;
				offsetMatrix(3,3) = m33;

				// Multiply the space and the offset.
				MMatrix finalMatrix = offsetMatrix * spaceMatrix;

				//Set the Maya output data.
				MDataHandle	outMatrix = in_Data.outputValue(out_Matrix, &returnStatus);
				outMatrix.set(finalMatrix);
				outMatrix.setClean();
			}
		}else{
			return MS::kUnknownParameter;
		}
	}else{
		MGlobal::displayError( "Node fkn_SpaceSwitchCns cannot get value.\n" );
	}

	return returnStatus;
};