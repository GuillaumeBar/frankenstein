/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Ik3BonesMemberNode.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Ik3BonesMemberNode.h"
#include <iostream>
using namespace std;

// Unique plugins ID.
MTypeId     fkn_Ik3BonesMemberNode::id( 0x80100 );

// Input Attributes.
MObject		fkn_Ik3BonesMemberNode::m_BlendIKFK;
MObject		fkn_Ik3BonesMemberNode::m_PinBones;
MObject		fkn_Ik3BonesMemberNode::m_SymBonesMatrix;
MObject		fkn_Ik3BonesMemberNode::m_PlanTwist;
MObject		fkn_Ik3BonesMemberNode::m_PlanBone2Twist;
MObject		fkn_Ik3BonesMemberNode::m_PlanBone3Twist;
MObject 	fkn_Ik3BonesMemberNode::m_GlobalScale;

MObject		fkn_Ik3BonesMemberNode::m_UseStretch;
MObject		fkn_Ik3BonesMemberNode::m_UseSoftIK;
MObject		fkn_Ik3BonesMemberNode::m_StartSoftIK;
MObject		fkn_Ik3BonesMemberNode::m_GlobalBonesScale;
MObject		fkn_Ik3BonesMemberNode::m_Bone1Scale;
MObject		fkn_Ik3BonesMemberNode::m_Bone2Scale;
MObject		fkn_Ik3BonesMemberNode::m_Bone3Scale;

MObject		fkn_Ik3BonesMemberNode::m_GuidRootMatrix;
MObject		fkn_Ik3BonesMemberNode::m_GuidEffMatrix;
MObject		fkn_Ik3BonesMemberNode::m_GuidBone2Matrix;
MObject		fkn_Ik3BonesMemberNode::m_GuidBone3Matrix;
MObject		fkn_Ik3BonesMemberNode::m_GuidMainUpVMatrix;

MObject		fkn_Ik3BonesMemberNode::m_IkRootMatrix;
MObject		fkn_Ik3BonesMemberNode::m_IkEffMatrix;
MObject		fkn_Ik3BonesMemberNode::m_IkBone2UpVMatrix;
MObject		fkn_Ik3BonesMemberNode::m_IkBone3UpVMatrix;
MObject		fkn_Ik3BonesMemberNode::m_IkMainUpVMatrix;
MObject		fkn_Ik3BonesMemberNode::m_IkRotationOffset;
MObject		fkn_Ik3BonesMemberNode::m_IkPinBone2Matrix;
MObject		fkn_Ik3BonesMemberNode::m_IkPinBone3Matrix;

MObject		fkn_Ik3BonesMemberNode::m_fkBone1Matrix;
MObject		fkn_Ik3BonesMemberNode::m_fkBone2Matrix;
MObject		fkn_Ik3BonesMemberNode::m_fkBone3Matrix;
MObject		fkn_Ik3BonesMemberNode::m_fkBone4Matrix;

//Out Attributes.
MObject		fkn_Ik3BonesMemberNode::out_defBone1Matrix;
MObject		fkn_Ik3BonesMemberNode::out_defBone2Matrix;
MObject		fkn_Ik3BonesMemberNode::out_defBone3Matrix;
MObject		fkn_Ik3BonesMemberNode::out_defBone4Matrix;

//Constructor.
fkn_Ik3BonesMemberNode::fkn_Ik3BonesMemberNode(){

};

//Destructor.
fkn_Ik3BonesMemberNode::~fkn_Ik3BonesMemberNode(){

};

//Maya Creator.
void* fkn_Ik3BonesMemberNode::creator(){
	return new fkn_Ik3BonesMemberNode();
};

//Help Function.
void fkn_Ik3BonesMemberNode::ConvertMatrix4x4ToMMatrix(Matrix4x4& in_Matrix4x4, MMatrix& out_MMatrix){
	for(unsigned int iRow=0; iRow<4; iRow++){
		for(unsigned int iCol=0; iCol<4; iCol++){
			out_MMatrix(iRow, iCol) = (double)in_Matrix4x4.GetValue(iRow,iCol);
		}
	}
};

void fkn_Ik3BonesMemberNode::ConvertMMatrixToMatrix4x4(MMatrix& in_MMatrix, Matrix4x4& out_Matrix4x4){
	for(unsigned int iRow=0; iRow<4; iRow++){
		for(unsigned int iCol=0; iCol<4; iCol++){
			out_Matrix4x4.SetValue(iRow, iCol, (float)in_MMatrix(iRow,iCol));
		}
	}
};

//Maya Initialize.
MStatus fkn_Ik3BonesMemberNode::initialize(){

	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 
	m_BlendIKFK = nAttr.create("BlendIKFK", "BlendIKFK", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_BlendIKFK);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_SymBonesMatrix = eAttr.create("SymBonesMatrix", "SymBonesMatrix", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No Symmetry", 0);
	eAttr.addField("Symmetry", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_SymBonesMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	
	m_UseStretch = eAttr.create("Stretch", "Stretch", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No Stretch", 0);
	eAttr.addField("Stretch", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_UseStretch);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_UseSoftIK = eAttr.create("SoftIK", "SoftIK", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No Soft IK", 0);
	eAttr.addField("Soft IK", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_UseSoftIK);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GlobalScale = nAttr.create("GlobalScale", "GlobalScale", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.000001f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_GlobalScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_StartSoftIK = nAttr.create("StartSoftIK", "StartSoftIK", MFnNumericData::kFloat, 0.98f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_StartSoftIK);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GlobalBonesScale = nAttr.create("GlobalBonesScale", "GlobalBonesScale", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(2.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_GlobalBonesScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_Bone1Scale = nAttr.create("Bone1Scale", "Bone1Scale", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(2.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_Bone1Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_Bone2Scale = nAttr.create("Bone2Scale", "Bone2Scale", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(2.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_Bone2Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_Bone3Scale = nAttr.create("Bone3Scale", "Bone3Scale", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(2.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_Bone3Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_PinBones = eAttr.create("PinBone2", "PinBone2", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No Pin", 0);
	eAttr.addField("Pin Bone2", 1);
	eAttr.addField("Pin Bone3", 2);
	eAttr.setKeyable(true);
	eAttr.setStorable(true);
	stat = addAttribute(m_PinBones);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_PlanTwist = nAttr.create("PlanTwist", "PlanTwist", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_PlanTwist);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_PlanBone2Twist = nAttr.create("PlanBone2Twist", "PlanBone2Twist", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_PlanBone2Twist);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_PlanBone3Twist = nAttr.create("PlanBone3Twist", "PlanBone3Twist", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_PlanBone3Twist);
	CHECK_MSTATUS_AND_RETURN_IT(stat);


	m_GuidRootMatrix = mAttr.create("GuidRoot", "GuidRoot");
	mAttr.setStorable(true);
	stat = addAttribute(m_GuidRootMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GuidBone2Matrix = mAttr.create("GuidBone2", "GuidBone2");
	mAttr.setStorable(true);
	stat = addAttribute(m_GuidBone2Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GuidBone3Matrix = mAttr.create("GuidBone3", "GuidBone3");
	mAttr.setStorable(true);
	stat = addAttribute(m_GuidBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GuidEffMatrix = mAttr.create("GuidEff", "GuidEff");
	mAttr.setStorable(true);
	stat = addAttribute(m_GuidEffMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GuidMainUpVMatrix = mAttr.create("GuidMainUpVector", "GuidMainUpVector");
	mAttr.setStorable(true);
	stat = addAttribute(m_GuidMainUpVMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkRootMatrix = mAttr.create("IkRoot", "IkRoot");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkRootMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkBone2UpVMatrix = mAttr.create("IkBone2UpVector", "IkBone2UpVector");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkBone2UpVMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkBone3UpVMatrix = mAttr.create("IkBone3UpVector", "IkBone3UpVector");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkBone3UpVMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkEffMatrix = mAttr.create("IkEff", "IkEff");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkEffMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkMainUpVMatrix = mAttr.create("IkMainUpVector", "IkMainUpVector");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkMainUpVMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkPinBone2Matrix = mAttr.create("IkPinBone2", "IkPinBone2");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkPinBone2Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkPinBone3Matrix = mAttr.create("IkPinBone3", "IkPinBone3");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkPinBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkRotationOffset = nAttr.create("IkRotationOffset", "IkRotationOffset", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_IkRotationOffset);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_fkBone1Matrix = mAttr.create("FkBone1", "FkBone1");
	mAttr.setStorable(true);
	stat = addAttribute(m_fkBone1Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_fkBone2Matrix = mAttr.create("FkBone2", "FkBone2");
	mAttr.setStorable(true);
	stat = addAttribute(m_fkBone2Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_fkBone3Matrix = mAttr.create("FkBone3", "FkBone3");
	mAttr.setStorable(true);
	stat = addAttribute(m_fkBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_fkBone4Matrix = mAttr.create("FkBone4", "FkBone4");
	mAttr.setStorable(true);
	stat = addAttribute(m_fkBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_defBone1Matrix = mAttr.create("OutBone1World", "OutBone1World");
	stat = addAttribute(out_defBone1Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_defBone2Matrix = mAttr.create("OutBone2World", "OutBone2World");
	stat = addAttribute(out_defBone2Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_defBone3Matrix = mAttr.create("OutBone3World", "OutBone3World");
	stat = addAttribute(out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_defBone4Matrix = mAttr.create("OutBone4World", "OutBone4World");
	stat = addAttribute(out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_BlendIKFK, out_defBone1Matrix);
	stat = attributeAffects( m_BlendIKFK, out_defBone2Matrix);
	stat = attributeAffects( m_BlendIKFK, out_defBone3Matrix);
	stat = attributeAffects( m_BlendIKFK, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GlobalScale, out_defBone1Matrix);
	stat = attributeAffects( m_GlobalScale, out_defBone2Matrix);
	stat = attributeAffects( m_GlobalScale, out_defBone3Matrix);
	stat = attributeAffects( m_GlobalScale, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_SymBonesMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_SymBonesMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_SymBonesMatrix, out_defBone3Matrix);
	stat = attributeAffects( m_SymBonesMatrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_UseStretch, out_defBone1Matrix);
	stat = attributeAffects( m_UseStretch, out_defBone2Matrix);
	stat = attributeAffects( m_UseStretch, out_defBone3Matrix);
	stat = attributeAffects( m_UseStretch, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_UseSoftIK, out_defBone1Matrix);
	stat = attributeAffects( m_UseSoftIK, out_defBone2Matrix);
	stat = attributeAffects( m_UseSoftIK, out_defBone3Matrix);
	stat = attributeAffects( m_UseSoftIK, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_StartSoftIK, out_defBone1Matrix);
	stat = attributeAffects( m_StartSoftIK, out_defBone2Matrix);
	stat = attributeAffects( m_StartSoftIK, out_defBone3Matrix);
	stat = attributeAffects( m_StartSoftIK, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GlobalBonesScale, out_defBone1Matrix);
	stat = attributeAffects( m_GlobalBonesScale, out_defBone2Matrix);
	stat = attributeAffects( m_GlobalBonesScale, out_defBone3Matrix);
	stat = attributeAffects( m_GlobalBonesScale, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_Bone1Scale, out_defBone1Matrix);
	stat = attributeAffects( m_Bone1Scale, out_defBone2Matrix);
	stat = attributeAffects( m_Bone1Scale, out_defBone3Matrix);
	stat = attributeAffects( m_Bone1Scale, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_Bone2Scale, out_defBone1Matrix);
	stat = attributeAffects( m_Bone2Scale, out_defBone2Matrix);
	stat = attributeAffects( m_Bone2Scale, out_defBone3Matrix);
	stat = attributeAffects( m_Bone2Scale, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_Bone3Scale, out_defBone1Matrix);
	stat = attributeAffects( m_Bone3Scale, out_defBone2Matrix);
	stat = attributeAffects( m_Bone3Scale, out_defBone3Matrix);
	stat = attributeAffects( m_Bone3Scale, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_PinBones, out_defBone1Matrix);
	stat = attributeAffects( m_PinBones, out_defBone2Matrix);
	stat = attributeAffects( m_PinBones, out_defBone3Matrix);
	stat = attributeAffects( m_PinBones, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_PlanTwist, out_defBone1Matrix);
	stat = attributeAffects( m_PlanTwist, out_defBone2Matrix);
	stat = attributeAffects( m_PlanTwist, out_defBone3Matrix);
	stat = attributeAffects( m_PlanTwist, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_PlanBone2Twist, out_defBone1Matrix);
	stat = attributeAffects( m_PlanBone2Twist, out_defBone2Matrix);
	stat = attributeAffects( m_PlanBone2Twist, out_defBone3Matrix);
	stat = attributeAffects( m_PlanBone2Twist, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_PlanBone3Twist, out_defBone1Matrix);
	stat = attributeAffects( m_PlanBone3Twist, out_defBone2Matrix);
	stat = attributeAffects( m_PlanBone3Twist, out_defBone3Matrix);
	stat = attributeAffects( m_PlanBone3Twist, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GuidRootMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_GuidRootMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_GuidRootMatrix, out_defBone3Matrix);
	stat = attributeAffects( m_GuidRootMatrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GuidBone2Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_GuidBone2Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_GuidBone2Matrix, out_defBone3Matrix);
	stat = attributeAffects( m_GuidBone2Matrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GuidBone3Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_GuidBone3Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_GuidBone3Matrix, out_defBone3Matrix);
	stat = attributeAffects( m_GuidBone3Matrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);	

	stat = attributeAffects( m_GuidEffMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_GuidEffMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_GuidEffMatrix, out_defBone3Matrix);
	stat = attributeAffects( m_GuidEffMatrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GuidMainUpVMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_GuidMainUpVMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_GuidMainUpVMatrix, out_defBone3Matrix);
	stat = attributeAffects( m_GuidMainUpVMatrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkRootMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_IkRootMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_IkRootMatrix, out_defBone3Matrix);
	stat = attributeAffects( m_IkRootMatrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkBone2UpVMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_IkBone2UpVMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_IkBone2UpVMatrix, out_defBone3Matrix);
	stat = attributeAffects( m_IkBone2UpVMatrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkBone3UpVMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_IkBone3UpVMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_IkBone3UpVMatrix, out_defBone3Matrix);
	stat = attributeAffects( m_IkBone3UpVMatrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkEffMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_IkEffMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_IkEffMatrix, out_defBone3Matrix);
	stat = attributeAffects( m_IkEffMatrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkMainUpVMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_IkMainUpVMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_IkMainUpVMatrix, out_defBone3Matrix);
	stat = attributeAffects( m_IkMainUpVMatrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkPinBone2Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_IkPinBone2Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_IkPinBone2Matrix, out_defBone3Matrix);
	stat = attributeAffects( m_IkPinBone2Matrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkPinBone3Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_IkPinBone3Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_IkPinBone3Matrix, out_defBone3Matrix);
	stat = attributeAffects( m_IkPinBone3Matrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkRotationOffset, out_defBone1Matrix);
	stat = attributeAffects( m_IkRotationOffset, out_defBone2Matrix);
	stat = attributeAffects( m_IkRotationOffset, out_defBone3Matrix);
	stat = attributeAffects( m_IkRotationOffset, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_fkBone1Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_fkBone1Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_fkBone1Matrix, out_defBone3Matrix);
	stat = attributeAffects( m_fkBone1Matrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_fkBone2Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_fkBone2Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_fkBone2Matrix, out_defBone3Matrix);
	stat = attributeAffects( m_fkBone2Matrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_fkBone3Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_fkBone3Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_fkBone3Matrix, out_defBone3Matrix);
	stat = attributeAffects( m_fkBone3Matrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_fkBone4Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_fkBone4Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_fkBone4Matrix, out_defBone3Matrix);
	stat = attributeAffects( m_fkBone4Matrix, out_defBone4Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Ik3BonesMemberNode::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		blendIKFKData = in_Data.inputValue(m_BlendIKFK, &returnStatus);
	float blendIKFK = blendIKFKData.asFloat();
			
	MDataHandle		symBonesMatrixData = in_Data.inputValue(m_SymBonesMatrix, &returnStatus);
	int symBonesMatrix = symBonesMatrixData.asShort();

	MDataHandle		pinBonesData = in_Data.inputValue(m_PinBones, &returnStatus);
	int pinBones = pinBonesData.asShort();

	MDataHandle		useStretchData = in_Data.inputValue(m_UseStretch, &returnStatus);
	int useStretch = useStretchData.asShort();

	MDataHandle		useSoftIKData = in_Data.inputValue(m_UseSoftIK, &returnStatus);
	int useSoftIK = useSoftIKData.asShort();

	MDataHandle		globalScaleData = in_Data.inputValue(m_GlobalScale, &returnStatus);
	float globalScale = globalScaleData.asFloat();

	MDataHandle		startSoftIKData = in_Data.inputValue(m_StartSoftIK, &returnStatus);
	float startSoftIK = startSoftIKData.asFloat();

	MDataHandle		globalBonesScaleData = in_Data.inputValue(m_GlobalBonesScale, &returnStatus);
	float globalBonesScale = globalBonesScaleData.asFloat();

	MDataHandle		bone1ScaleData = in_Data.inputValue(m_Bone1Scale, &returnStatus);
	float bone1Scale = bone1ScaleData.asFloat();

	MDataHandle		bone2ScaleData = in_Data.inputValue(m_Bone2Scale, &returnStatus);
	float bone2Scale = bone2ScaleData.asFloat();

	MDataHandle		bone3ScaleData = in_Data.inputValue(m_Bone3Scale, &returnStatus);
	float bone3Scale = bone3ScaleData.asFloat();	

	MDataHandle		planTwistData = in_Data.inputValue(m_PlanTwist, &returnStatus);
	float planTwist = planTwistData.asFloat();

	MDataHandle		planBone2TwistData = in_Data.inputValue(m_PlanBone2Twist, &returnStatus);
	float planBone2Twist = planBone2TwistData.asFloat();

	MDataHandle		planBone3TwistData = in_Data.inputValue(m_PlanBone3Twist, &returnStatus);
	float planBone3Twist = planBone3TwistData.asFloat();

	MDataHandle		ikRotationOffsetData = in_Data.inputValue(m_IkRotationOffset, &returnStatus);
	float ikRotationOffset = ikRotationOffsetData.asFloat();

	MDataHandle		guidRootMatriXData = in_Data.inputValue(m_GuidRootMatrix, &returnStatus);
	MMatrix			guidRootMatrix = guidRootMatriXData.asMatrix();

	MDataHandle		guidEffMatriXData = in_Data.inputValue(m_GuidEffMatrix, &returnStatus);
	MMatrix			guidEffMatrix = guidEffMatriXData.asMatrix();

	MDataHandle		guidBone2MatriXData = in_Data.inputValue(m_GuidBone2Matrix, &returnStatus);
	MMatrix			guidBone2Matrix = guidBone2MatriXData.asMatrix();

	MDataHandle		guidBone3MatriXData = in_Data.inputValue(m_GuidBone3Matrix, &returnStatus);
	MMatrix			guidBone3Matrix = guidBone3MatriXData.asMatrix();

	MDataHandle		guidMainUpVMatriXData = in_Data.inputValue(m_GuidMainUpVMatrix, &returnStatus);
	MMatrix			guidMainUpVMatrix = guidMainUpVMatriXData.asMatrix();

	MDataHandle		ikRootMatriXData = in_Data.inputValue(m_IkRootMatrix, &returnStatus);
	MMatrix			ikRootMatrix = ikRootMatriXData.asMatrix();

	MDataHandle		ikEffMatriXData = in_Data.inputValue(m_IkEffMatrix, &returnStatus);
	MMatrix			ikEffMatrix = ikEffMatriXData.asMatrix();

	MDataHandle		ikBone2UpVMatriXData = in_Data.inputValue(m_IkBone2UpVMatrix, &returnStatus);
	MMatrix			ikBone2UpVMatrix = ikBone2UpVMatriXData.asMatrix();

	MDataHandle		ikBone3UpVMatriXData = in_Data.inputValue(m_IkBone3UpVMatrix, &returnStatus);
	MMatrix			ikBone3UpVMatrix = ikBone3UpVMatriXData.asMatrix();

	MDataHandle		ikMainUpVMatriXData = in_Data.inputValue(m_IkMainUpVMatrix, &returnStatus);
	MMatrix			ikMainUpVMatrix = ikMainUpVMatriXData.asMatrix();

	MDataHandle		ikPinBone2Data = in_Data.inputValue(m_IkPinBone2Matrix, &returnStatus);
	MMatrix			ikPinBone2 = ikPinBone2Data.asMatrix();

	MDataHandle		ikPinBone3Data = in_Data.inputValue(m_IkPinBone3Matrix, &returnStatus);
	MMatrix			ikPinBone3 = ikPinBone3Data.asMatrix();

	MDataHandle		fkBone1MatriXData = in_Data.inputValue(m_fkBone1Matrix, &returnStatus);
	MMatrix			fkBone1Matrix = fkBone1MatriXData.asMatrix();

	MDataHandle		fkBone2MatriXData = in_Data.inputValue(m_fkBone2Matrix, &returnStatus);
	MMatrix			fkBone2Matrix = fkBone2MatriXData.asMatrix();

	MDataHandle		fkBone3MatriXData = in_Data.inputValue(m_fkBone3Matrix, &returnStatus);
	MMatrix			fkBone3Matrix = fkBone3MatriXData.asMatrix();

	MDataHandle		fkBone4MatriXData = in_Data.inputValue(m_fkBone4Matrix, &returnStatus);
	MMatrix			fkBone4Matrix = fkBone4MatriXData.asMatrix();

	if ( returnStatus == MS::kSuccess ){	
		//Convert Maya Data to Frankenstein Data.
		Matrix4x4 fknGuidRoot, fknGuidEff, fknGuidBone2, fknGuidBone3, fknGuidMainUpV;
		Matrix4x4 fknIkRoot, fknIkEff, fknIkMainUpV, fknIkBone2UpV, fknIkBone3UpV, fknIkPinBone2, fknIkPinBone3;
		Matrix4x4 fknFkBone1, fknFkBone2, fknFkBone3, fknFkBone4;
	
		ConvertMMatrixToMatrix4x4(guidRootMatrix, fknGuidRoot);
		ConvertMMatrixToMatrix4x4(guidEffMatrix, fknGuidEff);
		ConvertMMatrixToMatrix4x4(guidBone2Matrix, fknGuidBone2);
		ConvertMMatrixToMatrix4x4(guidBone3Matrix, fknGuidBone3);
		ConvertMMatrixToMatrix4x4(guidMainUpVMatrix, fknGuidMainUpV);

		ConvertMMatrixToMatrix4x4(ikRootMatrix, fknIkRoot);
		ConvertMMatrixToMatrix4x4(ikEffMatrix, fknIkEff);
		ConvertMMatrixToMatrix4x4(ikBone2UpVMatrix, fknIkBone2UpV);
		ConvertMMatrixToMatrix4x4(ikBone3UpVMatrix, fknIkBone3UpV);
		ConvertMMatrixToMatrix4x4(ikMainUpVMatrix, fknIkMainUpV);
		ConvertMMatrixToMatrix4x4(ikPinBone2, fknIkPinBone2);
		ConvertMMatrixToMatrix4x4(ikPinBone3, fknIkPinBone3);

		ConvertMMatrixToMatrix4x4(fkBone1Matrix, fknFkBone1);
		ConvertMMatrixToMatrix4x4(fkBone2Matrix, fknFkBone2);
		ConvertMMatrixToMatrix4x4(fkBone3Matrix, fknFkBone3);
		ConvertMMatrixToMatrix4x4(fkBone4Matrix, fknFkBone4);

		//Init the frankenstein Ik3BonesMember.
		Ik3BonesMember	fknIk3BonesMember(	fknGuidRoot, fknGuidBone2, fknGuidBone3, fknGuidEff, fknGuidMainUpV,
											fknIkRoot, fknIkMainUpV, fknIkBone2UpV, fknIkBone3UpV, fknIkEff, ikRotationOffset, fknIkPinBone2, fknIkPinBone3,
											fknFkBone1, fknFkBone2, fknFkBone3, fknFkBone4,
											pinBones, symBonesMatrix, blendIKFK, planTwist, planBone2Twist, planBone3Twist,
											useSoftIK, startSoftIK, globalBonesScale, bone1Scale, bone2Scale, bone3Scale, useStretch, globalScale);

		//Compute the frankenstein Ik3BonesMember.
		fknIk3BonesMember.ComputeBasicIK();


		if (in_Plug == out_defBone1Matrix || in_Plug == out_defBone2Matrix || in_Plug == out_defBone3Matrix || in_Plug == out_defBone4Matrix){
			//Convert Frankenstein Data to Maya Data.
			MMatrix	defBone1Matrix, defBone2Matrix, defBone3Matrix, defBone4Matrix;

			Matrix4x4 finalBone1, finalBone2, finalBone3, finalBone4;

			finalBone1 = fknIk3BonesMember.GetDefBone1();
			finalBone2 = fknIk3BonesMember.GetDefBone2();
			finalBone3 = fknIk3BonesMember.GetDefBone3();
			finalBone4 = fknIk3BonesMember.GetDefBone4();

			ConvertMatrix4x4ToMMatrix(finalBone1, defBone1Matrix);
			ConvertMatrix4x4ToMMatrix(finalBone2, defBone2Matrix);
			ConvertMatrix4x4ToMMatrix(finalBone3, defBone3Matrix);
			ConvertMatrix4x4ToMMatrix(finalBone4, defBone4Matrix);

			//Set the Maya output data.
			MDataHandle	outDefBone1Matrix = in_Data.outputValue(out_defBone1Matrix, &returnStatus);
			outDefBone1Matrix.set(defBone1Matrix);
			outDefBone1Matrix.setClean();

			MDataHandle	outDefBone2Matrix = in_Data.outputValue(out_defBone2Matrix, &returnStatus);
			outDefBone2Matrix.set(defBone2Matrix);
			outDefBone2Matrix.setClean();

			MDataHandle	outDefBone3Matrix = in_Data.outputValue(out_defBone3Matrix, &returnStatus);
			outDefBone3Matrix.set(defBone3Matrix);
			outDefBone3Matrix.setClean();

			MDataHandle	outDefBone4Matrix = in_Data.outputValue(out_defBone4Matrix, &returnStatus);
			outDefBone4Matrix.set(defBone4Matrix);
			outDefBone4Matrix.setClean();

		}else{
			return MS::kUnknownParameter;
		}
	}else{
		MGlobal::displayError( "Node fkn_SplineMember cannot get value.\n" );
	}

	return returnStatus;
};