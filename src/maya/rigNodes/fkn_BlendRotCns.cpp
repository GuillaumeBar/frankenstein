/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_BlendRotCns.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_BlendRotCns.h"

// Unique plugins ID.
MTypeId fkn_BlendRotCns::id( fkn_BlendRotCns_ID );

// Input Attributes.
MObject	fkn_BlendRotCns::m_BlendRotation;
MObject	fkn_BlendRotCns::m_RefMatRotA;
MObject	fkn_BlendRotCns::m_RefMatRotB;
MObject	fkn_BlendRotCns::m_RefMatPos;

//Out Attributes.
MObject	fkn_BlendRotCns::out_Matrix;


//Constructor.
fkn_BlendRotCns::fkn_BlendRotCns(){

};

//Destructor.
fkn_BlendRotCns::~fkn_BlendRotCns(){

};

//Maya Creator.
void* fkn_BlendRotCns::creator(){
	return new fkn_BlendRotCns();
};

//Maya Initialize.
MStatus fkn_BlendRotCns::initialize(){

	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 
	m_BlendRotation = nAttr.create("Blend", "Blend", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_BlendRotation);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_RefMatRotA = mAttr.create("MatrixRotationA", "MatrixRotationA");
	mAttr.setStorable(true);
	stat = addAttribute(m_RefMatRotA);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_RefMatRotB = mAttr.create("MatrixRotationB", "MatrixRotationB");
	mAttr.setStorable(true);
	stat = addAttribute(m_RefMatRotB);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_RefMatPos = mAttr.create("MatrixPosition", "MatrixPosition");
	mAttr.setStorable(true);
	stat = addAttribute(m_RefMatPos);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_Matrix = mAttr.create("Result", "Result");
	stat = addAttribute(out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_BlendRotation, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_RefMatRotA, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_RefMatRotB, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_RefMatPos, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat)

	return MS::kSuccess;
};


MStatus fkn_BlendRotCns::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		blendData = in_Data.inputValue(m_BlendRotation, &returnStatus);
	float blend = blendData.asFloat();
	float invBlend = 1.0f - blend;

	MDataHandle		refRotAData = in_Data.inputValue(m_RefMatRotA, &returnStatus);
	MMatrix			refRotA = refRotAData.asMatrix();

	MDataHandle		refRotBData = in_Data.inputValue(m_RefMatRotB, &returnStatus);
	MMatrix			refRotB = refRotBData.asMatrix();

	MDataHandle		refPosData = in_Data.inputValue(m_RefMatPos, &returnStatus);
	MMatrix			refPos = refPosData.asMatrix();

	if ( returnStatus == MS::kSuccess ){	

		if (in_Plug == out_Matrix ){

			/*
			// Extract the data to frankenstein.
			Vector3D refAAxeY, refAAxeZ;
			Vector3D refBAxeY, refBAxeZ;
			// Extract axis for Ref A.
			refAAxeY.Set(refRotA(1,0), refRotA(1,1), refRotA(1,2));
			refAAxeZ.Set(refRotA(2,0), refRotA(2,1), refRotA(2,2));
			// Extract axis for Ref B.
			refBAxeY.Set(refRotB(1,0), refRotB(1,1), refRotB(1,2));
			refBAxeZ.Set(refRotB(2,0), refRotB(2,1), refRotB(2,2));

			// Compute the new axis.
			Vector3D outAxeX, outAxeY, outAxeZ;
			// Compute the blended axe Y.
			refAAxeY.SelfMulByFloat(invBlend);
			refBAxeY.SelfMulByFloat(blend);
			outAxeY.Add(refAAxeY, refBAxeY);
			// Compute the blended axe Z.
			refAAxeZ.SelfMulByFloat(invBlend);
			refBAxeZ.SelfMulByFloat(blend);
			outAxeZ.Add(refAAxeZ, refBAxeZ);
			// Compute the axe X.
			outAxeX.CrossProduct(outAxeY, outAxeZ);
			// Compute the axe Y.
			outAxeY.CrossProduct(outAxeZ, outAxeX);
			// Normalize the axis.
			outAxeX.SelfNormalize();	
			outAxeY.SelfNormalize();	
			outAxeZ.SelfNormalize();	

			// Reconstruct the final Matrix.
			Matrix4x4 fknOutMat(outAxeX.GetX(), outAxeX.GetY(), outAxeX.GetZ(), 0.0f,
								outAxeY.GetX(), outAxeY.GetY(), outAxeY.GetZ(), 0.0f, 
								outAxeZ.GetX(), outAxeZ.GetY(), outAxeZ.GetZ(), 0.0f, 
								(float)refPos(3,0), (float)refPos(3,1), (float)refPos(3,2), 1.0f);

			// Convert to Maya.
			MMatrix outMat;
			for(unsigned int iRow=0; iRow<4; iRow++){
				for(unsigned int iCol=0; iCol<4; iCol++){
					outMat(iRow, iCol) = fknOutMat.GetValue(iRow, iCol);
				}
			}

			//Set the Maya output data.
			MDataHandle	outMatrix = in_Data.outputValue(out_Matrix, &returnStatus);
			outMatrix.set(outMat);
			outMatrix.setClean();


			*/
			
			// Convert Data to fkn.
			Matrix4x4 fknRefMatA, fknRefMatB;
			// Convert reference Matrix.
			for(unsigned int iRow=0; iRow<4; iRow++){
				for(unsigned int iCol=0; iCol<4; iCol++){
					fknRefMatA.SetValue(iRow, iCol, refRotA(iRow, iCol));
					fknRefMatB.SetValue(iRow, iCol, refRotB(iRow, iCol));
				}
			}
			// Get the scale.
			float sclAX, sclAY, sclAZ;
			float sclBX, sclBY, sclBZ;
			fknRefMatA.GetScale(sclAX, sclAY, sclAZ);
			fknRefMatB.GetScale(sclBX, sclBY, sclBZ);

			// Convert reference orientation without the scale..
			Matrix3x3 fknRefRotA, fknRefRotB;
			float scaleFactorA, scaleFactorB;
			for(unsigned int iRow=0; iRow<3; iRow++){
				for(unsigned int iCol=0; iCol<3; iCol++){
					if(iRow == 0){
						scaleFactorA = sclAX;
						scaleFactorB = sclBX;
					}else if(iRow == 1){
						scaleFactorA = sclAY;
						scaleFactorB = sclBY;
					}else{
						scaleFactorA = sclAZ;
						scaleFactorB = sclBZ;
					}
					fknRefRotA.SetValue(iRow, iCol, refRotA(iRow, iCol) / scaleFactorA);
					fknRefRotB.SetValue(iRow, iCol, refRotB(iRow, iCol) /scaleFactorB);
				}
			}

			// Convert the rotation to Quaternion.
			Quaternion quatRotA, quatRotB;
			quatRotA.FromMatrix3x3(fknRefRotA);
			quatRotB.FromMatrix3x3(fknRefRotB);

			// Compute the blend between the quaternion rotation.
			Quaternion quatBlend;
			quatBlend.Slerp(quatRotA, quatRotB, blend, true);

			// Convert the Quaterion to Matrix3x3.
			Matrix3x3 mat3Blend;
			mat3Blend = quatBlend.ToMatrix3x3();

			// Convert the Data to Maya.
			MMatrix finalTrans;
			for(unsigned int iRow=0; iRow<4; iRow++){
				for(unsigned int iCol=0; iCol<3; iCol++){
					if(iRow < 3){
						// Transfert Rotation
						finalTrans(iRow, iCol) = mat3Blend.GetValue(iRow, iCol);
					}else{
						// Transfert Position.
						finalTrans(iRow, iCol) = refPos(iRow, iCol);
					}
				}
			}

			//Set the Maya output data.
			MDataHandle	outMatrix = in_Data.outputValue(out_Matrix, &returnStatus);
			outMatrix.set(finalTrans);
			outMatrix.setClean();
			

		}else{
			return MS::kUnknownParameter;
		}
	}else{
		MGlobal::displayError( "Node fkn_SplineMember cannot get value.\n" );
	}

	return returnStatus;
};