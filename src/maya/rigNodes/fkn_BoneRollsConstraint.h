/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_BoneRollsConstraint.h
@author Guillaume Baratte
@date 2016-05-31
@brief Create some bone rolls.
*/
#ifndef _fkn_BoneRollsConstraint_
#define _fkn_BoneRollsConstraint_

#include "../fkn_Maya_Tools.h"

/*! @brief Maya Class for compute the Ik 3 bones Member.*/
class fkn_BoneRollsConstraint : public MPxNode
{
	public:
		/*! @bief The constructor.*/
		fkn_BoneRollsConstraint();
		/*! @brief The destructor. */
		virtual	~fkn_BoneRollsConstraint();

		//Maya Methode.
		/*! @brief The Maya compute function. 
		@param[in] in_Plug The input connection to the node.
		@param[in] in_Data The data block of the node.
		@return Success.
		*/
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		/*! @biref The Maya creator function. */
		static void*		creator();
		/*! @brief The Maya initialize function. */
		static MStatus		initialize();

		/*!@brief Convert the maya matrix to Frankenstein matrix.
		@param[in] in_MMatrix The Maya Matrix.
		@param[out] out_Matrix4x4 The Frankenstein Matrix.
		*/
		void ConvertMMatrixToMatrix4x4(MMatrix& in_MMatrix, Matrix4x4& out_Matrix4x4);

		/*!@brief Convert the Frankenstein matrix to Maya matrix.
		@param[in] in_Matrix4x4 The Frankenstein Matrix.
		@param[out] out_MMatrix The Maya Matrix.
		*/
		void ConvertMatrix4x4ToMMatrix(Matrix4x4& in_Matrix4x4, MMatrix& out_MMatrix);

	public:
		static	MTypeId		id;

		static MObject		m_RollsNumber;			/*! The number of roll along the bone 1.*/
		static MObject		m_AxeAlign;				/*! The axe to align the roll. 0->X. 1->Y. 2->Z.*/
		static MObject		m_Symmetrize;			/*! Active the symmetrize rotation. 0->Off. 1->On.*/
		static MObject		m_Bone1Matrix;			/*! The first bone transformation. */
		static MObject		m_Bone2Matrix;			/*! The second bone transformation. */
		static MObject		m_StartDistribution;	/*! The start range of the roll distribution. */
		static MObject		m_EndDistribution;		/*! The end range of the roll distribution. */
		static MObject		m_StartRollsOffset;		/*! The start distribution angle offset. */
		static MObject		m_EndRollsOffset;		/*! The end distribution angle offset. */
		static MObject		m_ArrayRollsBlend;		/*! The array filter for rotation blend from the start to the end.*/

		static MObject		out_ArrayRollsMatrix; /*! The array of transformation for the rolls object. */
};

#endif