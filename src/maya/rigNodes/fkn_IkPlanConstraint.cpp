/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_IkPlanConstraint.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_IkPlanConstraint.h"

// Unique plugins ID.
MTypeId     fkn_IkPlanConstraint::id( 0x80101 );

// Input Attributes.
MObject		fkn_IkPlanConstraint::m_PlanTwist;

MObject		fkn_IkPlanConstraint::m_IkRootMatrix;
MObject		fkn_IkPlanConstraint::m_IkEffMatrix;
MObject		fkn_IkPlanConstraint::m_IkMainUpVMatrix;
MObject		fkn_IkPlanConstraint::m_GlobalScale;

//Out Attributes.
MObject		fkn_IkPlanConstraint::out_Matrix;

//Constructor.
fkn_IkPlanConstraint::fkn_IkPlanConstraint(){

};

//Destructor.
fkn_IkPlanConstraint::~fkn_IkPlanConstraint(){

};

//Maya Creator.
void* fkn_IkPlanConstraint::creator(){
	return new fkn_IkPlanConstraint();
};

//Help Function.
void fkn_IkPlanConstraint::ConvertMatrix4x4ToMMatrix(Matrix4x4& in_Matrix4x4, MMatrix& out_MMatrix){
	for(unsigned int iRow=0; iRow<4; iRow++){
		for(unsigned int iCol=0; iCol<4; iCol++){
			out_MMatrix(iRow, iCol) = (double)in_Matrix4x4.GetValue(iRow,iCol);
		}
	}
};

void fkn_IkPlanConstraint::ConvertMMatrixToMatrix4x4(MMatrix& in_MMatrix, Matrix4x4& out_Matrix4x4){
	for(unsigned int iRow=0; iRow<4; iRow++){
		for(unsigned int iCol=0; iCol<4; iCol++){
			out_Matrix4x4.SetValue(iRow, iCol, (float)in_MMatrix(iRow,iCol));
		}
	}
};

//Maya Initialize.
MStatus fkn_IkPlanConstraint::initialize(){

	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 
	m_PlanTwist = nAttr.create("PlanTwist", "PlanTwist", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_PlanTwist);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GlobalScale = nAttr.create("GlobalScale", "GlobalScale", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setMin(0.000001f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_GlobalScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkRootMatrix = mAttr.create("IkRoot", "IkRoot");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkRootMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkEffMatrix = mAttr.create("IkEff", "IkEff");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkEffMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkMainUpVMatrix = mAttr.create("IkMainUpVector", "IkMainUpVector");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkMainUpVMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_Matrix = mAttr.create("OutMatrix", "OutMatrix");
	stat = addAttribute(out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_PlanTwist, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_IkRootMatrix, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_IkEffMatrix, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_IkMainUpVMatrix, out_Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_IkPlanConstraint::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		planTwistData = in_Data.inputValue(m_PlanTwist, &returnStatus);
	float planTwist = planTwistData.asFloat();

	MDataHandle		globalScaleData = in_Data.inputValue(m_GlobalScale, &returnStatus);
	float globalScale = globalScaleData.asFloat();

	MDataHandle		ikRootMatriXData = in_Data.inputValue(m_IkRootMatrix, &returnStatus);
	MMatrix			ikRootMatrix = ikRootMatriXData.asMatrix();

	MDataHandle		ikEffMatriXData = in_Data.inputValue(m_IkEffMatrix, &returnStatus);
	MMatrix			ikEffMatrix = ikEffMatriXData.asMatrix();

	MDataHandle		ikMainUpVMatriXData = in_Data.inputValue(m_IkMainUpVMatrix, &returnStatus);
	MMatrix			ikMainUpVMatrix = ikMainUpVMatriXData.asMatrix();

	if ( returnStatus == MS::kSuccess ){	
		//Convert Maya Data to Frankenstein Data.

		Matrix4x4 fknIkRoot, fknIkEff, fknIkMainUpV;

		ConvertMMatrixToMatrix4x4(ikRootMatrix, fknIkRoot);
		ConvertMMatrixToMatrix4x4(ikEffMatrix, fknIkEff);
		ConvertMMatrixToMatrix4x4(ikMainUpVMatrix, fknIkMainUpV);

		//Init the frankenstein Ik3BonesMember.
		Matrix4x4 finalMatrix;
		Quaternion quatRot;
		float twistAngleRad = planTwist * 3.14f / 180.0f;
		Vector3D ikRootPos, ikEffPos, ikMainUpVPos;
		Vector3D vAxeX, vAxeY, vAxeZ, vPos;

		ikRootPos.Set(fknIkRoot.GetValue(3,0), fknIkRoot.GetValue(3,1), fknIkRoot.GetValue(3,2));
		ikEffPos.Set(fknIkEff.GetValue(3,0), fknIkEff.GetValue(3,1), fknIkEff.GetValue(3,2));
		ikMainUpVPos.Set(fknIkMainUpV.GetValue(3,0), fknIkMainUpV.GetValue(3,1), fknIkMainUpV.GetValue(3,2));

		vAxeY.Sub(ikEffPos, ikRootPos);
		vAxeY.SelfMulByFloat(0.5f);
		vPos.Add(ikRootPos, vAxeY);
		vAxeY.SelfNormalize();

		vAxeX.Sub(ikMainUpVPos, vPos);
		quatRot.FromAxisAndAngle(vAxeY.GetX(), vAxeY.GetY(), vAxeY.GetZ(), twistAngleRad);
		vAxeX.SelfRotateByQuaternionFG(quatRot);

		vAxeZ.CrossProduct(vAxeX, vAxeY);
		vAxeX.CrossProduct(vAxeY, vAxeZ);

		vAxeX.SelfNormalize();
		vAxeZ.SelfNormalize();

		vAxeX.SelfMulByFloat(globalScale);
		vAxeY.SelfMulByFloat(globalScale);
		vAxeZ.SelfMulByFloat(globalScale);

		finalMatrix.Set(vAxeX.GetX(), vAxeX.GetY(), vAxeX.GetZ(), 0.0f,
						vAxeY.GetX(), vAxeY.GetY(), vAxeY.GetZ(), 0.0f,
						vAxeZ.GetX(), vAxeZ.GetY(), vAxeZ.GetZ(), 0.0f,
						vPos.GetX(), vPos.GetY(), vPos.GetZ(), 1.0f );


		if (in_Plug == out_Matrix){
			//Convert Frankenstein Data to Maya Data.
			MMatrix	outMayaMatrix;

			ConvertMatrix4x4ToMMatrix(finalMatrix, outMayaMatrix);

			//Set the Maya output data.
			MDataHandle	outMatrix = in_Data.outputValue(out_Matrix, &returnStatus);
			outMatrix.set(outMayaMatrix);
			outMatrix.setClean();

		}else{
			return MS::kUnknownParameter;
		}
	}else{
		MGlobal::displayError( "Node fkn_SplineMember cannot get value.\n" );
	}

	return returnStatus;
};