/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_SplineMemberNode.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_SplineMemberNode.h"


// Unique plugins ID.
MTypeId     fkn_SplineMemberNode::id( 0x80004 );

// Input Attributes.
MObject		fkn_SplineMemberNode::m_ArrayControlers;
MObject 	fkn_SplineMemberNode::m_ArrayUserTangents;
MObject		fkn_SplineMemberNode::m_ParentInvertMatrix;
MObject		fkn_SplineMemberNode::m_AxeAlignDeformers;
MObject		fkn_SplineMemberNode::m_EndAddRollAngle;
MObject		fkn_SplineMemberNode::m_MaxLengthMulti;
MObject		fkn_SplineMemberNode::m_MaxStretchAxe1Scale;
MObject		fkn_SplineMemberNode::m_MaxStretchAxe2Scale;
MObject		fkn_SplineMemberNode::m_MinLengthMulti;
MObject		fkn_SplineMemberNode::m_MinStretchAxe1Scale;
MObject		fkn_SplineMemberNode::m_MinStretchAxe2Scale;
MObject		fkn_SplineMemberNode::m_NormalizeIteration;
MObject		fkn_SplineMemberNode::m_NormalizePosition;
MObject		fkn_SplineMemberNode::m_NumberDeformers;
MObject		fkn_SplineMemberNode::m_RestLength;
MObject		fkn_SplineMemberNode::m_SplEndRange;
MObject		fkn_SplineMemberNode::m_SplMoveRange;
MObject		fkn_SplineMemberNode::m_SplStartRange;
MObject		fkn_SplineMemberNode::m_StartAddRollAngle;
MObject		fkn_SplineMemberNode::m_StretchType;
MObject		fkn_SplineMemberNode::m_UseAddRoll;
MObject		fkn_SplineMemberNode::m_UseSquatch;
MObject		fkn_SplineMemberNode::m_FilterAxeAlign;
MObject		fkn_SplineMemberNode::m_FilterRotationBlend;
MObject		fkn_SplineMemberNode::m_FilterScaleAxe1;
MObject		fkn_SplineMemberNode::m_FilterScaleAxe2;
MObject		fkn_SplineMemberNode::m_TangentType;
MObject		fkn_SplineMemberNode::m_ScaleControlerType;
MObject		fkn_SplineMemberNode::m_HierarchieType;
MObject		fkn_SplineMemberNode::m_UseOnlyAddRoll;
MObject     fkn_SplineMemberNode::m_RotationType;
MObject     fkn_SplineMemberNode::m_CurveType;
MObject 	fkn_SplineMemberNode::m_GlobalScale;
MObject 	fkn_SplineMemberNode::m_UseUserTangents;

//Out Attributes.
MObject		fkn_SplineMemberNode::out_ArrayDefPosition;
MObject		fkn_SplineMemberNode::out_RotX;
MObject		fkn_SplineMemberNode::out_RotY;
MObject		fkn_SplineMemberNode::out_RotZ;
MObject		fkn_SplineMemberNode::out_ArrayDefRotEuler;
MObject		fkn_SplineMemberNode::out_ArrayDefScale;

//Constructor.
fkn_SplineMemberNode::fkn_SplineMemberNode()
{

};

//Destructor.
fkn_SplineMemberNode::~fkn_SplineMemberNode()
{

};

//Maya Creator.
void* fkn_SplineMemberNode::creator()
{
	return new fkn_SplineMemberNode();
};

MStatus fkn_SplineMemberNode::AddInputAttribute(	MObject&				in_Attribute,
													MString					in_LongName,
													MString					in_ShortName,
													MFnNumericData::Type 	in_Type,
													double					in_Val,
													double					in_MinVal,
													double					in_MaxVal,
													bool					in_Keyable,
													bool					in_Array,
													bool					in_Connectable,
													bool					in_Storable,
													bool					in_ChannelBox,
													bool					in_Writable )
{
	MFnNumericAttribute nAttr;
	MStatus				stat;
	in_Attribute = nAttr.create(in_LongName, in_ShortName, in_Type, in_Val, &stat );
	nAttr.setKeyable(in_Keyable);
	nAttr.setArray(in_Array);
	nAttr.setConnectable(in_Connectable);
	nAttr.setStorable(in_Storable);
	nAttr.setChannelBox(in_ChannelBox);
	nAttr.setWritable(in_Writable);


	stat = addAttribute(in_Attribute);

	return stat;
}

//Maya Initialize.
MStatus fkn_SplineMemberNode::initialize()
{
	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 
	m_NumberDeformers = nAttr.create("NumberDeformer", "nbDef", MFnNumericData::kInt, 10, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(100);
	nAttr.setMin(3);
	nAttr.setStorable(true);
	stat = addAttribute(m_NumberDeformers);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_RestLength = nAttr.create("RestLength", "restL", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(50.0f);
	nAttr.setMin(0.001f);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	stat = addAttribute(m_RestLength);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	
	m_AxeAlignDeformers = eAttr.create("AxeAlign", "axeA", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Axe X", 0);
	eAttr.addField("Axe Y", 1);
	eAttr.addField("Axe Z", 2);
	eAttr.setStorable(true);
	stat = addAttribute(m_AxeAlignDeformers);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_CurveType = eAttr.create("CurveType", "CurveType", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Bezier", 0);
	eAttr.addField("Fit Bezier", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_CurveType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_UseUserTangents = eAttr.create("UseUserTangents", "UseUserTangents", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Auto", 0);
	eAttr.addField("User", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_UseUserTangents);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_RotationType = eAttr.create("RotationType", "RotType", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("First And Last", 0);
	eAttr.addField("All", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_RotationType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_NormalizePosition = nAttr.create("NormalizePosition", "normP", MFnNumericData::kBoolean, 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	stat = addAttribute(m_NormalizePosition);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_NormalizeIteration = nAttr.create("NormalizeIteration", "normIt", MFnNumericData::kInt, 10, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(100);
	nAttr.setMin(1);
	nAttr.setStorable(true);
	nAttr.setKeyable(false);
	stat = addAttribute(m_NormalizeIteration);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GlobalScale = nAttr.create("GlobalScale", "GlobalScale", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.00001f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_GlobalScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_SplStartRange = nAttr.create("StartRange", "startR", MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(1.0f);
	nAttr.setMax(1.0f);
	nAttr.setMin(0.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_SplStartRange);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_SplEndRange = nAttr.create("EndRange", "endR", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(1.0f);
	nAttr.setMax(1.0f);
	nAttr.setMin(0.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_SplEndRange);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_SplMoveRange = nAttr.create("MoveRange", "moveR", MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(1.0f);
	nAttr.setMax(1.0f);
	nAttr.setMin(-1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_SplMoveRange);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = AddInputAttribute(m_UseAddRoll, "UseAddRoll", "useAR", MFnNumericData::kBoolean, 0, 0, 1, true, false, true, true, false, true );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_StartAddRollAngle = nAttr.create("StartAddRoll", "startAR", MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(360.0f);
	nAttr.setSoftMin(-360.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_StartAddRollAngle);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_EndAddRollAngle = nAttr.create("EndAddRoll", "endAR", MFnNumericData::kFloat, 0.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(360.0f);
	nAttr.setSoftMin(-360.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_EndAddRollAngle);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_StretchType = eAttr.create("StretchType", "stretchT", 2, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No Stretch", 0);
	eAttr.addField("Limited Stretch", 1);
	eAttr.addField("Free Stretch", 2);
	eAttr.setStorable(true);
	eAttr.setKeyable(true);
	stat = addAttribute(m_StretchType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_MaxLengthMulti = nAttr.create("MaxLengthMulti", "maxLM", MFnNumericData::kFloat, 2.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(10.0f);
	nAttr.setSoftMin(0.01f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_MaxLengthMulti);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_MaxStretchAxe1Scale = nAttr.create("MaxStretchAxe1Scale", "maxSA1S", MFnNumericData::kFloat, 0.5f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(10.0f);
	nAttr.setSoftMin(0.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_MaxStretchAxe1Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_MaxStretchAxe2Scale = nAttr.create("MaxStretchAxe2Scale", "maxSA2S", MFnNumericData::kFloat, 0.5f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(10.0f);
	nAttr.setSoftMin(0.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_MaxStretchAxe2Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_MinLengthMulti = nAttr.create("MinLengthMulti", "minLM", MFnNumericData::kFloat, 0.5f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(10.0f);
	nAttr.setSoftMin(0.01f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_MinLengthMulti);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_MinStretchAxe1Scale = nAttr.create("MinStretchAxe1Scale", "minSA1S", MFnNumericData::kFloat, 2.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(10.0f);
	nAttr.setSoftMin(0.01f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_MinStretchAxe1Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_MinStretchAxe2Scale = nAttr.create("MinStretchAxe2Scale", "minSA2S", MFnNumericData::kFloat, 2.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setSoftMax(10.0f);
	nAttr.setSoftMin(0.01f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_MinStretchAxe2Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = AddInputAttribute(m_UseSquatch, "UseSquatch", "useSq", MFnNumericData::kBoolean, 0, 0, 1, true, false, true, true, false, true );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	
	m_FilterAxeAlign = rAttr.createCurveRamp("FilterAxeAlign", "fAxeA", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_FilterAxeAlign);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_FilterRotationBlend = rAttr.createCurveRamp("FilterRotationBlend", "fRotB", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_FilterRotationBlend);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_FilterScaleAxe1 = rAttr.createCurveRamp("FilterScaleAxe1", "fSclA1", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_FilterScaleAxe1);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_FilterScaleAxe2 = rAttr.createCurveRamp("FilterScaleAxe2", "fSclA2", &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = addAttribute(m_FilterScaleAxe2);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_TangentType = eAttr.create("TangentType", "tangType", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Point To Point", 0);
	eAttr.addField("True Tangent", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_TangentType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ScaleControlerType = eAttr.create("ConrtolerScaleType", "conSclTyp", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("None", 0);
	eAttr.addField("First And Last", 1);
	eAttr.addField("All", 2);
	eAttr.setStorable(true);
	stat = addAttribute(m_ScaleControlerType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_HierarchieType = eAttr.create("HierarchieType", "hierarType", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Parallel", 0);
	eAttr.addField("Serial", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_HierarchieType);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_UseOnlyAddRoll = eAttr.create("UseOnlyAddRoll", "UseOnlyAR", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Quaternion Slerp", 0);
	eAttr.addField("End Add Roll", 1);
	stat = addAttribute(m_UseOnlyAddRoll);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayControlers = mAttr.create("ControlersIK", "aConIK");
	mAttr.setArray(true);
	mAttr.setKeyable(false);
	stat = addAttribute( m_ArrayControlers );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ArrayUserTangents = mAttr.create("UserTangents", "UserTangents");
	mAttr.setArray(true);
	mAttr.setKeyable(false);
	stat = addAttribute( m_ArrayUserTangents );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ParentInvertMatrix = mAttr.create("ParentInvertMatrix", "pInvMat");
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	mAttr.setKeyable(false);
	mAttr.setStorable(false);
	stat = addAttribute(m_ParentInvertMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);


	//Define the output attribut.
	out_ArrayDefPosition = nAttr.create("OutDefPos", "outDP", MFnNumericData::k3Float, 0.0f, &stat);
	nAttr.setWritable(false);
	nAttr.setStorable(false);
	nAttr.setArray(true);
	nAttr.setUsesArrayDataBuilder(true);
	stat = addAttribute( out_ArrayDefPosition );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_RotX = uAttr.create("OutRotX", "outRX", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_RotY = uAttr.create("OutRotY", "outRY", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_RotZ = uAttr.create("OutRotZ", "outRZ", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_ArrayDefRotEuler = nAttr.create("OutDefEuler", "outDE", out_RotX, out_RotY, out_RotZ, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	nAttr.setArray(true);
	nAttr.setUsesArrayDataBuilder(true);
	stat = addAttribute( out_ArrayDefRotEuler );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_ArrayDefScale = nAttr.create("OutDefScale", "outDS", MFnNumericData::k3Float, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	nAttr.setArray(true);
	nAttr.setUsesArrayDataBuilder(true);
	stat = addAttribute( out_ArrayDefScale );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_FilterAxeAlign, out_ArrayDefPosition);
	stat = attributeAffects( m_FilterAxeAlign, out_ArrayDefRotEuler);
	stat = attributeAffects( m_FilterAxeAlign, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_ArrayUserTangents, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayUserTangents, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayUserTangents, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_UseUserTangents, out_ArrayDefPosition);
	stat = attributeAffects( m_UseUserTangents, out_ArrayDefRotEuler);
	stat = attributeAffects( m_UseUserTangents, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GlobalScale, out_ArrayDefPosition);
	stat = attributeAffects( m_GlobalScale, out_ArrayDefRotEuler);
	stat = attributeAffects( m_GlobalScale, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_FilterRotationBlend, out_ArrayDefPosition);
	stat = attributeAffects( m_FilterRotationBlend, out_ArrayDefRotEuler);
	stat = attributeAffects( m_FilterRotationBlend, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_FilterScaleAxe1, out_ArrayDefPosition);
	stat = attributeAffects( m_FilterScaleAxe1, out_ArrayDefRotEuler);
	stat = attributeAffects( m_FilterScaleAxe1, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_CurveType, out_ArrayDefPosition);
	stat = attributeAffects( m_CurveType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_CurveType, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_RotationType, out_ArrayDefPosition);
	stat = attributeAffects( m_RotationType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_RotationType, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_FilterScaleAxe2, out_ArrayDefPosition);
	stat = attributeAffects( m_FilterScaleAxe2, out_ArrayDefRotEuler);
	stat = attributeAffects( m_FilterScaleAxe2, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_ArrayControlers, out_ArrayDefPosition);
	stat = attributeAffects( m_ArrayControlers, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ArrayControlers, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_AxeAlignDeformers, out_ArrayDefPosition);
	stat = attributeAffects( m_AxeAlignDeformers, out_ArrayDefRotEuler);
	stat = attributeAffects( m_AxeAlignDeformers, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_EndAddRollAngle, out_ArrayDefPosition);
	stat = attributeAffects( m_EndAddRollAngle, out_ArrayDefRotEuler);
	stat = attributeAffects( m_EndAddRollAngle, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);	

	stat = attributeAffects( m_MaxLengthMulti, out_ArrayDefPosition);
	stat = attributeAffects( m_MaxLengthMulti, out_ArrayDefRotEuler);
	stat = attributeAffects( m_MaxLengthMulti, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_MaxStretchAxe1Scale, out_ArrayDefPosition);
	stat = attributeAffects( m_MaxStretchAxe1Scale, out_ArrayDefRotEuler);
	stat = attributeAffects( m_MaxStretchAxe1Scale, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);	

	stat = attributeAffects( m_MaxStretchAxe2Scale, out_ArrayDefPosition);
	stat = attributeAffects( m_MaxStretchAxe2Scale, out_ArrayDefRotEuler);
	stat = attributeAffects( m_MaxStretchAxe2Scale, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);	

	stat = attributeAffects( m_MinLengthMulti, out_ArrayDefPosition);
	stat = attributeAffects( m_MinLengthMulti, out_ArrayDefRotEuler);
	stat = attributeAffects( m_MinLengthMulti, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_MinStretchAxe1Scale, out_ArrayDefPosition);
	stat = attributeAffects( m_MinStretchAxe1Scale, out_ArrayDefRotEuler);
	stat = attributeAffects( m_MinStretchAxe1Scale, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_MinStretchAxe2Scale, out_ArrayDefPosition);
	stat = attributeAffects( m_MinStretchAxe2Scale, out_ArrayDefRotEuler);
	stat = attributeAffects( m_MinStretchAxe2Scale, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_NormalizeIteration, out_ArrayDefPosition);
	stat = attributeAffects( m_NormalizeIteration, out_ArrayDefRotEuler);
	stat = attributeAffects( m_NormalizeIteration, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_NormalizePosition, out_ArrayDefPosition);
	stat = attributeAffects( m_NormalizePosition, out_ArrayDefRotEuler);
	stat = attributeAffects( m_NormalizePosition, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_NumberDeformers, out_ArrayDefPosition);
	stat = attributeAffects( m_NumberDeformers, out_ArrayDefRotEuler);
	stat = attributeAffects( m_NumberDeformers, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_RestLength, out_ArrayDefPosition);
	stat = attributeAffects( m_RestLength, out_ArrayDefRotEuler);
	stat = attributeAffects( m_RestLength, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_SplEndRange, out_ArrayDefPosition);
	stat = attributeAffects( m_SplEndRange, out_ArrayDefRotEuler);
	stat = attributeAffects( m_SplEndRange, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_SplMoveRange, out_ArrayDefPosition);
	stat = attributeAffects( m_SplMoveRange, out_ArrayDefRotEuler);
	stat = attributeAffects( m_SplMoveRange, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_SplStartRange, out_ArrayDefPosition);
	stat = attributeAffects( m_SplStartRange, out_ArrayDefRotEuler);
	stat = attributeAffects( m_SplStartRange, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_StartAddRollAngle, out_ArrayDefPosition);
	stat = attributeAffects( m_StartAddRollAngle, out_ArrayDefRotEuler);
	stat = attributeAffects( m_StartAddRollAngle, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_StretchType, out_ArrayDefPosition);
	stat = attributeAffects( m_StretchType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_StretchType, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_UseAddRoll, out_ArrayDefPosition);
	stat = attributeAffects( m_UseAddRoll, out_ArrayDefRotEuler);
	stat = attributeAffects( m_UseAddRoll, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_UseSquatch, out_ArrayDefPosition);
	stat = attributeAffects( m_UseSquatch, out_ArrayDefRotEuler);
	stat = attributeAffects( m_UseSquatch, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_TangentType, out_ArrayDefPosition);
	stat = attributeAffects( m_TangentType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_TangentType, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_ScaleControlerType, out_ArrayDefPosition);
	stat = attributeAffects( m_ScaleControlerType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ScaleControlerType, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_HierarchieType, out_ArrayDefPosition);
	stat = attributeAffects( m_HierarchieType, out_ArrayDefRotEuler);
	stat = attributeAffects( m_HierarchieType, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_UseOnlyAddRoll, out_ArrayDefPosition);
	stat = attributeAffects( m_UseOnlyAddRoll, out_ArrayDefRotEuler);
	stat = attributeAffects( m_UseOnlyAddRoll, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_ParentInvertMatrix, out_ArrayDefPosition);
	stat = attributeAffects( m_ParentInvertMatrix, out_ArrayDefRotEuler);
	stat = attributeAffects( m_ParentInvertMatrix, out_ArrayDefScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_SplineMemberNode::compute(const MPlug& in_Plug, MDataBlock& in_Data)
{
	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		numberDeformersData = in_Data.inputValue(m_NumberDeformers, &returnStatus);
	int numberDeformers = numberDeformersData.asInt();
			
	MDataHandle		axeAlignData = in_Data.inputValue(m_AxeAlignDeformers, &returnStatus);
	int axeAlign = axeAlignData.asShort();

	MDataHandle		useUserTangentsData = in_Data.inputValue(m_UseUserTangents, &returnStatus);
	int useUserTangents = useUserTangentsData.asShort();

	MDataHandle		endAddRollAngleData = in_Data.inputValue(m_EndAddRollAngle, &returnStatus);
	float endAddRollAngle = endAddRollAngleData.asFloat();

	MDataHandle		maxLenghtMultiData = in_Data.inputValue(m_MaxLengthMulti, &returnStatus);
	float maxLengthMulti = maxLenghtMultiData.asFloat();

	MDataHandle		maxStretchAxe1ScaleData = in_Data.inputValue(m_MaxStretchAxe1Scale, &returnStatus);
	float maxStretchAxe1Scale = maxStretchAxe1ScaleData.asFloat();

	MDataHandle		maxStretchAxe2ScaleData = in_Data.inputValue(m_MaxStretchAxe2Scale, &returnStatus);
	float maxStretchAxe2Scale = maxStretchAxe2ScaleData.asFloat();

	MDataHandle		globalScaleData = in_Data.inputValue(m_GlobalScale, &returnStatus);
	float globalScale = globalScaleData.asFloat();

	MDataHandle		minLengthMultiData = in_Data.inputValue(m_MinLengthMulti, &returnStatus);
	float minLengthMulti = minLengthMultiData.asFloat();

	MDataHandle		minStretchAxe1ScaleData = in_Data.inputValue(m_MinStretchAxe1Scale, &returnStatus);
	float minStretchAxe1Scale = minStretchAxe1ScaleData.asFloat();

	MDataHandle		minStretchAxe2ScaleData = in_Data.inputValue(m_MinStretchAxe2Scale, &returnStatus);
	float minStretchAxe2Scale = minStretchAxe2ScaleData.asFloat();

	MDataHandle		normalizeIterationData = in_Data.inputValue(m_NormalizeIteration, &returnStatus);
	int normalizeIteration = normalizeIterationData.asInt();

	MDataHandle		normalizePositionData = in_Data.inputValue(m_NormalizePosition, &returnStatus);
	bool normalizePosition = normalizePositionData.asBool();

	MDataHandle		restLengthData = in_Data.inputValue(m_RestLength, &returnStatus);
	float restLength = restLengthData.asFloat();

	MDataHandle		splineEndRangeData = in_Data.inputValue(m_SplEndRange, &returnStatus);
	float splineEndRange = splineEndRangeData.asFloat();

	MDataHandle		splineMoveRangeData = in_Data.inputValue(m_SplMoveRange, &returnStatus);
	float splineMoveRange = splineMoveRangeData.asFloat();

	MDataHandle		splineStartRangeData = in_Data.inputValue(m_SplStartRange, &returnStatus);
	float splineStartRange = splineStartRangeData.asFloat();

	MDataHandle		startAddRollAngleData = in_Data.inputValue(m_StartAddRollAngle, &returnStatus);
	float startAddRollAngle = startAddRollAngleData.asFloat();

	MDataHandle		stretchTypeData = in_Data.inputValue(m_StretchType, &returnStatus);
	int stretchType = stretchTypeData.asShort();

	MDataHandle		curveTypeData = in_Data.inputValue(m_CurveType, &returnStatus);
	int curveType = curveTypeData.asShort();

	MDataHandle		rotationTypeData = in_Data.inputValue(m_RotationType, &returnStatus);
	int rotationType = rotationTypeData.asShort();

	MDataHandle		useAddRollData = in_Data.inputValue(m_UseAddRoll, &returnStatus);
	bool useAddRoll = useAddRollData.asBool();

	MDataHandle		useSquatchData = in_Data.inputValue(m_UseSquatch, &returnStatus);
	bool useSquatch = useSquatchData.asBool();

	MDataHandle		tangentTypeData = in_Data.inputValue(m_TangentType, &returnStatus);
	int tangentType = tangentTypeData.asShort();

	MDataHandle		scaleControlerTypeData = in_Data.inputValue(m_ScaleControlerType, &returnStatus);
	int scaleControlerType = scaleControlerTypeData.asShort();

	MDataHandle		hierarchieTypeData = in_Data.inputValue(m_HierarchieType, &returnStatus);
	int hierarchieType = hierarchieTypeData.asShort();

	MDataHandle		useOnlyAddRollData = in_Data.inputValue(m_UseOnlyAddRoll, &returnStatus);
	int useOnlyAddRoll = useOnlyAddRollData.asShort();

	MObject			thisNode = thisMObject();
	MRampAttribute	filterAxeAlignData ( thisNode, m_FilterAxeAlign, &returnStatus );
	MRampAttribute	filterRotationBlendData ( thisNode, m_FilterRotationBlend, &returnStatus );
	MRampAttribute	filterScaleAxe1Data ( thisNode, m_FilterScaleAxe1, &returnStatus );
	MRampAttribute	filterScaleAxe2Data ( thisNode, m_FilterScaleAxe2, &returnStatus );

	MArrayDataHandle arrayControlers = in_Data.inputArrayValue(m_ArrayControlers, &returnStatus);

	MArrayDataHandle arrayUserTangents = in_Data.inputArrayValue(m_ArrayUserTangents, &returnStatus);

	MDataHandle		parentInvertMatrixData = in_Data.inputValue(m_ParentInvertMatrix, &returnStatus);
	MMatrix			parentInvertMatrix = parentInvertMatrixData.asMatrix();

	if ( returnStatus == MS::kSuccess )
	{
			
		//Convert Maya Data to Frankenstein Data.
		int nbControlers = arrayControlers.elementCount();
		arrayControlers.jumpToElement(0);
		int nbTangents = arrayUserTangents.elementCount();
		arrayUserTangents.jumpToElement(0);

		Matrix4x4				parentInvMat;
		DataArray<Matrix4x4>	listControlersIK(nbControlers);
		DataArray<Matrix4x4>	listUserTangents(nbTangents);
		DataArray<float>		filterAxeAlign(numberDeformers);
		DataArray<float>		filterRotationBlend(numberDeformers);
		DataArray<float>		filterScaleAxe1(numberDeformers);
		DataArray<float>		filterScaleAxe2(numberDeformers);

		//Convert the controler transformation matrix.
		MMatrix					currentControler;
		for ( int iCon=0; iCon<nbControlers; iCon++ )
		{
			currentControler = arrayControlers.inputValue().asMatrix();
			for ( int iRow=0; iRow<4; iRow++)
			{
				for ( int iCol=0; iCol<4; iCol++ )
				{
					listControlersIK[iCon].SetValue(iRow, iCol, (float)currentControler(iRow, iCol));
				}
			}

			if ( iCon < nbControlers )
			{
				arrayControlers.next();
			}
		}

		for ( int iTan=0; iTan<nbTangents; iTan++ )
		{
			currentControler = arrayUserTangents.inputValue().asMatrix();
			for ( int iRow=0; iRow<4; iRow++)
			{
				for ( int iCol=0; iCol<4; iCol++ )
				{
					listUserTangents[iTan].SetValue(iRow, iCol, (float)currentControler(iRow, iCol));
				}
			}

			if ( iTan < nbTangents )
			{
				arrayUserTangents.next();
			}
		}		

		for (int iRow=0; iRow<4; iRow++)
		{
			for (int iCol=0; iCol<4; iCol++)
			{
				parentInvMat.SetValue(iRow, iCol, (float)parentInvertMatrix(iRow, iCol));
			}
		}

		//Convert the curves filter.
		float				curvePos, AxeAlignVal, RotBlendVal, Scale1Val, Scale2Val;
		for ( int iDef=0; iDef<numberDeformers; iDef++ )
		{
			curvePos = (float)iDef / (float)(numberDeformers-1);

			filterAxeAlignData.getValueAtPosition(curvePos, AxeAlignVal, &returnStatus);
			filterAxeAlign[iDef] = AxeAlignVal;

			filterRotationBlendData.getValueAtPosition(curvePos, RotBlendVal, &returnStatus);
			filterRotationBlend[iDef] = RotBlendVal;

			filterScaleAxe1Data.getValueAtPosition(curvePos, Scale1Val, &returnStatus);
			filterScaleAxe1[iDef] = Scale1Val;

			filterScaleAxe2Data.getValueAtPosition(curvePos, Scale2Val, &returnStatus);
			filterScaleAxe2[iDef] = Scale2Val;
		}
			
		//Init the frankenstein splineMember.
		SplineMember	fknSplineMember;
		fknSplineMember.Set (	listControlersIK,
								filterAxeAlign,
								filterRotationBlend,
								filterScaleAxe1,
								filterScaleAxe2,
								numberDeformers,
								normalizePosition,
								normalizeIteration,
								axeAlign,
								useAddRoll,
								startAddRollAngle,
								endAddRollAngle,
								stretchType,
								restLength,
								maxLengthMulti,
								minLengthMulti,
								useSquatch,
								maxStretchAxe1Scale,
								maxStretchAxe2Scale,
								minStretchAxe1Scale,
								minStretchAxe2Scale,
								splineStartRange,
								splineEndRange,
								splineMoveRange,
								tangentType,
								scaleControlerType,
								hierarchieType,
								1,
								parentInvMat,
								useOnlyAddRoll,
								rotationType,
								curveType,
								globalScale,
								useUserTangents,
								listUserTangents);

		//Compute the frankenstein SplineMember.
		fknSplineMember.ComputeDeformersPosition();
		fknSplineMember.ComputeDeformersOrientation();
		fknSplineMember.ComputeDeformerScale();
		fknSplineMember.ComputeHierarchie();

		if (in_Plug == out_ArrayDefPosition || in_Plug == out_ArrayDefRotEuler || in_Plug == out_ArrayDefScale)
		{
			//Convert Frankenstein Data to Maya Data.
			MArrayDataBuilder	outArrayDefPosition ( out_ArrayDefPosition, numberDeformers, &returnStatus );
			Vector3D			fromFknPosition;
			MDataHandle			outSetPosition;
			
			MArrayDataBuilder	outArrayDefEuler ( out_ArrayDefRotEuler, numberDeformers, &returnStatus);
			Vector3D			fromFknEuler;
			MVector				toEuler;
			MDataHandle			outSetEuler;
			
			MArrayDataBuilder	outArrayDefScale ( out_ArrayDefScale, numberDeformers, &returnStatus);
			Vector3D			fromFknScale;
			MDataHandle			outSetScale;
			
			for ( int iDef=0; iDef<numberDeformers; iDef++ )
			{
				//Get Position
				fromFknPosition = fknSplineMember.GetDefPositionAt(iDef);
				outSetPosition = outArrayDefPosition.addElement(iDef);
				outSetPosition.set(fromFknPosition.GetX(), fromFknPosition.GetY(), fromFknPosition.GetZ());
				//Get Rotation.
				fromFknEuler = fknSplineMember.GetDefRotEulerAt(iDef, 1);
				outSetEuler = outArrayDefEuler.addElement(iDef);
				toEuler.x = fromFknEuler.GetX(); 
				toEuler.y = fromFknEuler.GetY(); 
				toEuler.z = fromFknEuler.GetZ(); 
				outSetEuler.set(toEuler);
				//Get Scale.
				fromFknScale = fknSplineMember.GetDefScaleAt(iDef, 0);
				outSetScale = outArrayDefScale.addElement(iDef);
				outSetScale.set (fromFknScale.GetX(), fromFknScale.GetY(), fromFknScale.GetZ());
			}

			MArrayDataHandle arrayOutDefPosition = in_Data.outputArrayValue(out_ArrayDefPosition, &returnStatus);
			arrayOutDefPosition.set ( outArrayDefPosition );
			arrayOutDefPosition.setAllClean();
			
			MArrayDataHandle arrayOutDefEuler = in_Data.outputArrayValue(out_ArrayDefRotEuler, &returnStatus);
			arrayOutDefEuler.set ( outArrayDefEuler );
			arrayOutDefEuler.setAllClean();
			
			MArrayDataHandle arrayOutDefScale = in_Data.outputArrayValue(out_ArrayDefScale, &returnStatus);
			arrayOutDefScale.set ( outArrayDefScale );
			arrayOutDefScale.setAllClean();
			
		}
		else
		{
			return MS::kUnknownParameter;
		}
	}
	else
	{
		MGlobal::displayError( "Node fkn_SplineMember cannot get value.\n" );
	}



	return returnStatus;
};