/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_ChainOnNurbsSurfaceCns.h
@author Guillaume Baratte
@date 2016-04-28

*/

#ifndef _fkn_ChainOnNurbsSurfaceCns_
#define _fkn_ChainOnNurbsSurfaceCns_


#include "../fkn_Maya_Tools.h"

class fkn_ChainOnNurbsSurfaceCns : public MPxNode
{
	public:
		//Constructor.
							fkn_ChainOnNurbsSurfaceCns();
		//Destructor.
		virtual				~fkn_ChainOnNurbsSurfaceCns();

		//Maya Methode.
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		static void*		creator();
		static MStatus		initialize();

	public:
		static	MTypeId		id;

		static MObject m_NurbsSurface;
		static MObject m_RefOrientMat1;
		static MObject m_RefOrientMat2;
		static MObject m_RefOrientBlend;
		static MObject m_CoordUP0;
		static MObject m_CoordVP0;
		static MObject m_CoordUP1;
		static MObject m_CoordVP1;
		static MObject m_CoordUP2;
		static MObject m_CoordVP2;
		static MObject m_CoordUP3;
		static MObject m_CoordVP3;

		static MObject out_Mat0;
		static MObject out_Mat1;
		static MObject out_Mat2;
		static MObject out_Mat3;						
};

#endif