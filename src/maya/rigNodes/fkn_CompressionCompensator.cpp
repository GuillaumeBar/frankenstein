/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_CompressionCompensator.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_CompressionCompensator.h"

// Unique plugins ID.
MTypeId fkn_CompressionCompensator::id( fkn_CompressionCompensatorID );

// Input Attributes.
MObject	fkn_CompressionCompensator::m_MatrixA;
MObject	fkn_CompressionCompensator::m_MatrixB;
MObject	fkn_CompressionCompensator::m_GuideMatrixA;
MObject	fkn_CompressionCompensator::m_GuideMatrixB;
MObject	fkn_CompressionCompensator::m_UseStretch;
MObject	fkn_CompressionCompensator::m_UseGuideRest;
MObject	fkn_CompressionCompensator::m_RefAxe;
MObject	fkn_CompressionCompensator::m_RefAnchorAxe;
MObject	fkn_CompressionCompensator::m_RestLength;
MObject	fkn_CompressionCompensator::m_BlendRot;
MObject	fkn_CompressionCompensator::m_BlendPos;

//Out Attributes.
MObject	fkn_CompressionCompensator::out_Result;


//Constructor.
fkn_CompressionCompensator::fkn_CompressionCompensator(){

};

//Destructor.
fkn_CompressionCompensator::~fkn_CompressionCompensator(){

};

//Maya Creator.
void* fkn_CompressionCompensator::creator(){
	return new fkn_CompressionCompensator();
};

//Help Function.
void fkn_CompressionCompensator::ConvertMatrix4x4ToMMatrix(Matrix4x4& in_Matrix4x4, MMatrix& out_MMatrix){
	for(unsigned int iRow=0; iRow<4; iRow++){
		for(unsigned int iCol=0; iCol<4; iCol++){
			out_MMatrix(iRow, iCol) = (double)in_Matrix4x4.GetValue(iRow,iCol);
		}
	}
};

void fkn_CompressionCompensator::ConvertMMatrixToMatrix4x4(MMatrix& in_MMatrix, Matrix4x4& out_Matrix4x4){
	for(unsigned int iRow=0; iRow<4; iRow++){
		for(unsigned int iCol=0; iCol<4; iCol++){
			out_Matrix4x4.SetValue(iRow, iCol, (float)in_MMatrix(iRow,iCol));
		}
	}
};

//Maya Initialize.
MStatus fkn_CompressionCompensator::initialize(){

	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 

	m_UseGuideRest = nAttr.create("UseGuideRest", "UseGuideRest",
									MFnNumericData::kInt, 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0);
	nAttr.setMax(1);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_UseGuideRest);
	CHECK_MSTATUS_AND_RETURN_IT(stat)

	m_RestLength = nAttr.create("RestLength", "RestLength",
									MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_RestLength);
	CHECK_MSTATUS_AND_RETURN_IT(stat)

	m_UseStretch = nAttr.create("UseStretch", "UseStretch",
									MFnNumericData::kInt, 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0);
	nAttr.setMax(1);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_UseStretch);
	CHECK_MSTATUS_AND_RETURN_IT(stat)

	m_RefAxe = eAttr.create("AxeAlign", "AxeAlign",
									0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Axe X", 0);
	eAttr.addField("Axe Y", 1);
	eAttr.addField("Axe Z", 2);
	eAttr.setStorable(true);
	stat = addAttribute(m_RefAxe);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_RefAnchorAxe = eAttr.create("RefAnchorAxe", "RefAnchorAxe",
									0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("Axe X", 0);
	eAttr.addField("Axe Y", 1);
	eAttr.addField("Axe Z", 2);
	eAttr.setStorable(true);
	stat = addAttribute(m_RefAnchorAxe);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_BlendPos = nAttr.create("BlendPosition", "BlendPosition",
									MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_BlendPos);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_BlendRot = nAttr.create("BlendRotation", "BlendRotation",
									MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_BlendRot);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GuideMatrixA = mAttr.create("GuideMatrixA", "GuideMatrixA");
	mAttr.setStorable(true);
	stat = addAttribute(m_GuideMatrixA);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GuideMatrixB = mAttr.create("GuideMatrixB", "GuideMatrixB");
	mAttr.setStorable(true);
	stat = addAttribute(m_GuideMatrixB);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_MatrixA = mAttr.create("WorldMatrixA", "WorldMatrixA");
	mAttr.setStorable(true);
	stat = addAttribute(m_MatrixA);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_MatrixB = mAttr.create("WorldMatrixB", "WorldMatrixB");
	mAttr.setStorable(true);
	stat = addAttribute(m_MatrixB);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_Result = mAttr.create("Result", "Result");
	stat = addAttribute(out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_UseGuideRest, out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_UseStretch, out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_RestLength, out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_RefAxe, out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_RefAnchorAxe, out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_BlendPos, out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_BlendRot, out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GuideMatrixA, out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GuideMatrixB, out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_MatrixA, out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_MatrixB, out_Result);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};


MStatus fkn_CompressionCompensator::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.

	MDataHandle		useRestGuidData = in_Data.inputValue(m_UseGuideRest, &returnStatus);
	int 			useRestGuide = useRestGuidData.asShort();

	MDataHandle		useStretchData = in_Data.inputValue(m_UseStretch, &returnStatus);
	int 			useStretch = useStretchData.asShort();

	MDataHandle		restLengthData = in_Data.inputValue(m_RestLength, &returnStatus);
	float 			restLength = restLengthData.asFloat();

	MDataHandle		refAxeData = in_Data.inputValue(m_RefAxe, &returnStatus);
	int 			refAxe = refAxeData.asShort();

	MDataHandle		refAnchorAxeData = in_Data.inputValue(m_RefAnchorAxe, &returnStatus);
	int 			refAnchorAxe = refAnchorAxeData.asShort();

	MDataHandle		blendPosData = in_Data.inputValue(m_BlendPos, &returnStatus);
	float 			blendPos = blendPosData.asFloat();

	MDataHandle		blendRotData = in_Data.inputValue(m_BlendRot, &returnStatus);
	float 			blendRot = blendRotData.asFloat();

	MDataHandle		guideMatrixAData = in_Data.inputValue(m_GuideMatrixA, &returnStatus);
	MMatrix			guideMatrixA = guideMatrixAData.asMatrix();

	MDataHandle		guideMatrixBData = in_Data.inputValue(m_GuideMatrixB, &returnStatus);
	MMatrix			guideMatrixB = guideMatrixBData.asMatrix();

	MDataHandle		matrixAData = in_Data.inputValue(m_MatrixA, &returnStatus);
	MMatrix			matrixA = matrixAData.asMatrix();

	MDataHandle		matrixBData = in_Data.inputValue(m_MatrixB, &returnStatus);
	MMatrix			matrixB = matrixBData.asMatrix();

	if ( returnStatus == MS::kSuccess ){

		if (in_Plug == out_Result ){
			// Convert data from Maya to Frankenstein.
			Matrix4x4 fknMatrixA, fknMatrixB, fknGuideMatrixA, fknGuideMatrixB, fknResult;
			ConvertMMatrixToMatrix4x4(matrixA, fknMatrixA);
			ConvertMMatrixToMatrix4x4(matrixB, fknMatrixB);
			ConvertMMatrixToMatrix4x4(guideMatrixA, fknGuideMatrixA);
			ConvertMMatrixToMatrix4x4(guideMatrixB, fknGuideMatrixB);

			// Get the compressioncompensator.
			CompressionCompensator fknComp(	fknMatrixA, fknMatrixB,
											refAxe, refAnchorAxe, restLength,
											blendRot, blendPos,
											useRestGuide,
											fknGuideMatrixA,
											fknGuideMatrixB,
											useStretch);
			fknComp.Compute();
			fknResult = fknComp.GetResultMatrix();
			
			// Convert from Frankenstein to Maya.
			MMatrix outResult;
			ConvertMatrix4x4ToMMatrix(fknResult, outResult);

			//Set the Maya output data.
			MDataHandle	outMatrix = in_Data.outputValue(out_Result, &returnStatus);
			outMatrix.set(outResult);
			outMatrix.setClean();
		}else{
			return MS::kUnknownParameter;
		}
	}else{
		MGlobal::displayError( "Node fkn_CompressionCompensator cannot get value.\n" );
	}

	return returnStatus;
};