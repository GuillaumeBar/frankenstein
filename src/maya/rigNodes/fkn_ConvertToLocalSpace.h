/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_ConvertToLocalSpace.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Ik 2 Bones Member node for maya.
*/
#ifndef _fkn_ConvertToLocalSpace_
#define _fkn_ConvertToLocalSpace_

#include "../fkn_Maya_Tools.h"
/*! @brief Maya Class for compute the Ik 3 bones Member.*/
class fkn_ConvertToLocalSpace : public MPxNode
{
	public:
		/*! @bief The constructor.*/
		fkn_ConvertToLocalSpace();
		/*! @brief The destructor. */
		virtual	~fkn_ConvertToLocalSpace();

		//Maya Methode.
		/*! @brief The Maya compute function. 
		@param[in] in_Plug The input connection to the node.
		@param[in] in_Data The data block of the node.
		@return Success.
		*/
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		/*! @biref The Maya creator function. */
		static void*		creator();
		/*! @brief The Maya initialize function. */
		static MStatus		initialize();

	public:
		static MTypeId	id;

		static MObject m_GlobalSpaceMatrix;		
		static MObject m_InvertParentMatrix;

		static MObject out_Position;
		static MObject out_PosX;
		static MObject out_PosY;
		static MObject out_PosZ;
		static MObject out_Rotation;
		static MObject out_RotX;
		static MObject out_RotY;
		static MObject out_RotZ;
		static MObject out_Scale;
		static MObject out_SclX;
		static MObject out_SclY;
		static MObject out_SclZ;						
};

#endif