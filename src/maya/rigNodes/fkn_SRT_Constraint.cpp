/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_SRT_Constraint.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_SRT_Constraint.h"


// Unique plugins ID.
MTypeId fkn_SRT_Constraint::id( fkn_SRT_Constraint_ID );

MObject	fkn_SRT_Constraint::m_DriverMatrix;
MObject fkn_SRT_Constraint::m_ParentMatrix;
MObject fkn_SRT_Constraint::m_UseOffset;
MObject fkn_SRT_Constraint::m_LocalTransform;

MObject fkn_SRT_Constraint::m_OffsetMatrix00;
MObject fkn_SRT_Constraint::m_OffsetMatrix01;
MObject fkn_SRT_Constraint::m_OffsetMatrix02;

MObject fkn_SRT_Constraint::m_OffsetMatrix10;
MObject fkn_SRT_Constraint::m_OffsetMatrix11;
MObject fkn_SRT_Constraint::m_OffsetMatrix12;

MObject fkn_SRT_Constraint::m_OffsetMatrix20;
MObject fkn_SRT_Constraint::m_OffsetMatrix21;
MObject fkn_SRT_Constraint::m_OffsetMatrix22;

MObject fkn_SRT_Constraint::m_OffsetMatrix30;
MObject fkn_SRT_Constraint::m_OffsetMatrix31;
MObject fkn_SRT_Constraint::m_OffsetMatrix32;

MObject	fkn_SRT_Constraint::out_Translation;
MObject	fkn_SRT_Constraint::out_TranslationX;
MObject	fkn_SRT_Constraint::out_TranslationY;
MObject	fkn_SRT_Constraint::out_TranslationZ;

MObject	fkn_SRT_Constraint::out_Rotation;
MObject	fkn_SRT_Constraint::out_RotationX;
MObject	fkn_SRT_Constraint::out_RotationY;
MObject	fkn_SRT_Constraint::out_RotationZ;

MObject	fkn_SRT_Constraint::out_Scale;
MObject	fkn_SRT_Constraint::out_ScaleX;
MObject	fkn_SRT_Constraint::out_ScaleY;
MObject	fkn_SRT_Constraint::out_ScaleZ;


//Constructor.
fkn_SRT_Constraint::fkn_SRT_Constraint(){

};

//Destructor.
fkn_SRT_Constraint::~fkn_SRT_Constraint(){

};

//Maya Creator.
void* fkn_SRT_Constraint::creator(){

	return new fkn_SRT_Constraint();
};

//Maya Initialize.
MStatus fkn_SRT_Constraint::initialize(){

	MFnNumericAttribute		nAttr;
	MFnEnumAttribute		eAttr;
	MFnMatrixAttribute		mAttr;
	MFnUnitAttribute		uAttr;

	MStatus					stat;

	// Define and add the input attribute to he node.
	m_DriverMatrix = mAttr.create("DriverMatrix", "DriverMatrix");
	mAttr.setStorable(true);
	mAttr.setKeyable(true);
	mAttr.setHidden(true);
	stat = addAttribute(m_DriverMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix00 = nAttr.create("OffsetMatrix00", "OffsetMatrix00", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix00);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix01 = nAttr.create("OffsetMatrix01", "OffsetMatrix01", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix01);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix02 = nAttr.create("OffsetMatrix02", "OffsetMatrix02", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix02);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix10 = nAttr.create("OffsetMatrix10", "OffsetMatrix10", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix10);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix11 = nAttr.create("OffsetMatrix11", "OffsetMatrix11", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix11);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix12 = nAttr.create("OffsetMatrix12", "OffsetMatrix12", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix12);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix20 = nAttr.create("OffsetMatrix20", "OffsetMatrix20", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix20);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix21 = nAttr.create("OffsetMatrix21", "OffsetMatrix21", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix21);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix22 = nAttr.create("OffsetMatrix22", "OffsetMatrix22", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix22);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix30 = nAttr.create("OffsetMatrix30", "OffsetMatrix30", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix30);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix31 = nAttr.create("OffsetMatrix31", "OffsetMatrix31", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix31);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_OffsetMatrix32 = nAttr.create("OffsetMatrix32", "OffsetMatrix32", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_OffsetMatrix32);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_ParentMatrix= mAttr.create("ParentMatrix", "ParentMatrix");
	mAttr.setStorable(true);
	mAttr.setKeyable(true);
	mAttr.setHidden(true);
	stat = addAttribute(m_ParentMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_UseOffset = nAttr.create("UseOffset", "UseOffset", MFnNumericData::kBoolean, 0, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_UseOffset);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_LocalTransform = eAttr.create("MatrixSpace", "MatrixSpace", 0, &stat);
	eAttr.addField("Local", 0);
	eAttr.addField("World", 1);
	eAttr.setStorable(true);
	eAttr.setKeyable(true);
	stat = addAttribute(m_LocalTransform);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_TranslationX = nAttr.create("TranslationX", "TranslationX", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_TranslationY = nAttr.create("TranslationY", "TranslationY", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_TranslationZ = nAttr.create("TranslationZ", "TranslationZ", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_Translation = nAttr.create("Translation", "Translation", out_TranslationX, out_TranslationY, out_TranslationZ, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	stat = addAttribute( out_Translation );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_RotationX = uAttr.create("RotationX", "RotationX", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_RotationY = uAttr.create("RotationY", "RotationY", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_RotationZ = uAttr.create("RotationZ", "RotationZ", MFnUnitAttribute::kAngle, 0.0f, &stat);
	uAttr.setStorable(false);
	uAttr.setWritable(false);

	out_Rotation = nAttr.create("Rotation", "Rotation", out_RotationX, out_RotationY, out_RotationZ, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	stat = addAttribute( out_Rotation );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_ScaleX = nAttr.create("ScaleX", "ScaleX", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_ScaleY = nAttr.create("ScaleY", "ScaleY", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_ScaleZ = nAttr.create("ScaleZ", "ScaleZ", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);

	out_Scale = nAttr.create("Scale", "Scale", out_ScaleX, out_ScaleY, out_ScaleZ, &stat);
	nAttr.setStorable(false);
	nAttr.setWritable(false);
	stat = addAttribute( out_Scale );
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_DriverMatrix, out_Translation);
	stat = attributeAffects( m_DriverMatrix, out_Rotation);
	stat = attributeAffects( m_DriverMatrix, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix00, out_Translation);
	stat = attributeAffects( m_OffsetMatrix00, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix00, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix01, out_Translation);
	stat = attributeAffects( m_OffsetMatrix01, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix01, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix02, out_Translation);
	stat = attributeAffects( m_OffsetMatrix02, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix02, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix10, out_Translation);
	stat = attributeAffects( m_OffsetMatrix10, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix10, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix11, out_Translation);
	stat = attributeAffects( m_OffsetMatrix11, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix11, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix12, out_Translation);
	stat = attributeAffects( m_OffsetMatrix12, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix12, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix20, out_Translation);
	stat = attributeAffects( m_OffsetMatrix20, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix20, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix21, out_Translation);
	stat = attributeAffects( m_OffsetMatrix21, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix21, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix22, out_Translation);
	stat = attributeAffects( m_OffsetMatrix22, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix22, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix30, out_Translation);
	stat = attributeAffects( m_OffsetMatrix30, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix30, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix31, out_Translation);
	stat = attributeAffects( m_OffsetMatrix31, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix31, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_OffsetMatrix32, out_Translation);
	stat = attributeAffects( m_OffsetMatrix32, out_Rotation);
	stat = attributeAffects( m_OffsetMatrix32, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_ParentMatrix, out_Translation);
	stat = attributeAffects( m_ParentMatrix, out_Rotation);
	stat = attributeAffects( m_ParentMatrix, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_UseOffset, out_Translation);
	stat = attributeAffects( m_UseOffset, out_Rotation);
	stat = attributeAffects( m_UseOffset, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	stat = attributeAffects( m_LocalTransform, out_Translation);
	stat = attributeAffects( m_LocalTransform, out_Rotation);
	stat = attributeAffects( m_LocalTransform, out_Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_SRT_Constraint::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		driverMatrixData = in_Data.inputValue(m_DriverMatrix, &returnStatus);
	MMatrix			driverMatrix = driverMatrixData.asMatrix();

	MDataHandle		offsetMatrix00Data = in_Data.inputValue(m_OffsetMatrix00, &returnStatus);
	float			offsetMatrix00 = offsetMatrix00Data.asFloat();

	MDataHandle		offsetMatrix01Data = in_Data.inputValue(m_OffsetMatrix01, &returnStatus);
	float			offsetMatrix01 = offsetMatrix01Data.asFloat();

	MDataHandle		offsetMatrix02Data = in_Data.inputValue(m_OffsetMatrix02, &returnStatus);
	float			offsetMatrix02 = offsetMatrix02Data.asFloat();

	MDataHandle		offsetMatrix10Data = in_Data.inputValue(m_OffsetMatrix10, &returnStatus);
	float			offsetMatrix10 = offsetMatrix10Data.asFloat();

	MDataHandle		offsetMatrix11Data = in_Data.inputValue(m_OffsetMatrix11, &returnStatus);
	float			offsetMatrix11 = offsetMatrix11Data.asFloat();

	MDataHandle		offsetMatrix12Data = in_Data.inputValue(m_OffsetMatrix12, &returnStatus);
	float			offsetMatrix12 = offsetMatrix12Data.asFloat();

	MDataHandle		offsetMatrix20Data = in_Data.inputValue(m_OffsetMatrix20, &returnStatus);
	float			offsetMatrix20 = offsetMatrix20Data.asFloat();

	MDataHandle		offsetMatrix21Data = in_Data.inputValue(m_OffsetMatrix21, &returnStatus);
	float			offsetMatrix21 = offsetMatrix21Data.asFloat();

	MDataHandle		offsetMatrix22Data = in_Data.inputValue(m_OffsetMatrix22, &returnStatus);
	float			offsetMatrix22 = offsetMatrix22Data.asFloat();

	MDataHandle		offsetMatrix30Data = in_Data.inputValue(m_OffsetMatrix30, &returnStatus);
	float			offsetMatrix30 = offsetMatrix30Data.asFloat();

	MDataHandle		offsetMatrix31Data = in_Data.inputValue(m_OffsetMatrix31, &returnStatus);
	float			offsetMatrix31 = offsetMatrix31Data.asFloat();

	MDataHandle		offsetMatrix32Data = in_Data.inputValue(m_OffsetMatrix32, &returnStatus);
	float			offsetMatrix32 = offsetMatrix32Data.asFloat();

	MDataHandle		parentMatrixData = in_Data.inputValue(m_ParentMatrix, &returnStatus);
	MMatrix			parentMatrix = parentMatrixData.asMatrix();

	MDataHandle		useOffsetData = in_Data.inputValue(m_UseOffset, &returnStatus);
	bool			useOffset = useOffsetData.asBool();

	MDataHandle		localTransformData = in_Data.inputValue(m_LocalTransform, &returnStatus);
	int				localTransform = localTransformData.asShort();

	if (returnStatus == MS::kSuccess){

		// Convert Data to Frankenstein.
		Matrix4x4 fknDriver, fknOffset, fknParent, fknDriven;

		for(unsigned iRow=0; iRow<4; iRow++){
			for(unsigned iCol=0; iCol<4; iCol++){
				fknDriver.SetValue(iRow, iCol, (float)driverMatrix(iRow, iCol));
				fknParent.SetValue(iRow, iCol, (float)parentMatrix(iRow, iCol));
			}
		}

		fknOffset.Set(offsetMatrix00, offsetMatrix01, offsetMatrix02, 0.0f,
						offsetMatrix10, offsetMatrix11, offsetMatrix12, 0.0f,
						offsetMatrix20, offsetMatrix21, offsetMatrix22, 0.0f,
						offsetMatrix30, offsetMatrix31, offsetMatrix32, 1.0f );

		// Use Frankenstein SRT Constraint.
		SRT_Constraint fknSrtConstraint(fknDriver, fknOffset, fknParent, useOffset, localTransform);
		fknSrtConstraint.computeDrivenMatrix();
		fknDriven = fknSrtConstraint.GetDrivenMatrix();

		if ( in_Plug == out_Translation || in_Plug == out_TranslationX || in_Plug == out_TranslationY || in_Plug == out_TranslationZ){

			MDataHandle	outTranslation = in_Data.outputValue(out_Translation, &returnStatus);

			outTranslation.set(fknDriven.GetValue(3,0), fknDriven.GetValue(3,1), fknDriven.GetValue(3,2));

			outTranslation.setClean();

		}else if (in_Plug == out_Rotation || in_Plug == out_RotationX || in_Plug == out_RotationY || in_Plug == out_RotationZ){

			MVector toEuler;
			Vector3D rtnRot;
			Matrix3x3 convertMat;
			Vector3D axeX, axeY, axeZ;
			float xX, xY, xZ, yX, yY, yZ, zX, zY, zZ;
			fknDriven.GetMatrix3x3(xX, xY, xZ, yX, yY, yZ, zX, zY, zZ);
			axeX.Set(xX, xY, xZ);
			axeY.Set(yX, yY, yZ);
			axeZ.Set(zX, zY, zZ);
			axeX.SelfNormalize();
			axeY.SelfNormalize();
			axeZ.SelfNormalize();
			convertMat.Set(	axeX.GetX(), axeX.GetY(), axeX.GetZ(),
						axeY.GetX(), axeY.GetY(), axeY.GetZ(),
						axeZ.GetX(), axeZ.GetY(), axeZ.GetZ());

			float rotX, rotY, rotZ;
			convertMat.ToEuler(rotX, rotY, rotZ, 1);
			toEuler.x = rotX;
			toEuler.y = rotY;
			toEuler.z = rotZ;
			
			MDataHandle	outRotation = in_Data.outputValue(out_Rotation, &returnStatus);

			outRotation.set(toEuler);

			outRotation.setClean();

		}else if (in_Plug == out_Scale || in_Plug == out_ScaleX || in_Plug == out_ScaleY || in_Plug == out_ScaleZ){

			MDataHandle	outScale = in_Data.outputValue(out_Scale, &returnStatus);
			float sclX, sclY, sclZ;
			fknDriven.GetScale(sclX, sclY, sclZ);
			outScale.set(sclX, sclY, sclZ);

			outScale.setClean();

		}else{
			return MS::kUnknownParameter;
		}
	}
	else{

		MGlobal::displayError( "Node fkn_Vector3_Add cannot get value.\n" );
	}

	return returnStatus;
};