/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_CompressionCompensator.h
@author Guillaume Baratte
@date 2016-06-04
@brief CompressionCompensator.
*/
#ifndef _fkn_CompressionCompensator_
#define _fkn_CompressionCompensator_

#include "../fkn_Maya_Tools.h"
/*! @brief Maya Class for compute the Ik 3 bones Member.*/
class fkn_CompressionCompensator : public MPxNode
{
	public:
		/*! @bief The constructor.*/
		fkn_CompressionCompensator();
		/*! @brief The destructor. */
		virtual	~fkn_CompressionCompensator();

		//Maya Methode.
		/*! @brief The Maya compute function. 
		@param[in] in_Plug The input connection to the node.
		@param[in] in_Data The data block of the node.
		@return Success.
		*/
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		/*! @biref The Maya creator function. */
		static void*		creator();
		/*! @brief The Maya initialize function. */
		static MStatus		initialize();

		/*!@brief Convert the maya matrix to Frankenstein matrix.
		@param[in] in_MMatrix The Maya Matrix.
		@param[out] out_Matrix4x4 The Frankenstein Matrix.
		*/
		void ConvertMMatrixToMatrix4x4(MMatrix& in_MMatrix, Matrix4x4& out_Matrix4x4);

		/*!@brief Convert the Frankenstein matrix to Maya matrix.
		@param[in] in_Matrix4x4 The Frankenstein Matrix.
		@param[out] out_MMatrix The Maya Matrix.
		*/
		void ConvertMatrix4x4ToMMatrix(Matrix4x4& in_Matrix4x4, MMatrix& out_MMatrix);

	public:
		static MTypeId	id;

        static MObject   m_MatrixA;      /*! Matrix of the first anchor.*/
        static MObject   m_MatrixB;      /*! Matrix of the second anchor.*/
        static MObject   m_GuideMatrixA; /*! Matrix of the first guide.*/
        static MObject   m_GuideMatrixB; /*! Matrix of the second guide.*/
        static MObject   m_UseStretch;   /*! Define if we can stretch the matrix along the align axe.*/
        static MObject   m_UseGuideRest; /*! Define if we use the guide for compute the rest length.*/
        static MObject   m_RefAxe;       /*! The axe to align between the anchor.*/
        static MObject   m_RefAnchorAxe; /*! The axe of anchor that user as upvector. */
        static MObject   m_RestLength;   /*! Distance of the rest position. */
        static MObject   m_BlendRot;     /*! The blend factor for rotation. */
        static MObject   m_BlendPos;     /*! The blend factor for position. */

        static MObject   out_Result;       /*! The result matrix.*x;*/
};

#endif