/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_BlendRotCns.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Ik 2 Bones Member node for maya.
*/
#ifndef _fkn_SpaceSwitchDynamicCns_
#define _fkn_SpaceSwitchDynamicCns_

#include "../fkn_Maya_Tools.h"
/*! @brief Maya Class for compute the Ik 3 bones Member.*/
class fkn_SpaceSwitchDynamicCns : public MPxNode
{
	public:
		/*! @bief The constructor.*/
		fkn_SpaceSwitchDynamicCns();
		/*! @brief The destructor. */
		virtual	~fkn_SpaceSwitchDynamicCns();

		//Maya Methode.
		/*! @brief The Maya compute function. 
		@param[in] in_Plug The input connection to the node.
		@param[in] in_Data The data block of the node.
		@return Success.
		*/
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		/*! @biref The Maya creator function. */
		static void*		creator();
		/*! @brief The Maya initialize function. */
		static MStatus		initialize();

	public:
		static	MTypeId		id;

		static MObject		m_SpaceSwitch;
		static MObject		m_ArraySpaces;

		static MObject		m_AxeX;
		static MObject		m_AxeY;
		static MObject		m_AxeZ;
		static MObject		m_Position;
		
		static MObject		m_00;
		static MObject		m_01;
		static MObject		m_02;
		static MObject		m_03;
		static MObject		m_10;
		static MObject		m_11;
		static MObject		m_12;
		static MObject		m_13;
		static MObject		m_20;
		static MObject		m_21;
		static MObject		m_22;
		static MObject		m_23;
		static MObject		m_30;
		static MObject		m_31;
		static MObject		m_32;
		static MObject		m_33;

		static MObject		out_Matrix;
};

#endif