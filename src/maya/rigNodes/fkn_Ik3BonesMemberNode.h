/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Ik3BonesMemberNode.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Ik 3 Bones Member node for maya.
*/

#ifndef _fkn_Ik3BonesMemberNode_
#define _fkn_Ik3BonesMemberNode_

#include "../fkn_Maya_Tools.h"
/*! @brief Maya Class for compute the Ik 3 bones Member.*/
class fkn_Ik3BonesMemberNode : public MPxNode
{
	public:
		/*! @bief The constructor.*/
		fkn_Ik3BonesMemberNode();
		/*! @brief The destructor. */
		virtual	~fkn_Ik3BonesMemberNode();

		//Maya Methode.
		/*! @brief The Maya compute function. 
		@param[in] in_Plug The input connection to the node.
		@param[in] in_Data The data block of the node.
		@return Success.
		*/
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		/*! @biref The Maya creator function. */
		static void*		creator();
		/*! @brief The Maya initialize function. */
		static MStatus		initialize();

		/*!@brief Convert the maya matrix to Frankenstein matrix.
		@param[in] in_MMatrix The Maya Matrix.
		@param[out] out_Matrix4x4 The Frankenstein Matrix.
		*/
		void ConvertMMatrixToMatrix4x4(MMatrix& in_MMatrix, Matrix4x4& out_Matrix4x4);

		/*!@brief Convert the Frankenstein matrix to Maya matrix.
		@param[in] in_Matrix4x4 The Frankenstein Matrix.
		@param[out] out_MMatrix The Maya Matrix.
		*/
		void ConvertMatrix4x4ToMMatrix(Matrix4x4& in_Matrix4x4, MMatrix& out_MMatrix);

	public:
		static	MTypeId		id;

		static MObject		m_BlendIKFK;			/*! The blend value for switch between ik and fk.*/
		static MObject		m_SymBonesMatrix;		/*! The switch for symmetrize the deformer bones matrix.*/
		static MObject		m_PinBones;				/*! The switch for pin the bone 2 position.*/
		static MObject		m_PlanTwist;			/*! The angle for twist chain plane.*/
		static MObject		m_PlanBone2Twist;		/*! The angle for twist chain bone 2 plane.*/
		static MObject		m_PlanBone3Twist;		/*! The angle for twist chain bone 3 plane.*/
		static MObject		m_UseStretch;			/*! Activate the stretch bones.*/
		static MObject		m_UseSoftIK;			/*! Activate the soft ik.*/
		static MObject		m_StartSoftIK;			/*! The pourcentage of the max length of the chain to start smoothing IK.*/
		static MObject		m_GlobalBonesScale;		/*! The scale factor for the length of all the bones.*/
		static MObject		m_Bone1Scale;			/*! The scale factor for the length of the bone1.*/
		static MObject		m_Bone2Scale;			/*! The scale factor for the length of the bone2.*/
		static MObject		m_Bone3Scale;           /*! The scale factor for the length of the bone3.*/
		static MObject		m_GlobalScale;			/*! The global scale factor. */

		static MObject		m_GuidRootMatrix;		/*! The world transformation matrix of the guid root.*/
		static MObject		m_GuidEffMatrix;		/*! The world transformation matrix of the guid Effector.*/
		static MObject		m_GuidBone2Matrix;		/*! The world transformation matrix of the guid bone 2.*/
		static MObject		m_GuidBone3Matrix;		/*! The world transformation matrix of the guid bone 3.*/
		static MObject		m_GuidMainUpVMatrix;	/*! The world transformation matrix of the guid main up vector.*/

		static MObject		m_IkRootMatrix;			/*! The world transformation matrix of the ik root.*/
		static MObject		m_IkEffMatrix;			/*! The world transformation matrix of the ik effector.*/
		static MObject		m_IkMainUpVMatrix;		/*! The world transformation matrix of the Main Up Vector.*/
		static MObject		m_IkBone2UpVMatrix;		/*! The world transformation matrix of the bone 1 Up Vector.*/
		static MObject		m_IkBone3UpVMatrix;		/*! The world transformation matrix of the bone 2 Up Vector.*/
		static MObject		m_IkRotationOffset;		/*! The rotation offset of the ik bone transformatoin.*/
		static MObject		m_IkPinBone2Matrix;		/*! The pin reference postion for the bone 2.*/
		static MObject		m_IkPinBone3Matrix;		/*! The pin reference postion for the bone 3.*/

		static MObject		m_fkBone1Matrix;		/*! The world transformatoin matrix of the fk bone 1.*/
		static MObject		m_fkBone2Matrix;		/*! The world transformatoin matrix of the fk bone 2.*/
		static MObject		m_fkBone3Matrix;		/*! The world transformatoin matrix of the fk bone 3.*/
		static MObject		m_fkBone4Matrix;		/*! The world transformatoin matrix of the fk bone 4.*/

		static MObject		out_defBone1Matrix;		/*! The world transformation matrix of the deformer bone 1.*/
		static MObject		out_defBone2Matrix;		/*! The world transformation matrix of the deformer bone 2.*/
		static MObject		out_defBone3Matrix;		/*! The world transformation matrix of the deformer bone 3.*/
		static MObject		out_defBone4Matrix;		/*! The world transformation matrix of the deformer bone 4.*/
};

#endif