/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_SplineMemberNode.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Spline Member node for maya.
*/

#ifndef _fkn_SplineMemberNode_
#define _fkn_SplineMemberNode_

#include "../fkn_Maya_Tools.h"

class fkn_SplineMemberNode : public MPxNode
{
	public:
		//Constructor.
							fkn_SplineMemberNode();
		//Destructor.
		virtual				~fkn_SplineMemberNode();

		//Maya Methode.
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		static void*		creator();
		static MStatus		initialize();

		static MStatus		AddInputAttribute (	MObject&				in_Attribute,
												MString					in_LongName,
												MString					in_ShortName,
												MFnNumericData::Type 	in_Type,
												double					in_Val,
												double					in_MinVal,
												double					in_MaxVal,
												bool					in_Keyable,
												bool					in_Array,
												bool					in_Connectable,
												bool					in_Storable,
												bool					in_ChannelBox,
												bool					in_Writable);

	public:
		static	MTypeId		id;

		static MObject		m_ArrayControlers;
		static MObject		m_ArrayUserTangents;		
		static MObject		m_ParentInvertMatrix;
		static MObject		m_NumberDeformers;
		static MObject		m_NormalizePosition;
		static MObject		m_NormalizeIteration;
		static MObject		m_RestLength;
		static MObject		m_SplStartRange;
		static MObject		m_SplEndRange;
		static MObject		m_SplMoveRange;
		static MObject		m_UseAddRoll;
		static MObject		m_StartAddRollAngle;
		static MObject		m_EndAddRollAngle;
		static MObject		m_StretchType;
		static MObject		m_MaxLengthMulti;
		static MObject		m_MinLengthMulti;
		static MObject		m_UseSquatch;
		static MObject		m_MaxStretchAxe1Scale; 
		static MObject		m_MaxStretchAxe2Scale;
		static MObject		m_MinStretchAxe1Scale;
		static MObject		m_MinStretchAxe2Scale;
		static MObject		m_AxeAlignDeformers;
		static MObject		m_FilterAxeAlign;
		static MObject		m_FilterRotationBlend;
		static MObject		m_FilterScaleAxe1;
		static MObject		m_FilterScaleAxe2;
		static MObject		m_ScaleControlerType;
		static MObject		m_TangentType;
		static MObject		m_HierarchieType;
		static MObject		m_UseOnlyAddRoll;
		static MObject		m_RotationType;
		static MObject		m_CurveType;
		static MObject		m_GlobalScale;
		static MObject		m_UseUserTangents;

		static MObject		out_ArrayDefPosition;
		static MObject		out_RotX;
		static MObject		out_RotY;
		static MObject		out_RotZ;
		static MObject		out_ArrayDefRotEuler;
		static MObject		out_ArrayDefScale;
};

#endif