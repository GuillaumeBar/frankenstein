/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_surfaceConstraintNode.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Spline Member node for maya.
*/

#ifndef _fkn_surfaceConstraintNode_
#define _fkn_surfaceConstraintNode_

#include "../fkn_Maya_Tools.h"

class fkn_surfaceConstraintNode : public MPxNode
{
	public:
		//Constructor.
							fkn_surfaceConstraintNode();
		//Destructor.
		virtual				~fkn_surfaceConstraintNode();

		//Maya Methode.
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		static void*		creator();
		static MStatus		initialize();

	public:
		static	MTypeId		id;

		static MObject		m_mesh;
		static MObject		m_arraySurfaceData;
		static MObject 		m_faceID;
		static MObject		m_faceU;
		static MObject		m_faceV;
		static MObject		m_vertexIDX;
		static MObject		m_vertexIDZ;

		static MObject		m_outPosX;
		static MObject		m_outPosY;
		static MObject		m_outPosZ;
		static MObject		m_outArrayPos;
		static MObject		m_outRotX;
		static MObject		m_outRotY;
		static MObject		m_outRotZ;
		static MObject		m_outArrayEuler;
};

#endif