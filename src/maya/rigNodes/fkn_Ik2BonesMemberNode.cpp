/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_Ik2BonesMemberNode.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "fkn_Ik2BonesMemberNode.h"

// Unique plugins ID.
MTypeId     fkn_Ik2BonesMemberNode::id( fkn_Ik2BonesMemberNodeID );

// Input Attributes.
MObject		fkn_Ik2BonesMemberNode::m_BlendIKFK;
MObject		fkn_Ik2BonesMemberNode::m_SymBonesMatrix;
MObject		fkn_Ik2BonesMemberNode::m_PlanTwist;
MObject		fkn_Ik2BonesMemberNode::m_PinBone;
MObject		fkn_Ik2BonesMemberNode::m_GlobalScale;
MObject		fkn_Ik2BonesMemberNode::m_UseStretch;
MObject		fkn_Ik2BonesMemberNode::m_UseSoftIK;
MObject		fkn_Ik2BonesMemberNode::m_StartSoftIK;
MObject		fkn_Ik2BonesMemberNode::m_GlobalBonesScale;
MObject		fkn_Ik2BonesMemberNode::m_Bone1Scale;
MObject		fkn_Ik2BonesMemberNode::m_Bone2Scale;
MObject		fkn_Ik2BonesMemberNode::m_LastBoneAlign;
MObject		fkn_Ik2BonesMemberNode::m_ReverseIKAngle;

MObject		fkn_Ik2BonesMemberNode::m_GuidRootMatrix;
MObject		fkn_Ik2BonesMemberNode::m_GuidEffMatrix;
MObject		fkn_Ik2BonesMemberNode::m_GuidBone2Matrix;

MObject		fkn_Ik2BonesMemberNode::m_IkRootMatrix;
MObject		fkn_Ik2BonesMemberNode::m_IkEffMatrix;
MObject		fkn_Ik2BonesMemberNode::m_IkMainUpVMatrix;
MObject		fkn_Ik2BonesMemberNode::m_IkPinMatrix;

MObject		fkn_Ik2BonesMemberNode::m_fkBone1Matrix;
MObject		fkn_Ik2BonesMemberNode::m_fkBone2Matrix;
MObject		fkn_Ik2BonesMemberNode::m_fkBone3Matrix;

//Out Attributes.
MObject		fkn_Ik2BonesMemberNode::out_defBone1Matrix;
MObject		fkn_Ik2BonesMemberNode::out_defBone2Matrix;
MObject		fkn_Ik2BonesMemberNode::out_defBone3Matrix;


//Constructor.
fkn_Ik2BonesMemberNode::fkn_Ik2BonesMemberNode(){

};

//Destructor.
fkn_Ik2BonesMemberNode::~fkn_Ik2BonesMemberNode(){

};

//Maya Creator.
void* fkn_Ik2BonesMemberNode::creator(){
	return new fkn_Ik2BonesMemberNode();
};

//Help Function.
void fkn_Ik2BonesMemberNode::ConvertMatrix4x4ToMMatrix(Matrix4x4& in_Matrix4x4, MMatrix& out_MMatrix){
	for(unsigned int iRow=0; iRow<4; iRow++){
		for(unsigned int iCol=0; iCol<4; iCol++){
			out_MMatrix(iRow, iCol) = (double)in_Matrix4x4.GetValue(iRow,iCol);
		}
	}
};

void fkn_Ik2BonesMemberNode::ConvertMMatrixToMatrix4x4(MMatrix& in_MMatrix, Matrix4x4& out_Matrix4x4){
	for(unsigned int iRow=0; iRow<4; iRow++){
		for(unsigned int iCol=0; iCol<4; iCol++){
			out_Matrix4x4.SetValue(iRow, iCol, (float)in_MMatrix(iRow,iCol));
		}
	}
};

//Maya Initialize.
MStatus fkn_Ik2BonesMemberNode::initialize(){

	// Define the basic attribute type.
	MFnNumericAttribute		nAttr;
	MFnMatrixAttribute		mAttr;
	MFnEnumAttribute		eAttr;
	MRampAttribute			rAttr;
	MFnUnitAttribute		uAttr;

	MStatus					stat;

	// Define and add the input attribute to he node. 
	m_BlendIKFK = nAttr.create("BlendIKFK", "BlendIKFK", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_BlendIKFK);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_SymBonesMatrix = eAttr.create("SymBonesMatrix", "SymBonesMatrix", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No Symmetry", 0);
	eAttr.addField("Symmetry", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_SymBonesMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_UseStretch = eAttr.create("Stretch", "Stretch", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No Stretch", 0);
	eAttr.addField("Stretch", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_UseStretch);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_UseSoftIK = eAttr.create("SoftIK", "SoftIK", 0, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No Soft IK", 0);
	eAttr.addField("Soft IK", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_UseSoftIK);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_StartSoftIK = nAttr.create("StartSoftIK", "StartSoftIK", MFnNumericData::kFloat, 0.98f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_StartSoftIK);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	
	m_ReverseIKAngle = nAttr.create("ReverseIKAngle", "ReverseIKAngle", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(-1.0f);
	nAttr.setMax(1.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_ReverseIKAngle);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_PinBone = eAttr.create("PinBone", "PinBone", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("No Pin", 0);
	eAttr.addField("Pin", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_PinBone);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_LastBoneAlign = eAttr.create("LastBoneAlign", "LastBoneAlign", 0, &stat);
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	eAttr.addField("IK", 0);
	eAttr.addField("Bone2", 1);
	eAttr.setStorable(true);
	stat = addAttribute(m_LastBoneAlign);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_PlanTwist = nAttr.create("PlanTwist", "PlanTwist", MFnNumericData::kFloat, 0.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_PlanTwist);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GlobalScale = nAttr.create("GlobalScale", "GlobalScale", MFnNumericData::kFloat, 1.0f, &stat);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_GlobalScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GlobalBonesScale = nAttr.create("GlobalBonesScale", "GlobalBonesScale", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(2.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_GlobalBonesScale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_Bone1Scale = nAttr.create("Bone1Scale", "Bone1Scale", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(2.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_Bone1Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_Bone2Scale = nAttr.create("Bone2Scale", "Bone2Scale", MFnNumericData::kFloat, 1.0f, &stat );
	CHECK_MSTATUS_AND_RETURN_IT(stat);
	nAttr.setMin(0.0f);
	nAttr.setMax(2.0f);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	stat = addAttribute(m_Bone2Scale);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GuidRootMatrix = mAttr.create("GuidRoot", "GuidRoot");
	mAttr.setStorable(true);
	stat = addAttribute(m_GuidRootMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GuidBone2Matrix = mAttr.create("GuidBone2", "GuidBone2");
	mAttr.setStorable(true);
	stat = addAttribute(m_GuidBone2Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_GuidEffMatrix = mAttr.create("GuidEff", "GuidEff");
	mAttr.setStorable(true);
	stat = addAttribute(m_GuidEffMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkRootMatrix = mAttr.create("IkRoot", "IkRoot");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkRootMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkEffMatrix = mAttr.create("IkEff", "IkEff");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkEffMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkMainUpVMatrix = mAttr.create("IkMainUpVector", "IkMainUpVector");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkMainUpVMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_IkPinMatrix = mAttr.create("IkPin", "IkPin");
	mAttr.setStorable(true);
	stat = addAttribute(m_IkPinMatrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_fkBone1Matrix = mAttr.create("FkBone1", "FkBone1");
	mAttr.setStorable(true);
	stat = addAttribute(m_fkBone1Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_fkBone2Matrix = mAttr.create("FkBone2", "FkBone2");
	mAttr.setStorable(true);
	stat = addAttribute(m_fkBone2Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	m_fkBone3Matrix = mAttr.create("FkBone3", "FkBone3");
	mAttr.setStorable(true);
	stat = addAttribute(m_fkBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	//Define the output attribut.
	out_defBone1Matrix = mAttr.create("OutBone1World", "OutBone1World");
	stat = addAttribute(out_defBone1Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_defBone2Matrix = mAttr.create("OutBone2World", "OutBone2World");
	stat = addAttribute(out_defBone2Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	out_defBone3Matrix = mAttr.create("OutBone3World", "OutBone3World");
	stat = addAttribute(out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	// Define the link between the input and output.
	stat = attributeAffects( m_ReverseIKAngle, out_defBone1Matrix);
	stat = attributeAffects( m_ReverseIKAngle, out_defBone2Matrix);
	stat = attributeAffects( m_ReverseIKAngle, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_BlendIKFK, out_defBone1Matrix);
	stat = attributeAffects( m_BlendIKFK, out_defBone2Matrix);
	stat = attributeAffects( m_BlendIKFK, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_SymBonesMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_SymBonesMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_SymBonesMatrix, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_PinBone, out_defBone1Matrix);
	stat = attributeAffects( m_PinBone, out_defBone2Matrix);
	stat = attributeAffects( m_PinBone, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_LastBoneAlign, out_defBone1Matrix);
	stat = attributeAffects( m_LastBoneAlign, out_defBone2Matrix);
	stat = attributeAffects( m_LastBoneAlign, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_UseStretch, out_defBone1Matrix);
	stat = attributeAffects( m_UseStretch, out_defBone2Matrix);
	stat = attributeAffects( m_UseStretch, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_StartSoftIK, out_defBone1Matrix);
	stat = attributeAffects( m_StartSoftIK, out_defBone2Matrix);
	stat = attributeAffects( m_StartSoftIK, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GlobalBonesScale, out_defBone1Matrix);
	stat = attributeAffects( m_GlobalBonesScale, out_defBone2Matrix);
	stat = attributeAffects( m_GlobalBonesScale, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_Bone1Scale, out_defBone1Matrix);
	stat = attributeAffects( m_Bone1Scale, out_defBone2Matrix);
	stat = attributeAffects( m_Bone1Scale, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_Bone2Scale, out_defBone1Matrix);
	stat = attributeAffects( m_Bone2Scale, out_defBone2Matrix);
	stat = attributeAffects( m_Bone2Scale, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GlobalScale, out_defBone1Matrix);
	stat = attributeAffects( m_GlobalScale, out_defBone2Matrix);
	stat = attributeAffects( m_GlobalScale, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_PlanTwist, out_defBone1Matrix);
	stat = attributeAffects( m_PlanTwist, out_defBone2Matrix);
	stat = attributeAffects( m_PlanTwist, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GuidRootMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_GuidRootMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_GuidRootMatrix, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GuidBone2Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_GuidBone2Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_GuidBone2Matrix, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_GuidEffMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_GuidEffMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_GuidEffMatrix, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkRootMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_IkRootMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_IkRootMatrix, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkEffMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_IkEffMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_IkEffMatrix, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkMainUpVMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_IkMainUpVMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_IkMainUpVMatrix, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_IkPinMatrix, out_defBone1Matrix);
	stat = attributeAffects( m_IkPinMatrix, out_defBone2Matrix);
	stat = attributeAffects( m_IkPinMatrix, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_fkBone1Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_fkBone1Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_fkBone1Matrix, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_fkBone2Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_fkBone2Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_fkBone2Matrix, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	stat = attributeAffects( m_fkBone3Matrix, out_defBone1Matrix);
	stat = attributeAffects( m_fkBone3Matrix, out_defBone2Matrix);
	stat = attributeAffects( m_fkBone3Matrix, out_defBone3Matrix);
	CHECK_MSTATUS_AND_RETURN_IT(stat);

	return MS::kSuccess;
};

MStatus fkn_Ik2BonesMemberNode::compute(const MPlug& in_Plug, MDataBlock& in_Data){

	MStatus returnStatus;

	// Get the input value from the MDataBlock.
	MDataHandle		blendIKFKData = in_Data.inputValue(m_BlendIKFK, &returnStatus);
	float blendIKFK = blendIKFKData.asFloat();
			
	MDataHandle		symBonesMatrixData = in_Data.inputValue(m_SymBonesMatrix, &returnStatus);
	int symBonesMatrix = symBonesMatrixData.asShort();

	MDataHandle		pinBoneData = in_Data.inputValue(m_PinBone, &returnStatus);
	int pinBone = pinBoneData.asShort();

	MDataHandle		lastBoneAlignData = in_Data.inputValue(m_LastBoneAlign, &returnStatus);
	int lastBoneAlign = lastBoneAlignData.asShort();

	MDataHandle		planTwistData = in_Data.inputValue(m_PlanTwist, &returnStatus);
	float planTwist = planTwistData.asFloat();

	MDataHandle		reverseIKAngleData = in_Data.inputValue(m_ReverseIKAngle, &returnStatus);
	float reverserIKAngle = reverseIKAngleData.asFloat();

	MDataHandle		globalScaleData = in_Data.inputValue(m_GlobalScale, &returnStatus);
	float globalScale = globalScaleData.asFloat();

	MDataHandle		useStretchData = in_Data.inputValue(m_UseStretch, &returnStatus);
	int useStretch = useStretchData.asShort();

	MDataHandle		useSoftIKData = in_Data.inputValue(m_UseSoftIK, &returnStatus);
	int useSoftIK = useSoftIKData.asShort();

	MDataHandle		startSoftIKData = in_Data.inputValue(m_StartSoftIK, &returnStatus);
	float startSoftIK = startSoftIKData.asFloat();

	MDataHandle		globalBonesScaleData = in_Data.inputValue(m_GlobalBonesScale, &returnStatus);
	float globalBonesScale = globalBonesScaleData.asFloat();

	MDataHandle		bone1ScaleData = in_Data.inputValue(m_Bone1Scale, &returnStatus);
	float bone1Scale = bone1ScaleData.asFloat();

	MDataHandle		bone2ScaleData = in_Data.inputValue(m_Bone2Scale, &returnStatus);
	float bone2Scale = bone2ScaleData.asFloat();

	MDataHandle		guidRootMatriXData = in_Data.inputValue(m_GuidRootMatrix, &returnStatus);
	MMatrix			guidRootMatrix = guidRootMatriXData.asMatrix();

	MDataHandle		guidEffMatriXData = in_Data.inputValue(m_GuidEffMatrix, &returnStatus);
	MMatrix			guidEffMatrix = guidEffMatriXData.asMatrix();

	MDataHandle		guidBone2MatriXData = in_Data.inputValue(m_GuidBone2Matrix, &returnStatus);
	MMatrix			guidBone2Matrix = guidBone2MatriXData.asMatrix();

	MDataHandle		ikRootMatriXData = in_Data.inputValue(m_IkRootMatrix, &returnStatus);
	MMatrix			ikRootMatrix = ikRootMatriXData.asMatrix();

	MDataHandle		ikEffMatriXData = in_Data.inputValue(m_IkEffMatrix, &returnStatus);
	MMatrix			ikEffMatrix = ikEffMatriXData.asMatrix();

	MDataHandle		ikMainUpVMatriXData = in_Data.inputValue(m_IkMainUpVMatrix, &returnStatus);
	MMatrix			ikMainUpVMatrix = ikMainUpVMatriXData.asMatrix();

	MDataHandle		ikPinMatriXData = in_Data.inputValue(m_IkPinMatrix, &returnStatus);
	MMatrix			ikPinMatrix = ikPinMatriXData.asMatrix();

	MDataHandle		fkBone1MatriXData = in_Data.inputValue(m_fkBone1Matrix, &returnStatus);
	MMatrix			fkBone1Matrix = fkBone1MatriXData.asMatrix();

	MDataHandle		fkBone2MatriXData = in_Data.inputValue(m_fkBone2Matrix, &returnStatus);
	MMatrix			fkBone2Matrix = fkBone2MatriXData.asMatrix();

	MDataHandle		fkBone3MatriXData = in_Data.inputValue(m_fkBone3Matrix, &returnStatus);
	MMatrix			fkBone3Matrix = fkBone3MatriXData.asMatrix();

	if ( returnStatus == MS::kSuccess ){	
		//Convert Maya Data to Frankenstein Data.
		Matrix4x4 fknGuidRoot, fknGuidEff, fknGuidBone2;
		Matrix4x4 fknIkRoot, fknIkEff, fknIkMainUpV, fknIkPin;
		Matrix4x4 fknFkBone1, fknFkBone2, fknFkBone3;
	
		ConvertMMatrixToMatrix4x4(guidRootMatrix, fknGuidRoot);
		ConvertMMatrixToMatrix4x4(guidEffMatrix, fknGuidEff);
		ConvertMMatrixToMatrix4x4(guidBone2Matrix, fknGuidBone2);

		ConvertMMatrixToMatrix4x4(ikRootMatrix, fknIkRoot);
		ConvertMMatrixToMatrix4x4(ikEffMatrix, fknIkEff);
		ConvertMMatrixToMatrix4x4(ikMainUpVMatrix, fknIkMainUpV);
		ConvertMMatrixToMatrix4x4(ikPinMatrix, fknIkPin);

		ConvertMMatrixToMatrix4x4(fkBone1Matrix, fknFkBone1);
		ConvertMMatrixToMatrix4x4(fkBone2Matrix, fknFkBone2);
		ConvertMMatrixToMatrix4x4(fkBone3Matrix, fknFkBone3);

		//Init the frankenstein Ik3BonesMember.
		Ik2BonesMember	fknIk2BonesMember(	fknGuidRoot, fknGuidBone2, fknGuidEff,
											fknIkRoot, fknIkMainUpV, fknIkEff, fknIkPin,
											fknFkBone1, fknFkBone2, fknFkBone3,
											pinBone, symBonesMatrix, blendIKFK, planTwist,
											useSoftIK, startSoftIK, globalBonesScale,
											bone1Scale, bone2Scale, useStretch, globalScale,
											lastBoneAlign, reverserIKAngle);

		//Compute the frankenstein Ik3BonesMember.
		fknIk2BonesMember.ComputeBasicIK();

		if (in_Plug == out_defBone1Matrix || in_Plug == out_defBone2Matrix || in_Plug == out_defBone3Matrix){
			//Convert Frankenstein Data to Maya Data.
			MMatrix	defBone1Matrix, defBone2Matrix, defBone3Matrix;
			Matrix4x4 finalBone1, finalBone2, finalBone3;

			finalBone1 = fknIk2BonesMember.GetDefBone1();
			finalBone2 = fknIk2BonesMember.GetDefBone2();
			finalBone3 = fknIk2BonesMember.GetDefBone3();

			ConvertMatrix4x4ToMMatrix(finalBone1, defBone1Matrix);
			ConvertMatrix4x4ToMMatrix(finalBone2, defBone2Matrix);
			ConvertMatrix4x4ToMMatrix(finalBone3, defBone3Matrix);

			//Set the Maya output data.
			MDataHandle	outDefBone1Matrix = in_Data.outputValue(out_defBone1Matrix, &returnStatus);
			outDefBone1Matrix.set(defBone1Matrix);
			outDefBone1Matrix.setClean();

			MDataHandle	outDefBone2Matrix = in_Data.outputValue(out_defBone2Matrix, &returnStatus);
			outDefBone2Matrix.set(defBone2Matrix);
			outDefBone2Matrix.setClean();

			MDataHandle	outDefBone3Matrix = in_Data.outputValue(out_defBone3Matrix, &returnStatus);
			outDefBone3Matrix.set(defBone3Matrix);
			outDefBone3Matrix.setClean();
		}else{
			return MS::kUnknownParameter;
		}
	}else{
		MGlobal::displayError( "Node fkn_SplineMember cannot get value.\n" );
	}

	return returnStatus;
};