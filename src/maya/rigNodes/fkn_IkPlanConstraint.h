/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_IkPlanConstraint.h
@author Guillaume Baratte
@date 2016-03-27
@brief Create a Ik Plan Constraint node for maya.
*/

#ifndef _fkn_IkPlanConstraint_
#define _fkn_IkPlanConstraint_

#include "../fkn_Maya_Tools.h"

/*! @brief Maya Class for compute the Ik plan constraint.*/
class fkn_IkPlanConstraint : public MPxNode
{
	public:
		/*! @bief The constructor.*/
		fkn_IkPlanConstraint();
		/*! @brief The destructor. */
		virtual	~fkn_IkPlanConstraint();

		//Maya Methode.
		/*! @brief The Maya compute function. 
		@param[in] in_Plug The input connection to the node.
		@param[in] in_Data The data block of the node.
		@return Success.
		*/
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		/*! @biref The Maya creator function. */
		static void*		creator();
		/*! @brief The Maya initialize function. */
		static MStatus		initialize();

		/*!@brief Convert the maya matrix to Frankenstein matrix.
		@param[in] in_MMatrix The Maya Matrix.
		@param[out] out_Matrix4x4 The Frankenstein Matrix.
		*/
		void ConvertMMatrixToMatrix4x4(MMatrix& in_MMatrix, Matrix4x4& out_Matrix4x4);

		/*!@brief Convert the Frankenstein matrix to Maya matrix.
		@param[in] in_Matrix4x4 The Frankenstein Matrix.
		@param[out] out_MMatrix The Maya Matrix.
		*/
		void ConvertMatrix4x4ToMMatrix(Matrix4x4& in_Matrix4x4, MMatrix& out_MMatrix);

	public:
		static	MTypeId		id;

		static MObject		m_PlanTwist;			/*! The angle for twist chain plane.*/

		static MObject		m_IkRootMatrix;			/*! The world transformation matrix of the ik root.*/
		static MObject		m_IkEffMatrix;			/*! The world transformation matrix of the ik effector.*/
		static MObject		m_IkMainUpVMatrix;		/*! The world transformation matrix of the Main Up Vector.*/
		static MObject		m_GlobalScale;			/*! The global scale factor.*/

		static MObject		out_Matrix;				/*! The world transformation matrix.*/
};

#endif