/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file fkn_SplineMember2Node.h
@author Guillaume Baratte
@date 2017-02-03
@brief Create a Spline Member node for maya.
*/

#ifndef _fkn_SplineMember2Node_
#define _fkn_SplineMember2Node_

#include "../fkn_Maya_Tools.h"

class fkn_SplineMember2Node : public MPxNode
{
	public:
		//Constructor.
							fkn_SplineMember2Node();
		//Destructor.
		virtual				~fkn_SplineMember2Node();

		//Maya Methode.
		virtual MStatus		compute(const MPlug& in_Plug, MDataBlock& in_Data);
		static void*		creator();
		static MStatus		initialize();

	public:
		static	MTypeId		id;

		static MObject		m_ArrayControllers;
		static MObject		m_ArrayUserTangents;
		static MObject		m_NumberDeformers;
		static MObject		m_CurveResolution;
		static MObject 		m_PositionType;
		static MObject 		m_StartRange;
		static MObject 		m_EndRange;
		static MObject 		m_MoveRange;
		static MObject 		m_RestLength;
		static MObject 		m_StretchType;
		static MObject 		m_MaxStretch;
		static MObject 		m_MinStretch;
		static MObject 		m_CurveType;
		static MObject 		m_InterpolatedOutPosition;
		static MObject 		m_rotationType;
		static MObject 		m_scaleType;
		static MObject 		m_alignType;
		static MObject 		m_useAdditiveRotation;
		static MObject 		m_addStartRotation;
		static MObject 		m_addEndRotation;
		static MObject 		m_inverseCurve;
		static MObject		m_globalScale;
		static MObject		m_compressionMaxScaleAxis1;
		static MObject		m_compressionMaxScaleAxis2;
		static MObject		m_stretchMaxScaleAxis1;
		static MObject		m_stretchMaxScaleAxis2;
		static MObject		m_blendToLock;
		static MObject		m_blendToLimited;
		static MObject		m_useUserTangents;
		static MObject		m_inTangentLength;
		static MObject		m_outTangentLength;
		static MObject		m_useLimitedSoftStretch;
		static MObject		m_stretchStartSoft;
		static MObject		m_compressionStartSoft;

		static MObject 		m_ArrayPointDistribution;
		static MObject		m_ArrayPointRotation;
		static MObject		m_ArrayPointFollowTangent;
		static MObject		m_ArrayPointScale;
		static MObject		m_ArrayPointScaleCompressionAxis1;
		static MObject		m_ArrayPointScaleCompressionAxis2;
		static MObject		m_ArrayPointScaleStretchAxis1;
		static MObject		m_ArrayPointScaleStretchAxis2;

		static MObject 		out_localCurveLength;
		static MObject		out_globalCurveLength;
		static MObject		out_ArrayDefPosition;
		static MObject		out_RotX;
		static MObject		out_RotY;
		static MObject		out_RotZ;
		static MObject		out_ArrayDefRotEuler;
		static MObject		out_ArrayDefScale;
};

#endif