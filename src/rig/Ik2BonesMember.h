/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Ik2BonesMember.h
@author Guillaume Baratte
@date 2016-05-28
@brief Define the Ik2BonesMember class.
This create a 2 bones Ik Chain for characters.
*/

#ifndef __IK_2_BONES_MEMBER__
#define __IK_2_BONES_MEMBER__

#include "../core/DataArray.h"
#include "../core/Matrix4x4.h"
#include "../core/Matrix3x3.h"
#include "../core/Quaternion.h"
#include "../core/Vector3D.h"
#include "BlendMatrix4x4.h"

class Ik2BonesMember
{
	public:
		//CONSTRUCTOR.
		/*! @brief Primary Constructor.*/
		Ik2BonesMember();

		Ik2BonesMember(	Matrix4x4& in_GuidRoot,
						Matrix4x4& in_GuidBone2,
						Matrix4x4& in_GuidEff,
						Matrix4x4& in_IkRoot,
						Matrix4x4& in_IkMainUpV,
						Matrix4x4& in_IkEff,
						Matrix4x4& in_IkPin,
						Matrix4x4& in_FkBone1,
						Matrix4x4& in_FkBone2,
						Matrix4x4& in_FkBone3,
						int& in_PinBones,
						int& in_SymMatrixSwitch,
						float& in_BlendIKFK,
						float& in_PlanTwist, 
						int& in_UseSoftIK,
						float& in_StartSoftIK,
						float& in_GlobalBonesScale,
						float& in_Bone1Scale,
						float& in_Bone2Scale,
						int& in_UseStretch,
						float& in_GlobalScale,
						int& in_LastBoneAlign,
						float& in_ReserveIKAngle);

		//DESTRUCTOR.
		/*! @brief Destructor.*/
		~Ik2BonesMember();

		// COMPUTE METHODE.
		/*!@brief Compute the angle between the segment A and the segment B.
		@param[in] in_SegA The length of the segment A.
		@param[in] in_SegB The length of the segment B.
		@param[in] in_SegC The length of the segment C.
		@return The angle in degrees.
		*/
		float ComputeCosinusLaw(float& in_SegA, float& in_SegB, float in_SegC);

		/*!@brief Compute the main basic IK.*/
		void ComputeBasicIK();

		/*!@brief Compute the final deformer orientation.*/
		void ComputeRealignBone();

		/*!@brief Compute the symmetry matrix for the final orientation.*/
		void ComputeSymBoneMatrix();

		// HELP MEHTODS.

		/*!@brief Compute the transformation matrix from two axis and a position vector.
		@param[out] out_Matrix The transformation matrix.
		@param[in] in_Axe1 The first axe for orientation.
		@param[in] in_Axe2 The second axe for orientatoin.
		@param[in] in_Position The position vector.
		*/
		void ComputeMatrixFromTreeAxis(Matrix4x4& out_Matrix, Vector3D& in_Axe1, Vector3D& in_Axe2, Vector3D& in_Position);

		/*!@brief Extract the position from a matrix4x4.
		@param[in] in_Matrix The matrix.
		@param[out] out_Vector The Vector position.
		*/
		void ExtractMatrixPosition(Matrix4x4& in_Matrix, Vector3D& out_Vector);

		void ExtractMatrixPosRot(Matrix4x4& in_Matrix, Vector3D& out_Vector, Quaternion& out_Rot);

		void InsertPosRotMatrix(Vector3D& in_Vector, Quaternion& int_Rot, Matrix4x4& out_Matrix);

		void ComputePosLinearInterpolate(Vector3D& in_VectorA, Vector3D& in_VectorB, float& in_Blend, Vector3D& out_Vector);

		void ComputePinedIK(float& in_Bone1Length, float& in_Bone2Length, int& in_StretchType, Vector3D& in_RootPos, Vector3D& in_PinPos, Vector3D& in_EffPos, Matrix4x4& out_Bone1, Matrix4x4& out_Bone2, Matrix4x4& out_Bone3);

		// GET METHODS.

		/*!@brief Get the transformation matrix of the deformer bone 1.
		@return The world transformation matrix.
		*/
		Matrix4x4 GetDefBone1();

		/*!@brief Get the transformation matrix of the deformer bone 2.
		@return The world transformation matrix.
		*/
		Matrix4x4 GetDefBone2();

		/*!@brief Get the transformation matrix of the deformer bone 3.
		@return The world transformation matrix.
		*/
		Matrix4x4 GetDefBone3();

		// SET METHODS.

		/*!@brief Set the transformation matrix for the guid root.
		@param[in] in_GuidRoot The world transformation matrix.
		*/
		void SetGuidRoot(Matrix4x4& in_GuidRoot);

		/*!@brief Set the transformation matrix for the guid bone 2.
		@param[in] in_GuidBone2 The world transformation matrix.
		*/
		void SetGuidBone2(Matrix4x4& in_GuidBone2);

		/*!@brief Set the transformation matrix for the guid effector.
		@param[in] in_GuidEff The world transformation matrix.
		*/
		void SetGuidEff(Matrix4x4& in_GuidEff);

		/*!@brief Set the transformation matrix for the ik root.
		@param[in] in_IkRoot The world transformation matrix.
		*/
		void SetIkRoot(Matrix4x4& in_IkRoot);

		/*!@brief Set the transformation matrix for the ik main up vector.
		@param[in] in_IkMainUpV The world transformation matrix.
		*/
		void SetIkMainUpV(Matrix4x4& in_IkMainUpV);

		/*!@brief Set the transformation matrix for the ik effector.
		@param[in] in_IkEff The world transformation matrix.
		*/
		void SetIkEff(Matrix4x4& in_IkEff);

		/*!@brief Set the transformation matrix for the ik pin.
		@param[in] in_IkPin The world transformation matrix.
		*/
		void SetIkPin(Matrix4x4& in_IkPin);

		/*!@brief Set the transformation matrix for the fk bone 1.
		@param[in] in_GuidEff The world transformation matrix.
		*/
		void SetFkBone1(Matrix4x4& in_FkBone1);

		/*!@brief Set the transformation matrix for the fk bone 2.
		@param[in] in_GuidEff The world transformation matrix.
		*/
		void SetFkBone2(Matrix4x4& in_FkBone2);

		/*!@brief Set the transformation matrix for the fk bone 3.
		@param[in] in_GuidEff The world transformation matrix.
		*/
		void SetFkBone3(Matrix4x4& in_FkBone3);

		/*!@brief Set the pin switch for the bones.
		@param[in] in_PinBones The switch value for the bones. 0 ->Free. 1 -> Pin Bone2. 2 -> Pin Bone3.
		*/
		void SetPinBones(int& in_PinBones);

		/*!@brief Set the symmetry switch for symmetrize the deformer matrix.
		@param[in] in_PinBone3 The switch value for the bone 3. 0 ->Free. 1 -> Pin.
		*/
		void SetSymMatrixSwitch(int& in_SymMatrixSwitch);

		/*!@brief Set the blend ik fk value for switch between ik and fk controler.
		@param[in] in_BlendIKFK The blend value.
		*/
		void SetBlendIKFK(float& in_BlendIKFK);

		/*!@brief Set twist angle for re orient the chain plan.
		@param[in] in_PlanTwist The twist angle.
		*/
		void SetPlanTwist(float& in_PlanTwist);

		/*!@brief Set the activation of the soft IK.
		@param[in] in_UseSoftIK The value to activate the soft IK.
		*/
		void SetUseSoftIK(int& in_UseSoftIK);

		/*!@brief Set the start pourcentage value to use soft IK.
		@param[in] in_StartSoftIK The pourcentage of the global chain length.
		*/
		void SetStartSoftIK(float& in_StartSoftIK);

		/*!@brief Set the global scale for all the bones of the chain.
		@param[in] in_GlobalBonesScale The scale factor.
		*/
		void SetGlobalBonesScale(float& in_GlobalBonesScale);

		/*!@brief Set the scale factor for the bone1.
		@param[in] in_Bone1Scale The bone1 scale factor.
		*/
		void SetBone1Scale(float& in_Bone1Scale);

		/*!@brief Set the scale factor for the bone2.
		@param[in] in_Bone2Scale The bone1 scale factor.
		*/		
		void SetBone2Scale(float& in_Bone2Scale);

		/*!@brief Set the activation of stretch bone.
		@param[in] in_UseStretch Activate the stretch bone.
		*/
		void SetUseStretch(int& in_UseStretch);

		/*!@brief Select the last bone alignement.
		@param[in] in_LastBoneAlign Set the align type.
		*/
		void SetLastBoneAlign(int& in_LastBoneAlign);

		/*!@brief Multiplier for change the first bone angle.
		@param[in] in_LastBoneAlign Set the align type.
		*/
		void SetReserveIKAngle(float& in_ReserveIKAngle);

	private:

		// Options.
		float blendIKFK;				/*! The blend value for switch between ik and fk.*/
		int symMatrixSwitch;			/*! The switch for symmetrize the deformer bones matrix.*/
		int pinBones;					/*! The switch for pin the bone 2 position.*/
		float planTwist;				/*! The angle for twist chain plane.*/
		float planBone2Twist;			/*! The angle for twist the bone 2 plane. */
		float planBone3Twist;			/*! The angle for twist the bone 3 plane. */
		int useStretch;					/*! Activate the stretch bones.*/
		int lastBoneAlign;				/*! Select the align type of the last bone.*/
		int useSoftIK;					/*! Activate the soft ik.*/
		float startSoftIK;				/*! The pourcentage of the max length of the chain to start smoothing IK.*/
		float globalBonesScale;			/*! The scale factor for the length of all the bones.*/
		float bone1Scale;				/*! The scale factor for the length of the bone1.*/
		float bone2Scale;				/*! The scale factor for the length of the bone2.*/
		float bone3Scale;				/*! The scale factor for the length of the bone3.*/
		float globalScale;				/*! The global scale factor. */
		float reserveIKAngle;			/*! Multiplier for change the first bone angle. */
		//CacheData.
		float addTransition;			/*! The transition value for blend stretch and soft ik.*/

		// Guids matrix.
		Matrix4x4 guidRoot;				/*! The world transformation matrix of the guid root.*/
		Matrix4x4 guidBone2;			/*! The world transformation matrix of the guid bone 2.*/
		Matrix4x4 guidEff;				/*! The world transformation matrix of the guid Effector.*/

		// IK Controler matrix.
		Matrix4x4 ikRoot;				/*! The world transformation matrix of the ik root.*/
		Matrix4x4 ikMainUpV;			/*! The world transformation matrix of the Main Up Vector.*/
		Matrix4x4 ikEff;				/*! The world transformation matrix of the ik effector.*/
		Matrix4x4 ikPin;				/*! The world transformation matrix of the pin. */

		// FK Controler matrix.
		Matrix4x4 fkBone1;				/*! The world transformatoin matrix of the fk bone 1.*/
		Matrix4x4 fkBone2;				/*! The world transformatoin matrix of the fk bone 2.*/
		Matrix4x4 fkBone3;				/*! The world transformatoin matrix of the fk bone 3.*/

		// Result Deformer matrix.
		Matrix4x4 defBone1;				/*! The world transformation matrix of the deformer bone 1.*/
		Matrix4x4 defBone2;				/*! The world transformation matrix of the deformer bone 2.*/
		Matrix4x4 defBone3;				/*! The world transformation matrix of the deformer bone 3.*/
};

#endif