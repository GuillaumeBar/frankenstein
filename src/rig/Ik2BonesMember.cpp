/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Ik2BonesMember.cpp
@author Guillaume Baratte
@date 2016-05-28
*/

#include "Ik2BonesMember.h"
#include <iostream>
using namespace std;

// CONSTRUCTOR.
Ik2BonesMember::Ik2BonesMember(){
	guidRoot = Matrix4x4();
	guidBone2 = Matrix4x4();
	guidEff = Matrix4x4();
	ikRoot = Matrix4x4();
	ikMainUpV = Matrix4x4();
	ikEff = Matrix4x4();
	ikPin = Matrix4x4();
	fkBone1 = Matrix4x4();
	fkBone2 = Matrix4x4();
	fkBone3 = Matrix4x4();
	pinBones = 0;
	symMatrixSwitch = 0;
	blendIKFK = 1.0f;
	planTwist = 0.0f;
	useSoftIK = 1;
	startSoftIK = 0.99f;
	globalBonesScale = 1.0f;
	bone1Scale = 1.0f;
	bone2Scale = 1.0f;
	useStretch = 0;
	addTransition = -1.0f;
	globalScale = 1.0f;
	lastBoneAlign = 0;
	reserveIKAngle = 1.0f;
};

Ik2BonesMember::Ik2BonesMember(	Matrix4x4& in_GuidRoot,
								Matrix4x4& in_GuidBone2,
								Matrix4x4& in_GuidEff,
								Matrix4x4& in_IkRoot,
								Matrix4x4& in_IkMainUpV,
								Matrix4x4& in_IkEff,
								Matrix4x4& in_IkPin,
								Matrix4x4& in_FkBone1,
								Matrix4x4& in_FkBone2,
								Matrix4x4& in_FkBone3,
								int& in_PinBones,
								int& in_SymMatrixSwitch,
								float& in_BlendIKFK,
								float& in_PlanTwist, 
								int& in_UseSoftIK,
								float& in_StartSoftIK,
								float& in_GlobalBonesScale,
								float& in_Bone1Scale,
								float& in_Bone2Scale,
								int& in_UseStretch,
								float& in_GlobalScale,
								int& in_LastBoneAlign,
								float& in_ReserveIKAngle){

	guidRoot = in_GuidRoot;
	guidBone2 = in_GuidBone2;
	guidEff = in_GuidEff;
	ikRoot = in_IkRoot;
	ikMainUpV = in_IkMainUpV;
	ikEff = in_IkEff;
	ikPin = in_IkPin;
	fkBone1 = in_FkBone1;
	fkBone2 = in_FkBone2;
	fkBone3 = in_FkBone3;
	pinBones = in_PinBones;
	symMatrixSwitch = in_SymMatrixSwitch;
	blendIKFK = in_BlendIKFK;
	planTwist = in_PlanTwist;
	useSoftIK = in_UseSoftIK;
	startSoftIK = in_StartSoftIK;
	globalBonesScale = in_GlobalBonesScale;
	bone1Scale = in_Bone1Scale;
	bone2Scale = in_Bone2Scale;
	useStretch = in_UseStretch;
	addTransition = -1.0f;
	globalScale = in_GlobalScale;
	lastBoneAlign = in_LastBoneAlign;
	reserveIKAngle = in_ReserveIKAngle;
};

//DESTRUCTOR.
Ik2BonesMember::~Ik2BonesMember(){

};

// HELP METHODS.
void Ik2BonesMember::ComputeMatrixFromTreeAxis(Matrix4x4& out_Matrix, Vector3D& in_Axe1, Vector3D& in_Axe2, Vector3D& in_Position){
	Vector3D axeX, axeY, axeZ;
	//Compute the orthogonal axis.
	axeY.CrossProduct(in_Axe2, in_Axe1);
	axeZ.CrossProduct(in_Axe1, axeY);
	//Normalize the axis.
	axeX.Normalize(in_Axe1);
	axeY.SelfNormalize();
	axeZ.SelfNormalize();
	//Update the transformation matrix.
	out_Matrix.Set(axeX.GetX(), axeX.GetY(), axeX.GetZ(), 0.0f,
					axeY.GetX(), axeY.GetY(), axeY.GetZ(), 0.0f,
					axeZ.GetX(), axeZ.GetY(), axeZ.GetZ(), 0.0f,
					in_Position.GetX(), in_Position.GetY(), in_Position.GetZ(), 1.0f);
};

void Ik2BonesMember::ComputePosLinearInterpolate(Vector3D& in_VectorA, Vector3D& in_VectorB, float& in_Blend, Vector3D& out_Vector){

	float tx, ty, tz;

	tx = in_VectorA.GetX() * in_Blend + in_VectorB.GetX() * ( 1 - in_Blend );
	ty = in_VectorA.GetY() * in_Blend + in_VectorB.GetY() * ( 1 - in_Blend );
	tz = in_VectorA.GetZ() * in_Blend + in_VectorB.GetZ() * ( 1 - in_Blend );

	out_Vector.Set(tx, ty, tz);
};

void Ik2BonesMember::InsertPosRotMatrix(Vector3D& in_Vector, Quaternion& int_Rot, Matrix4x4& out_Matrix){

	Vector3D axeX(1.0f, 0.0f, 0.0f);
	Vector3D axeY(0.0f, 1.0f, 0.0f);
	Vector3D axeZ(0.0f, 0.0f, 1.0f);

	axeX.SelfRotateByQuaternionFG(int_Rot);
	axeY.SelfRotateByQuaternionFG(int_Rot);
	axeZ.SelfRotateByQuaternionFG(int_Rot);

	out_Matrix.Set( axeX.GetX(), axeX.GetY(), axeX.GetZ(), 0.0f,
					axeY.GetX(), axeY.GetY(), axeY.GetZ(), 0.0f,
					axeZ.GetX(), axeZ.GetY(), axeZ.GetZ(), 0.0f,
					in_Vector.GetX(), in_Vector.GetY(), in_Vector.GetZ(), 1.0f);
};

void Ik2BonesMember::ExtractMatrixPosRot(Matrix4x4& in_Matrix, Vector3D& out_Vector, Quaternion& out_Rot){

	Matrix3x3 tempRot;

	tempRot.Set(	in_Matrix.GetValue(0,0), in_Matrix.GetValue(0,1), in_Matrix.GetValue(0,2),
					in_Matrix.GetValue(1,0), in_Matrix.GetValue(1,1), in_Matrix.GetValue(1,2),
					in_Matrix.GetValue(2,0), in_Matrix.GetValue(2,1), in_Matrix.GetValue(2,1));

	out_Rot.FromMatrix3x3(tempRot);

	out_Vector.Set(in_Matrix.GetValue(3,0), in_Matrix.GetValue(3,1), in_Matrix.GetValue(3,2));
};

void Ik2BonesMember::ExtractMatrixPosition(Matrix4x4& in_Matrix, Vector3D& out_Vector){
	float posX, posY, posZ;
	in_Matrix.GetPosition(posX, posY, posZ);
	out_Vector.Set(posX, posY, posZ);
};

void Ik2BonesMember::ComputePinedIK(float& in_Bone1Length, float& in_Bone2Length, int& in_StretchType, Vector3D& in_RootPos, Vector3D& in_PinPos, Vector3D& in_EffPos, Matrix4x4& out_Bone1, Matrix4x4& out_Bone2, Matrix4x4& out_Bone3){
	// Compute the vector Root -> Pin, Pin -> Eff.
	Vector3D vRootPin, vPinEff;
	vRootPin.Sub(in_PinPos, in_RootPos);
	vPinEff.Sub(in_EffPos, in_PinPos);
	// Define the bones position.
	Vector3D bone1Pos, bone2Pos, bone3Pos;
	bone1Pos = in_RootPos;
	bone2Pos = in_PinPos;
	bone3Pos = in_EffPos;
	// Check the stretch type. If equal to zero we don't need the stretch.
	if(in_StretchType == 0){
		// Adjust the position of the second bone.
		vRootPin.SelfNormalize();
		vRootPin.SelfMulByFloat(in_Bone1Length);
		bone2Pos.Add(in_RootPos, vRootPin);
		// Adjust the position of the third bone.
		vPinEff.Sub(in_EffPos, bone2Pos);
		vPinEff.SelfNormalize();
		vPinEff.SelfMulByFloat(in_Bone2Length);
		bone3Pos.Add(bone2Pos, vPinEff);
	}
	// Compute the bones rotation.
	Vector3D boneAxeX, boneAxeY, boneAxeZ, planAxe;
	planAxe.CrossProduct(vPinEff, vRootPin);
	planAxe.SelfNormalize();
	// Compute the bone1 matrix.
	boneAxeX.Normalize(vRootPin);
	boneAxeY.CrossProduct(planAxe, boneAxeX);
	out_Bone1.Set(	boneAxeX.GetX(), boneAxeX.GetY(), boneAxeX.GetZ(), 0.0f,
					boneAxeY.GetX(), boneAxeY.GetY(), boneAxeY.GetZ(), 0.0f,
					planAxe.GetX(), planAxe.GetY(), planAxe.GetZ(), 0.0f,
					bone1Pos.GetX(), bone1Pos.GetY(), bone1Pos.GetZ(), 1.0f);
	// Compute the bone2 matrix.
	boneAxeX.Normalize(vPinEff);
	boneAxeY.CrossProduct(planAxe, boneAxeX);
	out_Bone2.Set(	boneAxeX.GetX(), boneAxeX.GetY(), boneAxeX.GetZ(), 0.0f,
					boneAxeY.GetX(), boneAxeY.GetY(), boneAxeY.GetZ(), 0.0f,
					planAxe.GetX(), planAxe.GetY(), planAxe.GetZ(), 0.0f,
					bone2Pos.GetX(), bone2Pos.GetY(), bone2Pos.GetZ(), 1.0f);
	// Compute the bone3 matrix.
	out_Bone3.Set(	boneAxeX.GetX(), boneAxeX.GetY(), boneAxeX.GetZ(), 0.0f,
					boneAxeY.GetX(), boneAxeY.GetY(), boneAxeY.GetZ(), 0.0f,
					planAxe.GetX(), planAxe.GetY(), planAxe.GetZ(), 0.0f,
					bone3Pos.GetX(), bone3Pos.GetY(), bone3Pos.GetZ(), 1.0f);
};

// COMPUTE METHODS.
void Ik2BonesMember::ComputeBasicIK(){

	//|-----------------------------------------------------------------|
	//| Get the length of the bone in rest pose and the maximum length. |
	//|-----------------------------------------------------------------|	
	// Get the guide position for compute the bone1 and bone2 rest length.
	Vector3D guideRootPos, guideBone2Pos, guideEffPos;
	Vector3D restBone1, restBone2;
	// Extract the position from the matrix.
	ExtractMatrixPosition(guidRoot, guideRootPos);
	ExtractMatrixPosition(guidBone2, guideBone2Pos);
	ExtractMatrixPosition(guidEff, guideEffPos);
	// Compute the rest bone vector.
	restBone1.Sub(guideBone2Pos, guideRootPos);
	restBone2.Sub(guideEffPos, guideBone2Pos);
	// Compute the rest bone size and maximum size.
	// Use the global scale to transfert the global scale of the rig.
	float boneScale = globalScale * globalBonesScale;
	float bone1Length = restBone1.GetLength() * bone1Scale * boneScale;
	float bone2Length = restBone2.GetLength() * bone2Scale * boneScale;
	float chainMaxLength = bone1Length + bone2Length;

	if(pinBones == 0){
		//|--------------------------------------|
		//| Compute the angle of the first bone. |
		//|--------------------------------------|
		Vector3D ikRootEff, ikRootPos, ikEffPos;
		// Extract the position from the matrix.
		ExtractMatrixPosition(ikRoot, ikRootPos);
		ExtractMatrixPosition(ikEff, ikEffPos);
		// Compute the ik vector Root -> Eff.
		ikRootEff.Sub(ikEffPos, ikRootPos);
		// Compute the size of the vecotr.
		float ikLenght = ikRootEff.GetLength();
		float ikLenghtSmooth;
		//Compute the smooth Ik length.
		if(useSoftIK==1){
			float distDelta = chainMaxLength * startSoftIK;
			float distSoft = chainMaxLength - distDelta;
			ikLenghtSmooth =  (1.0f - expf(-(ikLenght - distDelta) / distSoft)) * distSoft + distDelta;
			if(ikLenght<distDelta){
				ikLenghtSmooth = ikLenght;
			}
		}else{
			ikLenghtSmooth = ikLenght;
		}
		// Compute the first bone angle.
		float firstBoneAngle = ComputeCosinusLaw(bone1Length, ikLenghtSmooth, bone2Length);
		// Multiply the first bone angle by the reverse angle multiplier.
		firstBoneAngle = firstBoneAngle * reserveIKAngle;
		//|------------------------------|
		//| Compute the stretch factor.  |
		//|------------------------------|
		float stretchScaleFactor;
		//Compute the scale stretch factor.
		if(useStretch==1){
			if(useSoftIK==1){
				if(ikLenghtSmooth==0.0f){
					stretchScaleFactor = 0.0f;
				}else{
					stretchScaleFactor = ikLenght / ikLenghtSmooth;					
				}
				ikLenghtSmooth = ikLenght;
			}else{
				if(ikLenghtSmooth > chainMaxLength){
					if(chainMaxLength == 0.0f){
						stretchScaleFactor = 0.0f;
					}else{
						stretchScaleFactor = ikLenghtSmooth / chainMaxLength;
					}
				}else{
					stretchScaleFactor = 1.0f;
				}
			}
		}else{
			stretchScaleFactor = 1.0f;
		}

		//|------------------------------|
		//| Compute the plane up vector. |
		//|------------------------------|
		Vector3D ikRootUpv, ikPlanAxe, ikUpVPos;
		ExtractMatrixPosition(ikMainUpV, ikUpVPos);
		ikRootUpv.Sub(ikUpVPos, ikRootPos);
		ikUpVPos.CrossProduct(ikRootEff, ikRootUpv);
		ikUpVPos.SelfNormalize();
		// Rotate the up vector plan with the plan twist.
		Quaternion quatRotPlan;
		Vector3D axeTwist;
		axeTwist.Normalize(ikRootEff);
		float angle = planTwist * 3.14159265359f / 180.0f;
		quatRotPlan.FromAxisAndAngle(axeTwist.GetX(), axeTwist.GetY(), axeTwist.GetZ(), angle);
		ikUpVPos.SelfRotateByQuaternionFG(quatRotPlan);

		//|-----------------------------------------------|
		//| Compute the first bone transformation matrix. |
		//|-----------------------------------------------|	
		// Rotate the vector Root -> Eff to define the second vector to compute the rotation matrix.
		Vector3D b1AxeX;
		Quaternion quatRot;
		quatRot.FromAxisAndAngle(ikUpVPos.GetX(), ikUpVPos.GetY(), ikUpVPos.GetZ(), firstBoneAngle);
		b1AxeX.RotateByQuaternionFG(ikRootEff, quatRot);
		ComputeMatrixFromTreeAxis(defBone1, b1AxeX, ikUpVPos, ikRootPos);

		//|------------------------------------------------|
		//| Compute the second bone transformation matrix. |
		//|------------------------------------------------|		
		// Compute the bone 2 position.
		Vector3D b2Pos, b2Eff;
		float b1LengthStretched = bone1Length * stretchScaleFactor;
		b1AxeX.SelfNormalize();
		b2Pos.MulByFloat(b1AxeX, b1LengthStretched);
		b2Pos.SelfAdd(ikRootPos);
		// Compute the vector bone2 to the smoothed ik length vector.
		Vector3D ikRootEffNormalized;
		ikRootEffNormalized.Normalize(ikRootEff);
		ikRootEffNormalized.SelfMulByFloat(ikLenghtSmooth);
		ikRootEffNormalized.SelfAdd(ikRootPos);
		b2Eff.Sub(ikRootEffNormalized, b2Pos);
		ComputeMatrixFromTreeAxis(defBone2, b2Eff, ikUpVPos, b2Pos);

		//|-----------------------------------------------|
		//| Compute the third bone transformation matrix. |
		//|-----------------------------------------------|
		// Compute the bone 3 position.
		Vector3D b3Pos;
		b2Eff.SelfNormalize();
		float b2LengthStretched = bone2Length * stretchScaleFactor;
		b3Pos.MulByFloat(b2Eff, b2LengthStretched);
		b3Pos.SelfAdd(b2Pos);

		if (lastBoneAlign == 0){
			// Keep the rotation from the ik eff controller.
			defBone3.Set(ikEff.GetValue(0,0), ikEff.GetValue(0,1), ikEff.GetValue(0,2), 0.0f,
							ikEff.GetValue(1,0), ikEff.GetValue(1,1), ikEff.GetValue(1,2), 0.0f,
							ikEff.GetValue(2,0), ikEff.GetValue(2,1), ikEff.GetValue(2,2), 0.0f,
							b3Pos.GetX(), b3Pos.GetY(), b3Pos.GetZ(), 1.0f );
		}else if (lastBoneAlign == 1){
			// Keep the rotation from the ik eff controller.
			defBone3.Set(defBone2.GetValue(0,0), defBone2.GetValue(0,1), defBone2.GetValue(0,2), 0.0f,
							defBone2.GetValue(1,0), defBone2.GetValue(1,1), defBone2.GetValue(1,2), 0.0f,
							defBone2.GetValue(2,0), defBone2.GetValue(2,1), defBone2.GetValue(2,2), 0.0f,
							b3Pos.GetX(), b3Pos.GetY(), b3Pos.GetZ(), 1.0f );
		}


		// [MUTE] ComputeMatrixFromTreeAxis(defBone3, b2Eff, ikUpVPos, b3Pos);
	}else if(pinBones == 1){
		Vector3D ikRootPos, ikEffPos, ikPinPos;
		// Extract the position from the matrix.
		ExtractMatrixPosition(ikRoot, ikRootPos);
		ExtractMatrixPosition(ikPin, ikPinPos);
		ExtractMatrixPosition(ikEff, ikEffPos);
		// Compute the pined bone position.
		ComputePinedIK(bone1Length, bone2Length, useStretch, ikRootPos, ikPinPos, ikEffPos, defBone1, defBone2, defBone3);
		// Copy the ik eff rotation to the last bone.
		if (lastBoneAlign == 0){
			// Keep the rotation from the ik eff controller.
			defBone3.Set(ikEff.GetValue(0,0), ikEff.GetValue(0,1), ikEff.GetValue(0,2), 0.0f,
							ikEff.GetValue(1,0), ikEff.GetValue(1,1), ikEff.GetValue(1,2), 0.0f,
							ikEff.GetValue(2,0), ikEff.GetValue(2,1), ikEff.GetValue(2,2), 0.0f,
							defBone3.GetValue(3,0), defBone3.GetValue(3,1), defBone3.GetValue(3,2), 1.0f );
		}else if (lastBoneAlign == 1){
			// Keep the rotation from the ik eff controller.
			defBone3.Set(defBone2.GetValue(0,0), defBone2.GetValue(0,1), defBone2.GetValue(0,2), 0.0f,
							defBone2.GetValue(1,0), defBone2.GetValue(1,1), defBone2.GetValue(1,2), 0.0f,
							defBone2.GetValue(2,0), defBone2.GetValue(2,1), defBone2.GetValue(2,2), 0.0f,
							defBone3.GetValue(3,0), defBone3.GetValue(3,1), defBone3.GetValue(3,2), 1.0f );
		}
	}
	//|----------------------------------------|
	//| Symmetrize the ik bone transformation. |
	//|----------------------------------------|
	if(symMatrixSwitch){
		Matrix4x4 symMatrix(-1.0, 0.0f, 0.0f, 0.0f,
							0.0f, -1.0f, 0.0f, 0.0f, 
							0.0f, 0.0f, 1.0f, 0.0f, 
							0.0f, 0.0f, 0.0f, 1.0f);
		defBone1.Mul(symMatrix, defBone1);
		defBone2.Mul(symMatrix, defBone2);
		defBone3.Mul(symMatrix, defBone3);
	}

	//|---------------------------|
	//| Update the global scale.  |
	//|---------------------------|
	defBone1.SetScale(globalScale, globalScale, globalScale);
	defBone2.SetScale(globalScale, globalScale, globalScale);
	defBone3.SetScale(globalScale, globalScale, globalScale);

	//|---------------------------|
	//| Blend Ik FK world Matrix. |
	//|---------------------------|
	if ( blendIKFK <= 0.0f ){
		defBone1 = fkBone1;
		defBone2 = fkBone2;
		defBone3 = fkBone3;
	}else if ( blendIKFK > 0.0f && blendIKFK < 1.0f){
		BlendMatrix4x4 blendMat;
		blendMat.Set(fkBone1, defBone1, blendIKFK);
		blendMat.ComputeBlend();
		defBone1 = blendMat.GetResultMatrix();

		blendMat.Set(fkBone2, defBone2, blendIKFK);
		blendMat.ComputeBlend();
		defBone2 = blendMat.GetResultMatrix();

		blendMat.Set(fkBone3, defBone3, blendIKFK);
		blendMat.ComputeBlend();
		defBone3 = blendMat.GetResultMatrix();
	}
};


float Ik2BonesMember::ComputeCosinusLaw(float& in_SegA, float& in_SegB, float in_SegC){

	float angleRad = ((in_SegA * in_SegA) + (in_SegB * in_SegB) - (in_SegC * in_SegC)) / (2.0f * in_SegB * in_SegA);

	if(angleRad < -1.0f){
		angleRad = -1.0f;
	}
	else if(angleRad > 1.0f){
		angleRad = 1.0f;
	}

	return acosf(angleRad);
};

// GET METHODES.

Matrix4x4 Ik2BonesMember::GetDefBone1(){
	return defBone1;
};

Matrix4x4 Ik2BonesMember::GetDefBone2(){
	return defBone2;
};

Matrix4x4 Ik2BonesMember::GetDefBone3(){
	return defBone3;
};

// SET METHODES.

void Ik2BonesMember::SetGuidRoot(Matrix4x4& in_GuidRoot){
	guidRoot = in_GuidRoot;
};

void Ik2BonesMember::SetGuidBone2(Matrix4x4& in_GuidBone2){
	guidBone2 = in_GuidBone2;
};

void Ik2BonesMember::SetGuidEff(Matrix4x4& in_GuidEff){
	guidEff = in_GuidEff;
};

void Ik2BonesMember::SetIkRoot(Matrix4x4& in_IkRoot){
	ikRoot = in_IkRoot;
};

void Ik2BonesMember::SetIkMainUpV(Matrix4x4& in_IkMainUpV){
	ikMainUpV = in_IkMainUpV;
};

void Ik2BonesMember::SetIkEff(Matrix4x4& in_IkEff){
	ikEff = in_IkEff;
};

void Ik2BonesMember::SetFkBone1(Matrix4x4& in_FkBone1){
	fkBone1 = in_FkBone1;
};

void Ik2BonesMember::SetFkBone2(Matrix4x4& in_FkBone2){
	fkBone2 = in_FkBone2;
};

void Ik2BonesMember::SetFkBone3(Matrix4x4& in_FkBone3){
	fkBone3 = in_FkBone3;
};

void Ik2BonesMember::SetPinBones(int& in_PinBones){
	pinBones = in_PinBones;
};

void Ik2BonesMember::SetSymMatrixSwitch(int& in_SymMatrixSwitch){
	symMatrixSwitch = in_SymMatrixSwitch;
};

void Ik2BonesMember::SetBlendIKFK(float& in_BlendIKFK){
	blendIKFK = in_BlendIKFK;
};

void Ik2BonesMember::SetPlanTwist(float& in_PlanTwist){
	planTwist = in_PlanTwist;
}

void Ik2BonesMember::SetUseSoftIK(int& in_UseSoftIK){
	useSoftIK = in_UseSoftIK;
}

void Ik2BonesMember::SetStartSoftIK(float& in_StartSoftIK){
	startSoftIK = in_StartSoftIK;
}

void Ik2BonesMember::SetGlobalBonesScale(float& in_GlobalBonesScale){
	globalBonesScale = in_GlobalBonesScale;
}

void Ik2BonesMember::SetBone1Scale(float& in_Bone1Scale){
	bone1Scale = in_Bone1Scale;
}

void Ik2BonesMember::SetBone2Scale(float& in_Bone2Scale){
	bone2Scale = in_Bone2Scale;
}

void Ik2BonesMember::SetUseStretch(int& in_UseStretch){
	useStretch = in_UseStretch;
}

void Ik2BonesMember::SetLastBoneAlign(int& in_LastBoneAlign){
	lastBoneAlign = in_LastBoneAlign;
}

void Ik2BonesMember::SetReserveIKAngle(float& in_ReserveIKAngle){
	reserveIKAngle = in_ReserveIKAngle;
}