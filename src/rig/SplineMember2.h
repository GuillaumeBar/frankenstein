/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file SplineMember2.h
@author Guillaume Baratte
@date 2017-02-05
@brief Define the SplineMember class.
This create a spline that we can use to create spine for characters.
It's create n Deformers drive by n Controlers.
*/

#ifndef __SPLINE_MEMBER2__
#define __SPLINE_MEMBER2__

#include "../core/BezierCurveNPoints.h"
#include "../core/FitBezierCurveNPoints.h"
#include "../core/DataArray.h"
#include "../core/Matrix4x4.h"
#include "../core/Matrix3x3.h"
#include "../core/Quaternion.h"
#include "../core/Vector3D.h"

class SplineMember2 {

	public:
		//CONSTRUCTOR.
		/*! @brief     Primary Constructor.*/
		SplineMember2 ();

		//DESTRUCTOR.
		/*! @brief     Destructor.*/
		~SplineMember2 ();


		/**
		 * @brief      Calculates the curve.
		 */
		void computeCurve();

		/**
		 * @brief      Extract the postion, rotation and scale from the controllers matrix.
		 *             Compute the length of the controllers.
		 *             Compute the lengths of each controllers from the first to last. 
		 */
		void computeControllersData();

		/**
		 * @brief      Compute the base curve point position and tangent.
		 */
		void computeBaseCurve ();

		/**
		 * @brief      Compute the transformation of the point.
		 */
		void computeDeformerTransformation ();

		/**
		 * @brief      Compute the interpolated point position, tangent and length from the curve.
		 *
		 * @param[in]  in_T          The t value for cubic position.
		 * @param[in]  in_ID         The point ID.
		 * @param[in]  in_Type       The type of the position interpolation.
		 * @param      out_Length    Return the length of the point.
		 * @param      out_Position  Return the position of the point.
		 * @param      out_Tangent   Return the tangent of the point.
		 */
		void computePointData(float in_T,
										int in_ID,
										int in_Type,
										float& out_Length,
										Vector3D& out_Position,
										Vector3D& out_Tangent);

		void computePointRotation(int in_ID,
									Vector3D in_Tangent,
									int in_typeID,
									int in_prevID,
									int in_nextID,
									float in_alpha,
									Quaternion& out_Rotation);

		void computePointScale(int in_ID,
								float in_T,
								float in_Length,
								int in_typeID,
								int in_prevID,
								int in_nextID,
								float in_alpha,
								Vector3D& out_Scale);

		void getPrevNextControllerID(int in_ID,
										float in_Length,
										int& out_prevID,
										int& out_nextID,
										int& out_typeID,
										float& out_alpha);

		void Set(int in_numberDeformer,
			DataArray<Matrix4x4>& in_arrayControllers,
			DataArray<Matrix4x4>& in_arrayUserTangent,
			int in_baseCurveResolution,
			int in_positionType,
			float in_startRange,
			float in_endRange,
			float in_moveRange,
			float in_restLength,
			int in_stretchType,
			float in_curveMaxStretch,
			float in_curveMinStretch,
			int in_curveType,
			int in_interpolaeOutPosition,
			int in_rotationType,
			int in_scaleType,
			int in_alignType,
			int in_useAdditiveRotation,
			float in_addStartRotation,
			float in_addEndRotation,
			int in_inverseCurve,
			float in_globalScale,
			float in_compressionMaxScaleAxis1,
			float in_compressionMaxScaleAxis2,
			float in_stretchMaxScaleAxis1,
			float in_stretchMaxScaleAxis2,
			float in_blendToLock,
			float in_blendToLimited,
			int in_useUserTangents,
			float in_inTangentLength,
			float in_outTangentLength,
			int in_useLimitedSoftStretch,
			float in_stretchStartSoft,
			float in_m_compressionStartSoft,
			DataArray<float>& in_arrayPointDistribution,
			DataArray<float>& in_arrayPointRotation,
			DataArray<float>& in_arrayPointFollowTangent,
			DataArray<float>& in_ArrayPointScale,
			DataArray<float>& in_ArrayPointScaleCompressionAxis1,
			DataArray<float>& in_ArrayPointScaleCompressionAxis2,
			DataArray<float>& in_ArrayPointScaleStretchAxis1,
			DataArray<float>& in_ArrayPointScaleStretchAxis2);

		/**
		 * @brief      Get the position of the deformer.
		 *
		 * @param[in]  in_Index  In index
		 *
		 * @return     The position at.
		 */
		Vector3D getPositionAt(int in_Index);

		/**
		 * @brief      Get the euler rotation of the deformer.
		 *
		 * @param[in]  in_Index  In index
		 *
		 * @return     The euler at.
		 */
		Vector3D getEulerAt(int in_Index);

		/**
		 * @brief      Get the scale of the deformer.
		 *
		 * @param[in]  in_Index  In index
		 *
		 * @return     The scale at.
		 */
		Vector3D getScaleAt(int in_Index);

		/**
		 * @brief      Get the length of the curve.
		 *
		 * @return     The curve length.
		 */
		float getLocalCurveLength();

		/**
		 * @brief      Gets the global curve length.
		 *
		 * @return     The global curve length.
		 */
		float getGlobalCurveLength();

	private:
		// Store the parameters.
		int 					m_numberDeformers;	/*! The number of defromer. */
		int 					m_curveResolution; /*! The resolution of the base curve. */
		float 					m_startDistribution;
		float 					m_endDistribution;
		float 					m_moveDistribution;
		int 					m_userTangentsUser;
		int 					m_positionType;
		int 					m_stretchType;
		float 					m_restLength;
		float 					m_localCurveLength;
		float 					m_curveMaxStretch;
		float 					m_curveMinStretch;
		int 					m_curveType;
		int 					m_interpolateOutPosition;
		int 					m_rotationType;
		int 					m_scaleType;
		int 					m_alignType;
		int 					m_useAdditiveRotation;
		float					m_addStartRotation;
		float 					m_addEndRotation;
		int 					m_inverseCurve;
		float					m_globalScale;
		float					m_compressionMaxScaleAxis1;
		float					m_compressionMaxScaleAxis2;
		float					m_stretchMaxScaleAxis1;
		float					m_stretchMaxScaleAxis2;
		float					m_blendToLock;
		float					m_blendToLimited;
		float					m_inTangentLength;
		float 					m_outTangentLength;
		int 					m_useLimitedSoftStretch;
		float 					m_stretchStartSoft;
		float 					m_compressionStartSoft;

		DataArray<float>		m_ArrayPointDistribution;
		DataArray<float>		m_ArrayPointRotation;
		DataArray<float>		m_ArrayPointFollowTangent;

		DataArray<float>		m_ArrayPointScale;
		DataArray<float>		m_ArrayPointScaleCompressionAxis1;
		DataArray<float>		m_ArrayPointScaleCompressionAxis2;
		DataArray<float>		m_ArrayPointScaleStretchAxis1;
		DataArray<float>		m_ArrayPointScaleStretchAxis2;

		// Store the controllers Data.
		DataArray<Matrix4x4>	m_ArrayControllers;		/*! The array of the controlers of the spline. */
		DataArray<Matrix4x4>	m_ArrayUserTangent;
		DataArray<Vector3D>		m_ArrayControllersPosition;
		DataArray<Quaternion>	m_ArrayControllersRotation;
		DataArray<Vector3D>		m_ArrayControllersScale;
		DataArray<Vector3D>		m_ArrayUserTangentPosition;
		DataArray<float>		m_ArrayControllersLengths;
		DataArray<int>			m_ArrayControllersTs;
		float					m_controllersLength;

		// Store the base curve data.
		DataArray<float>		m_ArrayBaseCurveT;
		DataArray<Vector3D>		m_ArrayBaseCurvePosition;
		DataArray<float>		m_ArrayBaseCurveLength;
		float 					m_baseCurveLength;
		DataArray<Vector3D>		m_ArrayBaseCurveSubTangents;

		// Store the deformer transformation.
		DataArray<Matrix4x4>	m_ArrayDeformerTransformation; /*! Store the transformation of the defromer.*/

		static const int		m_MemoryBaseSize;
};

#endif