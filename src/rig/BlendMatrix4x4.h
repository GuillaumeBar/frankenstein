/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file BlendMatrix4x4.h
@author Guillaume Baratte
@date 2016-05-28
@brief Define the class for blend matrix 4x4.
*/

#ifndef _BLENDMATRIX4X4_
#define _BLENDMATRIX4X4_

#include "../core/Matrix4x4.h"
#include "../core/Matrix3x3.h"
#include "../core/Quaternion.h"
#include "../core/Vector3D.h"

class BlendMatrix4x4 {

    public:
        // Constructor.
        BlendMatrix4x4();
        BlendMatrix4x4(Matrix4x4& in_MatrixA, Matrix4x4& in_MatrixB, float& in_Blend);
        // Destructor.
        ~BlendMatrix4x4();

        /*@biref Compute the blend matrix.*/
        void ComputeBlend();

        /*@brief Set all the parameter of the class.
        *@param[in] in_MatrixA The matrix at blend 0.
        *@param[in] in_MatrixB The matrix at blend 1.
        *@param[in] in_Blend The blend factor.
        */
        void Set(Matrix4x4& in_MatrixA, Matrix4x4& in_MatrixB, float& in_Blend);
        /*@brief Set the matrix A.
        *@param[in] in_MatrixA The matrix at blend 0.
        */
        void SetMatrixA(Matrix4x4& in_MatrixA);
        /*@brief Set the matrix B.
        *@param[in] in_MatrixB The matrix at blend 1.
        */        
        void SetMatrixB(Matrix4x4& in_MatrixB);
        /*@brief Set blend factor.
        *@param[in] in_Blend Value of blend.
        */          
        void SetBlend(float& in_Blend);
        /*@brief Get the final matrix of the blend.
        *@return The result matrix.
        */
        Matrix4x4 GetResultMatrix();

    private:

        Matrix4x4   m_MatrixA;  /* The matrix at blend 0.*/
        Matrix4x4   m_MatrixB;  /* The matrix at blend 1.*/
        float       m_Blend;    /* The blend factor.*/
        Matrix4x4   m_Result;   /* The result matrix.*/
};

#endif