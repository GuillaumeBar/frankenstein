/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file SRT_Constraint.h
@author Guillaume Baratte
@date 2016-03-27
@brief Define the SRT Constraint class.
Help to create world space constraint between transform object for rigging.
*/

#ifndef __SRT_CONSTRAINT__
#define __SRT_CONSTRAINT__

#include "../core/Matrix4x4.h"

class SRT_Constraint
{
	public:
		//METHODES.
		
		//CONSTRUCTOR.
		/*! @brief Primary constructor. */
		SRT_Constraint();
		
		/*! @brief Overload of the constructor.
		@param[in] in_DriverMatrix The world matrix of the driver object.
		@param[in] in_OffsetMatrix The local offset matrix.
		@param[in] in_ParentMatrix The world matrix of the parent of the driven object.
		@param[in] in_UseOffset The value for using offset matrix.
		@param[in] in_LocalTransform The value to compute the local matrix.
		*/
		SRT_Constraint(Matrix4x4& in_DriverMatrix, Matrix4x4& in_OffsetMatrix, Matrix4x4& in_ParentMatrix, bool& in_UseOffset, int& in_LocalTransform);
		
		//DESTRUCTOR.
		/*! @brief Primary destructor. */
		~SRT_Constraint();
	
		// COMPUTE METHODES.
		/*! @brief Compute the matrix of the driven object. */
		void computeDrivenMatrix();

		// SET METHODES.
		/*! @brief Set the matrix of the driver object.
		@param[in] in_DriverMatrix The world matrix.
		*/
		void SetDriverMatrix(Matrix4x4& in_DriverMatrix);

		/*! @brief Set the offset matrix.
		@param[in] in_OffsetMatrix The local offset matrix.
		*/
		void SetOffsetMatrix(Matrix4x4& in_OffsetMatrix);

		/*! @brief Set the parent matrix.
		@param[in] in_ParentMatrix The world matrix.
		*/
		void SetParentMatrix(Matrix4x4& in_ParentMatrix);

		/*! @brief Set the value for using offset matrix.
		@param[in] in_UseOffset The value for using offset matrix.
		*/
		void SetUseOffset(bool& in_UseOffset);

		/*! @brief Set the value for compute the driven matrix in local space.
		@param[in] in_LocalTransform The value to compute the local matrix.
		*/
		void SetLocalTransform(int& in_LocalTransform);

		// GET METHODES.
		/*! @brief Get the driven matrix.
		@return The driven world space matrix.
		*/
		Matrix4x4 GetDrivenMatrix();

	private:
		// Attributs
		bool m_UseOffset;
		int m_LocalTransform;
		Matrix4x4 m_DriverMatrix;	/*! The world transform matrix of the driver object. */
		Matrix4x4 m_OffsetMatrix;	/*! The offset transform matrix. */
		Matrix4x4 m_DrivenMatrix;	/*! The world transform matrix of the driven object. */
		Matrix4x4 m_ParentMatrix;	/*! The world transform matrix of the parent of the driven object. */
};


#endif