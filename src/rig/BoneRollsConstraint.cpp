/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file BoneRollsConstraint.cpp
@author Guillaume Baratte
@date 2016-05-31
*/

#include "BoneRollsConstraint.h"
#include <iostream>
using namespace std;

const int BoneRollsConstraint::m_MemoryBaseSize = 50;

BoneRollsConstraint::BoneRollsConstraint(){
	m_RollsNumber = 3;
	m_Bone1Matrix = Matrix4x4();
	m_Bone2Matrix = Matrix4x4();
	m_StartDistribution = 0.0f;
	m_EndDistribution = 1.0f;
	m_StartRollsOffset = 0.0f;
	m_EndRollsOffset = 0.0f;
	m_ArrayRollsBlend.SetSizeAndClear(m_MemoryBaseSize);
	m_Symmetrize = 0;
	out_ArrayRollsMatrix.SetSizeAndClear(m_MemoryBaseSize);
	for(unsigned int iRoll=0; iRoll<3; iRoll++){
		m_ArrayRollsBlend[iRoll] = (float)iRoll / 3.0f;
		out_ArrayRollsMatrix[iRoll] = Matrix4x4();
	}
}

BoneRollsConstraint::BoneRollsConstraint(	int& in_RoolsNumber,
											Matrix4x4& in_Bone1Matrix,
											Matrix4x4& in_Bone2Matrix,
											float& in_StartDist,
											float& in_EndDist,
											float& in_StartAngle,
											float& in_EndAngle,
											DataArray<float>& in_ArrayRollsBlend,
											int& in_AxeAlign,
											int& in_Symmetrize){
	m_RollsNumber = in_RoolsNumber;
	m_Bone1Matrix = in_Bone1Matrix;
	m_Bone2Matrix = in_Bone2Matrix;
	m_StartDistribution = in_StartDist;
	m_EndDistribution = in_EndDist;
	m_StartRollsOffset = in_StartAngle;
	m_EndRollsOffset = in_EndAngle;
	m_AxeAlign = in_AxeAlign;
	m_Symmetrize = in_Symmetrize;
	m_ArrayRollsBlend = in_ArrayRollsBlend;
	out_ArrayRollsMatrix.SetSizeAndClear(in_RoolsNumber);
	for(int iRoll=0; iRoll<in_RoolsNumber; iRoll++){
		out_ArrayRollsMatrix[iRoll] = Matrix4x4();
	}
}

BoneRollsConstraint::~BoneRollsConstraint(){

}

void BoneRollsConstraint::Compute(){
	//-------------------------------------------------|
	// Split the bone matrix to vector and quaternion. |
	//-------------------------------------------------|
	Vector3D bone1Pos, bone2Pos;
	Matrix3x3 bone1Rot, bone2Rot;
	Quaternion bone1Quat, bone2Quat;
	// Extract the position.
	bone1Pos.Set(m_Bone1Matrix.GetValue(3,0), m_Bone1Matrix.GetValue(3,1), m_Bone1Matrix.GetValue(3,2));
	bone2Pos.Set(m_Bone2Matrix.GetValue(3,0), m_Bone2Matrix.GetValue(3,1), m_Bone2Matrix.GetValue(3,2));
	// Extract rotation axe vectors.
	Vector3D b1AxeX, b1AxeY, b1AxeZ;
	Vector3D b2AxeX, b2AxeY, b2AxeZ;
	// Extract axis from bone1 matrix.
	b1AxeX.Set(m_Bone1Matrix.GetValue(0, 0), m_Bone1Matrix.GetValue(0, 1), m_Bone1Matrix.GetValue(0, 2));
	b1AxeY.Set(m_Bone1Matrix.GetValue(1, 0), m_Bone1Matrix.GetValue(1, 1), m_Bone1Matrix.GetValue(1, 2));
	b1AxeZ.Set(m_Bone1Matrix.GetValue(2, 0), m_Bone1Matrix.GetValue(2, 1), m_Bone1Matrix.GetValue(2, 2));
	// Extract axis from bone2 matrix.
	b2AxeX.Set(m_Bone2Matrix.GetValue(0, 0), m_Bone2Matrix.GetValue(0, 1), m_Bone2Matrix.GetValue(0, 2));
	b2AxeY.Set(m_Bone2Matrix.GetValue(1, 0), m_Bone2Matrix.GetValue(1, 1), m_Bone2Matrix.GetValue(1, 2));
	b2AxeZ.Set(m_Bone2Matrix.GetValue(2, 0), m_Bone2Matrix.GetValue(2, 1), m_Bone2Matrix.GetValue(2, 2));
	// Normalize the bone1 axis.
	b1AxeX.SelfNormalize();
	b1AxeY.SelfNormalize();
	b1AxeZ.SelfNormalize();
	b2AxeX.SelfNormalize();
	b2AxeY.SelfNormalize();
	b2AxeZ.SelfNormalize();
	// Create the matrix3 rotation.
	bone1Rot.Set(b1AxeX.GetX(), b1AxeX.GetY(), b1AxeX.GetZ(), 
					b1AxeY.GetX(), b1AxeY.GetY(), b1AxeY.GetZ(), 
					b1AxeZ.GetX(), b1AxeZ.GetY(), b1AxeZ.GetZ() );

	bone2Rot.Set(b2AxeX.GetX(), b2AxeX.GetY(), b2AxeX.GetZ(), 
					b2AxeY.GetX(), b2AxeY.GetY(), b2AxeY.GetZ(), 
					b2AxeZ.GetX(), b2AxeZ.GetY(), b2AxeZ.GetZ() );
	/*
	// Extract the rotation.
	for(unsigned int iCol=0; iCol<3; iCol++){
		for(unsigned int iRow=0; iRow<3; iRow++){
			bone1Rot.SetValue(iCol, iRow, m_Bone1Matrix.GetValue(iCol, iRow));
			bone2Rot.SetValue(iCol, iRow, m_Bone2Matrix.GetValue(iCol, iRow));
		}
	}
	*/
	// Convert rotation to quaternion.
	bone1Quat.FromMatrix3x3(bone1Rot);
	bone2Quat.FromMatrix3x3(bone2Rot);

	//---------------------|
	// Get the driver axe. |
	//---------------------|
	Vector3D driverAxe;
	if(m_AxeAlign == 0){
		driverAxe.Set(1.0f, 0.0f, 0.0f);
	}else if(m_AxeAlign == 1){
		driverAxe.Set(0.0f, 1.0f, 0.0f);
	}else if(m_AxeAlign == 2){
		driverAxe.Set(0.0f, 0.0f, 1.0f);
	}else if(m_AxeAlign == 3){
		driverAxe.Set(-1.0f, 0.0f, 0.0f);
	}else if(m_AxeAlign == 4){
		driverAxe.Set(1.0f, -1.0f, 0.0f);
	}else if(m_AxeAlign == 5){
		driverAxe.Set(1.0f, 0.0f, -1.0f);
	}

	//-----------------------------------------|
	// Get the vector between bone1 and bone2. |
	//-----------------------------------------|
	Vector3D b1b2, normb1b2;
	b1b2.Sub(bone2Pos, bone1Pos);
	normb1b2.Normalize(b1b2);

	//----------------------|
	// Loop over all rolls. |
	//----------------------|
	Matrix4x4 symMatrix(-1.0, 0.0f, 0.0f, 0.0f,
					0.0f, -1.0f, 0.0f, 0.0f, 
					0.0f, 0.0f, 1.0f, 0.0f, 
					0.0f, 0.0f, 0.0f, 1.0f);
	Quaternion blendQuat;
	float linearBlend = 0.0f;
	float rollDist = 0.0f;
	float distRange = m_EndDistribution - m_StartDistribution;
	float angleRange = m_EndRollsOffset - m_StartRollsOffset;
	float rollAngle = 0.0f;
	float correctAngle = 0.0f;
	Vector3D rollPos, crossAlign, vectorAlign;
	Quaternion correctQuat, offsetQuat;
	Matrix3x3 rollRot;
	for(int iRol=0; iRol<m_RollsNumber; iRol++){
		// Compute the linear blend.
		linearBlend = (float)iRol / (float)(m_RollsNumber - 1);
		// Compute the current roll distribution.
		rollDist = linearBlend * distRange +  m_StartDistribution;
		// Compute the roll position.
		rollPos.MulByFloat(b1b2, rollDist);
		rollPos.SelfAdd(bone1Pos);
		// Compute the blend Quaternion.
		blendQuat.Slerp(bone1Quat, bone2Quat, m_ArrayRollsBlend[iRol], true);
		// Compute the align vector.
		vectorAlign.RotateByQuaternionFG(driverAxe, blendQuat);
		vectorAlign.SelfNormalize();
		// Compute the offset roll angle.
		rollAngle = (angleRange * m_ArrayRollsBlend[iRol] + m_StartRollsOffset) * 3.14f / 180.0f;
		// Compute the cross vector to align the blend quaternion on the bone1 axe.
		crossAlign.CrossProduct(vectorAlign, normb1b2);
		crossAlign.SelfNormalize();
		// Compute the correct angle.
		correctAngle = acos(vectorAlign.DotProduct(normb1b2));
		// Compute the corrective quaternion.
		correctQuat.FromAxisAndAngle(crossAlign.GetX(), crossAlign.GetY(), crossAlign.GetZ(), correctAngle);
		//If the angle is equal to 0 we can not compute the crossVector.
		//In this case we use the blend quaternion.
		if ( correctAngle >= 0.001f ){
			correctQuat.SelfMul(blendQuat);
		}else{
			correctQuat = blendQuat;
		}
		// Compute the offset quaternion.
		vectorAlign.RotateByQuaternionFG(driverAxe, correctQuat);
		offsetQuat.FromAxisAndAngle(vectorAlign.GetX(), vectorAlign.GetY(), vectorAlign.GetZ(), rollAngle);
		// Compute the finale quaternion.
		correctQuat.Mul(offsetQuat, correctQuat);
		// Convert the quaternion to matrix3x3.
		rollRot = correctQuat.ToMatrix3x3();
		// Update the roll transformation matrix.
		out_ArrayRollsMatrix[iRol].Set(rollRot.GetValue(0,0), rollRot.GetValue(0,1), rollRot.GetValue(0,2), 0.0f,
										rollRot.GetValue(1,0), rollRot.GetValue(1,1), rollRot.GetValue(1,2), 0.0f,
										rollRot.GetValue(2,0), rollRot.GetValue(2,1), rollRot.GetValue(2,2), 0.0f,
										rollPos.GetX(), rollPos.GetY(), rollPos.GetZ(), 1.0f);
		// Symmetrize the roll orientation if needed.
		if(m_Symmetrize == 1){
			out_ArrayRollsMatrix[iRol].Mul(symMatrix, out_ArrayRollsMatrix[iRol]);
		}
	}

}

DataArray<Matrix4x4> BoneRollsConstraint::Get(){
	return out_ArrayRollsMatrix;
}

Matrix4x4 BoneRollsConstraint::GetAt(int& in_Index){
	return out_ArrayRollsMatrix[in_Index];
}

void BoneRollsConstraint::Set(	int& in_RoolsNumber,
								Matrix4x4& in_Bone1Matrix,
								Matrix4x4& in_Bone2Matrix,
								float& in_StartDist,
								float& in_EndDist,
								float& in_StartAngle,
								float& in_EndAngle,
								DataArray<float>& in_ArrayRollsBlend,
								int& in_AxeAlign,
								int& in_Symmetrize){
	m_RollsNumber = in_RoolsNumber;
	m_Bone1Matrix = in_Bone1Matrix;
	m_Bone2Matrix = in_Bone2Matrix;
	m_StartDistribution = in_StartDist;
	m_EndDistribution = in_EndDist;
	m_StartRollsOffset = in_StartAngle;
	m_EndRollsOffset = in_EndAngle;
	m_AxeAlign = in_AxeAlign;
	m_Symmetrize = in_Symmetrize;
	m_ArrayRollsBlend = in_ArrayRollsBlend;
	out_ArrayRollsMatrix.SetSizeAndClear(in_RoolsNumber);
	for(int iRoll=0; iRoll<in_RoolsNumber; iRoll++){
		out_ArrayRollsMatrix[iRoll] = Matrix4x4();
	}
}

void BoneRollsConstraint::SetStartDistribution(float& in_StartDist){
	m_StartDistribution = in_StartDist;	
}

void BoneRollsConstraint::SetStartAngle(float& in_StartAngle){
	m_StartRollsOffset = in_StartAngle;
}

void BoneRollsConstraint::SetRollsNumber(int& in_RoolsNumber){
	m_RollsNumber = in_RoolsNumber;
}

void BoneRollsConstraint::SetEndDistribution(float& in_EndDist){
	m_EndDistribution = in_EndDist;
}

void BoneRollsConstraint::SetEndAngle(float& in_EndAngle){
	m_EndRollsOffset = in_EndAngle;
}

void BoneRollsConstraint::SetBone2Matrix(Matrix4x4& in_Bone2Matrix){
	m_Bone2Matrix = in_Bone2Matrix;
}

void BoneRollsConstraint::SetBone1Matrix(Matrix4x4& in_Bone1Matrix){
	m_Bone1Matrix = in_Bone1Matrix;
}

void BoneRollsConstraint::SetArrayRollsBlend(DataArray<float>& in_ArrayRollsBlend){
	m_ArrayRollsBlend = in_ArrayRollsBlend;
}

void BoneRollsConstraint::SetSymmetrize(int& in_Symmetrize){
	m_Symmetrize = in_Symmetrize;
}
