/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file BlendMatrix4x4.cpp
@author Guillaume Baratte
@date 2016-05-28
*/

#include "BlendMatrix4x4.h"

BlendMatrix4x4::BlendMatrix4x4(){
    m_MatrixA = Matrix4x4();
    m_MatrixB = Matrix4x4();
    m_Blend = 0.0f;
    m_Result = Matrix4x4();
}

BlendMatrix4x4::BlendMatrix4x4(Matrix4x4& in_MatrixA, Matrix4x4& in_MatrixB, float& in_Blend){
    m_MatrixA = in_MatrixA;
    m_MatrixB = in_MatrixB;
    m_Blend = in_Blend;
    m_Result = Matrix4x4();
}

BlendMatrix4x4::~BlendMatrix4x4(){

}

// COMPUTE METHODES
void BlendMatrix4x4::ComputeBlend(){
    // Split the matrix 4x4 to blend the position, rotation and scale separatly.
    Vector3D    posA, posB, finalPos;
    Matrix3x3   rotA, rotB, finalRot;
    Vector3D    axeX, axeY, axeZ;
    Quaternion  quatA, quatB, finalQuat;
    Vector3D    sclA, sclB, finalScl;
    float       sclX, sclY, sclZ;
    // Extract the position.
    posA.Set(m_MatrixA.GetValue(3,0), m_MatrixA.GetValue(3,1), m_MatrixA.GetValue(3,2));
    posB.Set(m_MatrixB.GetValue(3,0), m_MatrixB.GetValue(3,1), m_MatrixB.GetValue(3,2));
    // Extract the rotation.
    axeX.Set(m_MatrixA.GetValue(0,0), m_MatrixA.GetValue(0,1), m_MatrixA.GetValue(0,2));
    axeY.Set(m_MatrixA.GetValue(1,0), m_MatrixA.GetValue(1,1), m_MatrixA.GetValue(1,2));
    axeZ.Set(m_MatrixA.GetValue(2,0), m_MatrixA.GetValue(2,1), m_MatrixA.GetValue(2,2));
    axeX.SelfNormalize();
    axeY.SelfNormalize();
    axeZ.SelfNormalize();
    rotA.Set(   axeX.GetX(), axeX.GetY(), axeX.GetZ(),
                axeY.GetX(), axeY.GetY(), axeY.GetZ(),
                axeZ.GetX(), axeZ.GetY(), axeZ.GetZ());
    axeX.Set(m_MatrixB.GetValue(0,0), m_MatrixB.GetValue(0,1), m_MatrixB.GetValue(0,2));
    axeY.Set(m_MatrixB.GetValue(1,0), m_MatrixB.GetValue(1,1), m_MatrixB.GetValue(1,2));
    axeZ.Set(m_MatrixB.GetValue(2,0), m_MatrixB.GetValue(2,1), m_MatrixB.GetValue(2,2));
    axeX.SelfNormalize();
    axeY.SelfNormalize();
    axeZ.SelfNormalize();
    rotB.Set(   axeX.GetX(), axeX.GetY(), axeX.GetZ(),
                axeY.GetX(), axeY.GetY(), axeY.GetZ(),
                axeZ.GetX(), axeZ.GetY(), axeZ.GetZ());
    quatA.FromMatrix3x3(rotA);
    quatB.FromMatrix3x3(rotB);
    // Extract the scale.
    m_MatrixA.GetScale(sclX, sclY, sclZ);
    sclA.Set(sclX, sclY, sclZ);
    m_MatrixB.GetScale(sclX, sclY, sclZ);
    sclB.Set(sclX, sclY, sclZ);
    // Blend the position.
    finalPos.Sub(posB, posA);
    finalPos.SelfMulByFloat(m_Blend);
    finalPos.SelfAdd(posA);
    // Blend the rotation.
    finalQuat.Slerp(quatA, quatB, m_Blend, true);
    finalRot = finalQuat.ToMatrix3x3();
    // Blend the scale.
    finalScl.Sub(sclB, sclA);
    finalScl.SelfMulByFloat(m_Blend);
    finalScl.SelfAdd(sclA);
    // Update the finale matrix.
    m_Result.Set(finalRot.GetValue(0,0), finalRot.GetValue(0,1), finalRot.GetValue(0,2), 0.0f,
                    finalRot.GetValue(1,0), finalRot.GetValue(1,1), finalRot.GetValue(1,2), 0.0f,
                    finalRot.GetValue(2,0), finalRot.GetValue(2,1), finalRot.GetValue(2,2), 0.0f,
                    finalPos.GetX(), finalPos.GetY(), finalPos.GetZ(), 1.0f);
    m_Result.SetScale(finalScl.GetX(), finalScl.GetY(), finalScl.GetZ());
}

// GET METHODES
Matrix4x4 BlendMatrix4x4::GetResultMatrix(){
    return m_Result;
}

// SET METHODES
void BlendMatrix4x4::Set(Matrix4x4& in_MatrixA, Matrix4x4& in_MatrixB, float& in_Blend){
    m_MatrixA = in_MatrixA;
    m_MatrixB = in_MatrixB;
    m_Blend = in_Blend;  
}

void BlendMatrix4x4::SetMatrixA(Matrix4x4& in_MatrixA){
    m_MatrixA = in_MatrixA;   
}

void BlendMatrix4x4::SetMatrixB(Matrix4x4& in_MatrixB){
    m_MatrixB = in_MatrixB;   
}

void BlendMatrix4x4::SetBlend(float& in_Blend){
    m_Blend = in_Blend;   
}