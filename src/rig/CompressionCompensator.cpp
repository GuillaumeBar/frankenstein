/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file CompressionCompensator.cpp
@author Guillaume Baratte
@date 2016-06-24
*/

#include "CompressionCompensator.h"

CompressionCompensator::CompressionCompensator(){
    m_MatrixA = Matrix4x4();
    m_MatrixB = Matrix4x4();
    m_RefAxe = 0;
    m_RefAnchorAxe = 0;
    m_RestLength = 1.0f;
    m_BlendRot = 0.5f;
    m_BlendPos = 0.5f;
    m_useGuideRest = 0;
    m_GuideMatrixA = Matrix4x4();
    m_GuideMatrixB = Matrix4x4();
}

CompressionCompensator::CompressionCompensator( Matrix4x4& in_MatrixA,
                                                Matrix4x4& in_MatrixB,
                                                int& in_RefAxe,
                                                int& in_RefAnchorAxe,
                                                float& in_RestLength,
                                                float& in_BlendRot,
                                                float& in_BlendPos,
                                                int& in_UseGuideRest,
                                                Matrix4x4& in_GuideMatrixA,
                                                Matrix4x4& in_GuideMatrixB,
                                                int& in_UseStretch){
    Set(in_MatrixA,
            in_MatrixB,
            in_RefAxe,
            in_RefAnchorAxe,
            in_RestLength,
            in_BlendRot,
            in_BlendPos, 
            in_UseGuideRest,
            in_GuideMatrixA,
            in_GuideMatrixB,
            in_UseStretch);
}

CompressionCompensator::~CompressionCompensator(){

}

// COMPUTE METHODES
void CompressionCompensator::Compute(){
    // Check if we need to compute the guide rest length.
    float restLength = m_RestLength;
    if(m_useGuideRest == 1){
        Vector3D guide1Pos, guide2Pos, restVector;
        // Get the guide position.
        guide1Pos.Set(m_GuideMatrixA.GetValue(3,0),
                        m_GuideMatrixA.GetValue(3,1),
                        m_GuideMatrixA.GetValue(3,2));

        guide2Pos.Set(m_GuideMatrixB.GetValue(3,0),
                        m_GuideMatrixB.GetValue(3,1),
                        m_GuideMatrixB.GetValue(3,2));
        // Compute the rest vector.
        restVector.Sub(guide2Pos, guide1Pos);
        // Compute the rest length.
        restLength = restVector.GetLength();
    }

    // Split the matrix of the anchor.
    float       scaleFactor;
    Vector3D    anchor1AxeX, anchor1AxeY, anchor1AxeZ, anchor1Pos;
    Vector3D    anchor2AxeX, anchor2AxeY, anchor2AxeZ, anchor2Pos;
    // Extract anchor1 vector.
    anchor1AxeX.Set(m_MatrixA.GetValue(0,0), m_MatrixA.GetValue(0,1), m_MatrixA.GetValue(0,2));
    anchor1AxeY.Set(m_MatrixA.GetValue(1,0), m_MatrixA.GetValue(1,1), m_MatrixA.GetValue(1,2));
    anchor1AxeZ.Set(m_MatrixA.GetValue(2,0), m_MatrixA.GetValue(2,1), m_MatrixA.GetValue(2,2));
    anchor1Pos.Set(m_MatrixA.GetValue(3,0), m_MatrixA.GetValue(3,1), m_MatrixA.GetValue(3,2));
    // Get the scale factor from the length of the axeX.
    scaleFactor = anchor1AxeX.GetLength();
    // Extract anchor2 vector.
    anchor2AxeX.Set(m_MatrixB.GetValue(0,0), m_MatrixB.GetValue(0,1), m_MatrixB.GetValue(0,2));
    anchor2AxeY.Set(m_MatrixB.GetValue(1,0), m_MatrixB.GetValue(1,1), m_MatrixB.GetValue(1,2));
    anchor2AxeZ.Set(m_MatrixB.GetValue(2,0), m_MatrixB.GetValue(2,1), m_MatrixB.GetValue(2,2));
    anchor2Pos.Set(m_MatrixB.GetValue(3,0), m_MatrixB.GetValue(3,1), m_MatrixB.GetValue(3,2));

    // Compute the final Position.
    Vector3D alignAxe;
    Vector3D finalPos;
    // Compute the axe between the anchor.
    alignAxe.Sub(anchor2Pos, anchor1Pos);
    float distAnchor = alignAxe.GetLength();
    // Compute the position.
    finalPos.MulByFloat(alignAxe, m_BlendPos);
    finalPos.SelfAdd(anchor1Pos);
    // Normalize the axe.
    alignAxe.SelfNormalize();

    // Compute the anchor blend rotation.
    Vector3D anchor1Axe, anchor2Axe, anchorAxeBlend;
    // Select the ref axe.
    if(m_RefAnchorAxe == 0){
        anchor1Axe = anchor1AxeX;
        anchor2Axe = anchor2AxeX;
    }else if(m_RefAnchorAxe == 1){
        anchor1Axe = anchor1AxeY;
        anchor2Axe = anchor2AxeY;
    }else if(m_RefAnchorAxe == 2){
        anchor1Axe = anchor1AxeZ;
        anchor2Axe = anchor2AxeZ;
    }
    // Blend the anchor axe.
    anchorAxeBlend.Sub(anchor2Axe, anchor1Axe);
    anchorAxeBlend.SelfMulByFloat(m_BlendRot);
    anchorAxeBlend.SelfAdd(anchor1Axe);
    anchorAxeBlend.SelfNormalize();

    // Compute the final rotation.
    Vector3D AxeX, AxeY, AxeZ;
    float scaleAlignAxe = scaleFactor;
    if(m_useStretch == 1){
        scaleAlignAxe = (distAnchor / (restLength * scaleFactor)) * scaleFactor;
    }

    if(m_RefAxe == 0){
        AxeZ.CrossProduct(alignAxe, anchorAxeBlend);
        AxeY.CrossProduct(AxeZ, alignAxe);

        AxeX.Normalize(alignAxe);
        AxeY.SelfNormalize();
        AxeZ.SelfNormalize();

        AxeX.SelfMulByFloat(scaleAlignAxe);
        AxeY.SelfMulByFloat(scaleFactor);
        AxeZ.SelfMulByFloat(scaleFactor);

    }else if(m_RefAxe == 1){
        AxeX.CrossProduct(alignAxe, anchorAxeBlend);
        AxeZ.CrossProduct(AxeX, alignAxe);

        AxeY.Normalize(alignAxe);
        AxeZ.SelfNormalize();
        AxeX.SelfNormalize();

        AxeY.SelfMulByFloat( scaleAlignAxe);
        AxeZ.SelfMulByFloat(scaleFactor);
        AxeX.SelfMulByFloat(scaleFactor);

    }else if(m_RefAxe == 2){
        AxeY.CrossProduct(alignAxe, anchorAxeBlend);
        AxeX.CrossProduct(AxeY, alignAxe);

        AxeZ.Normalize(alignAxe);      
        AxeX.SelfNormalize();
        AxeY.SelfNormalize();

        AxeZ.SelfMulByFloat( scaleAlignAxe);
        AxeX.SelfMulByFloat(scaleFactor);
        AxeY.SelfMulByFloat(scaleFactor);
    }
   
    // Update the out matrix.
    m_Result.Set(   AxeX.GetX(), AxeX.GetY(), AxeX.GetZ(), 0.0f,
                    AxeY.GetX(), AxeY.GetY(), AxeY.GetZ(), 0.0f,
                    AxeZ.GetX(), AxeZ.GetY(), AxeZ.GetZ(), 0.0f,
                    finalPos.GetX(), finalPos.GetY(), finalPos.GetZ(), 1.0f);

}

// GET METHODES
Matrix4x4 CompressionCompensator::GetResultMatrix(){
    return m_Result;
}

// SET METHODES
void CompressionCompensator::Set(   Matrix4x4& in_MatrixA,
                                    Matrix4x4& in_MatrixB,
                                    int& in_RefAxe,
                                    int& in_RefAnchorAxe,
                                    float& in_RestLength,
                                    float& in_BlendRot,
                                    float& in_BlendPos,
                                    int& in_UseGuideRest,
                                    Matrix4x4& in_GuideMatrixA,
                                    Matrix4x4& in_GuideMatrixB,
                                    int& in_UseStretch){
    m_MatrixA = in_MatrixA;
    m_MatrixB = in_MatrixB;
    m_RefAxe = in_RefAxe;  
    m_RefAnchorAxe = in_RefAnchorAxe;
    m_RestLength = in_RestLength;
    m_BlendRot = in_BlendRot;
    m_BlendPos = in_BlendPos;
    m_useGuideRest = in_UseGuideRest;
    m_GuideMatrixA = in_GuideMatrixA;
    m_GuideMatrixB = in_GuideMatrixB;
    m_useStretch = in_UseStretch;
}

void CompressionCompensator::SetMatrixA(Matrix4x4& in_MatrixA){
    m_MatrixA = in_MatrixA;   
}

void CompressionCompensator::SetMatrixB(Matrix4x4& in_MatrixB){
    m_MatrixB = in_MatrixB;   
}

void CompressionCompensator::SetRefAxe(int& in_RefAxe){
    m_RefAxe = in_RefAxe;   
}

void CompressionCompensator::SetRefAnchorAxe(int& in_RefAnchorAxe){
    m_RefAnchorAxe = in_RefAnchorAxe; 
}

void CompressionCompensator::SetRestLength(float& in_RestLength){
    m_RestLength = in_RestLength; 
}

void CompressionCompensator::SetBlendRot(float& in_BlendRot){
    m_BlendRot = in_BlendRot; 
}

void CompressionCompensator::SetBlendPos(float& in_BlendPos){
    m_BlendPos = in_BlendPos; 
}

void CompressionCompensator::SetUseGuideRest(int& in_UseGuideRest){
    m_useGuideRest = in_UseGuideRest; 
}

void CompressionCompensator::SetGuideMatrixA(Matrix4x4& in_GuideMatrixA){
    m_GuideMatrixA = in_GuideMatrixA; 
}

void CompressionCompensator::SetGuideMatrixB(Matrix4x4& in_GuideMatrixB){
    m_GuideMatrixB = in_GuideMatrixB; 
}