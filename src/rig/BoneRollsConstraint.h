/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file BoneRollsConstraint.h
@author Guillaume Baratte
@date 2016-05-31
@brief Define the bone rolls Constraint class.
Help to create some rolls transformation to help the bone twist.
*/

#ifndef __BONEROLLSCONSTRAINT__
#define __BONEROLLSCONSTRAINT__

#include "../core/Vector3D.h"
#include "../core/Quaternion.h"
#include "../core/Matrix3x3.h"
#include "../core/Matrix4x4.h"
#include "../core/DataArray.h"

class BoneRollsConstraint
{
	public:
		//METHODES.
		
		//CONSTRUCTOR.
		/*! @brief Primary constructor. */
		BoneRollsConstraint();
		
		/*! @brief Constructor with initialize data.
		@param[in] in_RoolsNumber The number of rolls.
		@param[in] in_Bone1Matrix The first bone world transformation.
		@param[in] in_Bone2Matrix The second bone world transformation.
		@param[in] in_StartDist The value of the start distribution. default 0.0. 0.0 -> 1.0.
		@param[in] in_EndDist The value of the end distribution. default 1.0. 0.0 -> 1.0.
		@param[in] in_StartAngle The offset angle at the start distribution in degrees.
		@param[in] in_EndAngle The offset angle at the end distribution in degrees.
		@parma[in] in_ArrayRollsBlend The array of float the filter the blend between the start and the end.
		*/
		BoneRollsConstraint(	int& in_RoolsNumber,
								Matrix4x4& in_Bone1Matrix,
								Matrix4x4& in_Bone2Matrix,
								float& in_StartDist,
								float& in_EndDist,
								float& in_StartAngle,
								float& in_EndAngle,
								DataArray<float>& in_ArrayRollsBlend,
								int& in_AxeAlign,
								int& in_Symmetrize);

		//DESTRUCTOR.
		/*! @brief Primary destructor. */
		~BoneRollsConstraint();

		/*! @brief Compute the rolls transformation matrix in world space.*/
		void Compute();

		/*! @brief Get the array of rolls matrix.
		@return DataArray<Matrix4x4> The array of matrix for all rolls.
		*/
		DataArray<Matrix4x4> Get();

		/*! @brief Get the roll's matrix at specific index.
		@param[in] in_Index The index to get the roll.
		@return Matrix4x4 The specific matrix.
		*/
		Matrix4x4 GetAt(int& in_Index);

		/*! @brief Set the attributes.
		@param[in] in_RoolsNumber The number of rolls.
		@param[in] in_Bone1Matrix The first bone world transformation.
		@param[in] in_Bone2Matrix The second bone world transformation.
		@param[in] in_StartDist The value of the start distribution. default 0.0. 0.0 -> 1.0.
		@param[in] in_EndDist The value of the end distribution. default 1.0. 0.0 -> 1.0.
		@param[in] in_StartAngle The offset angle at the start distribution in degrees.
		@param[in] in_EndAngle The offset angle at the end distribution in degrees.
		@parma[in] in_ArrayRollsBlend The array of float the filter the blend between the start and the end.
		*/
		void Set(	int& in_RoolsNumber,
					Matrix4x4& in_Bone1Matrix,
					Matrix4x4& in_Bone2Matrix,
					float& in_StartDist,
					float& in_EndDist,
					float& in_StartAngle,
					float& in_EndAngle,
					DataArray<float>& in_ArrayRollsBlend,
					int& in_AxeAlign,
					int& in_Symmetrize);
		/*! @brief Set the rolls number.
		@param[in] in_RoolsNumber The number of rolls.
		*/
		void SetRollsNumber(int& in_RoolsNumber);
		/*! @brief Set the first bone matrix.
		@param[in] in_Bone1Matrix The first bone world transformation.
		*/
		void SetBone1Matrix(Matrix4x4& in_Bone1Matrix);
		/*! @brief Set the second bone matrix.
		@param[in] in_Bone2Matrix The second bone world transformation.
		*/
		void SetBone2Matrix(Matrix4x4& in_Bone2Matrix);
		/*! @brief Set start distribution.
		@param[in] in_StartDist The value of the start distribution. default 0.0. 0.0 -> 1.0.
		*/
		void SetStartDistribution(float& in_StartDist);
		/*! @brief Set end distribution.
		@param[in] in_EndDist The value of the end distribution. default 1.0. 0.0 -> 1.0.
		*/
		void SetEndDistribution(float& in_EndDist);
		/*! @brief Set start angle at start distribution.
		@param[in] in_StartAngle The offset angle at the start distribution in degrees.
		*/
		void SetStartAngle(float& in_StartAngle);
		/*! @brief Set end angle at end distribution.
		@param[in] in_EndAngle The offset angle at the end distribution in degrees.
		*/
		void SetEndAngle(float& in_EndAngle);
		/*! @brief Set the array to blend the rotation from the start to the end.
		@parma[in] in_ArrayRollsBlend The array of float the filter the blend between the start and the end.
		*/
		void SetArrayRollsBlend(DataArray<float>& in_ArrayRollsBlend);
		/*! @brief Set the axe to align the roll. 0->X. 1->Y. 2->Z..
		@parma[in] in_AxeAlign The axe to align the roll.
		*/
		void SetAxeAlign(int& in_AxeAlign);
		/*! @brief Set value of use the symmetrie.
		@parma[in] m_Symmetrize The value of symmetrie.
		*/
		void SetSymmetrize(int& in_Symmetrize);

	private:
		static const int		m_MemoryBaseSize;		/*! The memory base size. */
		int						m_RollsNumber;			/*! The number of roll along the bone 1.*/
		int						m_AxeAlign;				/*! The axe to align the roll. 0->X. 1->Y. 2->Z.*/
		int						m_Symmetrize;			/*! Active the symmetrize rotation. 0->Off. 1->On.*/
		int 					m_SymInputMatrix;		/*! Set the symmetry of the input matrix.*/
		Matrix4x4				m_Bone1Matrix;			/*! The first bone transformation. */
		Matrix4x4				m_Bone2Matrix;			/*! The second bone transformation. */
		float					m_StartDistribution;	/*! The start range of the roll distribution. */
		float					m_EndDistribution;		/*! The end range of the roll distribution. */
		float					m_StartRollsOffset;		/*! The start distribution angle offset. */
		float					m_EndRollsOffset;		/*! The end distribution angle offset. */
		DataArray<float>		m_ArrayRollsBlend;		/*! The array filter for rotation blend from the start to the end.*/

		DataArray<Matrix4x4>	out_ArrayRollsMatrix; /*! The array of transformation for the rolls object. */
};


#endif