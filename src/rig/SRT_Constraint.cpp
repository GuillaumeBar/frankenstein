/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file SRT_Constraint.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "SRT_Constraint.h"

// CONSTRUCTORS.
SRT_Constraint::SRT_Constraint(){

};

SRT_Constraint::SRT_Constraint(Matrix4x4& in_DriverMatrix, Matrix4x4& in_OffsetMatrix, Matrix4x4& in_ParentMatrix, bool& in_UseOffset, int& in_LocalTransform){
	m_DriverMatrix = in_DriverMatrix;
	m_OffsetMatrix = in_OffsetMatrix;
	m_ParentMatrix = in_ParentMatrix;
	m_UseOffset = in_UseOffset;
	m_LocalTransform = in_LocalTransform;
};

// DESTRUCTOR.
SRT_Constraint::~SRT_Constraint(){

};

// COMPUTE METHODES.
void SRT_Constraint::computeDrivenMatrix(){
	if(m_LocalTransform == 0){
		m_DrivenMatrix.Invert(m_ParentMatrix);
		m_DrivenMatrix.Mul(m_DriverMatrix, m_DrivenMatrix);
		if(m_UseOffset){
			m_DrivenMatrix.Mul(m_OffsetMatrix, m_DrivenMatrix);
		}
	}else{
		m_DrivenMatrix = m_DriverMatrix;
		if(m_UseOffset ){
			m_DrivenMatrix.Mul(m_OffsetMatrix, m_DrivenMatrix);
		}
	}
};

// GET METHODES.
Matrix4x4 SRT_Constraint::GetDrivenMatrix(){
	return m_DrivenMatrix;
};

// SET METHODES.
void SRT_Constraint::SetDriverMatrix(Matrix4x4& in_DriverMatrix){
	m_DriverMatrix = in_DriverMatrix;
};

void SRT_Constraint::SetOffsetMatrix(Matrix4x4& in_OffsetMatrix){
	m_OffsetMatrix = in_OffsetMatrix;
};

void SRT_Constraint::SetParentMatrix(Matrix4x4& in_ParentMatrix){
	m_ParentMatrix = in_ParentMatrix;
};

void SRT_Constraint::SetUseOffset(bool& in_UseOffset){
	m_UseOffset = in_UseOffset;
};

void SRT_Constraint::SetLocalTransform(int& in_LocalTransform){
	m_LocalTransform = in_LocalTransform;
};