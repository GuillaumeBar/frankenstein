/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file Ik3BonesMember.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "Ik3BonesMember.h"
#include <iostream>
using namespace std;

// CONSTRUCTOR.
Ik3BonesMember::Ik3BonesMember(){
	guidRoot = Matrix4x4();
	guidBone2 = Matrix4x4();
	guidBone3 = Matrix4x4();
	guidEff = Matrix4x4();
	guidMainUpV =Matrix4x4();
	ikRoot = Matrix4x4();
	ikMainUpV = Matrix4x4();
	ikBone2UpV = Matrix4x4();
	ikBone3UpV = Matrix4x4();
	ikEff = Matrix4x4();
	ikRotationOffset = 0.0f;
	fkBone1 = Matrix4x4();
	fkBone2 = Matrix4x4();
	fkBone3 = Matrix4x4();
	pinBones = 0;
	symMatrixSwitch = 0;
	blendIKFK = 1.0f;
	planTwist = 0.0f;
	useSoftIK = 1;
	startSoftIK = 0.99f;
	globalBonesScale = 1.0f;
	bone1Scale = 1.0f;
	bone2Scale = 1.0f;
	bone3Scale = 1.0f;
	useStretch = 0;
	addTransition = -1.0f;
	globalScale = 1.0f;
};

Ik3BonesMember::Ik3BonesMember(	Matrix4x4& in_GuidRoot,
								Matrix4x4& in_GuidBone2,
								Matrix4x4& in_GuidBone3,
								Matrix4x4& in_GuidEff,
								Matrix4x4& in_GuidMainUpV,
								Matrix4x4& in_IkRoot,
								Matrix4x4& in_IkMainUpV,
								Matrix4x4& in_IkBone2UpV,
								Matrix4x4& in_IkBone3UpV,
								Matrix4x4& in_IkEff,
								float& in_IkRotationOffset,
								Matrix4x4& in_IkPinBone2,
								Matrix4x4& in_IkPinBone3,
								Matrix4x4& in_FkBone1,
								Matrix4x4& in_FkBone2,
								Matrix4x4& in_FkBone3,
								Matrix4x4& in_FkBone4,
								int& in_PinBones,
								int& in_SymMatrixSwitch,
								float& in_BlendIKFK,
								float& in_PlanTwist,
								float& in_PlanBone2Twist,
								float& in_PlanBone3Twist,
								int& in_UseSoftIK,
								float& in_StartSoftIK,
								float& in_GlobalBonesScale,
								float& in_Bone1Scale,
								float& in_Bone2Scale,
								float& in_Bone3Scale,
								int& in_UseStretch,
								float& in_GlobalScale){

	guidRoot = in_GuidRoot;
	guidBone2 = in_GuidBone2;
	guidBone3 = in_GuidBone3;
	guidEff = in_GuidEff;
	guidMainUpV = in_GuidMainUpV;
	ikRoot = in_IkRoot;
	ikMainUpV = in_IkMainUpV;
	ikBone2UpV = in_IkBone2UpV;
	ikBone3UpV = in_IkBone3UpV;
	ikEff = in_IkEff;
	ikRotationOffset = in_IkRotationOffset;
	ikPinBone2 = in_IkPinBone2;
	ikPinBone3 = in_IkPinBone3;
	fkBone1 = in_FkBone1;
	fkBone2 = in_FkBone2;
	fkBone3 = in_FkBone3;
	fkBone4 = in_FkBone4;
	pinBones = in_PinBones;
	symMatrixSwitch = in_SymMatrixSwitch;
	blendIKFK = in_BlendIKFK;
	planTwist = in_PlanTwist;
	planBone2Twist = in_PlanBone2Twist;
	planBone3Twist = in_PlanBone3Twist;
	useSoftIK = in_UseSoftIK;
	startSoftIK = in_StartSoftIK;
	globalBonesScale = in_GlobalBonesScale;
	bone1Scale = in_Bone1Scale;
	bone2Scale = in_Bone2Scale;
	bone3Scale = in_Bone3Scale;
	useStretch = in_UseStretch;
	addTransition = -1.0f;
	globalScale = in_GlobalScale;
};

//DESTRUCTOR.
Ik3BonesMember::~Ik3BonesMember(){

};

// HELP METHODS.
void Ik3BonesMember::ComputeMatrixFromTreeAxis(Matrix4x4& out_Matrix, Vector3D& in_Axe1, Vector3D& in_Axe2, Vector3D& in_Position){
	Vector3D axeX, axeY, axeZ;
	//Compute the orthogonal axis.
	axeY.CrossProduct(in_Axe2, in_Axe1);
	axeZ.CrossProduct(in_Axe1, axeY);
	//Normalize the axis.
	axeX.Normalize(in_Axe1);
	axeY.SelfNormalize();
	axeZ.SelfNormalize();
	//Update the transformation matrix.
	out_Matrix.Set(axeX.GetX(), axeX.GetY(), axeX.GetZ(), 0.0f,
					axeY.GetX(), axeY.GetY(), axeY.GetZ(), 0.0f,
					axeZ.GetX(), axeZ.GetY(), axeZ.GetZ(), 0.0f,
					in_Position.GetX(), in_Position.GetY(), in_Position.GetZ(), 1.0f);
};

void Ik3BonesMember::ComputePosLinearInterpolate(Vector3D& in_VectorA, Vector3D& in_VectorB, float& in_Blend, Vector3D& out_Vector){

	float tx, ty, tz;

	tx = in_VectorA.GetX() * in_Blend + in_VectorB.GetX() * ( 1 - in_Blend );
	ty = in_VectorA.GetY() * in_Blend + in_VectorB.GetY() * ( 1 - in_Blend );
	tz = in_VectorA.GetZ() * in_Blend + in_VectorB.GetZ() * ( 1 - in_Blend );

	out_Vector.Set(tx, ty, tz);
};

void Ik3BonesMember::InsertPosRotMatrix(Vector3D& in_Vector, Quaternion& int_Rot, Matrix4x4& out_Matrix){

	Vector3D axeX(1.0f, 0.0f, 0.0f);
	Vector3D axeY(0.0f, 1.0f, 0.0f);
	Vector3D axeZ(0.0f, 0.0f, 1.0f);

	axeX.SelfRotateByQuaternionFG(int_Rot);
	axeY.SelfRotateByQuaternionFG(int_Rot);
	axeZ.SelfRotateByQuaternionFG(int_Rot);

	out_Matrix.Set( axeX.GetX(), axeX.GetY(), axeX.GetZ(), 0.0f,
					axeY.GetX(), axeY.GetY(), axeY.GetZ(), 0.0f,
					axeZ.GetX(), axeZ.GetY(), axeZ.GetZ(), 0.0f,
					in_Vector.GetX(), in_Vector.GetY(), in_Vector.GetZ(), 1.0f);
};

void Ik3BonesMember::ExtractMatrixPosRot(Matrix4x4& in_Matrix, Vector3D& out_Vector, Quaternion& out_Rot){

	Matrix3x3 tempRot;

	tempRot.Set(	in_Matrix.GetValue(0,0), in_Matrix.GetValue(0,1), in_Matrix.GetValue(0,2),
					in_Matrix.GetValue(1,0), in_Matrix.GetValue(1,1), in_Matrix.GetValue(1,2),
					in_Matrix.GetValue(2,0), in_Matrix.GetValue(2,1), in_Matrix.GetValue(2,1));

	out_Rot.FromMatrix3x3(tempRot);

	out_Vector.Set(in_Matrix.GetValue(3,0), in_Matrix.GetValue(3,1), in_Matrix.GetValue(3,2));
};

void Ik3BonesMember::ComputeGlobalPlanMatrix(Matrix4x4& out_Matrix, Vector3D& in_RootPos, Vector3D& in_EffPos, Vector3D& in_MainUpV, float& in_PlanTwist){

	Quaternion LocalRot;
	Vector3D axeX, axeY, axeZ, tempVec;
	//Compute the matrix axis.
	axeX.Sub(in_EffPos, in_RootPos);
	axeX.SelfNormalize();
	tempVec.Sub(in_MainUpV, in_RootPos);

	axeZ.CrossProduct(tempVec, axeX);
	axeZ.SelfNormalize();
	LocalRot.FromAxisAndAngle(axeX.GetX(), axeX.GetY(), axeX.GetZ(), in_PlanTwist);
	axeZ.SelfRotateByQuaternionFG(LocalRot);

	axeY.CrossProduct(axeZ, axeX);
	axeY.SelfNormalize();

	//Update the out matrix.
	out_Matrix.Set(axeX.GetX(), axeX.GetY(), axeX.GetZ(), 0.0f,
							axeY.GetX(), axeY.GetY(), axeY.GetZ(), 0.0f,
							axeZ.GetX(), axeZ.GetY(), axeZ.GetZ(), 0.0f,
							in_RootPos.GetX(), in_RootPos.GetY(), in_RootPos.GetZ(), 1.0f);
};

void Ik3BonesMember::ExtractMatrixPosition(Matrix4x4& in_Matrix, Vector3D& out_Vector){
	float posX, posY, posZ;
	in_Matrix.GetPosition(posX, posY, posZ);
	out_Vector.Set(posX, posY, posZ);
};

void Ik3BonesMember::ComputePinedIK(bool& in_InvertPlan, float& in_TwistPlan, Vector3D& in_RootPos, Vector3D& in_EffPos, Vector3D& in_UpVector, float& in_SegA, float& in_SegC, float& out_StretchFactor, Matrix4x4& out_Bone1, Matrix4x4& out_Bone2, Matrix4x4& out_Bone3){
	Vector3D vRE, vRENorm, vRESoft;
	Vector3D vRUp, vCross;
	Quaternion rotRE;

	//Compute the vector Root -> UpVector.
	vRUp.Sub(in_UpVector, in_RootPos);
	//Compute the vector Root -> Effector.
	vRE.Sub(in_EffPos, in_RootPos);
	//Compute the segment b length.
	float SegB = vRE.GetLength();
	float SegBSoft;
	float SubMaxChainLength = in_SegA + in_SegC;
	//Compute the smooth Ik length.
	if(useSoftIK==1){
		float distDelta = SubMaxChainLength * startSoftIK;
		float distSoft = SubMaxChainLength - distDelta;
		SegBSoft =  (1.0f - expf(-(SegB - distDelta) / distSoft)) * distSoft + distDelta;
		if(SegB<distDelta){
			SegBSoft = SegB;
		}
	}else{
		SegBSoft = SegB;
	}

	out_StretchFactor = 1.0f;
	//Compute the scale stretch factor.
	if(useStretch==1){
		if(useSoftIK==1){
			if(SegBSoft==0.0f){
				out_StretchFactor = 0.0f;
			}else{
				out_StretchFactor = SegB / SegBSoft;
			}
			SegBSoft = SegB;
		}else{
			if(SegBSoft > SubMaxChainLength){
				if(SubMaxChainLength == 0.0f){
					out_StretchFactor = 0.0f;
				}else{
					out_StretchFactor = SegBSoft / SubMaxChainLength;
				}
			}else{
				out_StretchFactor = 1.0f;
			}
		}
	}else{
		out_StretchFactor = 1.0f;
	}	

	//Compute the end chain.
	vRESoft.Normalize(vRE);
	vRESoft.SelfNormalize();
	vRESoft.SelfMulByFloat(SegBSoft);
	vRESoft.SelfAdd(in_RootPos);

	//Compute the angle AB.
	float abAngle = ComputeCosinusLaw(in_SegA, SegBSoft, in_SegC);
	//Compute the perpendicular vector.
	vCross.CrossProduct(vRUp, vRE);
	vCross.SelfNormalize();
	if(in_InvertPlan){
		vCross.Set(-vCross.GetX(), -vCross.GetY(), -vCross.GetZ());
		abAngle = -abAngle;
	}
	//Twist the perpendicular vector.
	Quaternion rotPlan;
	vRENorm.Normalize(vRE);
	rotPlan.FromAxisAndAngle(vRENorm.GetX(), vRENorm.GetY(), vRENorm.GetZ(), in_TwistPlan);
	vCross.SelfRotateByQuaternionFG(rotPlan);

	//Compute the quaternion rotation.
	rotRE.FromAxisAndAngle(vCross.GetX(), vCross.GetY(), vCross.GetZ(), -abAngle);
	//Resize the vector root effector.

	vRENorm.SelfMulByFloat(in_SegA * out_StretchFactor);
	//Rotate the vector.
	vRENorm.SelfRotateByQuaternionFG(rotRE);

	//Compute the bone world transformation.
	Vector3D b1Pos, b1AxeX, b1AxeY, b1AxeZ;
	Vector3D b2Pos, b2AxeX, b2AxeY, b2AxeZ;
	Vector3D b3Pos;

	//Compute the bone 1 transformation.
	b1Pos = in_RootPos;
	b1AxeX.Normalize(vRENorm);
	b1AxeY.CrossProduct(vCross, b1AxeX);
	b1AxeZ.CrossProduct(b1AxeX, b1AxeY);
	b1AxeY.SelfNormalize();
	b1AxeZ.SelfNormalize();
	//Compute the bone 2 transformation.
	b2Pos.Add(b1Pos, vRENorm);
	b2AxeX.Sub(vRESoft, b2Pos);
	b2AxeY.CrossProduct(vCross, b2AxeX);
	b2AxeZ.CrossProduct(b2AxeX, b2AxeY);
	b2AxeX.SelfNormalize();
	b2AxeY.SelfNormalize();
	b2AxeZ.SelfNormalize();
	//Compute the bone 3 transformation.
	b3Pos.MulByFloat(b2AxeX, in_SegC * out_StretchFactor);
	b3Pos.SelfAdd(b2Pos);

	//Update the output bones matrix.
	out_Bone1.Set(b1AxeX.GetX(), b1AxeX.GetY(), b1AxeX.GetZ(), 0.0f,
					b1AxeY.GetX(), b1AxeY.GetY(), b1AxeY.GetZ(), 0.0f,
					b1AxeZ.GetX(), b1AxeZ.GetY(), b1AxeZ.GetZ(), 0.0f,
					b1Pos.GetX(), b1Pos.GetY(), b1Pos.GetZ(), 1.0f);

	out_Bone2.Set(b2AxeX.GetX(), b2AxeX.GetY(), b2AxeX.GetZ(), 0.0f,
					b2AxeY.GetX(), b2AxeY.GetY(), b2AxeY.GetZ(), 0.0f,
					b2AxeZ.GetX(), b2AxeZ.GetY(), b2AxeZ.GetZ(), 0.0f,
					b2Pos.GetX(), b2Pos.GetY(), b2Pos.GetZ(), 1.0f);

	out_Bone3.Set(b2AxeX.GetX(), b2AxeX.GetY(), b2AxeX.GetZ(), 0.0f,
					b2AxeY.GetX(), b2AxeY.GetY(), b2AxeY.GetZ(), 0.0f,
					b2AxeZ.GetX(), b2AxeZ.GetY(), b2AxeZ.GetZ(), 0.0f,
					b3Pos.GetX(), b3Pos.GetY(), b3Pos.GetZ(), 1.0f);
};

// COMPUTE METHODS.
void Ik3BonesMember::ComputeBasicIK(){
	Vector3D Temp1, Temp2, Temp3, Temp4, Temp5;

	Matrix4x4 RestGlobaPlanelInv, GlobalPlan, GlobalScaledPlan;
	Vector3D guidRootPos, guidBone2Pos, guidBone3Pos, guidEffPos, guidMainUpVPos;
	Vector3D ikRootPos, ikEffPos, ikMainUpVPos, ikBone2UpVPos, ikBone3UpVPos, ikPinBone2Pos, ikPinBone3Pos;

	float planTwistDeg = planTwist * 3.14159265359f / 180.0f;
	float planBone2TwistDeg = -planBone2Twist * 3.14159265359f / 180.0f;
	float planBone3TwistDeg = -planBone3Twist * 3.14159265359f / 180.0f;

	//Extract the guid matrix position.
	ExtractMatrixPosition(guidRoot, guidRootPos);
	ExtractMatrixPosition(guidBone2, guidBone2Pos);
	ExtractMatrixPosition(guidBone3, guidBone3Pos);
	ExtractMatrixPosition(guidEff, guidEffPos);
	ExtractMatrixPosition(guidMainUpV, guidMainUpVPos);

	//Extract the ik matrix position.
	ExtractMatrixPosition(ikRoot, ikRootPos);
	ExtractMatrixPosition(ikBone2UpV, ikBone2UpVPos);
	ExtractMatrixPosition(ikBone3UpV, ikBone3UpVPos);
	ExtractMatrixPosition(ikEff, ikEffPos);
	ExtractMatrixPosition(ikMainUpV, ikMainUpVPos);
	ExtractMatrixPosition(ikPinBone2, ikPinBone2Pos);
	ExtractMatrixPosition(ikPinBone3, ikPinBone3Pos);

	//Compute the guid invert global plan.
	ComputeGlobalPlanMatrix(RestGlobaPlanelInv, guidRootPos, guidEffPos, guidMainUpVPos, planTwistDeg);
	RestGlobaPlanelInv.SelfInvert();

	// Compute the ik global plan.
	ComputeGlobalPlanMatrix(GlobalPlan, ikRootPos, ikEffPos, ikMainUpVPos, planTwistDeg);
	// Scale the gobal plan matrix by the global scale factor.
	GlobalScaledPlan = GlobalPlan;
	GlobalScaledPlan.SetScale(globalScale, globalScale, globalScale);

	//|------------------------------------------------------------------|
	//| Transfert the guid position from the guid space to the ik space. |
	//|------------------------------------------------------------------|

	guidRootPos.SelfMulByMatrix4x4(RestGlobaPlanelInv);
	guidRootPos.SelfMulByMatrix4x4(GlobalScaledPlan);

	guidBone2Pos.SelfMulByMatrix4x4(RestGlobaPlanelInv);
	guidBone2Pos.SelfMulByMatrix4x4(GlobalScaledPlan);

	guidBone3Pos.SelfMulByMatrix4x4(RestGlobaPlanelInv);
	guidBone3Pos.SelfMulByMatrix4x4(GlobalScaledPlan);

	guidEffPos.SelfMulByMatrix4x4(RestGlobaPlanelInv);
	guidEffPos.SelfMulByMatrix4x4(GlobalScaledPlan);

	//|---------------------------------------------|
	//| Compute the guid vector and ik main vector. |
	//|---------------------------------------------|

	Vector3D guidRE, guidRB2, guidB2B3, guidB3E, ikRE, endIkChainPos;
	guidRE.Sub(guidEffPos, guidRootPos);
	guidRB2.Sub(guidBone2Pos, guidRootPos);
	guidB2B3.Sub(guidBone3Pos, guidBone2Pos);
	guidB3E.Sub(guidEffPos, guidBone3Pos);
	ikRE.Sub(ikEffPos, ikRootPos);

	//|-----------------------|
	//| Compute the first IK. |
	//|-----------------------|

	Vector3D GlobalAxeZ(GlobalPlan.GetValue(2,0), GlobalPlan.GetValue(2,1), GlobalPlan.GetValue(2,2));
	float subChainPourcentage;
	float b1Length, b2Length, b3Length, guidLength, ikLenght, ikLenghtSmooth;
	float segC;
	float chainMaxLength, subChainMaxLength;
	float stretchScaleFactor;

	//Compute the rest length of the bone of the chain.
	b1Length = guidRB2.GetLength();
	b2Length = guidB2B3.GetLength();
	b3Length = guidB3E.GetLength();
	guidLength = guidRE.GetLength();

	//Compute the scaled bone length.
	b1Length = b1Length * globalBonesScale * bone1Scale;
	b2Length = b2Length * globalBonesScale * bone2Scale;
	b3Length = b3Length * globalBonesScale * bone3Scale;

	if(pinBones==0){
		//Compute the distance between the ik root and the ik eff.
		ikLenght = ikRE.GetLength();

		//Compute the maximum length of the chain and the maximum length of the sub chain.
		subChainMaxLength = b2Length + b3Length;
		chainMaxLength = subChainMaxLength + b1Length;

		//Compute the smooth Ik length.
		if(useSoftIK==1){
			float distDelta = chainMaxLength * startSoftIK;
			float distSoft = chainMaxLength - distDelta;
			ikLenghtSmooth =  (1.0f - expf(-(ikLenght - distDelta) / distSoft)) * distSoft + distDelta;
			if(ikLenght<distDelta){
				ikLenghtSmooth = ikLenght;
			}
		}else{
			ikLenghtSmooth = ikLenght;
		}

		//Compute the scale stretch factor.
		if(useStretch==1){
			if(useSoftIK==1){
				if(ikLenghtSmooth==0.0f){
					stretchScaleFactor = 0.0f;
				}else{
					stretchScaleFactor = ikLenght / ikLenghtSmooth;					
				}
				ikLenghtSmooth = ikLenght;
			}else{
				if(ikLenghtSmooth > chainMaxLength){
					if(chainMaxLength == 0.0f){
						stretchScaleFactor = 0.0f;
					}else{
						stretchScaleFactor = ikLenghtSmooth / chainMaxLength;
					}
				}else{
					stretchScaleFactor = 1.0f;
				}
			}
		}else{
			stretchScaleFactor = 1.0f;
		}

		//Compute the length pourcentage of the segC with the maximum chain length.
		if(chainMaxLength == 0.0f){
			subChainPourcentage = 0.0f;
		}else{
			subChainPourcentage = subChainMaxLength / chainMaxLength;
		}
		segC = ikLenghtSmooth * subChainPourcentage;

		//Compute the end chain position.
		endIkChainPos.Normalize(ikRE);
		endIkChainPos.SelfMulByFloat(ikLenghtSmooth);
		endIkChainPos.SelfAdd(ikRootPos);

		//Compute the fisrt chain angle for realigne the vector ikRE.
		float firstIkAngle = ComputeCosinusLaw(b1Length, ikLenghtSmooth, segC);

		//Realign the vector ikRE with the angle and the axeZ of the global ik plan.
		Vector3D ikRENorm;
		Quaternion quatRot;
		ikRENorm.Normalize(ikRE);
		quatRot.FromAxisAndAngle(GlobalPlan.GetValue(2,0), GlobalPlan.GetValue(2,1), GlobalPlan.GetValue(2, 2), firstIkAngle);
		ikRENorm.SelfMulByFloat(b1Length * stretchScaleFactor);
		ikRENorm.SelfRotateByQuaternionFG(quatRot);

		//Compute the first chain angle for realign the rest pose vector guidRE.
		//This is use to match the bone 2 position to the guid.
		segC = guidLength * subChainPourcentage;
		float firstGuidAngle = ComputeCosinusLaw(b1Length, guidLength, segC);	

		//Realign the vector guidRE with the angle and the axeZ of the global ik plan.
		Vector3D guidRENorm;
		guidRENorm.Normalize(guidRE);
		quatRot.FromAxisAndAngle(GlobalPlan.GetValue(2,0), GlobalPlan.GetValue(2,1), GlobalPlan.GetValue(2, 2), firstGuidAngle);
		guidRENorm.SelfRotateByQuaternionFG(quatRot);

		//Compute the angle and the vector axe for rotate the ikRENorm to match with the bone 2 guid position.
		Vector3D reAlignAxe;
		Vector3D guidRB2Norm;
		guidRB2Norm.Normalize(guidRB2);
		reAlignAxe.CrossProduct(guidRENorm, guidRB2Norm);
		reAlignAxe.SelfNormalize();
		float reAlignAngle = guidRENorm.GetAngle(guidRB2Norm, false);

		//Compute the value to progressively mute the realgin offset when the chain is at is maximum length.
		float reAlignFactor = (ikLenghtSmooth-guidLength)/(chainMaxLength-guidLength)*(0.0f-1.0f)+1.0f;
		//Clamp the realign factor.
		if ( reAlignFactor < 0.0f ){
			reAlignFactor = 0.0f;
		}else if ( reAlignFactor > 1.0f ){
			reAlignFactor = 1.0f;
		}

		//Compute the offset rotation.
		float offsetRotation = ikRotationOffset * 3.14159265359f / 180.0f * reAlignFactor;
		quatRot.FromAxisAndAngle(GlobalPlan.GetValue(2,0), GlobalPlan.GetValue(2,1), GlobalPlan.GetValue(2, 2), offsetRotation);

		//Compute the realign orientation offset.
		Quaternion reAlignRotation;
		reAlignAngle = reAlignAngle * reAlignFactor;
		reAlignRotation.FromAxisAndAngle(reAlignAxe.GetX(), reAlignAxe.GetY(), reAlignAxe.GetZ(), reAlignAngle);

		//Compute the bone 1 final AxeX Vector.
		Vector3D bone1AxeX;
		//reAlignRotation.SelfMul(quatRot);
		quatRot.SelfMul(reAlignRotation);
		bone1AxeX.RotateByQuaternionFG(ikRENorm, quatRot);

		//|----------------------------------------------|
		//| Compute the bone 2 position and AxeX Vector. |
		//|----------------------------------------------|

		//Compute the vector form the bone 2 position to the ik effector.
		Vector3D bone2Pos, bone2AxeX;
		Vector3D ikB2E;
		bone2Pos.Add(ikRootPos, bone1AxeX);
		ikB2E.Sub(endIkChainPos, bone2Pos);
		//Extract the vector length.
		float ikB2ELength = ikB2E.GetLength();
		//Normalize the vector and resize it with the length of the bone 2.
		ikB2E.SelfNormalize();
		ikB2E.SelfMulByFloat(b2Length * stretchScaleFactor);

		//Compute the subchain fisrt angle.
		float subChainAngle = ComputeCosinusLaw(b2Length, ikB2ELength, b3Length);

		//Compute the bone 2 AxeX.
		quatRot.FromAxisAndAngle(-GlobalPlan.GetValue(2,0), -GlobalPlan.GetValue(2,1), -GlobalPlan.GetValue(2,2), subChainAngle);
		bone2AxeX.RotateByQuaternionFG(ikB2E, quatRot);

		//|----------------------------------------------|
		//| Compute the bone 3 position and AxeX Vector. |
		//|----------------------------------------------|

		//Compute the vector form the bone 3 position to the ik effector.
		Vector3D bone3Pos, bone3AxeX;
		bone3Pos.Add(bone2Pos, bone2AxeX);
		bone3AxeX.Sub(endIkChainPos, bone3Pos);

		//|------------------------------------------------------|
		//| Compute the bones intermediate world transformation. |
		//|------------------------------------------------------|
		Matrix4x4 bone1InterTrans, bone2InterTrans, bone3InterTrans;
		ComputeMatrixFromTreeAxis(bone1InterTrans, 	bone1AxeX, GlobalAxeZ, ikRootPos);
		ComputeMatrixFromTreeAxis(bone2InterTrans, bone2AxeX, GlobalAxeZ, bone2Pos);
		ComputeMatrixFromTreeAxis(bone3InterTrans, bone3AxeX, GlobalAxeZ, bone3Pos);

		//|--------------------------------------------------------|
		//| Compute the bone 2 re orientation with bone2 UpVector. |
		//|--------------------------------------------------------|

		//Compute the local transformation matrix for the bone2 plan.
		Matrix4x4 InvBone2LocalMatrix;
		Vector3D axeXB2Local, axeYB2Local, axeZB2Local;
		axeXB2Local.Sub(bone3Pos, ikRootPos);
		axeZB2Local.Set(bone1InterTrans.GetValue(2,0), bone1InterTrans.GetValue(2,1), bone1InterTrans.GetValue(2,2));
		axeYB2Local.CrossProduct(axeZB2Local, axeXB2Local);
		axeXB2Local.SelfNormalize();
		axeYB2Local.SelfNormalize();
		InvBone2LocalMatrix.Set(axeXB2Local.GetX(), axeXB2Local.GetY(), axeXB2Local.GetZ(), 0.0f,
								axeYB2Local.GetX(), axeYB2Local.GetY(), axeYB2Local.GetZ(), 0.0f,
								axeZB2Local.GetX(), axeZB2Local.GetY(), axeZB2Local.GetZ(), 0.0f,
								ikRootPos.GetX(), ikRootPos.GetY(), ikRootPos.GetZ(), 1.0f);
		InvBone2LocalMatrix.SelfInvert();

		//Compute the matrix of the bone2 UpVector.
		Quaternion planRotB2;
		planRotB2.FromAxisAndAngle(axeXB2Local.GetX(), axeXB2Local.GetY(), axeXB2Local.GetZ(), planBone2TwistDeg);

		Matrix4x4 Bone2PlanMatrix;
		Vector3D vRootB2UpV, axeYB2Plan, axeZB2Plan;
		vRootB2UpV.Sub(ikRootPos, ikBone2UpVPos);
		axeZB2Plan.CrossProduct(vRootB2UpV, axeXB2Local);
		axeZB2Plan.SelfRotateByQuaternionFG(planRotB2);
		axeYB2Plan.CrossProduct(axeZB2Plan, axeXB2Local);
		axeYB2Plan.SelfNormalize();
		axeZB2Plan.SelfNormalize();
		Bone2PlanMatrix.Set(axeXB2Local.GetX(), axeXB2Local.GetY(), axeXB2Local.GetZ(), 0.0f,
							axeYB2Plan.GetX(), axeYB2Plan.GetY(), axeYB2Plan.GetZ(), 0.0f,
							axeZB2Plan.GetX(), axeZB2Plan.GetY(), axeZB2Plan.GetZ(), 0.0f,
							ikRootPos.GetX(), ikRootPos.GetY(), ikRootPos.GetZ(), 1.0f);

		//Compute the deformer bone2.
		defBone2.Mul(bone2InterTrans, InvBone2LocalMatrix);
		defBone2.SelfMul(Bone2PlanMatrix);

		//|--------------------------------------------------------|
		//| Compute the bone 3 re orientation with bone3 UpVector. |
		//|--------------------------------------------------------|

		//Compute the local transformation matrix for the bone3 plan.

		Matrix4x4 InvBone3LocalMatrix;
		Vector3D axeXB3Local, axeYB3Local, axeZB3Local;

		axeXB3Local.Sub(endIkChainPos, bone2Pos);
		axeXB3Local.SelfNormalize();

		Quaternion planRotB3;
		planRotB3.FromAxisAndAngle(axeXB3Local.GetX(), axeXB3Local.GetY(), axeXB3Local.GetZ(), planBone3TwistDeg);

		axeZB3Local.Set(bone2InterTrans.GetValue(2,0), bone2InterTrans.GetValue(2,1), bone2InterTrans.GetValue(2,2));
		axeZB3Local.SelfRotateByQuaternionFG(planRotB3);

		axeYB3Local.CrossProduct(axeXB3Local, axeZB3Local);
		axeYB3Local.SelfNormalize();
		InvBone3LocalMatrix.Set(axeXB3Local.GetX(), axeXB3Local.GetY(), axeXB3Local.GetZ(), 0.0f,
								axeYB3Local.GetX(), axeYB3Local.GetY(), axeYB3Local.GetZ(), 0.0f,
								axeZB3Local.GetX(), axeZB3Local.GetY(), axeZB3Local.GetZ(), 0.0f,
								bone2Pos.GetX(), bone2Pos.GetY(), bone2Pos.GetZ(), 1.0f);
		InvBone3LocalMatrix.SelfInvert();

		//Compute the matrix of the bone2 UpVector.
		Matrix4x4 Bone3PlanMatrix;
		Vector3D vRootB3UpV, axeYB3Plan, axeZB3Plan;
		vRootB3UpV.Sub(bone2Pos, ikBone3UpVPos);
		axeZB3Plan.CrossProduct(axeXB3Local, vRootB3UpV);
		axeYB3Plan.CrossProduct(axeXB3Local, axeZB3Plan);
		axeYB3Plan.SelfNormalize();
		axeZB3Plan.SelfNormalize();
		Bone3PlanMatrix.Set(axeXB3Local.GetX(), axeXB3Local.GetY(), axeXB3Local.GetZ(), 0.0f,
							axeYB3Plan.GetX(), axeYB3Plan.GetY(), axeYB3Plan.GetZ(), 0.0f,
							axeZB3Plan.GetX(), axeZB3Plan.GetY(), axeZB3Plan.GetZ(), 0.0f,
							bone2Pos.GetX(), bone2Pos.GetY(), bone2Pos.GetZ(), 1.0f);

		//Compute the defrome bone2.
		defBone3.Mul(bone3InterTrans, InvBone3LocalMatrix);
		defBone3.SelfMul(Bone3PlanMatrix);

		//|-------------------------------------------------------|
		//| Update the deformer bone world transformation matrix. |
		//|-------------------------------------------------------|
		Vector3D defBone2Pos, defBone3Pos;
		ExtractMatrixPosition(defBone2, defBone2Pos);
		ExtractMatrixPosition(defBone3, defBone3Pos);

		//Compute final world transform bone 1.
		Vector3D vB1B2, vAxeXB1, vAxeYB1, vAxeZB1;
		vB1B2.Sub(defBone2Pos, ikRootPos);
		vAxeZB1.Set(bone1InterTrans.GetValue(2,0), bone1InterTrans.GetValue(2,1), bone1InterTrans.GetValue(2,2));
		vAxeYB1.CrossProduct(vAxeZB1, vB1B2);
		vAxeZB1.CrossProduct(vB1B2, vAxeYB1);
		vAxeXB1.Normalize(vB1B2);
		vAxeXB1.SelfMulByFloat(globalBonesScale * bone1Scale * stretchScaleFactor);
		vAxeYB1.SelfNormalize();
		vAxeZB1.SelfNormalize();
		defBone1.Set(vAxeXB1.GetX(), vAxeXB1.GetY(), vAxeXB1.GetZ(), 0.0f,
						vAxeYB1.GetX(), vAxeYB1.GetY(), vAxeYB1.GetZ(), 0.0f,
						vAxeZB1.GetX(), vAxeZB1.GetY(), vAxeZB1.GetZ(), 0.0f,
						ikRootPos.GetX(), ikRootPos.GetY(), ikRootPos.GetZ(), 1.0f);

		//Compute final world transform bone 2.
		Vector3D vB2B3, vAxeXB2, vAxeYB2, vAxeZB2;
		vB2B3.Sub(defBone3Pos, defBone2Pos);
		vAxeZB2.Set(defBone2.GetValue(2,0), defBone2.GetValue(2,1), defBone2.GetValue(2,2));
		vAxeYB2.CrossProduct(vAxeZB2, vB2B3);
		vAxeZB2.CrossProduct(vB2B3, vAxeYB2);
		vAxeXB2.Normalize(vB2B3);
		vAxeXB2.SelfMulByFloat(globalBonesScale * bone2Scale * stretchScaleFactor);
		vAxeYB2.SelfNormalize();
		vAxeZB2.SelfNormalize();
		defBone2.Set(vAxeXB2.GetX(), vAxeXB2.GetY(), vAxeXB2.GetZ(), 0.0f,
						vAxeYB2.GetX(), vAxeYB2.GetY(), vAxeYB2.GetZ(), 0.0f,
						vAxeZB2.GetX(), vAxeZB2.GetY(), vAxeZB2.GetZ(), 0.0f,
						defBone2Pos.GetX(), defBone2Pos.GetY(), defBone2Pos.GetZ(), 1.0f);

		//Compute final world transform bone 3.
		Vector3D vB3E, vAxeXB3, vAxeYB3, vAxeZB3, vB3Pos;
		vB2B3.SelfNormalize();
		vB2B3.SelfMulByFloat(b2Length * stretchScaleFactor);
		vB3Pos.Add(defBone2Pos, vB2B3);

		vB3E.Sub(endIkChainPos, vB3Pos);
		vAxeZB3.Set(defBone3.GetValue(2,0), defBone3.GetValue(2,1), defBone3.GetValue(2,2));
		vAxeYB3.CrossProduct(vAxeZB3, vB3E);
		vAxeZB3.CrossProduct(vB3E, vAxeYB3);
		vAxeXB3.Normalize(vB3E);
		vAxeXB3.SelfMulByFloat(globalBonesScale * bone3Scale * stretchScaleFactor);
		vAxeYB3.SelfNormalize();
		vAxeZB3.SelfNormalize();
		defBone3.Set(vAxeXB3.GetX(), vAxeXB3.GetY(), vAxeXB3.GetZ(), 0.0f,
						vAxeYB3.GetX(), vAxeYB3.GetY(), vAxeYB3.GetZ(), 0.0f,
						vAxeZB3.GetX(), vAxeZB3.GetY(), vAxeZB3.GetZ(), 0.0f,
						vB3Pos.GetX(), vB3Pos.GetY(), vB3Pos.GetZ(), 1.0f);

		//Compute final world transform bone 4.
		Vector3D defBone4Pos;
		vB3E.SelfNormalize();
		vB3E.SelfMulByFloat(b3Length * stretchScaleFactor);
		defBone4Pos.Add(vB3E, vB3Pos);
		// Keep the rotation of the ik eff.
		defBone4.Set(ikEff.GetValue(0,0), ikEff.GetValue(0,1), ikEff.GetValue(0,2), 0.0f,
						ikEff.GetValue(1,0), ikEff.GetValue(1,1), ikEff.GetValue(1,2), 0.0f,
						ikEff.GetValue(2,0), ikEff.GetValue(2,1), ikEff.GetValue(2,2), 0.0f,
						defBone4Pos.GetX(), defBone4Pos.GetY(), defBone4Pos.GetZ(), 1.0f);

		//|------------------------|
		//| Compute the pin bones. |
		//|------------------------|
	}
	else if(pinBones == 2){
		//Compute the sub Effector.
		Vector3D PinedB3Pos;
		PinedB3Pos.Sub(ikPinBone3Pos, ikEffPos);
		PinedB3Pos.SelfNormalize();
		PinedB3Pos.SelfMulByFloat(b3Length * globalBonesScale * bone3Scale);
		PinedB3Pos.SelfAdd(ikEffPos);
		//Compute the sub chain.
		bool invertPlan = true;
		float stretchFactor;
		ComputePinedIK(invertPlan, planBone2TwistDeg, ikRootPos, PinedB3Pos, ikBone2UpVPos, b1Length, b2Length, stretchFactor, defBone1, defBone2, defBone3);
		//Update the scale of the bone 1 and bone 2 axe X.
		defBone1.SetScale(globalBonesScale * bone1Scale * stretchFactor, 1.0f, 1.0f);
		defBone2.SetScale(globalBonesScale * bone2Scale * stretchFactor, 1.0f, 1.0f);
		//Compute the bone 3 Transformation.
		Vector3D vB3E, vB3AxeX, vB3AxeY, vB3AxeZ, vBone3Pos;
		ExtractMatrixPosition(defBone3, vBone3Pos);
		vB3AxeZ.Set(defBone3.GetValue(2,0), defBone3.GetValue(2,1), defBone3.GetValue(2,2));
		vB3AxeX.Sub(ikEffPos, vBone3Pos);
		vB3AxeY.CrossProduct(vB3AxeZ, vB3AxeX);
		vB3AxeZ.CrossProduct(vB3AxeX, vB3AxeY);
		vB3AxeX.SelfNormalize();
		vB3AxeX.SelfMulByFloat(globalBonesScale * bone3Scale * stretchFactor);
		vB3AxeY.SelfNormalize();
		vB3AxeZ.SelfNormalize();

		defBone3.Set(vB3AxeX.GetX(), vB3AxeX.GetY(), vB3AxeX.GetZ(), 0.0f,
						vB3AxeY.GetX(), vB3AxeY.GetY(), vB3AxeY.GetZ(), 0.0f,
						vB3AxeZ.GetX(), vB3AxeZ.GetY(), vB3AxeZ.GetZ(), 0.0f,
						vBone3Pos.GetX(), vBone3Pos.GetY(), vBone3Pos.GetZ(), 1.0f);
		//Compute the bone 4 transformation.
		Vector3D vB4Pos, vB4AxeY, vB4AxeZ;
		vB4Pos.MulByFloat(vB3AxeX, b3Length);
		vB4Pos.SelfAdd(vBone3Pos);

		vB4AxeZ.Set(-ikEff.GetValue(0,0), -ikEff.GetValue(0,1), -ikEff.GetValue(0,2));
		vB4AxeY.CrossProduct(vB4AxeZ, vB3AxeX);
		vB4AxeZ.CrossProduct(vB3AxeX, vB4AxeY);
		vB4AxeY.SelfNormalize();
		vB4AxeZ.SelfNormalize();

		defBone4.Set(ikEff.GetValue(0,0), ikEff.GetValue(0,1), ikEff.GetValue(0,2), 0.0f,
						ikEff.GetValue(1,0), ikEff.GetValue(1,1), ikEff.GetValue(1,2), 0.0f,
						ikEff.GetValue(2,0), ikEff.GetValue(2,1), ikEff.GetValue(2,2), 0.0f,
						vB4Pos.GetX(), vB4Pos.GetY(), vB4Pos.GetZ(), 1.0f);

	}else if(pinBones == 1){
		//Compute the sub Root.
		Vector3D PinedB2Pos, vB1AxeX;
		vB1AxeX.Sub(ikPinBone2Pos, ikRootPos);
		vB1AxeX.SelfNormalize();
		PinedB2Pos.MulByFloat(vB1AxeX, b1Length);
		PinedB2Pos.SelfAdd(ikRootPos);
		//Compute the sub chain.
		bool invertPlan = false;
		float stretchFactor;
		ComputePinedIK(invertPlan, planBone3TwistDeg, PinedB2Pos, ikEffPos, ikBone3UpVPos, b2Length, b3Length, stretchFactor, defBone2, defBone3, defBone4);
		//Update the scale of the bone 2 and bone 3 axe X.
		defBone2.SetScale(globalBonesScale * bone2Scale * stretchFactor, 1.0f, 1.0f);
		defBone3.SetScale(globalBonesScale * bone3Scale * stretchFactor, 1.0f, 1.0f);

		//Compute the bone 1 Transformation.
		Vector3D vB1AxeY, vB1AxeZ;
		vB1AxeZ.Set(-ikRoot.GetValue(0,0), -ikRoot.GetValue(0,1), -ikRoot.GetValue(0,2));
		vB1AxeY.CrossProduct(vB1AxeZ, vB1AxeX);
		vB1AxeZ.CrossProduct(vB1AxeX, vB1AxeY);
		vB1AxeY.SelfNormalize();
		vB1AxeZ.SelfNormalize();
		vB1AxeX.SelfMulByFloat(globalBonesScale * bone1Scale * stretchFactor);

		defBone1.Set(vB1AxeX.GetX(), vB1AxeX.GetY(), vB1AxeX.GetZ(), 0.0f,
						vB1AxeY.GetX(), vB1AxeY.GetY(), vB1AxeY.GetZ(), 0.0f,
						vB1AxeZ.GetX(), vB1AxeZ.GetY(), vB1AxeZ.GetZ(), 0.0f,
						ikRootPos.GetX(), ikRootPos.GetY(), ikRootPos.GetZ(), 1.0f);
	}

	//|----------------------------------------|
	//| Symmetrize the ik bone transformation. |
	//|----------------------------------------|
	if(symMatrixSwitch){
		Matrix4x4 symMatrix(-1.0, 0.0f, 0.0f, 0.0f,
							0.0f, -1.0f, 0.0f, 0.0f, 
							0.0f, 0.0f, 1.0f, 0.0f, 
							0.0f, 0.0f, 0.0f, 1.0f);

		defBone1.Mul(symMatrix, defBone1);
		defBone2.Mul(symMatrix, defBone2);
		defBone3.Mul(symMatrix, defBone3);
		defBone4.Mul(symMatrix, defBone4);

	}

	//|---------------------------|
	//| Update the global scale.  |
	//|---------------------------|
	defBone1.SetScale(1.0f, globalScale, globalScale);
	defBone2.SetScale(1.0f, globalScale, globalScale);
	defBone3.SetScale(1.0f, globalScale, globalScale);
	defBone4.SetScale(globalScale, globalScale, globalScale);

	//|---------------------------|
	//| Blend Ik FK world Matrix. |
	//|---------------------------|
	if ( blendIKFK <= 0.0f ){
		defBone1 = fkBone1;
		defBone2 = fkBone2;
		defBone3 = fkBone3;
		defBone4 = fkBone4;
	}else if ( blendIKFK >= 1.0f){

	}else{
	/*
		Vector3D vAxeX(1.0f, 0.0f, 0.0f);
		Vector3D vAxeY(0.0f, 1.0f, 0.0f);
		Vector3D vAxeZ(0.0f, 0.0f, 1.0f);

		Vector3D ikB1AxeX, ikB2AxeX, ikB3AxeX;
		Vector3D fkB1AxeX, fkB2AxeX, fkB3AxeX;

		Vector3D axeB1Rot, axeB2Rot, axeB3Rot;
		float angleB1, angleB2, angleB3;

		Vector3D ikBone1Pos, ikBone2Pos, ikBone3Pos, ikBone4Pos;
		Vector3D fkBone1Pos, fkBone2Pos, fkBone3Pos, fkBone4Pos;

		quatRot;
		Quaternion ikQuatBone1, ikQuatBone2, ikQuatBone3, ikQuatBone4;
		Quaternion fkQuatBone1, fkQuatBone2, fkQuatBone3, fkQuatBone4;

		//Extract position and Rotation.
		ExtractMatrixPosRot(defBone1, ikBone1Pos, ikQuatBone1);
		ExtractMatrixPosRot(defBone2, ikBone2Pos, ikQuatBone2);
		ExtractMatrixPosRot(defBone3, ikBone3Pos, ikQuatBone3);
		ExtractMatrixPosRot(defBone4, ikBone4Pos, ikQuatBone4);

		ExtractMatrixPosRot(fkBone1, fkBone1Pos, fkQuatBone1);
		ExtractMatrixPosRot(fkBone2, fkBone2Pos, fkQuatBone2);
		ExtractMatrixPosRot(fkBone3, fkBone3Pos, fkQuatBone3);

		ikB1AxeX.RotateByQuaternionFG(vAxeX, ikQuatBone1);
		fkB1AxeX.RotateByQuaternionFG(vAxeX, fkQuatBone1);
		axeB1Rot.CrossProduct(ikB1AxeX, fkB1AxeX);
		axeB1Rot.SelfNormalize();
		angleB1 = ikB1AxeX.GetAngle(fkB1AxeX, true) * blendIKFK;
		quatRot.FromAxisAndAngle(axeB1Rot.GetX(), axeB1Rot.GetY(), axeB1Rot.GetZ(), angleB1);
		ikQuatBone1.Mul(quatRot, ikQuatBone1);

		ikB2AxeX.RotateByQuaternionFG(vAxeX, ikQuatBone2);
		fkB2AxeX.RotateByQuaternionFG(vAxeX, fkQuatBone2);
		axeB2Rot.CrossProduct(ikB2AxeX, fkB2AxeX);
		angleB2 = ikB2AxeX.GetAngle(fkB2AxeX, true);

		ikB3AxeX.RotateByQuaternionFG(vAxeX, ikQuatBone3);
		fkB3AxeX.RotateByQuaternionFG(vAxeX, fkQuatBone3);
		axeB3Rot.CrossProduct(ikB3AxeX, fkB3AxeX);
		angleB3 = ikB3AxeX.GetAngle(fkB3AxeX, true);

		InsertPosRotMatrix(ikBone1Pos, ikQuatBone1, defBone1);
		InsertPosRotMatrix(rslBone2Pos, rslQuatBone2, defBone2);
		InsertPosRotMatrix(rslBone3Pos, rslQuatBone3, defBone3);
		*/
	}
};


float Ik3BonesMember::ComputeCosinusLaw(float& in_SegA, float& in_SegB, float in_SegC){

	float angleRad = ((in_SegA * in_SegA) + (in_SegB * in_SegB) - (in_SegC * in_SegC)) / (2.0f * in_SegB * in_SegA);

	if(angleRad < -1.0f){
		angleRad = -1.0f;
	}
	else if(angleRad > 1.0f){
		angleRad = 1.0f;
	}

	return acosf(angleRad);
};

// GET METHODES.

Matrix4x4 Ik3BonesMember::GetDefBone1(){
	return defBone1;
};

Matrix4x4 Ik3BonesMember::GetDefBone2(){
	return defBone2;
};

Matrix4x4 Ik3BonesMember::GetDefBone3(){
	return defBone3;
};

Matrix4x4 Ik3BonesMember::GetDefBone4(){
	return defBone4;
};

// SET METHODES.

void Ik3BonesMember::SetGuidRoot(Matrix4x4& in_GuidRoot){
	guidRoot = in_GuidRoot;
};

void Ik3BonesMember::SetGuidBone2(Matrix4x4& in_GuidBone2){
	guidBone2 = in_GuidBone2;
};

void Ik3BonesMember::SetGuidBone3(Matrix4x4& in_GuidBone3){
	guidBone3 = in_GuidBone3;
};

void Ik3BonesMember::SetGuidEff(Matrix4x4& in_GuidEff){
	guidEff = in_GuidEff;
};
void Ik3BonesMember::SetGuidMainUpV(Matrix4x4& in_GuidMainUpV){
	guidMainUpV = in_GuidMainUpV;
};

void Ik3BonesMember::SetIkRoot(Matrix4x4& in_IkRoot){
	ikRoot = in_IkRoot;
};

void Ik3BonesMember::SetIkMainUpV(Matrix4x4& in_IkMainUpV){
	ikMainUpV = in_IkMainUpV;
};

void Ik3BonesMember::SetIkBone2UpV(Matrix4x4& in_IkBone2UpV){
	ikBone2UpV = in_IkBone2UpV;
};

void Ik3BonesMember::SetIkBone3UpV(Matrix4x4& in_IkBone3UpV){
	ikBone3UpV = in_IkBone3UpV;
};

void Ik3BonesMember::SetIkEff(Matrix4x4& in_IkEff){
	ikEff = in_IkEff;
};

void Ik3BonesMember::SetIkPinBone2(Matrix4x4& in_IkPinBone2){
	ikPinBone2 = in_IkPinBone2;
};

void Ik3BonesMember::SetIkPinBone3(Matrix4x4& in_IkPinBone3){
	ikPinBone3 = in_IkPinBone3;
};

void Ik3BonesMember::SetIkRotationOffset(float& in_IkRotationOffset){
	ikRotationOffset = in_IkRotationOffset;
};

void Ik3BonesMember::SetFkBone1(Matrix4x4& in_FkBone1){
	fkBone1 = in_FkBone1;
};

void Ik3BonesMember::SetFkBone2(Matrix4x4& in_FkBone2){
	fkBone2 = in_FkBone2;
};

void Ik3BonesMember::SetFkBone3(Matrix4x4& in_FkBone3){
	fkBone3 = in_FkBone3;
};

void Ik3BonesMember::SetFkBone4(Matrix4x4& in_FkBone4){
	fkBone3 = in_FkBone4;
};

void Ik3BonesMember::SetPinBones(int& in_PinBones){
	pinBones = in_PinBones;
};

void Ik3BonesMember::SetSymMatrixSwitch(int& in_SymMatrixSwitch){
	symMatrixSwitch = in_SymMatrixSwitch;
};

void Ik3BonesMember::SetBlendIKFK(float& in_BlendIKFK){
	blendIKFK = in_BlendIKFK;
};

void Ik3BonesMember::SetPlanTwist(float& in_PlanTwist){
	planTwist = in_PlanTwist;
}

void Ik3BonesMember::SetPlanBone2Twist(float& in_PlanBone2Twist){
	planBone2Twist = in_PlanBone2Twist;
}

void Ik3BonesMember::SetPlanBone3Twist(float& in_PlanBone3Twist){
	planBone3Twist = in_PlanBone3Twist;
}

void Ik3BonesMember::SetUseSoftIK(int& in_UseSoftIK){
	useSoftIK = in_UseSoftIK;
}

void Ik3BonesMember::SetStartSoftIK(float& in_StartSoftIK){
	startSoftIK = in_StartSoftIK;
}

void Ik3BonesMember::SetGlobalBonesScale(float& in_GlobalBonesScale){
	globalBonesScale = in_GlobalBonesScale;
}

void Ik3BonesMember::SetBone1Scale(float& in_Bone1Scale){
	bone1Scale = in_Bone1Scale;
}

void Ik3BonesMember::SetBone2Scale(float& in_Bone2Scale){
	bone2Scale = in_Bone2Scale;
}

void Ik3BonesMember::SetBone3Scale(float& in_Bone3Scale){
	bone3Scale = in_Bone3Scale;
}

void Ik3BonesMember::SetUseStretch(int& in_UseStretch){
	useStretch = in_UseStretch;
}