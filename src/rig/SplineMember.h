/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file SplineMember.h
@author Guillaume Baratte
@date 2016-03-27
@brief Define the SplineMember class.
This create a spline that we can use to create spine for characters.
It's create n Deformers drive by n Controlers.
*/

#ifndef __SPLINE_MEMBER__
#define __SPLINE_MEMBER__

#include "../core/BezierCurveNPoints.h"
#include "../core/FitBezierCurveNPoints.h"
#include "../core/DataArray.h"
#include "../core/Matrix4x4.h"
#include "../core/Matrix3x3.h"
#include "../core/Quaternion.h"
#include "../core/Vector3D.h"

class SplineMember : public BezierCurveNPoints
{
	public:
		//CONSTRUCTOR.
		/*! @brief Primary Constructor.*/
		SplineMember ();

		//DESTRUCTOR.
		/*! @brief Destructor.*/
		~SplineMember ();

		//COMPUTES METHODES.
		/*! @brief Compute the position of the deformer. Need to be use first. */
		void ComputeDeformersPosition ();
		/*! @brief Compute the rotation of the deformer. Need to be use second. */
		void ComputeDeformersOrientation ();
		/*! @brief Compute the scale of the deformer. Need to be use third. */
		void ComputeDeformerScale ();
		/*! @brief Compute the transformation deformer with the hierarchie type.*/
		void ComputeHierarchie ();
		/*! @brief Extract the rotation from matrix4x4 as Quaterion.
		*@param[in] in_Matrix The Matrix from extract.
		*@param[out] out_Quat The Quaternion to store the rotation.
		*/
		void ExtractRotationFromMatrix (Matrix4x4& in_Matrix, Quaternion& out_Quat);

		//SET METHODES.
		/*! @brief Set the parameters of the spline member.
		*@param in_ArrayControlers The array of matrix4x4 of the point controler of the spline.
		*@param in_FilterAxeAlign The array of float for filter the alignement of the tangent axe on the curve.
		*@param in_FilterRotationBlend The array of float for filter the rotation blend between the start and end controler.
		*@param in_FilterScaleAxe1 The array of float for filter the scale of the axe 1 when squatch is active.
		*@param in_FilterScaleAxe2 The array of float for filter the scale of the axe 2 when squatch is active.
		*@param in_NumberDeformers The number of deformer.
		*@param in_NormalizePosition Activation the normalization of the deformer position on the curve.
		*@param in_NormalizeIteration The iteration to compute the normalization of the deformer position.
		*@param in_AxeAlignDeformers Select the axe to align on the curve tangente. 0 -> Axe X. 1 -> Axe Y. 2 -> Axe Z.
		*@param in_UseAddRoll Use the start and end roll angle for infinit torsion.
		*@param in_StartAddRollAngle The angle to add for the start of the spline in degrees.
		*@param in_EndAddRollAngle The angle to add for the end of the spline in degrees.
		*@param in_StretchType Select the type of the stretch for the curve. 0 -> No Stretch. 1 -> Limited Stretch. 2 -> Free Stretch.
		*@param in_RestLength Define the rest length of the spline.
		*@param in_MaxLengthMulti Multiply the rest length to define the max stretch length. When Limited Stretch is Use.
		*@param in_MinLengthMulti Multiply the rest length to define the min stretch length. When Limited Stretch is Use.
		*@param in_UseSquatch Active the squatch on the deformers when use Limited Stretch or Free Stretch.
		*@param in_MaxStretchAxe1Scale The scale on axe 1 when we are at the max stretch.
		*@param in_MaxStretchAxe2Scale The scale on axe 2 when we are at the max stretch.
		*@param in_MinStretchAxe1Scale The scale on axe 1 when we are at the min stretch.
		*@param in_MinStretchAxe2Scale The scale on axe 2 when we are at the min stretch.
		*@param in_SplStartRange The value of the start of the distribution of the deformer on the curve. 0 to 1.
		*@param in_SplEndRange The value of the end of the distribution of the deformer on the curve. 0 to 1.
		*@param in_SplMoveRange The value for move the distribution of the deformer on the curve. -1 to 1.
		*/
		void Set (	DataArray<Matrix4x4> in_ArrayControlers,
							DataArray<float> in_FilterAxeAlign,
							DataArray<float> in_FilterRotationBlend,
							DataArray<float> in_FilterScaleAxe1,
							DataArray<float> in_FilterScaleAxe2,
							int in_NumberDeformers,
							bool in_NormalizePosition,
							int in_NormalizeIteration,
							int in_AxeAlignDeformers,
							bool in_UseAddRoll,
							float in_StartAddRollAngle,
							float in_EndAddRollAngle,
							int in_StretchType,
							float in_RestLength,
							float in_MaxLengthMulti,
							float in_MinLengthMulti,
							bool in_UseSquatch,
							float in_MaxStretchAxe1Scale,
							float in_MaxStretchAxe2Scale,
							float in_MinStretchAxe1Scale,
							float in_MinStretchAxe2Scale,
							float in_SplStartRange,
							float in_SplEndRange,
							float in_SplMoveRange,
							int in_TangentType,
							int in_ControlerScaleType,
							int in_HierarchieType,
							int in_UseParentingMatrix,
							Matrix4x4 in_ParentInvertMatrix,
							int in_UseOnlyAddRoll,
							int in_RotationType,
							int in_CurveType,
							float in_GlobalScale,
							int& in_UseUserTangent,
							DataArray<Matrix4x4>& in_ArrayUserTangents);

		/*! @brief Set the array of matrix4x4 of the point controler of the spline.
		*@param in_ArrayControlers The array of matrix4x4 of the point controler of the spline.
		*/
		void SetArrayControlers (DataArray<Matrix4x4> in_ArrayControlers);
		/*! @brief Set the array of float for filter the scale of the axe 1 when squatch is active.
		*@param in_FilterScaleAxe1 The array of float for filter the scale of the axe 1 when squatch is active.
		*/
		void SetFilterScaleAxe1 (DataArray<float> in_FilterScaleAxe1);
		/*! @brief Set the array of float for filter the scale of the axe 2 when squatch is active.
		*@param in_FilterScaleAxe2 The array of float for filter the scale of the axe 2 when squatch is active.
		*/
		void SetFilterScaleAxe2 (DataArray<float> in_FilterScaleAxe2);
		/*! @brief Set the array of float for filter the rotation blend between the start and end controler.
		*@param in_FilterRotationBlend The array of float for filter the rotation blend between the start and end controler.
		*/
		void SetFilterRotationBlend (DataArray<float> in_FilterRotationBlend);
		/*! @brief Set the array of float for filter the alignement of the tangent axe on the curve.
		*@param in_FilterAxeAlign The array of float for filter the alignement of the tangent axe on the curve.
		*/
		void setFilterAxeAlign (DataArray<float> in_FilterAxeAlign);
		/*! @brief Set the rest length of the spline.
		*@param in_RestLength Define the rest length of the spline.
		*/
		void SetRestLength ( float in_RestLength );
		/*! @brief Set the use the start and end roll angle for infinit torsion.
		*@param in_UseAddRoll Use the start and end roll angle for infinit torsion.
		*/
		void SetUseAddRoll ( bool in_UseAddRoll );
		/*! @brief Set the angle to add for the start of the spline in degrees.
		*@param in_StartAddRollAngle The angle to add for the start of the spline in degrees.
		*/
		void SetStartAddRollAngle ( float in_StartAddRollAngle );
		void SetEndAddRollAngle ( float in_EndAddRollAngle );
		void SetStretchType ( int in_StretchType );
		void SetMaxLengthMulti ( float in_MaxLengthMulti );
		void SetMinLengthMulti ( float in_MinLengthMulti );
		void SetMaxStretchAxe1Scale ( float in_MaxStretchAxe1Scale );
		void SetMaxStretchAxe2Scale ( float in_MaxStretchAxe2Scale );
		void SetMinStretchAxe1Scale ( float in_MinStretchAxe1Scale );
		void SetMinStretchAxe2Scale ( float in_MinStretchAxe2Scale );			
		void SetAxeAlignMembers ( int in_AxeAlignDeformers );
		void SetNormalizePosition ( bool in_NormalizePosition );
		void SetNormalizeIteration ( int in_NormalizeIteration );
		void SetSplStartRange ( float in_SplStartRange );
		void SetSplEndRange ( float in_SplEndRange );
		void SetSplMoveRange ( float in_SplMoveRange );
		void SetNumberDeformers ( int in_NumberDeformers );
		void SetUseSquatch ( bool in_UseSquatch );
		void SetTangentType ( int in_TangentType );
		void SetControlerScaleType ( int in_ControlerScaleType );
		void SetHierarchieType ( int in_HierarchieType );
		void SetUseParentingMatrix ( int in_UseParentingMatrix );
		void SetParentInvertMatrix ( Matrix4x4 in_ParentInvertMatrix );
		void SetUseOnlyAddRoll ( int in_UseOnlyAddRoll );
		void SetRotationType(int in_RotationType);

		//GET METHODES.
		Matrix4x4	GetDefMatrixAt ( int in_Index );
		Quaternion	GetDefRotQuatAt ( int in_Index );
		Vector3D	GetDefPositionAt ( int in_Index );
		Vector3D	GetDefScaleAt ( int in_Index, int in_ExtractFromMAtrix);
		Vector3D	GetDefRotEulerAt ( int in_Index, int in_Radians );

		DataArray<Matrix4x4> GetArrayDeformers ();

		float GetRestLength ();
		bool GetUseAddRoll ();
		float GetStartAddRollAngle ();
		float GetEndAddRollAngle ();
		int GetStretchType ();
		float GetMaxLengthMulti ();
		float GetMinLengthMulti ();
		float GetMaxStretchAxe1Scale ();
		float GetMaxStretchAxe2Scale ();
		float GetMinStretchAxe1Scale ();
		float GetMinStretchAxe2Scale ();
		int GetAxeAlignDeformers ();
		bool GetNormalizePosition ();
		int GetNormalizeIteration ();
		float GetSplStartRange ();
		float GetSplEndRange ();
		float GetSplMoveRange ();
		int GetNumberDeformers ();
		bool GetUseSquatch ();
		int GetRotationType ();

	private:
		DataArray<Matrix4x4>	m_ArrayControlers;		/*! The array of the controlers of the spline. */
		DataArray<Matrix4x4>	m_ArrayUserTangents;	/*! The array of the controllers use as curve tangents.*/
		DataArray<float>		m_FilterScaleAxe1;		/*! The array of value for filter the scale on Axe 1.*/
		DataArray<float>		m_FilterScaleAxe2;		/*! The array of value for filter the scale on Axe 2.*/
		DataArray<float>		m_FilterRotationBlend;	/*! The array of value for filter the rotation blend. */
		DataArray<float>		m_FilterAxeAlign;		/*! The array of value for filter the axe tangent align.*/
		int						m_NumberDeformers;		/*! The number of the spline deformer.*/
		bool					m_NormalizePosition;	/*! Compute the normalization of the bezier points.*/
		int						m_NormalizeIteration;	/*! The number of iteration for the normalizatoin.*/
		float					m_RestLength;			/*! The length of the spline at the rest pose. */
		float					m_SplStartRange;		/*! The local spline start range. */
		float					m_SplEndRange;			/*! The local spline end range. */
		float					m_SplMoveRange;			/*! The local spline move range. */
		bool					m_UseAddRoll;			/*! Use the additionnal roll angle for the start and the end of the spline. */
		float					m_StartAddRollAngle; 	/*! The angle to add at the start of the spline. */
		float					m_EndAddRollAngle;		/*! The angle to add at the end of the spline. */
		int						m_StretchType;			/*! The type of the stretch of the spine.*/
		float					m_MaxLengthMulti;		/*! The multiply value for limited max stretch. */
		float					m_MinLengthMulti;		/*! The multiply value for the limited min stretch. */
		bool					m_UseSquatch;			/*! Use the squatch to scale the axe1 and axe2 of the deformers. */
		float					m_MaxStretchAxe1Scale; 	/*! The scale of the axe 1 at the maximum stretch length. */
		float					m_MaxStretchAxe2Scale; 	/*! The scale of the axe 2 at the maximum stretch length. */
		float					m_MinStretchAxe1Scale; 	/*! The scale of the axe 1 at the minimum stretch length. */
		float					m_MinStretchAxe2Scale; 	/*! The scale of the axe 2 at the minimum stretch length. */
		int						m_AxeAlignDeformers;	/*! The axe to align vertebrae on the spine curve.*/
		int						m_TangentType;			/*! Select the computation for the deformer tangent. 0->PointToPoint. 1->True Tangent.*/
		int						m_ControlerScaleType;	/*! Select the influence of the scale controler on the scale deformer. 0->None. 1->FirstAndLast. 2->All Controler.*/
		int						m_HierarchieType;		/*! Select the out transformation for parallel or serial hierarchie for deformer. 0->Parallel. 1->Serial.*/
		int						m_UseParentingMatrix;	/*! Use the invert parenting matrix.*/
		Matrix4x4				m_ParentInvertMatrix;	/*! The invert matrix for of the parent of the deformers hierarchie.*/
		int						m_UseOnlyAddRoll;		/*! Use only the add roll for the torsion blend. */
		int						m_RotationType;			/*! Select the type of the rotation. */
		int 					m_CurveType;			/*! Select the type of the curve, Bezier or Fit Bezier.*/
		float 					m_GlobalScale;			/*! The global scale factor. */
		int 					m_UseUserTangents;		/*! Use the controller user tangent.*/

		static const int		m_MemoryBaseSize;

		DataArray<Matrix4x4>	m_ArrayDefMatrix;		/*! The array of matrix for deformer transformation.*/
		DataArray<Vector3D>		m_ArrayDefScale;		/*! The array of vector3d for deformer scale for serial hierarchie.*/
		FitBezierCurveNPoints 	m_FitBezierCurve;
};


#endif