/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file SplineMember.cpp
@author Guillaume Baratte
@date 2016-03-27
*/

#include "SplineMember.h"
#include <iostream>
using namespace std;

const int SplineMember::m_MemoryBaseSize = 100;

//CONSTRUCTOR.
SplineMember::SplineMember () : BezierCurveNPoints ()
{
	m_ArrayControlers.SetSizeAndClear(4);
	m_ArrayUserTangents.SetSizeAndClear(10);
	m_NormalizePosition = false;
	m_NormalizeIteration = 10;
	m_SplStartRange = 0.0f;
	m_SplEndRange = 1.0f;
	m_SplMoveRange = 0.0f;
	m_ArrayDefMatrix.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayDefScale.SetSizeAndClear(m_MemoryBaseSize);
	m_FilterAxeAlign.SetSizeAndClear(m_MemoryBaseSize);
	m_FilterRotationBlend.SetSizeAndClear(m_MemoryBaseSize);
	m_FilterScaleAxe1.SetSizeAndClear(m_MemoryBaseSize);
	m_FilterScaleAxe2.SetSizeAndClear(m_MemoryBaseSize);
	m_AxeAlignDeformers = 0;
	m_EndAddRollAngle = 0.0f;
	m_MaxLengthMulti = 2.0f;
	m_MaxStretchAxe1Scale = 0.5f;
	m_MaxStretchAxe2Scale = 0.5f;
	m_UseSquatch = false;
	m_MinLengthMulti = 0.5f;
	m_MinStretchAxe1Scale = 2.0f;
	m_MinStretchAxe2Scale = 2.0f;
	m_RestLength = 1.0f;
	m_StartAddRollAngle = 0.0f;
	m_StretchType = 0;
	m_UseAddRoll = false;
	m_HierarchieType = 0;
	m_TangentType = 0;
	m_ControlerScaleType = 0;
	m_RotationType = 0;
	m_CurveType = 0;
	m_GlobalScale = 1.0f;
	m_UseUserTangents = 0;
}

//DESTRUCTOR.
SplineMember::~SplineMember ()
{

}

//COMPUTE METHODS.
void SplineMember::ComputeDeformersPosition ()
{
	//Get the position of the controlers for the BezierCurve Computation.
	int controlersNumber = m_ArrayControlers.GetSize();
	DataArray<Vector3D> arrayControlPoints ( controlersNumber );
	float pX, pY, pZ = 0.0f;
	for (int iPnt=0; iPnt<controlersNumber; iPnt++ )
	{
		//Extract the position from matrix.
		m_ArrayControlers[iPnt].GetPosition(pX, pY, pZ);
		//Set the position of the current array point controler.
		arrayControlPoints[iPnt].Set(pX,pY,pZ);
	}

	// Select which type of curve we want to use.
	if(m_CurveType == 0){
		//Set the array of the controler point for the BezierCurve.
		SetPointsControl ( arrayControlPoints );
		//Set the range of the BezierCurve.
		SetRange ( m_SplStartRange, m_SplEndRange, m_SplMoveRange);
		//Seconde compute of the point position with the stretch limitation.
		ComputeArrayPointsPosition();

		//If normalize spline is need.
		if ( m_NormalizePosition )
		{
			//Compute the length of the spline for the normalization.
			ComputeCurveLength();
			//Normalize the spline point position.
			NormalizeArrayPointsPosition ( m_NormalizeIteration );
		}
		else
		{
			if (  m_StretchType == 0 || m_StretchType == 1 )
			{
				//Compute the length of the spline for the normalization.
				ComputeCurveLength();
			}
		}

		//Compute the stretch limitation.
		float stretchScaleFactor = 1.0f;
		if ( m_StretchType == 0 )
		{
			stretchScaleFactor = m_RestLength * m_GlobalScale / BezierCurveNPoints::GetCurveLength();
		}
		else if ( m_StretchType == 1 )
		{
			float sclRestLength = m_RestLength * m_GlobalScale;
			float maxLength = sclRestLength * m_MaxLengthMulti;
			float minLength = sclRestLength * m_MinLengthMulti;
			if ( BezierCurveNPoints::GetCurveLength() >= maxLength )
			{
				stretchScaleFactor = maxLength / BezierCurveNPoints::GetCurveLength();
			}
			else if ( BezierCurveNPoints::GetCurveLength() <= minLength )
			{
				stretchScaleFactor = minLength / BezierCurveNPoints::GetCurveLength();
			}
		}

		//Compute the new position of the bezier points.
		DataArray<Vector3D>	stertchedPointPosition(m_NumberDeformers);
		Vector3D			scaleVector;
		for ( int iDef=0; iDef<m_NumberDeformers; iDef++ )
		{
			if ( iDef == 0 )
			{
				stertchedPointPosition[iDef] = 	GetPointPositionAt(iDef);
			}
			else
			{
				scaleVector.Sub(GetPointPositionAt(iDef), GetPointPositionAt(iDef-1));
				scaleVector.SelfMulByFloat(stretchScaleFactor);
				scaleVector.SelfAdd(stertchedPointPosition[iDef-1]);
				stertchedPointPosition[iDef] = scaleVector;
			}
		}
		//Set the position of the deformer array.
		for (int iDef=0; iDef<m_NumberDeformers; iDef++)
		{
			m_ArrayDefMatrix[iDef].SetPosition(	stertchedPointPosition[iDef].GetX(),
												stertchedPointPosition[iDef].GetY(),
												stertchedPointPosition[iDef].GetZ() );
		}
	}else if(m_CurveType == 1){
		DataArray<Vector3D> arrayUserTangentsPosition(m_ArrayUserTangents.GetSize());
		for (int iPnt=0; iPnt<m_ArrayUserTangents.GetSize(); iPnt++ )
		{
			//Extract the position from matrix.
			m_ArrayUserTangents[iPnt].GetPosition(pX, pY, pZ);
			//Set the position of the current array point controler.
			arrayUserTangentsPosition[iPnt].Set(pX,pY,pZ);
		}

		m_FitBezierCurve.Set(m_NumberDeformers, arrayControlPoints, m_SplStartRange, m_SplEndRange, m_SplMoveRange, m_UseUserTangents, arrayUserTangentsPosition);
		
		// Compute the curve.
		if(m_UseUserTangents == 1){
			m_FitBezierCurve.AddUserTangentsToArrayAllPoints();
		}else{
			m_FitBezierCurve.ComputeTangents();
		}
		m_FitBezierCurve.ComputeAllFitT();
		m_FitBezierCurve.ComputeAllFitPoint();
		Vector3D defPosition;
		//Set the position of the deformer array.
		for (int iDef=0; iDef<m_NumberDeformers; iDef++)
		{
			defPosition = m_FitBezierCurve.GetCurvePointsAt(iDef);

			m_ArrayDefMatrix[iDef].SetPosition(	defPosition.GetX(),
												defPosition.GetY(),
												defPosition.GetZ() );

		}		
	}	
}

void SplineMember::ExtractRotationFromMatrix (Matrix4x4& in_Matrix, Quaternion& out_Quat){

	// Init local data.
	float xx, xy, xz, yx, yy, yz, zx, zy, zz = 0.0f;
	Vector3D axeX, axeY, axeZ;
	Matrix3x3 convertMat;

	// Get the value of each component of the rotation matrix.
	in_Matrix.GetMatrix3x3(xx, xy, xz, yx, yy, yz, zx, zy, zz);
	// Remove the scale.
	axeX.Set(xx, xy, xz);
	axeY.Set(yx, yy, yz);
	axeZ.Set(zx, zy, zz);
	axeX.SelfNormalize();
	axeY.SelfNormalize();
	axeZ.SelfNormalize();
	// Update the convertion matrix3x3.
	convertMat.Set(axeX.GetX(), axeX.GetY(), axeX.GetZ(),
					axeY.GetX(), axeY.GetY(), axeY.GetZ(),
					axeZ.GetX(), axeZ.GetY(), axeZ.GetZ());
	// Update the out quaternion.
	out_Quat.FromMatrix3x3(convertMat);
}

void SplineMember::ComputeDeformersOrientation (){
	Quaternion 		quatTemp;

	//Get the first controler.
	Matrix4x4 transFirstCtrl = m_ArrayControlers[0];
	//Get the last controler.
	Matrix4x4 transLastCtrl = m_ArrayControlers[m_ArrayControlers.GetSize()-1];

	//Convertion float.
	float Xx,Xy,Xz,Yx,Yy,Yz,Zx,Zy,Zz;
	Vector3D vAxeX, vAxeY, vAxeZ;
	Matrix3x3 oriConvert;

	//Get the first rotation quaternion.
	Quaternion quatFistCtrl;
	ExtractRotationFromMatrix(transFirstCtrl, quatFistCtrl);

	//Get the last rotatoin quaternion.
	Quaternion quatLastCtrl;
	ExtractRotationFromMatrix(transLastCtrl, quatLastCtrl);

	//Compute the Axe Reference Align.
	Vector3D refAxeToAlign;
	if ( m_AxeAlignDeformers == 0 ) // Axe X.
	{
		refAxeToAlign.Set ( 1.0f, 0.0f, 0.0f ) ;
	}
	else if ( m_AxeAlignDeformers == 1 ) // Axe Y.
	{
		refAxeToAlign.Set ( 0.0f, 1.0f, 0.0f );	
	}
	else if ( m_AxeAlignDeformers == 2 ) // Axe Z.
	{
		refAxeToAlign.Set ( 0.0f, 0.0f, 1.0f );		
	}

	//Temps Data for the loop computation.

	Quaternion		quatBlend;
	Quaternion		quatCorrectif;
	Quaternion		quatAddRoll;
	Matrix3x3		mat3Blend;
	Vector3D		vRefAlign;
	Vector3D		vTangent, vTangent1, vTangent2;
	Vector3D		vPos0, vPos1, vPos2;
	Vector3D		vCrossVector;
	float			angle;

	//Loop over the array of the deformers.
	for ( int iDef=0; iDef<m_NumberDeformers; iDef++ ){

		if(m_RotationType == 0){
			//Compute the current quaternion blend.
			if (m_UseOnlyAddRoll == 1 ){
				if ( iDef == 0 ){
					quatBlend = quatFistCtrl;
				}
			}
			else if (m_UseOnlyAddRoll == 0){
				quatBlend.Slerp ( quatFistCtrl, quatLastCtrl, m_FilterRotationBlend[iDef], true);
			}

		}else if(m_RotationType == 1){
			// Compute the defromer u value.
			float deformerUValue = (float)iDef / (float)(m_NumberDeformers-1);

			if(deformerUValue == 0.0f){
				Matrix4x4 conMat;
				conMat = m_ArrayControlers[0];
				ExtractRotationFromMatrix(conMat, quatBlend); 
			}
			else if(deformerUValue == 1.0f){
				Matrix4x4 conMat;
				conMat = m_ArrayControlers[m_ArrayControlers.GetSize()-1];
				ExtractRotationFromMatrix(conMat, quatBlend);
			}
			else{
				// Find between which controller the deformer is.
				int minControllerID = 0;
				int maxControllerID = m_ArrayControlers.GetSize()-1;
				float minControllerU = 0.0f;
				float maxControllerU = 1.0f;
				float minDist = 1000000.0f;
				float maxDist = 1000000.0f;
				// Loop over the controllers.
				for(int iCon=0; iCon<m_ArrayControlers.GetSize(); iCon++){
					// Compute controller U value.
					float controllerUValue = (float)iCon / (float)(m_ArrayControlers.GetSize()-1);
					// Check min distance.
					if(deformerUValue >= controllerUValue){
						// Compute locale min distance.
						float localMinDist = deformerUValue - controllerUValue; 
						if(minDist > localMinDist){
							minDist = localMinDist;
							minControllerID = iCon;
							minControllerU = controllerUValue;
						}
					}
					// Check max distance.
					else if(controllerUValue > deformerUValue){
						// Compute locale max distance.
						float localMaxDist = controllerUValue - deformerUValue;
						if(maxDist > localMaxDist){
							maxDist = localMaxDist;
							maxControllerID = iCon;
							maxControllerU = controllerUValue;
						}
					}
				}
				
				// Compute the interpolated rotation between the min and max controller.
				// Compute the blend factor for rotation.
				float blendRotation = (deformerUValue - minControllerU) / (maxControllerU - minControllerU);

				//Get the controllers rotation.
				Matrix4x4 minMat, maxMat;
				Quaternion minQuat, maxQuat;
				minMat = m_ArrayControlers[minControllerID];
				maxMat = m_ArrayControlers[maxControllerID];
				ExtractRotationFromMatrix(minMat, minQuat);
				ExtractRotationFromMatrix(maxMat, maxQuat);

				// Blend the Quaternion controller rotation.
				quatBlend.Slerp(minQuat, maxQuat, blendRotation, true);
			}			
		}

		//Align the reference axis to the quat Blend.
		vRefAlign.RotateByQuaternionFG( refAxeToAlign, quatBlend );

		if ( iDef == 0 ){
			//Get the next deformer position.
			vPos1 = GetDefPositionAt(iDef+1);
			//Get the current deformer position.
			vPos0 = GetDefPositionAt(iDef);
			//Compute the normalized tangent vector between the two points.
			vTangent.Sub(vPos1, vPos0);
			vTangent.SelfNormalize();
		}
		else if ( iDef < (m_NumberDeformers-1) ){
			if ( m_TangentType == 0 ){
				//Get the next deformer position.
				vPos1 = GetDefPositionAt(iDef+1);
				//Get the current deformer position.
				vPos0 = GetDefPositionAt(iDef);
				//Compute the normalized tangent vector between the two points.
				vTangent.Sub(vPos1, vPos0);
				vTangent.SelfNormalize();
			}
			else if ( m_TangentType == 1 ){
				//Get the previous deformer position.
				vPos2 = GetDefPositionAt(iDef-1);
				//Get the next deformer position.
				vPos1 = GetDefPositionAt(iDef+1);
				//Get the current deformer position.
				vPos0 = GetDefPositionAt(iDef);
				//Compute the vector previous -> current.
				vTangent1.Sub ( vPos0, vPos2 );
				//Compute the vector current -> next.
				vTangent2.Sub ( vPos1, vPos0 );
				//Compute the average tangent.
				vTangent.Add ( vTangent1, vTangent2);
				vTangent.SelfMulByFloat ( 0.5 );
				vTangent.SelfNormalize();
			}
		}
		else if ( iDef == (m_NumberDeformers-1) ){
			if ( m_TangentType == 1 ){
				//Get the previous deformer position.
				vPos1 = GetDefPositionAt(iDef-1);
				//Get the current deformer position.
				vPos0 = GetDefPositionAt(iDef);
				//Compute the normalized tangent vector between the two points.
				vTangent.Sub(vPos0, vPos1);
				vTangent.SelfNormalize();
			}
		}

		//Compute the angle in radians between the reference axis and the tangent.
		angle = acos(vRefAlign.DotProduct(vTangent));

		//Filter the tangent alignment angle.
		angle = angle * m_FilterAxeAlign[iDef];

		//If the angle is equal to 0 we can not compute the crossVector.
		//In this case we use the blend quaternion.
		if ( angle >= 0.000001f ){
			//Compute the normalized cross product vector between the reference axis and the tangent.
			vCrossVector.CrossProduct(vRefAlign, vTangent);
			vCrossVector.SelfNormalize();
			//Convert the axis and angle to quaternion.
			quatCorrectif.FromAxisAndAngle ( vCrossVector.GetX(), vCrossVector.GetY(), vCrossVector.GetZ(), angle );
			//Multiply the quat patch with the quat blend to align the quat blend orientation to the reference axis.
			quatCorrectif.SelfMul(quatBlend);
		}else{
			quatCorrectif = quatBlend;
		}

		if ( m_UseOnlyAddRoll == 1 ){
			quatBlend = quatCorrectif;
		}

		//Add the roll rotation.
		if ( m_UseAddRoll ){
			float addRollAngle = ( m_StartAddRollAngle * (1 - m_FilterRotationBlend[iDef]) + m_EndAddRollAngle * m_FilterRotationBlend[iDef] ) * 3.14f / 180.0f;
			vRefAlign.RotateByQuaternionFG(refAxeToAlign, quatCorrectif);
			quatAddRoll.FromAxisAndAngle (vRefAlign.GetX(), vRefAlign.GetY(), vRefAlign.GetZ(), addRollAngle );

			quatCorrectif.Mul(quatAddRoll, quatCorrectif);
		}

		//Convert the quaternion to matrix33.
		mat3Blend = quatCorrectif.ToMatrix3x3();
		//Copy the matrix in the deformers transformation.
		m_ArrayDefMatrix[iDef].SetFromMatrix3x3 (	mat3Blend.GetValue(0,0), mat3Blend.GetValue(0,1), mat3Blend.GetValue(0,2),
													mat3Blend.GetValue(1,0), mat3Blend.GetValue(1,1), mat3Blend.GetValue(1,2),
													mat3Blend.GetValue(2,0), mat3Blend.GetValue(2,1), mat3Blend.GetValue(2,2) );
	}	
};

void SplineMember::ComputeDeformerScale ()
{
	DataArray<Vector3D> ArrayScaleDeformer (m_ArrayDefMatrix.GetSize());

	if ( m_ControlerScaleType == 0 )
	{
		for ( int iDef=0; iDef<m_NumberDeformers; iDef++)
		{
			ArrayScaleDeformer[iDef].Set (m_GlobalScale, m_GlobalScale, m_GlobalScale);
		}
	}
	else if ( m_ControlerScaleType == 1 )
	{
		Vector3D scaleFirstControler, scaleLastControler;
		float sclX, sclY, sclZ;

		m_ArrayControlers[0].GetScale(sclX, sclY, sclZ);
		scaleFirstControler.Set(sclX, sclY, sclZ);

		m_ArrayControlers[m_ArrayControlers.GetSize()-1].GetScale(sclX, sclY, sclZ);
		scaleLastControler.Set(sclX, sclY, sclZ);

		float cUValue;
		for ( int iDef=0; iDef<m_NumberDeformers; iDef++)
		{
			cUValue = (float)iDef / (float)(m_NumberDeformers-1);
			sclX = scaleFirstControler.GetX() * (1.0f - cUValue) + scaleLastControler.GetX() * cUValue;
			sclY = scaleFirstControler.GetY() * (1.0f - cUValue) + scaleLastControler.GetY() * cUValue;  ;
			sclZ = scaleFirstControler.GetZ() * (1.0f - cUValue) + scaleLastControler.GetZ() * cUValue;  ;

			ArrayScaleDeformer[iDef].Set (sclX, sclY, sclZ);
		}
	}
	else if ( m_ControlerScaleType == 2 )
	{
		int numberControler = m_ArrayControlers.GetSize();

		DataArray<float>	ArrayControlerU(numberControler);
		DataArray<Vector3D>	ArrayControlerScale(numberControler);
		float sclX, sclY, sclZ;

		for (int iCon=0; iCon<numberControler; iCon++)
		{
			ArrayControlerU[iCon] = (float)iCon/(float)(numberControler-1);
			m_ArrayControlers[iCon].GetScale(sclX, sclY, sclZ);
			ArrayControlerScale[iCon].Set(sclX, sclY, sclZ);
		}
		
		float cUValue;
		float minRange, maxRange;
		Vector3D minScale, maxScale;
		float cScaleBlend;

		for (int iDef=0; iDef<m_NumberDeformers; iDef++)
		{
			cUValue = (float)iDef/(float)(m_NumberDeformers-1);

			if ( iDef > 0 && iDef <= (m_NumberDeformers-2) )
			{
				for (int iCon=0; iCon<(numberControler-1); iCon++)
				{
					if ( cUValue >= ArrayControlerU[iCon] && cUValue < ArrayControlerU[iCon+1] )
					{
						minRange = ArrayControlerU[iCon];
						maxRange = ArrayControlerU[iCon+1];
						minScale = ArrayControlerScale[iCon];
						maxScale = ArrayControlerScale[iCon+1];
					}
				}

				cScaleBlend = (cUValue - minRange) / (maxRange - minRange);

				ArrayScaleDeformer[iDef].Set(	minScale.GetX() * (1 - cScaleBlend) + maxScale.GetX() * cScaleBlend,
												minScale.GetY() * (1 - cScaleBlend) + maxScale.GetY() * cScaleBlend,
												minScale.GetZ() * (1 - cScaleBlend) + maxScale.GetZ() * cScaleBlend );
												
			}
			else if ( iDef == 0 )
			{
				ArrayScaleDeformer[iDef].Set( ArrayControlerScale[0].GetX(), ArrayControlerScale[0].GetY(), ArrayControlerScale[0].GetZ() );
			}
			else if ( iDef == (m_NumberDeformers-1) )
			{
				ArrayScaleDeformer[iDef].Set( ArrayControlerScale[numberControler-1].GetX(), ArrayControlerScale[numberControler-1].GetY(), ArrayControlerScale[numberControler-1].GetZ() );
			}
		}
	}

	//Compute Squatch only if we are in stretch type 1 or 2.
	if ( m_StretchType == 1 || m_StretchType == 2)
	{
		//If use squatch is activated.
		if ( m_UseSquatch )
		{
			BezierCurveNPoints::ComputeCurveLength();
			float sclRestLength = m_RestLength * m_GlobalScale;
			//Compute the referenec min and max length.
			float maxLength = sclRestLength * m_MaxLengthMulti;
			float minLength = sclRestLength * m_MinLengthMulti;
			//Init the scale axes.
			float scaleAxe1, scaleAxe2 = 1.0f;
			//If the length is greater than the max length.
			if ( BezierCurveNPoints::GetCurveLength() > maxLength )
			{
				scaleAxe1 = m_MaxStretchAxe1Scale;
				scaleAxe2 = m_MaxStretchAxe2Scale;
			}
			//If the length is lesser than the min length.
			else if ( BezierCurveNPoints::GetCurveLength() < minLength )
			{
				scaleAxe1 = m_MinStretchAxe1Scale;
				scaleAxe2 = m_MinStretchAxe2Scale;
			}
			else 
			{
				float scaleFactor = 0.0f;
				if ( BezierCurveNPoints::GetCurveLength() >= sclRestLength )
				{
					scaleFactor = (BezierCurveNPoints::GetCurveLength() - sclRestLength) / (maxLength - sclRestLength);
					scaleAxe1 = ( 1.0f - scaleFactor ) + ( m_MaxStretchAxe1Scale * scaleFactor );
					scaleAxe2 = ( 1.0f - scaleFactor ) + ( m_MaxStretchAxe2Scale * scaleFactor );
				}
				else
				{
					scaleFactor = (sclRestLength - BezierCurveNPoints::GetCurveLength()) / (sclRestLength - minLength);
					scaleAxe1 = ( 1.0f - scaleFactor ) + ( m_MinStretchAxe1Scale * scaleFactor );
					scaleAxe2 = ( 1.0f - scaleFactor ) + ( m_MinStretchAxe2Scale * scaleFactor );
				}
			}

			float blendScale1, blendScale2;

			//Loop over the deformers.
			for ( int iDef=0; iDef<m_NumberDeformers; iDef++ )
			{
				//Filter the axe scale.
				blendScale1 = (1-m_FilterScaleAxe1[iDef]) + (scaleAxe1*m_FilterScaleAxe1[iDef]);
				blendScale2 = (1-m_FilterScaleAxe2[iDef]) + (scaleAxe2*m_FilterScaleAxe2[iDef]);

				//Update the array scale with the axe align reference.
				if ( m_AxeAlignDeformers == 0 )
				{
					ArrayScaleDeformer[iDef].Set (	ArrayScaleDeformer[iDef].GetX(),
													ArrayScaleDeformer[iDef].GetY() * blendScale1,
													ArrayScaleDeformer[iDef].GetZ() * blendScale2);
				}
				else if ( m_AxeAlignDeformers == 1 )
				{
					ArrayScaleDeformer[iDef].Set (	ArrayScaleDeformer[iDef].GetX() * blendScale1,
													ArrayScaleDeformer[iDef].GetY(),
													ArrayScaleDeformer[iDef].GetZ() * blendScale2);
				}
				else
				{
					ArrayScaleDeformer[iDef].Set (	ArrayScaleDeformer[iDef].GetX() * blendScale1,
													ArrayScaleDeformer[iDef].GetY() * blendScale2,
													ArrayScaleDeformer[iDef].GetZ() );
				}
			}
		}
	}

	m_ArrayDefScale = ArrayScaleDeformer;		
}

void SplineMember::ComputeHierarchie()
{	
	if (m_HierarchieType == 0 )
	{
		if (m_UseParentingMatrix == 1)
		{
			for(int iDef=0; iDef<m_NumberDeformers; iDef++)
			{
				m_ArrayDefMatrix[iDef].SelfMul (m_ParentInvertMatrix);
			}
		}
	}
	//If want the serial transformation.
	if (m_HierarchieType == 1)
	{
		//Create the temp array for local transformation.
		Matrix4x4 prevTrans, prevInvertTrans, localTrans;

		//Loop over the current deformer transformation.
		for (int iDef=0; iDef<m_NumberDeformers; iDef++)
		{
			if (iDef == 0)
			{
				if(m_UseParentingMatrix == 1)
				{
					m_ArrayDefMatrix[iDef].SelfMul (m_ParentInvertMatrix);
				}

				prevTrans = m_ArrayDefMatrix[iDef];
			}
			else
			{		
				//Invert the previous transformation.
				prevInvertTrans.Invert(prevTrans);
				//Compute the local transformation.
				localTrans.Mul ( m_ArrayDefMatrix[iDef], prevInvertTrans );
				//Update the previous transformation.
				prevTrans = m_ArrayDefMatrix[iDef];
				//Update the temp array matrix.
				m_ArrayDefMatrix[iDef] = localTrans;
			}
		}
	}	
}

//GET METHODS.
Matrix4x4 SplineMember::GetDefMatrixAt (int in_Index )
{
	return m_ArrayDefMatrix[in_Index];
}

Vector3D SplineMember::GetDefPositionAt (int in_Index)
{
	Vector3D	rtnPos( m_ArrayDefMatrix[in_Index].GetValue(3,0),
						m_ArrayDefMatrix[in_Index].GetValue(3,1),
						m_ArrayDefMatrix[in_Index].GetValue(3,2) );

	return rtnPos;
}

Quaternion SplineMember::GetDefRotQuatAt (int in_Index)
{
	float xX, Xy, Xz, yX, yY, yZ, zX, zY, zZ;
	m_ArrayDefMatrix[in_Index].GetMatrix3x3(xX, Xy, Xz, yX, yY, yZ, zX, zY, zZ);
	Matrix3x3	convertMat (xX, Xy, Xz, yX, yY, yZ, zX, zY, zZ);
	Quaternion	rtnQuat;
	rtnQuat.FromMatrix3x3(convertMat);

	return rtnQuat;
}

Vector3D SplineMember::GetDefRotEulerAt (int in_Index, int in_Radians)
{
	Vector3D rtnRot;
	Matrix3x3 convertMat;
	Vector3D axeX, axeY, axeZ;
	float xX, xY, xZ, yX, yY, yZ, zX, zY, zZ;
	m_ArrayDefMatrix[in_Index].GetMatrix3x3(xX, xY, xZ, yX, yY, yZ, zX, zY, zZ);
	axeX.Set(xX, xY, xZ);
	axeY.Set(yX, yY, yZ);
	axeZ.Set(zX, zY, zZ);
	axeX.SelfNormalize();
	axeY.SelfNormalize();
	axeZ.SelfNormalize();
	convertMat.Set(	axeX.GetX(), axeX.GetY(), axeX.GetZ(),
					axeY.GetX(), axeY.GetY(), axeY.GetZ(),
					axeZ.GetX(), axeZ.GetY(), axeZ.GetZ());
	float rotX, rotY, rotZ;
	convertMat.ToEuler(rotX, rotY, rotZ, in_Radians);
	rtnRot.Set(rotX, rotY, rotZ);

	return rtnRot;
}

Vector3D SplineMember::GetDefScaleAt (int in_Index, int in_ExtractFromMAtrix)
{
	Vector3D rtnScale;

	if (in_ExtractFromMAtrix == 1)
	{
		float sclX, sclY, sclZ;
		m_ArrayDefMatrix[in_Index].GetScale(sclX, sclY, sclZ);
		rtnScale.Set(sclX, sclY, sclZ);
	}
	else if (in_ExtractFromMAtrix == 0)
	{
		rtnScale = m_ArrayDefScale[in_Index];
	}

	return rtnScale;
}

DataArray<Matrix4x4> SplineMember::GetArrayDeformers ()
{
	return m_ArrayDefMatrix;
}

int SplineMember::GetNumberDeformers ()
{
	return m_NumberDeformers;
}

float SplineMember::GetRestLength ()
{
	return m_RestLength;
}

bool SplineMember::GetUseAddRoll ()
{
	return m_UseAddRoll;
}

float SplineMember::GetStartAddRollAngle ()
{
	return m_StartAddRollAngle;
}

float SplineMember::GetEndAddRollAngle ()
{
	return m_EndAddRollAngle;
}

int SplineMember::GetStretchType ()
{
	return m_StretchType;
}

float SplineMember::GetMaxLengthMulti ()
{
	return m_MaxLengthMulti;
}

float SplineMember::GetMinLengthMulti ()
{
	return m_MinLengthMulti;
}

float SplineMember::GetMaxStretchAxe1Scale ()
{
	return m_MaxStretchAxe1Scale;
}

float SplineMember::GetMaxStretchAxe2Scale ()
{
	return m_MaxStretchAxe2Scale;
}

float SplineMember::GetMinStretchAxe1Scale ()
{
	return m_MinStretchAxe1Scale;
}

float SplineMember::GetMinStretchAxe2Scale ()
{
	return m_MinStretchAxe2Scale;
}

bool SplineMember::GetNormalizePosition ()
{
	return m_NormalizePosition;
}

int SplineMember::GetNormalizeIteration ()
{
	return m_NormalizeIteration;
}

float SplineMember::GetSplStartRange ()
{
	return m_SplStartRange;
}

float SplineMember::GetSplEndRange ()
{
	return m_SplEndRange;
}

float SplineMember::GetSplMoveRange ()
{
	return m_SplMoveRange;
}

bool SplineMember::GetUseSquatch ()
{
	return m_UseSquatch;
}

int SplineMember::GetRotationType(){
	return m_RotationType;
}

//SET METHODS.
void SplineMember::Set (	DataArray<Matrix4x4> in_ArrayControlers,
							DataArray<float> in_FilterAxeAlign,
							DataArray<float> in_FilterRotationBlend,
							DataArray<float> in_FilterScaleAxe1,
							DataArray<float> in_FilterScaleAxe2,
							int in_NumberDeformers,
							bool in_NormalizePosition,
							int in_NormalizeIteration,
							int in_AxeAlignDeformers,
							bool in_UseAddRoll,
							float in_StartAddRollAngle,
							float in_EndAddRollAngle,
							int in_StretchType,
							float in_RestLength,
							float in_MaxLengthMulti,
							float in_MinLengthMulti,
							bool in_UseSquatch,
							float in_MaxStretchAxe1Scale,
							float in_MaxStretchAxe2Scale,
							float in_MinStretchAxe1Scale,
							float in_MinStretchAxe2Scale,
							float in_SplStartRange,
							float in_SplEndRange,
							float in_SplMoveRange,
							int in_TangentType,
							int in_ControlerScaleType,
							int in_HierarchieType,
							int in_UseParentingMatrix,
							Matrix4x4 in_ParentInvertMatrix,
							int in_UseOnlyAddRoll,
							int in_RotationType,
							int in_CurveType,
							float in_GlobalScale,
							int& in_UseUserTangent,
							DataArray<Matrix4x4>& in_ArrayUserTangents)
{
	m_ArrayControlers = in_ArrayControlers;
	m_FilterAxeAlign = in_FilterAxeAlign;
	m_FilterRotationBlend = in_FilterRotationBlend;
	m_FilterScaleAxe1 = in_FilterScaleAxe1;
	m_FilterScaleAxe2 = in_FilterScaleAxe2;
	SetPointsNumbers ( in_NumberDeformers );
	SetNumberDeformers (in_NumberDeformers);
	m_NormalizePosition = in_NormalizePosition;
	m_NormalizeIteration = in_NormalizeIteration;
	m_AxeAlignDeformers = in_AxeAlignDeformers;
	m_UseAddRoll = in_UseAddRoll;
	m_StartAddRollAngle = in_StartAddRollAngle;
	m_EndAddRollAngle = in_EndAddRollAngle;
	m_StretchType = in_StretchType;
	m_RestLength = in_RestLength;
	m_MaxLengthMulti = in_MaxLengthMulti;
	m_MinLengthMulti = in_MinLengthMulti;
	m_UseSquatch = in_UseSquatch;
	m_MaxStretchAxe1Scale = in_MaxStretchAxe1Scale;
	m_MaxStretchAxe2Scale = in_MaxStretchAxe2Scale;
	m_MinStretchAxe1Scale = in_MinStretchAxe1Scale;
	m_MinStretchAxe2Scale = in_MinStretchAxe2Scale;
	m_SplStartRange = in_SplStartRange;
	m_SplEndRange = in_SplEndRange;
	m_SplMoveRange = in_SplMoveRange;
	m_TangentType = in_TangentType;
	m_ControlerScaleType = in_ControlerScaleType;
	m_HierarchieType = in_HierarchieType;
	m_UseParentingMatrix = in_UseParentingMatrix;
	m_ParentInvertMatrix = in_ParentInvertMatrix;
	m_UseOnlyAddRoll = in_UseOnlyAddRoll;
	m_RotationType = in_RotationType;
	m_CurveType = in_CurveType;
	m_GlobalScale = in_GlobalScale;
	m_UseUserTangents = in_UseUserTangent;
	m_ArrayUserTangents = in_ArrayUserTangents;
}

void SplineMember::SetNumberDeformers ( int in_NumberDeformers )
{
	m_NumberDeformers = in_NumberDeformers;
	if ( m_NumberDeformers > m_MemoryBaseSize )
	{
		m_ArrayDefMatrix.SetSizeAndClear(in_NumberDeformers);
		m_ArrayDefScale.SetSizeAndClear(in_NumberDeformers);
	}
}

void SplineMember::SetAxeAlignMembers ( int in_AxeAlignDeformers )
{
	m_AxeAlignDeformers = in_AxeAlignDeformers;
}

void SplineMember::SetEndAddRollAngle ( float in_EndAddRollAngle )
{
	m_EndAddRollAngle = in_EndAddRollAngle;
}

void SplineMember::SetMaxLengthMulti ( float in_MaxLengthMulti )
{
	m_MaxLengthMulti = in_MaxLengthMulti;
}

void SplineMember::SetMaxStretchAxe1Scale ( float in_MaxStretchAxe1Scale )
{
	m_MaxStretchAxe1Scale = in_MaxStretchAxe1Scale;
}

void SplineMember::SetMaxStretchAxe2Scale ( float in_MaxStretchAxe2Scale )
{
	m_MaxStretchAxe2Scale = in_MaxStretchAxe2Scale;
}

void SplineMember::SetMinLengthMulti ( float in_MinLengthMulti )
{
	m_MinLengthMulti = in_MinLengthMulti;
}

void SplineMember::SetMinStretchAxe1Scale ( float in_MinStretchAxe1Scale )
{
	m_MinStretchAxe1Scale = in_MinStretchAxe1Scale;
}

void SplineMember::SetMinStretchAxe2Scale ( float in_MinStretchAxe2Scale )
{
	m_MinStretchAxe2Scale = in_MinStretchAxe2Scale;
}

void SplineMember::SetStartAddRollAngle ( float in_StartAddRollAngle )
{
	m_StartAddRollAngle = in_StartAddRollAngle;
}

void SplineMember::SetStretchType ( int in_StretchType )
{
	m_StretchType = in_StretchType;
}

void SplineMember::SetRestLength ( float in_RestLength )
{
	m_RestLength = in_RestLength;
}

void SplineMember::SetUseAddRoll ( bool in_UseAddRoll )
{
	m_UseAddRoll = in_UseAddRoll;
}

void SplineMember::SetNormalizePosition ( bool in_NormalizePosition )
{
	m_NormalizePosition = in_NormalizePosition;
}

void SplineMember::SetNormalizeIteration ( int in_NormalizeIteration )
{
	m_NormalizeIteration = in_NormalizeIteration;
}

void SplineMember::SetSplStartRange ( float in_SplStartRange )
{
	m_SplStartRange = in_SplStartRange;
}

void SplineMember::SetSplEndRange ( float in_SplEndRange )
{
	m_SplEndRange = in_SplEndRange;
}

void SplineMember::SetSplMoveRange ( float in_SplMoveRange )
{
	m_SplMoveRange = in_SplMoveRange;
}

void SplineMember::SetUseSquatch ( bool in_UseSquatch )
{
	m_UseSquatch = in_UseSquatch;
}

void SplineMember::setFilterAxeAlign ( DataArray<float> in_FilterAxeAlign )
{
	m_FilterAxeAlign = in_FilterAxeAlign;
}

void SplineMember::SetFilterRotationBlend ( DataArray<float> in_FilterRotationBlend )
{
	m_FilterRotationBlend = in_FilterRotationBlend;
}

void SplineMember::SetFilterScaleAxe1 ( DataArray<float> in_FilterScaleAxe1 )
{
	m_FilterScaleAxe1 = in_FilterScaleAxe1;
}

void SplineMember::SetFilterScaleAxe2 ( DataArray<float> in_FilterScaleAxe2 )
{
	m_FilterScaleAxe2 = in_FilterScaleAxe2;
}

void SplineMember::SetTangentType( int in_TangentType )
{
	m_TangentType = in_TangentType;
}

void SplineMember::SetControlerScaleType ( int in_ControlerScaleType )
{
	m_ControlerScaleType = in_ControlerScaleType;
}

void SplineMember::SetHierarchieType ( int in_HierarchieType )
{
	m_HierarchieType = in_HierarchieType;
}

void SplineMember::SetUseParentingMatrix ( int in_UseParentingMatrix )
{
	m_UseParentingMatrix = in_UseParentingMatrix;
}

void SplineMember::SetParentInvertMatrix ( Matrix4x4 in_ParentInvertMatrix )
{
	m_ParentInvertMatrix = in_ParentInvertMatrix;
}

void SplineMember::SetUseOnlyAddRoll ( int in_UseOnlyAddRoll )
{
	m_UseOnlyAddRoll = in_UseOnlyAddRoll;
}

void SplineMember::SetRotationType(int in_RotationType){
	m_RotationType = in_RotationType;
}