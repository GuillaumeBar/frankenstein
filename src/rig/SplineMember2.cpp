/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file SplineMember2.cpp
@author Guillaume Baratte
@date 2017-02-05
*/

#include "SplineMember2.h"
#include <iostream>
using namespace std;

const int SplineMember2::m_MemoryBaseSize = 500;

//CONSTRUCTOR.
SplineMember2::SplineMember2 ()
{
	m_numberDeformers = 5;
	m_curveResolution = 50;
	m_startDistribution = 0.0f;
	m_endDistribution = 1.0f;
	m_moveDistribution = 0.0f;
	m_userTangentsUser = 0;
	m_positionType = 0;
	m_localCurveLength = 0.0f;
	m_restLength = 1.0f;
	m_stretchType = 0;
	m_curveMaxStretch = 2.0f;
	m_curveMinStretch = 0.5f;
	m_curveType = 0;
	m_baseCurveLength = 0.0f;
	m_interpolateOutPosition = 0;
	m_controllersLength = 0.0f;
	m_rotationType = 0;
	m_scaleType = 0;
	m_alignType = 0;
	m_useAdditiveRotation = 0;
	m_addStartRotation = 0.0f;
	m_addEndRotation = 0.0f;
	m_inverseCurve = 0;
	m_globalScale = 1.0f;
	m_compressionMaxScaleAxis1 = 2.0f;
	m_compressionMaxScaleAxis2 = 2.0f;
	m_stretchMaxScaleAxis1 = 0.5f;
	m_stretchMaxScaleAxis2 = 0.5f;
	m_blendToLock = 0.0f;
	m_blendToLimited = 0.0f;
	m_inTangentLength = 0.3333f;
	m_outTangentLength = 0.3333f;
	m_useLimitedSoftStretch = 0;
	m_stretchStartSoft = 0.5f;
	m_compressionStartSoft = 0.5f;

	m_ArrayControllers.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayUserTangent.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayControllersPosition.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayControllersRotation.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayControllersScale.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayUserTangentPosition.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayControllersLengths.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayControllersTs.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayDeformerTransformation.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayPointDistribution.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayBaseCurveT.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayBaseCurvePosition.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayBaseCurveLength.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayBaseCurveSubTangents.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayPointRotation.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayPointFollowTangent.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayPointScale.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayPointScaleCompressionAxis1.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayPointScaleCompressionAxis2.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayPointScaleStretchAxis1.SetSizeAndClear(m_MemoryBaseSize);
	m_ArrayPointScaleStretchAxis2.SetSizeAndClear(m_MemoryBaseSize);

	for(int iPnt=0; iPnt<m_numberDeformers; iPnt++){
		m_ArrayPointDistribution[iPnt] = (float)iPnt / (float)(m_numberDeformers - 1);
		m_ArrayPointRotation[iPnt] = (float)iPnt / (float)(m_numberDeformers - 1);
		m_ArrayPointFollowTangent[iPnt] = (float)iPnt / (float)(m_numberDeformers - 1);
	}
}

//DESTRUCTOR.
SplineMember2::~SplineMember2 ()
{

};

void SplineMember2::Set(int in_numberDeformer,
						DataArray<Matrix4x4>& in_arrayControllers,
						DataArray<Matrix4x4>& in_arrayUserTangent,
						int in_baseCurveResolution,
						int in_positionType,
						float in_startRange,
						float in_endRange,
						float in_moveRange,
						float in_restLength,
						int in_stretchType,
						float in_curveMaxStretch,
						float in_curveMinStretch,
						int in_curveType,
						int in_interpolaeOutPosition,
						int in_rotationType,
						int in_scaleType,
						int in_alignType,
						int in_useAdditiveRotation,
						float in_addStartRotation,
						float in_addEndRotation,
						int in_inverseCurve,
						float in_globalScale,
						float in_compressionMaxScaleAxis1,
						float in_compressionMaxScaleAxis2,
						float in_stretchMaxScaleAxis1,
						float in_stretchMaxScaleAxis2,
						float in_blendToLock,
						float in_blendToLimited,
						int in_useUserTangents,
						float in_inTangentLength,
						float in_outTangentLength,
						int in_useLimitedSoftStretch,
						float in_stretchStartSoft,
						float in_m_compressionStartSoft,
						DataArray<float>& in_arrayPointDistribution,
						DataArray<float>& in_arrayPointRotation,
						DataArray<float>& in_arrayPointFollowTangent,
						DataArray<float>& in_ArrayPointScale,
						DataArray<float>& in_ArrayPointScaleCompressionAxis1,
						DataArray<float>& in_ArrayPointScaleCompressionAxis2,
						DataArray<float>& in_ArrayPointScaleStretchAxis1,
						DataArray<float>& in_ArrayPointScaleStretchAxis2){

	m_numberDeformers = in_numberDeformer;
	m_curveResolution = in_baseCurveResolution;
	m_ArrayControllers = in_arrayControllers;
	m_ArrayUserTangent = in_arrayUserTangent;

	m_positionType = in_positionType;
	m_startDistribution = in_startRange;
	m_endDistribution = in_endRange;
	m_moveDistribution = in_moveRange;
	m_restLength = in_restLength;
	m_stretchType = in_stretchType;
	m_curveMaxStretch = in_curveMaxStretch;
	m_curveMinStretch = in_curveMinStretch;
	m_curveType = in_curveType;
	m_baseCurveLength = 0.0f;
	m_interpolateOutPosition = in_interpolaeOutPosition;
	m_controllersLength = 0.0f;
	m_rotationType = in_rotationType;
	m_scaleType = in_scaleType;
	m_alignType = in_alignType;
	m_useAdditiveRotation = in_useAdditiveRotation;
	m_addStartRotation = in_addStartRotation;
	m_addEndRotation = in_addEndRotation;
	m_inverseCurve = in_inverseCurve;
	m_globalScale = in_globalScale;
	m_compressionMaxScaleAxis1 = in_compressionMaxScaleAxis1;
	m_compressionMaxScaleAxis2 = in_compressionMaxScaleAxis2;
	m_stretchMaxScaleAxis1 = in_stretchMaxScaleAxis1;
	m_stretchMaxScaleAxis2 = in_stretchMaxScaleAxis2;
	m_blendToLock = in_blendToLock;
	m_blendToLimited = in_blendToLimited;
	m_userTangentsUser = in_useUserTangents;
	m_inTangentLength = in_inTangentLength;
	m_outTangentLength = in_outTangentLength;
	m_useLimitedSoftStretch = in_useLimitedSoftStretch;
	m_stretchStartSoft = in_stretchStartSoft;
	m_compressionStartSoft = in_m_compressionStartSoft;

	m_ArrayControllersLengths.SetSizeAndClear(m_ArrayControllers.GetSize());
	m_ArrayControllersPosition.SetSizeAndClear(m_ArrayControllers.GetSize());
	m_ArrayControllersRotation.SetSizeAndClear(m_ArrayControllers.GetSize());
	m_ArrayControllersScale.SetSizeAndClear(m_ArrayControllers.GetSize());
	m_ArrayUserTangentPosition.SetSizeAndClear(m_ArrayUserTangent.GetSize());
	m_ArrayControllersTs.SetSizeAndClear(m_ArrayControllers.GetSize());

	if(in_numberDeformer > m_MemoryBaseSize){

		m_ArrayDeformerTransformation.SetSizeAndClear(in_numberDeformer);
		m_ArrayPointDistribution.SetSizeAndClear(in_numberDeformer);
	}

	if(in_baseCurveResolution > m_MemoryBaseSize){

		m_ArrayBaseCurveT.SetSizeAndClear(in_baseCurveResolution);
		m_ArrayBaseCurvePosition.SetSizeAndClear(in_baseCurveResolution);
		m_ArrayBaseCurveLength.SetSizeAndClear(in_baseCurveResolution);
		m_ArrayBaseCurveSubTangents.SetSizeAndClear(in_baseCurveResolution);
	}

	m_ArrayPointDistribution = in_arrayPointDistribution;
	m_ArrayPointRotation = in_arrayPointRotation;
	m_ArrayPointFollowTangent = in_arrayPointFollowTangent;
	m_ArrayPointScale = in_ArrayPointScale;
	m_ArrayPointScaleCompressionAxis1 = in_ArrayPointScaleCompressionAxis1;
	m_ArrayPointScaleCompressionAxis2 = in_ArrayPointScaleCompressionAxis2;
	m_ArrayPointScaleStretchAxis1 = in_ArrayPointScaleStretchAxis1;
	m_ArrayPointScaleStretchAxis2 = in_ArrayPointScaleStretchAxis2;

};

void SplineMember2::computeControllersData(){

	// Split the matrix to position, rotation ans scale.
	// Compute the controllers length.
	// Compute the controolers T value.
	float pX, pY, pZ;
	float sX, sY, sZ;
	float rXX, rXY, rXZ, rYX, rYY, rYZ, rZX, rZY, rZZ;
	Vector3D conDist;
	Matrix3x3 rotation;
	Vector3D axisX, axisY, axisZ;

	m_ArrayControllersLengths[0] = 0.0f;

	// Loop over the controllers.
	for(int iCon=0; iCon < m_ArrayControllers.GetSize(); iCon++){

		// Compute the T value of the controllers.
		m_ArrayControllersTs[iCon] = (float)iCon / (float)(m_ArrayControllers.GetSize() - 1);

		// Extract the controllers position.
		m_ArrayControllers[iCon].GetPosition(pX, pY, pZ);
		m_ArrayControllersPosition[iCon].Set(pX, pY, pZ) ;

		// Extract the controllers rotation.
		// Extract matrix 3 from matrix 4.
		m_ArrayControllers[iCon].GetMatrix3x3(rXX, rXY, rXZ,
												rYX, rYY, rYZ,
												rZX, rZY, rZZ);
		axisX.Set(rXX, rXY, rXZ);
		axisY.Set(rYX, rYY, rYZ);
		axisZ.Set(rZX, rZY, rZZ);

		axisX.SelfNormalize();
		axisY.SelfNormalize();
		axisZ.SelfNormalize();

		rotation.Set(axisX.GetX(), axisX.GetY(), axisX.GetZ(),
						axisY.GetX(), axisY.GetY(), axisY.GetZ(),
						axisZ.GetX(), axisZ.GetY(), axisZ.GetZ());
		// Convert to quaternion.
		m_ArrayControllersRotation[iCon].FromMatrix3x3(rotation);

		// Extract the controllers scale.
		m_ArrayControllers[iCon].GetScale(sX, sY, sZ);
		m_ArrayControllersScale[iCon].Set(sX, sY, sZ) ;

		// Compute the distance between the controllers.
		if(iCon > 0){
			// Compute the distance vector.
			conDist.Sub(m_ArrayControllersPosition[iCon], m_ArrayControllersPosition[iCon - 1]);
			// Cumul the controllers distance.
			m_controllersLength += conDist.GetLength();
			// Compute and store the distance between controllers.
			m_ArrayControllersLengths[iCon] = m_controllersLength;
		}
	}

	// Loop over the user tangent.
	for(int iTan=0; iTan<m_ArrayUserTangent.GetSize(); iTan++){
		// Extract the tangents position.
		m_ArrayUserTangent[iTan].GetPosition(pX, pY, pZ);
		m_ArrayUserTangentPosition[iTan].Set(pX, pY, pZ) ;
	}
}

void SplineMember2::computeBaseCurve(){

	// Check which curve we want to use.
	// 0 -> Bezier Curve
	// 1 -> Fit Bezier Curve
	if(m_curveType == 0){

		BezierCurveNPoints oCurve(m_curveResolution,
									m_ArrayControllersPosition);
		// Compute the base curve.
		oCurve.ComputeCurve();
		// Copy the curve data to the 
		m_ArrayBaseCurveT = oCurve.GetCurveTs();
		m_ArrayBaseCurvePosition = oCurve.GetCurvePoints();
		m_ArrayBaseCurveLength = oCurve.GetCurveLengths();
		m_baseCurveLength = oCurve.GetCurveLength();
		m_ArrayBaseCurveSubTangents = oCurve.GetCurveSubTangents();
	}else{

		FitBezierCurveNPoints oCurve;
		// Define the distribution data.
		float startDist = 0.0f;
		float endDist = 1.0f;
		float moveDist = 0.0f;
		// Check the user tangent data. If not good swith to automatic tangent.
		int tangentNumber = (m_ArrayControllers.GetSize() - 2) * 2 + 2;
		if(m_ArrayUserTangent.GetSize() != tangentNumber){
			m_userTangentsUser = 0;
		}
		// Init the base bezier fit curve.
		oCurve.Set(m_curveResolution,
					m_ArrayControllersPosition,
					startDist,
					endDist,
					moveDist,
					m_userTangentsUser,
					m_inTangentLength,
					m_outTangentLength,
					m_ArrayUserTangentPosition);
		if(m_userTangentsUser == 1){
			// Use the user tangents.
			oCurve.AddUserTangentsToArrayAllPoints();
		}else{
			// Compute the automatic tangent.
			oCurve.ComputeTangents();
		}
		// Compute the base curve.
		oCurve.ComputeCurve();
		// Copy the curve data to the 
		m_ArrayBaseCurveT = oCurve.GetCurveTs();
		m_ArrayBaseCurvePosition = oCurve.GetCurvePoints();
		m_ArrayBaseCurveLength = oCurve.GetCurveSubLengths();
		m_baseCurveLength = oCurve.GetLength();
		m_ArrayBaseCurveSubTangents = oCurve.GetCurveSubTangents();
	}
};

void SplineMember2::computePointData(float in_T,
										int in_ID,
										int in_Type,
										float& out_Length,
										Vector3D& out_Position,
										Vector3D& out_Tangent){

	float pointValue;

	if(in_Type == 0){
		// Compute the distribution for cubic position.
		float tRange = m_endDistribution - m_startDistribution;
		float freeMaxRange, limitedMaxRange, lockMaxRange, maxRange;

		// Free stretch.
		freeMaxRange = tRange;

		// Limited stretch.

		float maxLength = m_restLength * m_curveMaxStretch * m_globalScale;
		float minLength = m_restLength * m_curveMinStretch * m_globalScale;

		if(m_interpolateOutPosition == 0){
			// No out interpolate.
			if(maxLength < m_baseCurveLength ){

				limitedMaxRange = maxLength / m_baseCurveLength * tRange;
			}else{

				limitedMaxRange = tRange;
			}
		}else{
			// Out interpolation enable.
			if(maxLength < m_baseCurveLength ){

				limitedMaxRange = maxLength / m_baseCurveLength * tRange;
			}else if(minLength > m_baseCurveLength){

				limitedMaxRange = minLength / m_baseCurveLength * tRange;
			}else{

				limitedMaxRange = tRange;
			}
		}

		// Lock stretch.
		float scaledRestLength = m_restLength * m_globalScale;
		if(m_interpolateOutPosition == 0){
			// No out interpolate.
			if(scaledRestLength > m_baseCurveLength){

				lockMaxRange = tRange;
			}else{

				lockMaxRange = scaledRestLength / m_baseCurveLength * tRange;
			}
		}else{
			// Out interpolation enable.
			lockMaxRange = scaledRestLength / m_baseCurveLength * tRange;
		}


		if(m_stretchType == 0){
			maxRange = freeMaxRange;
		}else if(m_stretchType == 1){
			maxRange = limitedMaxRange;
		}else if(m_stretchType == 2){
			maxRange = lockMaxRange;
		}else if(m_stretchType == 3){
			maxRange = freeMaxRange * (1.0f - m_blendToLock) + lockMaxRange * m_blendToLock;
			maxRange = maxRange * (1.0f - m_blendToLimited) + limitedMaxRange * m_blendToLimited;
		}

		// Compute the point length.
		//pointValue = in_T * maxRange + m_startDistribution + m_moveDistribution;
		pointValue = m_ArrayPointDistribution[in_ID] * maxRange + m_startDistribution + m_moveDistribution;
		// Clamp the length value.
		if(m_interpolateOutPosition == 0){
			// if no interpolation, clamp the t value.
			if(pointValue > 1.0f){
				pointValue = 1.0f;
			}else if(pointValue < 0.0f){
				pointValue = 0.0f;
			}
		}
	}else{
		// Compute the distribution for normalized position.
		float startLength = m_baseCurveLength * m_startDistribution;
		float freeCurveMaxLength, limitedCurveMaxLength, lockCurveMaxLength, curveMaxLength;

		// Free stretch.
		freeCurveMaxLength =  m_baseCurveLength;

		// Limited Stretch
		float scaledRestLength = m_restLength * m_globalScale;
		float maxLength = scaledRestLength * m_curveMaxStretch;
		float minLength = scaledRestLength * m_curveMinStretch;

		if(m_interpolateOutPosition == 0){
			// No out interpolation.
			if(maxLength < m_baseCurveLength ){

				limitedCurveMaxLength = maxLength;
			}else{

				limitedCurveMaxLength = m_baseCurveLength;
			}
		}else{
			// Out interpolation enable.
			if(maxLength < m_baseCurveLength ){

				limitedCurveMaxLength = maxLength;
			}else if(minLength > m_baseCurveLength){

				limitedCurveMaxLength = minLength;
			}else{

				limitedCurveMaxLength = m_baseCurveLength;
			}
		} 

		// Locked stretch
		if(m_interpolateOutPosition == 0){
			// No out interpolation.
			if(scaledRestLength > m_baseCurveLength){

				lockCurveMaxLength = m_baseCurveLength;
			}else{

				lockCurveMaxLength = scaledRestLength;
			}
		}else{
			// Out interpolation enable.
			lockCurveMaxLength = scaledRestLength;
		}

		if(m_stretchType == 0){
			curveMaxLength = freeCurveMaxLength;
		}else if(m_stretchType == 1){
			curveMaxLength = limitedCurveMaxLength;

			if(m_useLimitedSoftStretch){
				if(curveMaxLength > scaledRestLength){

					float distRange = maxLength - scaledRestLength;
					float distDelta = distRange * m_stretchStartSoft;
					float distSoft = distRange - distDelta;
					float distLength = (curveMaxLength - scaledRestLength);

					if( distLength > distDelta){
						float soft =  (1.0f - expf(-(distLength - distDelta) / distSoft)) * distSoft + distDelta;
						curveMaxLength = scaledRestLength + soft;
					}
				}else if(curveMaxLength <= scaledRestLength){

					float distRange = scaledRestLength - minLength;
					float distDelta = distRange * m_compressionStartSoft;
					float distSoft = distRange - distDelta;
					float distLength = -(curveMaxLength - scaledRestLength);

					if( distLength > distDelta){
						float soft = ( expf(-(distLength - distDelta) / distSoft)) * distSoft;
						curveMaxLength = minLength + soft;
					}
				}
			}

		}else if(m_stretchType == 2){
			curveMaxLength = lockCurveMaxLength;
		}else if(m_stretchType == 3){
			curveMaxLength = freeCurveMaxLength * ( 1.0f - m_blendToLock) + lockCurveMaxLength * m_blendToLock;
			curveMaxLength = curveMaxLength * ( 1.0f - m_blendToLimited) + limitedCurveMaxLength * m_blendToLimited;
		}




		float endLength = curveMaxLength * m_endDistribution;
		float moveLenght = m_baseCurveLength * m_moveDistribution;
		float lengthRange = endLength - startLength;
		// Compute the point length.
		//pointValue = (float)in_ID / (float)(m_numberDeformers - 1) * lengthRange + startLength + moveLenght;
		pointValue = m_ArrayPointDistribution[in_ID] * lengthRange + startLength + moveLenght;
		// Clamp the length value.
		if(m_interpolateOutPosition == 0){
			// If no out interpolation clamp the point value.
			if(pointValue > m_baseCurveLength){
				pointValue = m_baseCurveLength;
			}else if(pointValue < 0.0f){
				pointValue = 0.0f;
			}
		}
	}


	// Find the previous and next point ID
	int prevID = -1;
	int nextID = -1;
	float prevValue, nextValue;

	for(int iPnt=0; iPnt < (m_curveResolution - 1); iPnt++){

		if(in_Type == 0){
			prevValue = m_ArrayBaseCurveT[iPnt];
			nextValue = m_ArrayBaseCurveT[iPnt+1];
		}else{
			prevValue = m_ArrayBaseCurveLength[iPnt];
			nextValue = m_ArrayBaseCurveLength[iPnt+1];
		}

		if(prevValue == pointValue){

			// Check if t equal to the previous t.
			prevID = iPnt;
			break;
		}else if(nextValue == pointValue){

			// Check if t equal to the next t.
			nextID = iPnt + 1;
			break;
		}else if(prevValue < pointValue &&
					nextValue > pointValue){

			// Check if t is inbetween the prev and next t.
			// Get the previous and next point position;
			prevID = iPnt;
			nextID = iPnt + 1;
			break;
		}
	}

	//cout << "Previous ID : " << prevID << endl;
	//cout << "Next ID : " << nextID << endl;

	Vector3D prevPos, nextPos;
	Vector3D prevTangent, nextTangent;
	float prevLength, nextLength;
	float alpha, invAlpha;

	if(nextID < 0){
		out_Position = m_ArrayBaseCurvePosition[prevID];
		out_Length = m_ArrayBaseCurveLength[prevID];
		out_Tangent = m_ArrayBaseCurveSubTangents[prevID];
	}else if(prevID < 0){
		out_Position = m_ArrayBaseCurvePosition[nextID];
		out_Length = m_ArrayBaseCurveLength[nextID];
		out_Tangent = m_ArrayBaseCurveSubTangents[nextID];
	}else{

		prevPos = m_ArrayBaseCurvePosition[prevID];
		nextPos = m_ArrayBaseCurvePosition[nextID];

		prevLength = m_ArrayBaseCurveLength[prevID];
		nextLength = m_ArrayBaseCurveLength[nextID];

		prevTangent = m_ArrayBaseCurveSubTangents[prevID];
		nextTangent = m_ArrayBaseCurveSubTangents[nextID];
		// Compute the alpha value of the current T.
		alpha = (pointValue - prevValue) / (nextValue - prevValue);
		// Compute the invert alpha.
		invAlpha = 1.0f - alpha;
		// Interpolate the previous and next position.
		out_Position = prevPos * invAlpha + nextPos * alpha;
		out_Length = prevLength *invAlpha + nextLength * alpha;
		out_Tangent = prevTangent * invAlpha + nextTangent * alpha;

		//cout << "Alpha : " << alpha << endl;

		//cout << "Previous Point Position : " << prevPos.GetX() << "," << prevPos.GetY() << "," << prevPos.GetZ() << endl;
		//cout << "Next Point Position : " << nextPos.GetX() << "," << nextPos.GetY() << "," << nextPos.GetZ() << endl;

	}


	if(m_interpolateOutPosition == 1){
		// Out interpolation enable.
		Vector3D vectorInter;
		float distInter;

		if(in_Type == 0){
			// Cubic position interpolation.
			if(pointValue > 1.0f){
				// Interpolate after the end of the curve.
				prevPos = m_ArrayBaseCurvePosition[m_curveResolution - 2];
				nextPos = m_ArrayBaseCurvePosition[m_curveResolution - 1];

				vectorInter.Sub(nextPos, prevPos);
				vectorInter.SelfNormalize();
				vectorInter.Normalize(m_ArrayBaseCurveSubTangents[m_curveResolution - 1]);

				distInter = m_baseCurveLength / (float)(m_curveResolution - 1) * (pointValue - 1.0f);
				out_Position = vectorInter * distInter * m_curveResolution + nextPos;
				out_Length = m_baseCurveLength + distInter;
				out_Tangent = m_ArrayBaseCurveSubTangents[m_curveResolution - 1];

			}else if(pointValue < 0.0f){
				// Interpolate before the start of the curve.
				prevPos = m_ArrayBaseCurvePosition[0];
				nextPos = m_ArrayBaseCurvePosition[1];

				vectorInter.Sub(nextPos, prevPos);
				vectorInter.SelfNormalize();
				vectorInter.Normalize(m_ArrayBaseCurveSubTangents[0]);

				distInter = m_baseCurveLength / (float)(m_curveResolution - 1) * pointValue;
				out_Position = vectorInter * distInter * m_curveResolution + prevPos;
				out_Length = distInter;
				out_Tangent = m_ArrayBaseCurveSubTangents[0];
			}
		}else{
			// Normalized position interpolation.
			if(pointValue > m_baseCurveLength){
				// Interpolate after the end of the curve.
				prevPos = m_ArrayBaseCurvePosition[m_curveResolution - 2];
				nextPos = m_ArrayBaseCurvePosition[m_curveResolution - 1];

				vectorInter.Sub(nextPos, prevPos);
				vectorInter.SelfNormalize();
				vectorInter.Normalize(m_ArrayBaseCurveSubTangents[m_curveResolution - 1]);

				nextPos = m_ArrayBaseCurvePosition[m_curveResolution - 1];

				distInter = pointValue - m_baseCurveLength;

				out_Length = m_baseCurveLength + distInter;
				out_Position = vectorInter * distInter + nextPos;
				out_Tangent = m_ArrayBaseCurveSubTangents[m_curveResolution - 1];

			}else if(pointValue < 0.0f){
				// Interpolate before the start of the curve.
				prevPos = m_ArrayBaseCurvePosition[0];
				nextPos = m_ArrayBaseCurvePosition[1];

				vectorInter.Sub(nextPos, prevPos);
				vectorInter.SelfNormalize();
				vectorInter.Normalize(m_ArrayBaseCurveSubTangents[0]);

				distInter = pointValue;

				out_Length = distInter;
				out_Position = vectorInter * distInter + prevPos;
				out_Tangent = m_ArrayBaseCurveSubTangents[0];
			}
		}
	}
}

void SplineMember2::getPrevNextControllerID(int in_ID,
												float in_Length,
												int& out_prevID,
												int& out_nextID,
												int& out_typeID,
												float& out_alpha){

	float pointT, prevConT, nextConT;
	pointT = in_Length / m_baseCurveLength;

	for(int iCon=0; iCon<m_ArrayControllers.GetSize(); iCon++){

		if(iCon > 0){

			prevConT = m_ArrayControllersLengths[iCon - 1] / m_controllersLength;
			nextConT = m_ArrayControllersLengths[iCon ] / m_controllersLength;

			if(prevConT == pointT){
				out_prevID = iCon - 1;
				out_typeID = 0;
				break;
			}else if(nextConT == pointT){
				out_nextID = iCon;
				out_typeID = 1;
				break;
			}else if(prevConT < pointT && pointT < nextConT){
				out_prevID = iCon - 1;
				out_nextID = iCon;
				out_typeID = 2;
				out_alpha = (pointT - prevConT) / (nextConT - prevConT);
				break;
			}else if(pointT > 1.0f){
				out_prevID = m_ArrayControllers.GetSize() - 1;
				out_typeID = 3;
				break;
			}else if(pointT < 0.0f){
				out_nextID = 0;
				out_typeID = 4;
				break;
			}
		}
	}
}

void SplineMember2::computePointRotation(int in_ID,
											Vector3D in_Tangent,
											int in_typeID,
											int in_prevID,
											int in_nextID,
											float in_alpha,
											Quaternion& out_Rotation){

	// Compute the align axis.
	Vector3D alignAxis;
	if(m_alignType == 0){
		// Axis +X.
		alignAxis.Set(1.0f, 0.0f, 0.0f);
	}else if(m_alignType == 1){
		// Axis +Y.
		alignAxis.Set(0.0f, 1.0f, 0.0f);
	}else if(m_alignType == 2){
		// Axis +Z.
		alignAxis.Set(0.0f, 0.0f, 1.0f);
	}else if(m_alignType == 3){
		// Axis -X.
		alignAxis.Set(-1.0f, 0.0f, 0.0f);
	}else if(m_alignType == 4){
		// Axis -Y.
		alignAxis.Set(0.0f, -1.0f, 0.0f);
	}else{
		// Axis -Z.
		alignAxis.Set(0.0f, 0.0f, -1.0f);
	}

	if(m_rotationType == 1){
		// Find the previous and next controller.
		if(in_typeID == 0){
			out_Rotation = m_ArrayControllersRotation[in_prevID];
		}else if(in_typeID == 1){
			out_Rotation =  m_ArrayControllersRotation[in_nextID];
		}else if(in_typeID == 2){
			// Distribute rotation between each controller.
			out_Rotation.Slerp(m_ArrayControllersRotation[in_prevID],
								m_ArrayControllersRotation[in_nextID],
								in_alpha,
								true);
		}else if(in_typeID == 3){

			out_Rotation = m_ArrayControllersRotation[in_prevID];
		}else if(in_typeID == 4){

			out_Rotation = m_ArrayControllersRotation[in_nextID];
		}
	}else{
		// Distribute rotation between the first and last controller.
		out_Rotation.Slerp(m_ArrayControllersRotation[0],
							m_ArrayControllersRotation[m_ArrayControllers.GetSize() - 1],
							m_ArrayPointRotation[in_ID],
							true);
	}

	if(m_inverseCurve == 1){
		in_Tangent.SelfMulByFloat(-1.0f);
	}
	in_Tangent.SelfNormalize();

	Vector3D rotatedAlignAxis, crossVector;
	// Rotate the algin axis to the quaternion blend.
	rotatedAlignAxis.RotateByQuaternionFG(alignAxis, out_Rotation);

	float angle;

	float dot = rotatedAlignAxis.DotProduct(in_Tangent);

	angle = acos(dot);

	//Filter the tangent alignment angle.
	angle = angle * m_ArrayPointFollowTangent[in_ID];

	// Check if we need to rotated the quaternion blend to the tangent.
	if(angle > 0.001f){
		// Compute the cross product for align the quaternion blend to the curve tangent.
		crossVector.CrossProduct(rotatedAlignAxis, in_Tangent);
		crossVector.SelfNormalize();
		// Create the corrective rotation from the axis and angle.
		Quaternion correctiveQuat;
		correctiveQuat.FromAxisAndAngle(crossVector.GetX(),
										crossVector.GetY(),
										crossVector.GetZ(),
										angle);
		// Multiply the corrective rotation by the quaternion blend.
		out_Rotation.Mul(correctiveQuat, out_Rotation);
	}

	// Check if we want to use the additive rotation.
	if(m_useAdditiveRotation == 1){
		// Create the additive rotation.
		float additiveAngle = (m_addStartRotation * (1.0f - m_ArrayPointRotation[in_ID]) + m_addEndRotation * m_ArrayPointRotation[in_ID]) * 3.14f / 180.0f;
		Quaternion additiveQuat;
		additiveQuat.FromAxisAndAngle(alignAxis.GetX(),
										alignAxis.GetY(),
										alignAxis.GetZ(),
										additiveAngle);
		out_Rotation.Mul(out_Rotation, additiveQuat);
	}

}

void SplineMember2::computePointScale(int in_ID,
										float in_T,
										float in_Length,
										int in_typeID,
										int in_prevID,
										int in_nextID,
										float in_alpha, 
										Vector3D& out_Scale){

	out_Scale.Set( 1.0f, 1.0f, 1.0f);

	if(m_scaleType == 0){
		out_Scale.Set(m_globalScale, m_globalScale, m_globalScale);
	}else if(m_scaleType == 1){
		// Stretch type.
		float retargetT = in_T *(m_endDistribution - m_startDistribution);
		float localSubLength = retargetT * m_restLength * m_globalScale;
		float maxLocalSubLength = localSubLength * m_curveMaxStretch;
		float minLocalSubLength = localSubLength * m_curveMinStretch;
		float retargetLength = in_Length - ( (m_startDistribution + m_moveDistribution) * m_baseCurveLength);

		float localBlend, scaleAxis1, scaleAxis2;
		if(retargetLength == 0){
			scaleAxis1 = 0.0f;
			scaleAxis2 = 0.0f;
		}else if(retargetLength > localSubLength){
			localBlend = (in_Length - localSubLength) / (maxLocalSubLength - localSubLength);
			scaleAxis1 = -( 1.0f - m_stretchMaxScaleAxis1) * localBlend * m_ArrayPointScaleStretchAxis1[in_ID];
			scaleAxis2 = -( 1.0f - m_stretchMaxScaleAxis2) * localBlend * m_ArrayPointScaleStretchAxis2[in_ID];

		}else if(retargetLength <= localSubLength){
			localBlend = 1.0f - (in_Length - minLocalSubLength) / (localSubLength - minLocalSubLength);
			scaleAxis1 = (m_compressionMaxScaleAxis1 - 1.0f) * localBlend * m_ArrayPointScaleCompressionAxis1[in_ID];
			scaleAxis2 = (m_compressionMaxScaleAxis2 - 1.0f) * localBlend * m_ArrayPointScaleCompressionAxis2[in_ID];
		}

		if(m_alignType == 0){
			float y = out_Scale.GetY() + scaleAxis1;
			float z = out_Scale.GetZ() + scaleAxis2;
			out_Scale.Set(out_Scale.GetX(), y, z);
		}else if(m_alignType == 1){
			float x = out_Scale.GetX() + scaleAxis1;
			float z = out_Scale.GetZ() + scaleAxis2;
			out_Scale.Set(x, out_Scale.GetY(), z);
		}else if(m_alignType == 2){
			float x = out_Scale.GetX() + scaleAxis1;
			float y = out_Scale.GetY() + scaleAxis2;
			out_Scale.Set(x, y, out_Scale.GetZ());
		}

		out_Scale.SelfMulByFloat(m_globalScale);

	}else if(m_scaleType == 2){
		// First and last.
		Vector3D prevScale, nextScale;
		float invAlpha = 1.0f - m_ArrayPointScale[in_ID];

		prevScale = m_ArrayControllersScale[0];
		nextScale = m_ArrayControllersScale[m_ArrayControllersScale.GetSize() - 1];

		prevScale.SelfMulByFloat(invAlpha);
		nextScale.SelfMulByFloat(m_ArrayPointScale[in_ID]);

		out_Scale.Add(prevScale, nextScale);
	}else if(m_scaleType == 3){
		// All
		if(in_typeID == 0){
			out_Scale = m_ArrayControllersScale[in_prevID];
		}else if(in_typeID == 1){
			out_Scale =  m_ArrayControllersScale[in_nextID];
		}else if(in_typeID == 2){
			// Distribute rotation between each controller.
			Vector3D prevScale, nextScale;
			float invAlpha = 1.0f - in_alpha;

			prevScale = m_ArrayControllersScale[in_prevID];
			nextScale = m_ArrayControllersScale[in_nextID];

			prevScale.SelfMulByFloat(invAlpha);
			nextScale.SelfMulByFloat(in_alpha);

			out_Scale.Add(prevScale, nextScale);
		}else if(in_typeID == 3){

			out_Scale = m_ArrayControllersScale[in_prevID];
		}else if(in_typeID == 4){

			out_Scale = m_ArrayControllersScale[in_nextID];
		}
	}
}

void SplineMember2::computeDeformerTransformation(){

	Vector3D defPosition, defTangent, defScale;
	float defT;
	float startLenght, endLength, pointLength;
	Quaternion defQuat;
	Matrix3x3 defRotation;
	int prevID, nextID, typeID;
	float defAlpha;

	//Set the position of the deformer array.
	for (int iDef=0; iDef<m_numberDeformers; iDef++)
	{
		// Compute the T value of the deformer.
		defT = (float)iDef / (float)(m_numberDeformers - 1);
		// Compute the position of the deformer.
		computePointData(defT, iDef, m_positionType, pointLength, defPosition, defTangent);
		// cout << "Point position : " << defPosition.GetX() << "," << defPosition.GetY() << "," << defPosition.GetZ() << endl;
		// Get the previous and next ID and the local alpha.
		getPrevNextControllerID(iDef, pointLength, prevID, nextID, typeID, defAlpha);
		// Compute the rotation of the deformer.
		computePointRotation(iDef,
								defTangent,
								typeID,
								prevID,
								nextID,
								defAlpha,
								defQuat);
		// Compute the scale of the deformer.
		computePointScale(iDef,
							defT,
							pointLength,
							typeID,
							prevID,
							nextID,
							defAlpha,
							defScale);
		// Store the start and end point length for compute the point curve length.
		if(iDef == 0){
			startLenght = pointLength;
		}else if(iDef == (m_numberDeformers - 1)){
			endLength = pointLength;
		}

		defRotation = defQuat.ToMatrix3x3();

		// Set the deformer transformation matrix.
		m_ArrayDeformerTransformation[iDef].Set(defRotation.GetValue(0,0) * defScale.GetX(),
												defRotation.GetValue(0,1) * defScale.GetX(),
												defRotation.GetValue(0,2) * defScale.GetX(),
												0.0f,
												defRotation.GetValue(1,0) * defScale.GetY(),
												defRotation.GetValue(1,1) * defScale.GetY(),
												defRotation.GetValue(1,2) * defScale.GetY(),
												0.0f,
												defRotation.GetValue(2,0) * defScale.GetZ(),
												defRotation.GetValue(2,1) * defScale.GetZ(),
												defRotation.GetValue(2,2) * defScale.GetZ(),
												0.0f,
												defPosition.GetX(),
												defPosition.GetY(),
												defPosition.GetZ(),
												1.0f);
	}

	// Compute the curve length.
	m_localCurveLength = endLength - startLenght;
};

void SplineMember2::computeCurve(){

	// Compute the controllers datas.
	computeControllersData();
	// Compute the base curve.
	computeBaseCurve();
	// Compute the deformer Transformation.
	computeDeformerTransformation();
}

Vector3D SplineMember2::getPositionAt(int in_Index){

	Vector3D	rtnPos( m_ArrayDeformerTransformation[in_Index].GetValue(3,0),
						m_ArrayDeformerTransformation[in_Index].GetValue(3,1),
						m_ArrayDeformerTransformation[in_Index].GetValue(3,2) );

	return rtnPos;
}

Vector3D SplineMember2::getEulerAt(int in_Index){

	Vector3D rtnRot;
	Matrix3x3 convertMat;
	Vector3D axeX, axeY, axeZ;
	float xX, xY, xZ, yX, yY, yZ, zX, zY, zZ;
	m_ArrayDeformerTransformation[in_Index].GetMatrix3x3(xX, xY, xZ, yX, yY, yZ, zX, zY, zZ);
	axeX.Set(xX, xY, xZ);
	axeY.Set(yX, yY, yZ);
	axeZ.Set(zX, zY, zZ);
	axeX.SelfNormalize();
	axeY.SelfNormalize();
	axeZ.SelfNormalize();
	convertMat.Set(	axeX.GetX(), axeX.GetY(), axeX.GetZ(),
					axeY.GetX(), axeY.GetY(), axeY.GetZ(),
					axeZ.GetX(), axeZ.GetY(), axeZ.GetZ());
	float rotX, rotY, rotZ;
	convertMat.ToEuler(rotX, rotY, rotZ, 1);
	rtnRot.Set(rotX, rotY, rotZ);

	return rtnRot;
}

Vector3D SplineMember2::getScaleAt(int in_Index){

	Vector3D rtnScale;

	float sclX, sclY, sclZ;
	m_ArrayDeformerTransformation[in_Index].GetScale(sclX, sclY, sclZ);
	rtnScale.Set(sclX, sclY, sclZ);

	return rtnScale;
}

float SplineMember2::getLocalCurveLength(){
	return m_localCurveLength;
}

float SplineMember2::getGlobalCurveLength(){
	return m_baseCurveLength;
}