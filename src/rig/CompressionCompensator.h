/*!
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
@file CompressionCompensator.h
@author Guillaume Baratte
@date 2016-06-04
@brief Define the class for CompressionCompensator.
*/

#ifndef _COMPRESSIONCOMPENSATOR_
#define _COMPRESSIONCOMPENSATOR_

#include "../core/Matrix4x4.h"
#include "../core/Matrix3x3.h"
#include "../core/Quaternion.h"
#include "../core/Vector3D.h"

class CompressionCompensator {

    public:
        // Constructor.
        CompressionCompensator();
        CompressionCompensator( Matrix4x4& in_MatrixA,
                                Matrix4x4& in_MatrixB,
                                int& in_RefAxe,
                                int& in_RefAnchorAxe,
                                float& in_RestLength,
                                float& in_BlendRot,
                                float& in_BlendPos,
                                int& in_UseGuideRest,
                                Matrix4x4& in_GuideMatrixA,
                                Matrix4x4& in_GuideMatrixB,
                                int& in_UseStretch);
        // Destructor.
        ~CompressionCompensator();

        /*@biref Compute the blend matrix.*/
        void Compute();

        /*@brief Set all the parameter of the class.
        *@param[in] in_MatrixA The matrix at blend 0.
        *@param[in] in_MatrixB The matrix at blend 1.
        *@param[in] in_RefAxe The axe to align between the anchor.
        *@param[in] in_RefAnchorAxe The axe of anchor that user as upvector.
        *@param[in] in_RestLength Distance of the rest position.
        *@param[in] in_BlendRot The blend factor for rotation.
        *@param[in] m_BlendPos The blend factor for position.
        */
        void Set(   Matrix4x4& in_MatrixA,
                    Matrix4x4& in_MatrixB,
                    int& in_RefAxe,
                    int& in_RefAnchorAxe,
                    float& in_RestLength,
                    float& in_BlendRot,
                    float& in_BlendPos,
                    int& in_UseGuideRest,
                    Matrix4x4& in_GuideMatrixA,
                    Matrix4x4& in_GuideMatrixB,
                    int& in_UseStretch);

        /*@brief Set the matrix A.
        *@param[in] in_MatrixA The matrix at blend 0.
        */
        void SetMatrixA(Matrix4x4& in_MatrixA);

        /*@brief Set the matrix B.
        *@param[in] in_MatrixB The matrix at blend 1.
        */        
        void SetMatrixB(Matrix4x4& in_MatrixB);

        /*@brief Set blend factor.
        *@param[in] in_RefAxe The axe to align between the anchor.
        */          
        void SetRefAxe(int& in_RefAxe);

        /*@brief Set blend factor.
        *@param[in] in_RefAnchorAxe The axe of anchor that user as upvector.
        */          
        void SetRefAnchorAxe(int& in_RefAnchorAxe);

        /*@brief Set blend factor.
        *@param[in] in_RestLength Distance of the rest position.
        */          
        void SetRestLength(float& in_RestLength);

        /*@brief Set blend factor.
        *@param[in] in_BlendRot The blend factor for rotation
        */          
        void SetBlendRot(float& in_BlendRot);

        /*@brief Set blend factor.
        *@param[in] m_BlendPos The blend factor for position.
        */          
        void SetBlendPos(float& in_BlendPos);

        /*@brief Set use guide for compute the rest length.
        *@param[in] in_UseGuideRest 0 -> Not used, 1 -> Used.
        */          
        void SetUseGuideRest(int& in_UseGuideRest);

        /*@brief Set use stretch to scale the matrix along the align axe..
        *@param[in] in_UseGuideRest 0 -> Not used, 1 -> Used.
        */          
        void SetUseStretch(int& in_UseStretch);

        /*@brief Set the guide matrix A.
        *@param[in] in_GuideMatrixA The guide matrix at blend 0.
        */
        void SetGuideMatrixA(Matrix4x4& in_GuideMatrixA);

        /*@brief Set the guide matrix B.
        *@param[in] in_GuideMatrixB The guide matrix at blend 1.
        */
        void SetGuideMatrixB(Matrix4x4& in_GuideMatrixB);

        /*@brief Get the final matrix of the blend.
        *@return The result matrix.
        */
        Matrix4x4 GetResultMatrix();

    private:

        Matrix4x4   m_MatrixA;      /*! Matrix of the first anchor.*/
        Matrix4x4   m_MatrixB;      /*! Matrix of the second anchor.*/
        Matrix4x4   m_GuideMatrixA; /*! Matrix of the first guide.*/
        Matrix4x4   m_GuideMatrixB; /*! Matrix of the second guide.*/
        int         m_useGuideRest; /*! Define if we use the guide for compute the rest length.*/
        int         m_useStretch;   /*! Define if we can stretch the matrix along the align axe.*/
        int         m_RefAxe;       /*! The axe to align between the anchor.*/
        int         m_RefAnchorAxe; /*! The axe of anchor that user as upvector. */
        float       m_RestLength;   /*! Distance of the rest position. */
        float       m_BlendRot;     /*! The blend factor for rotation. */
        float       m_BlendPos;     /*! The blend factor for position. */

        Matrix4x4   m_Result;       /*! The result matrix.*/
};

#endif