cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project(fkn_Maya_Tools)
set(CMAKE_MODULE_PATH /rodeo/setup/cmake/modules)
# Declare the version
set( PROJECT_VERSION_MAJOR  ${MAJOR_VERSION} )
set( PROJECT_VERSION_MINOR  ${MINOR_VERSION} )
set( PROJECT_VERSION_PATCH  ${PATCH_VERSION} )
set( PROJECT_VERSION
     ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH} )
set( VERSION ${PROJECT_VERSION} )
set(CMAKE_BUILD_TYPE RELEASE)
set(CMAKE_VERBOSE_MAKEFILE TRUE)
set(CMAKE_SKIP_RPATH TRUE)
# Compiler flags
if( "${CMAKE_SYSTEM_NAME}" MATCHES "Linux" )
    add_definitions(
        -DLINUX
        -D_LINUX
        -DBits64_
        -DUNIX
        -D_BOOL
        -DFUNCPROTO
        -D_GNU_SOURCE
        -DLINUX_64
        -DREQUIRE_IOSTREAM
        -DMAJOR_VERSION="${MAJOR_VERSION}"
        -DMINOR_VERSION="${MINOR_VERSION}"
        -DPATCH_VERSION="${PATCH_VERSION}"
    )
    list(APPEND CMAKE_CXX_FLAGS "-O3 -fPIC -m64 -fno-strict-aliasing -Wall -Wno-multichar -Wno-comment -Wno-sign-compare -funsigned-char -pthread -Wno-deprecated -Wno-reorder -ftemplate-depth-25 -fno-gnu-keywords -Wl,-Bsymbolic -shared")
else()
    message(FATAL_ERROR "Platform not supported")
endif()
message("${CMAKE_CXX_FLAGS}")
# Find maya.
find_package(Maya REQUIRED)
# Add source directory.
add_subdirectory(src)

# Setup installation.
set(LOCAL_MAYA_INSTALL "/mnt/users/gbaratte/rdoenv/workgroups/maya/modules")
set(PLUGIN_FOLDER "frankenstein_v${PROJECT_VERSION}")

set(INSTALL_DIR "${LOCAL_MAYA_INSTALL}/${PLUGIN_FOLDER}")

message( STATUS 'InstallDir' ${INSTALL_DIR})

# Add scripts.
install(DIRECTORY "dcc/maya/scripts" DESTINATION ${INSTALL_DIR})
# Add icons.
install(DIRECTORY "dcc/maya/icons" DESTINATION ${INSTALL_DIR})
# Add Maya module files.
install(FILES "dcc/maya/fkn_Maya_Tools.mod" DESTINATION ${LOCAL_MAYA_INSTALL})
# Add Maya plugins files.
install(FILES "build/src/lib${PROJECT_NAME}.so" DESTINATION "${INSTALL_DIR}/plug-ins")