'''
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
@file baseClass.py
@author Guillaume Baratte
@date 2016-03-27
@brief Define the Base class for fkn class tools.
'''

import pymel.core as pm
import maya.OpenMaya as om
import inspect

class BaseClass(object):
    '''@brief Basic class for init all the class of the fkn tool.
    '''

    def LogInfo(self, in_Message):
        '''@brief Log an info message with the trace of the class and the function.
        @param[in] in_Message The message to log.
        '''
        #Get the current class.
        currentClass = self.__class__.__name__
        #Get the current function.
        currentFunction = inspect.stack()[1][3]
        print '%s | %s | %s' % (currentClass, currentFunction, in_Message)

    def LogError(self, in_Message):
        '''@brief Log an error message with the trace of the class and the function.
        @param[in] in_Message The message to log.
        '''
        #Get the current class.
        currentClass = self.__class__.__name__
        #Get the current function.
        currentFunction = inspect.stack()[1][3]
        om.MGlobal.displayError('%s | %s | %s' % (currentClass, currentFunction, in_Message))

    def LogWarning(self, in_Message):
        '''@brief Log an warning message with the trace of the class and the function.
        @param[in] in_Message The message to log.
        '''
        #Get the current class.
        currentClass = self.__class__.__name__
        #Get the current function.
        currentFunction = inspect.stack()[1][3]
        om.MGlobal.displayWarning('%s | %s | %s' % (currentClass, currentFunction, in_Message))        