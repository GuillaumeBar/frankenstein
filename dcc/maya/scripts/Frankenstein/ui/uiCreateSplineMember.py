from PySide import QtGui
import pymel.core as pm
import Frankenstein.ui.baseUI as fknUI
import Frankenstein.rig.createSplineMember as fknRig

class uiCreateSplineMember (fknUI.BaseUI):

	def __init__(self):
		super(uiCreateSplineMember, self).__init__()

	def execute(self):
		self.getMayaWindow()

		#Create Layout.
		layout = QtGui.QVBoxLayout()

		#Create 