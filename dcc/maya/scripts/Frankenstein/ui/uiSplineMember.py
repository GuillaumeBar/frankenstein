'''
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

    Frankenstein Tools is a free libraries: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License as
    published  by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
@file uiSplineMember.py
@author Guillaume Baratte
@date 2016-04-12
@brief Define the Base class for fkn class tools.
'''

import pymel.core as pm
import maya.OpenMaya as om
import Frankenstein.rig.splineMember as fknSplineMember

from PySide import QtGui
from PySide import QtCore
from maya.app.general.mayaMixin import MayaQWidgetBaseMixin

class SplineMemberDialog(MayaQWidgetBaseMixin, QtGui.QDialog):
    '''@brief Create the ui interface for spline member setup creation.'''

    def __init__(self, parent=None ):
        super(SplineMemberDialog, self).__init__(parent)
        self.resize(500, 200)
        self.setWindowTitle('Create Spline Member Setup')

        #Main layout.
        mainBox = QtGui.QVBoxLayout(self)
        label_width = 150

        #Define the group optoins.
        oGroup = QtGui.QGroupBox('Options')
        mainBox.addWidget(oGroup)

        #Define the option Layout.
        optionLayout = QtGui.QVBoxLayout()
        oGroup.setLayout(optionLayout)

        #Create the prefix row.
        hbox = QtGui.QHBoxLayout()
        optionLayout.addLayout(hbox)

        label = QtGui.QLabel('Spline Prefix')
        label.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        label.setMinimumWidth(label_width)
        label.setMaximumWidth(label_width)
        hbox.addWidget(label)

        self.prefixRow = QtGui.QLineEdit()
        hbox.addWidget(self.prefixRow)

        #Create the Select object type.
        hbox = QtGui.QHBoxLayout()
        optionLayout.addLayout(hbox)

        label = QtGui.QLabel('Deformer Object Type')
        label.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        label.setMinimumWidth(label_width)
        label.setMaximumWidth(label_width)
        hbox.addWidget(label)

        self.deformerType = QtGui.QComboBox()
        self.deformerType.addItem('cube')
        self.deformerType.addItem('group')
        self.deformerType.addItem('locator')
        hbox.addWidget(self.deformerType)

        #Create the Number deformer.
        hbox = QtGui.QHBoxLayout()
        optionLayout.addLayout(hbox)

        label = QtGui.QLabel('Deformer Number')
        label.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        label.setMinimumWidth(label_width)
        label.setMaximumWidth(label_width)
        hbox.addWidget(label)

        self.deformerNumber = QtGui.QSpinBox()
        self.deformerNumber.setRange(3,1000)
        self.deformerNumber.setValue(10)
        hbox.addWidget(self.deformerNumber)

        #Create the structure type.
        hbox = QtGui.QHBoxLayout()
        optionLayout.addLayout(hbox)

        label = QtGui.QLabel('Structure Type')
        label.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        label.setMinimumWidth(label_width)
        label.setMaximumWidth(label_width)
        hbox.addWidget(label)

        self.structureType = QtGui.QComboBox()
        self.structureType.addItem('parallel')
        self.structureType.addItem('serial')
        hbox.addWidget(self.structureType)

        #Create the align axis.
        hbox = QtGui.QHBoxLayout()
        optionLayout.addLayout(hbox)

        label = QtGui.QLabel('Align Axis')
        label.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        label.setMinimumWidth(label_width)
        label.setMaximumWidth(label_width)
        hbox.addWidget(label)

        self.alignAxis = QtGui.QComboBox()
        self.alignAxis.addItem('Axe X')
        self.alignAxis.addItem('Axe Y')
        self.alignAxis.addItem('Axe Z')
        hbox.addWidget(self.alignAxis)

        #Create the tangent type.
        hbox = QtGui.QHBoxLayout()
        optionLayout.addLayout(hbox)

        label = QtGui.QLabel('Tangent Type')
        label.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        label.setMinimumWidth(label_width)
        label.setMaximumWidth(label_width)
        hbox.addWidget(label)

        self.tangentType = QtGui.QComboBox()
        self.tangentType.addItem('Point To Point')
        self.tangentType.addItem('True Tangent')
        hbox.addWidget(self.tangentType)

        #Create normalize distribution.
        hbox = QtGui.QHBoxLayout()
        optionLayout.addLayout(hbox)

        label = QtGui.QLabel('Normalize Distribution')
        label.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        label.setMinimumWidth(label_width)
        label.setMaximumWidth(label_width)
        hbox.addWidget(label)

        self.normalizeDist = QtGui.QCheckBox('Activate')
        self.normalizeDist.setCheckState(QtCore.Qt.CheckState.Checked)
        hbox.addWidget(self.normalizeDist)

        #Create the stretch type.
        hbox = QtGui.QHBoxLayout()
        optionLayout.addLayout(hbox)

        label = QtGui.QLabel('Stretch Type')
        label.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        label.setMinimumWidth(label_width)
        label.setMaximumWidth(label_width)
        hbox.addWidget(label)

        self.stretchType = QtGui.QComboBox()
        self.stretchType.addItem('No Stretch')
        self.stretchType.addItem('Limited Stretch')
        self.stretchType.addItem('Free Stretch')
        hbox.addWidget(self.stretchType)

        #Add Spacer
        oSpacer = QtGui.QSpacerItem(1, 100)
        optionLayout.addSpacerItem(oSpacer)

        #Add button
        hbox = QtGui.QHBoxLayout()
        mainBox.addLayout(hbox)
        applyBtn = QtGui.QPushButton('Apply')
        applyBtn.clicked.connect(self.ApplyButtonClicked)
        hbox.addWidget(applyBtn)

        closeBtn = QtGui.QPushButton('Close')
        closeBtn.clicked.connect(self.CloseButtonClicked)
        hbox.addWidget(closeBtn)

    def ApplyButtonClicked(self):
        '''@brief Execute the spline member creation.'''

        uiPrefix = self.prefixRow.text()
        if uiPrefix:
            uiDefType = 'None'
            if self.deformerType.currentIndex() == 0:
                uiDefType = 'cube'
            elif self.deformerType.currentIndex() == 1:
                uiDefType = 'group'
            elif self.deformerType.currentIndex() == 2:
                uiDefType = 'locator'
            print uiDefType
            uiDefNumber = self.deformerNumber.value()
            uiStrucType = self.structureType.currentIndex()
            uiAlignAxis = self.alignAxis.currentIndex()
            uiTangType = self.tangentType.currentIndex()
            uiNormDist = False
            if self.normalizeDist.checkState() == QtCore.Qt.CheckState.Checked:
                uiNormDist = True
            uiStretchType = self.stretchType.currentIndex()

            fknSplineMember.SplineMember().createSplineMember(uiPrefix, uiDefType, uiDefNumber, uiStrucType, uiAlignAxis, uiTangType, uiNormDist, uiStretchType)

        else:
            print 'Please enter a prefix name.'

    def CloseButtonClicked(self):
        '''@brief Close the ui.'''
        self.close()

SplineMemberDialog().show()