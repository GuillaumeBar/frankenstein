'''
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
@file uiFKNMenu.py
@author Guillaume Baratte
@date 2016-03-28
@brief Define the Base class for fkn class tools.
'''
import pymel.core as pm
import maya.OpenMayaUI as OpenMayaUI
from PySide import QtGui, QtCore
from shiboken import wrapInstance

import Frankenstein.rig.fknConstraint as fknRigCns
import Frankenstein.ui.uiSRTConstraint as fknUiCns
reload(fknUiCns)

class FKNMenu(object):

    def AddConstraintMenuItem(self):
        pm.menuItem(label='Constraints', divider=True)

        pm.menuItem(label='SRT', command='fknRigCns.FKNConstraint().CreateSRTConstraint(pm.ls(sl=1)[0], pm.ls(sl=1)[1], "SRT", False, 0)')
        pm.menuItem(optionBox=True, command='fknUiCns.SRTConstraintDialog(srt="SRT").show()')

        pm.menuItem(label='Position', command='fknRigCns.FKNConstraint().CreateSRTConstraint(pm.ls(sl=1)[0], pm.ls(sl=1)[1], "T", False, 0)')
        pm.menuItem(optionBox=True, command='fknUiCns.SRTConstraintDialog(srt="T").show()')

        pm.menuItem(label='Rotation', command='fknRigCns.FKNConstraint().CreateSRTConstraint(pm.ls(sl=1)[0], pm.ls(sl=1)[1], "R", False, 0)')
        pm.menuItem(optionBox=True, command='fknUiCns.SRTConstraintDialog(srt="R").show()')

        pm.menuItem(label='Scale', command='fknRigCns.FKNConstraint().CreateSRTConstraint(pm.ls(sl=1)[0], pm.ls(sl=1)[1], "S", False, 0)')
        pm.menuItem(optionBox=True, command='fknUiCns.SRTConstraintDialog(srt="S").show()')

        pm.menuItem(divider=True)
        pm.menuItem(label='Select Driver objects', command='fknRigCns.FKNConstraint().SelectDriversObject(pm.ls(sl=1)[0])')
        pm.menuItem(divider=True)
        pm.menuItem(label='Remove SRT Constraints', command='fknRigCns.FKNConstraint().DeleteFKNConstraint(pm.ls(sl=1)[0], "fkn_SRT_Constraint")')

    def AddMainMenu(self):
        '''@brief Add the main menu for Frankenstein Tools.
        '''
        mainMenuName = 'Frankenstein Tools'
        #Get the main windows.
        main_window_ptr = OpenMayaUI.MQtUtil.mainWindow()
        main_window = wrapInstance(long(main_window_ptr), QtGui.QMainWindow)
        #Get the list of menu in the main window
        listMenus = main_window.findChildren(QtGui.QMenu)
        #Search the Frankenstein Tools Menu.
        searchMenu = False
        for oMenu in listMenus:
            if not oMenu.title().find(mainMenuName) == -1:
                searchMenu = oMenu
                break

        #Check if we find the menu.
        if not searchMenu == False:
            searchMenu.clear()
            pm.setParent(searchMenu.objectName(), menu=True)
        else:
            pm.setParent(main_window.objectName())
            pm.menu(label=mainMenuName, to=True)

        #Add Constraint Menu Item.
        self.AddConstraintMenuItem()

FKNMenu().AddMainMenu()