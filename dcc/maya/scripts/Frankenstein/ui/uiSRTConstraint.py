'''
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

	Frankenstein Tools is a free libraries: you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License as
	published  by the Free Software Foundation, either version 3 of the 
	License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
@file uiSRTConstraint.py
@author Guillaume Baratte
@date 2016-03-28
@brief Define the Base class for fkn class tools.
'''

import pymel.core as pm
import maya.OpenMaya as om
import Frankenstein.rig.fknConstraint as fknCns

from PySide import QtGui
from PySide import QtCore
from maya.app.general.mayaMixin import MayaQWidgetBaseMixin

class SRTConstraintDialog(MayaQWidgetBaseMixin, QtGui.QDialog):

	def __init__(self, parent=None, srt='SRT'):
		super(SRTConstraintDialog, self).__init__(parent)
		self.resize(500, 200)
		self.setWindowTitle('Create SRT Constraint')

		#Main layout.
		mainBox = QtGui.QVBoxLayout(self)
		label_width = 150

		oGroup = QtGui.QGroupBox('Options')
		mainBox.addWidget(oGroup)

		optionLayout = QtGui.QVBoxLayout()
		oGroup.setLayout(optionLayout)
		
		#Select the constraint type.
		hbox = QtGui.QHBoxLayout()
		optionLayout.addLayout(hbox)

		label = QtGui.QLabel('Select Constraint Type')
		label.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
		label.setMinimumWidth(label_width)
		label.setMaximumWidth(label_width)
		hbox.addWidget(label)

		self.checkBoxTranslation = QtGui.QCheckBox('Translation')
		hbox.addWidget(self.checkBoxTranslation)

		self.checkBoxRotation = QtGui.QCheckBox('Rotation')
		hbox.addWidget(self.checkBoxRotation)

		self.checkBoxScale = QtGui.QCheckBox('Scale')	
		hbox.addWidget(self.checkBoxScale)

		if srt == 'SRT':
			self.checkBoxTranslation.setCheckState(QtCore.Qt.CheckState.Checked)
			self.checkBoxRotation.setCheckState(QtCore.Qt.CheckState.Checked)		
			self.checkBoxScale.setCheckState(QtCore.Qt.CheckState.Checked)		
		elif srt == 'T':
			self.checkBoxTranslation.setCheckState(QtCore.Qt.CheckState.Checked)
		elif srt == 'R':
			self.checkBoxRotation.setCheckState(QtCore.Qt.CheckState.Checked)
		elif srt == 'S':
			self.checkBoxScale.setCheckState(QtCore.Qt.CheckState.Checked)			

		#Select Use Offset
		hbox = QtGui.QHBoxLayout()
		optionLayout.addLayout(hbox)

		label = QtGui.QLabel('Use Offset')
		label.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
		label.setMinimumWidth(label_width)
		label.setMaximumWidth(label_width)
		hbox.addWidget(label)

		self.checkBoxOffset = QtGui.QCheckBox('Use Offset')
		self.checkBoxOffset.setCheckState(QtCore.Qt.CheckState.Checked)	
		hbox.addWidget(self.checkBoxOffset)		

		#Select space matrix.
		hbox = QtGui.QHBoxLayout()
		optionLayout.addLayout(hbox)

		label = QtGui.QLabel('Matrix Space')
		label.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
		label.setMinimumWidth(label_width)
		label.setMaximumWidth(label_width)
		hbox.addWidget(label)

		self.comboBox = QtGui.QComboBox()
		self.comboBox.addItem('local')
		self.comboBox.addItem('world')
		hbox.addWidget(self.comboBox)	

		#Add Spacer
		oSpacer = QtGui.QSpacerItem(1, 100)
		optionLayout.addSpacerItem(oSpacer)

		#Add button
		hbox = QtGui.QHBoxLayout()
		mainBox.addLayout(hbox)
		applyBtn = QtGui.QPushButton('Apply')
		applyBtn.clicked.connect(self.ApplyButtonClicked)
		hbox.addWidget(applyBtn)

		closeBtn = QtGui.QPushButton('Close')
		closeBtn.clicked.connect(self.CloseButtonClicked)
		hbox.addWidget(closeBtn)

	def ApplyButtonClicked(self):
		'''@brief Execute the SRT Constraint.
		'''
		srtType = ''
		useOffset = False

		#Get the data from the interface.
		if self.checkBoxScale.checkState() == QtCore.Qt.CheckState.Checked:
			srtType = 'S'

		if self.checkBoxRotation.checkState() == QtCore.Qt.CheckState.Checked:
			srtType = '%sR' % srtType

		if self.checkBoxTranslation.checkState() == QtCore.Qt.CheckState.Checked:
			srtType = '%sT' % srtType

		if self.checkBoxOffset.checkState() == QtCore.Qt.CheckState.Checked:
			useOffset = True

		#Get the current selection.
		oSel = pm.ls(sl=1)

		if len(oSel) == 2:
			oDriver = oSel[0]
			oDriven = oSel[1]

			fknCns.FKNConstraint().CreateSRTConstraint(oDriver, oDriven, srtType, useOffset, self.comboBox.currentIndex())
		else:
			om.MGlobal.displayError('Please select 2 objects.')

	def CloseButtonClicked(self):
		'''@brief Close the ui.
		'''
		self.close()