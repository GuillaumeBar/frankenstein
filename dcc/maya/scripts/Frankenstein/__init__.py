import pymel.core as pm
import maya.cmds as mc

# PYTHON CONSTANTE

FKN_RIG_ATTRIBUTE_PREFIX = 'rig_'
FKN_RIG_SRT_TYPE = ['SRT', 'SR', 'ST', 'RT', 'S', 'R', 'T']

def global_checkSRTType(in_srt):
    '''Check if the SRT type is correct.

    :param str in_srt: The srt type to test.
    :returns: If the srt type is correct.
    :rtype: bool
    '''
    for oSrt in FKN_RIG_SRT_TYPE:
        if in_srt == oSrt:
            return True
    return False

def global_connectTransform(in_SrcObj='',
                            in_TrgObj='',
                            in_SrcTrans='',
                            in_TrgTrans='',
                            in_srt='RT'):
    '''Connect the source transform attribute to the target transform attribute with SRT.

    :param str in_SrcObj: The name of the source object.
    :param str in_TrgObj: The name of the target object.
    :param list[str] in_SrcTrans: The list of source transform.
    :param list[str] in_TrgTrans: The list of target transform.
    :param str in_srt: The connection type. 
    '''
    # Define the source attribute
    srcPositionX = '%s.%s' % (in_SrcObj, in_SrcTrans[0])
    srcPositionY = '%s.%s' % (in_SrcObj, in_SrcTrans[1])
    srcPositionZ = '%s.%s' % (in_SrcObj, in_SrcTrans[2])

    srcRotationX = '%s.%s' % (in_SrcObj, in_SrcTrans[3])
    srcRotationY = '%s.%s' % (in_SrcObj, in_SrcTrans[4])
    srcRotationZ = '%s.%s' % (in_SrcObj, in_SrcTrans[5])

    srcScaleX = '%s.%s' % (in_SrcObj, in_SrcTrans[6])
    srcScaleY = '%s.%s' % (in_SrcObj, in_SrcTrans[7])
    srcScaleZ = '%s.%s' % (in_SrcObj, in_SrcTrans[8])

    # Define the target attribute.
    trgPositionX = '%s.%s' % (in_TrgObj, in_TrgTrans[0])
    trgPositionY = '%s.%s' % (in_TrgObj, in_TrgTrans[1])
    trgPositionZ = '%s.%s' % (in_TrgObj, in_TrgTrans[2])

    trgRotationX = '%s.%s' % (in_TrgObj, in_TrgTrans[3])
    trgRotationY = '%s.%s' % (in_TrgObj, in_TrgTrans[4])
    trgRotationZ = '%s.%s' % (in_TrgObj, in_TrgTrans[5])

    trgScaleX = '%s.%s' % (in_TrgObj, in_TrgTrans[6])
    trgScaleY = '%s.%s' % (in_TrgObj, in_TrgTrans[7])
    trgScaleZ = '%s.%s' % (in_TrgObj, in_TrgTrans[8])

    # Connection filter by srt type.
    if in_srt == 'SRT':
        pm.connectAttr(srcPositionX, trgPositionX, f=True)
        pm.connectAttr(srcPositionY, trgPositionY, f=True)
        pm.connectAttr(srcPositionZ, trgPositionZ, f=True)

        pm.connectAttr(srcRotationX, trgRotationX, f=True)
        pm.connectAttr(srcRotationY, trgRotationY, f=True)
        pm.connectAttr(srcRotationZ, trgRotationZ, f=True)

        pm.connectAttr(srcScaleX, trgScaleX, f=True)
        pm.connectAttr(srcScaleY, trgScaleY, f=True)
        pm.connectAttr(srcScaleZ, trgScaleZ, f=True)

    if in_srt == 'SR':
        pm.connectAttr(srcRotationX, trgRotationX, f=True)
        pm.connectAttr(srcRotationY, trgRotationY, f=True)
        pm.connectAttr(srcRotationZ, trgRotationZ, f=True)

        pm.connectAttr(srcScaleX, trgScaleX, f=True)
        pm.connectAttr(srcScaleY, trgScaleY, f=True)
        pm.connectAttr(srcScaleZ, trgScaleZ, f=True)
    if in_srt == 'ST':
        pm.connectAttr(srcPositionX, trgPositionX, f=True)
        pm.connectAttr(srcPositionY, trgPositionY, f=True)
        pm.connectAttr(srcPositionZ, trgPositionZ, f=True)

        pm.connectAttr(srcScaleX, trgScaleX, f=True)
        pm.connectAttr(srcScaleY, trgScaleY, f=True)
        pm.connectAttr(srcScaleZ, trgScaleZ, f=True)
    if in_srt == 'RT':
        pm.connectAttr(srcPositionX, trgPositionX, f=True)
        pm.connectAttr(srcPositionY, trgPositionY, f=True)
        pm.connectAttr(srcPositionZ, trgPositionZ, f=True)

        pm.connectAttr(srcRotationX, trgRotationX, f=True)
        pm.connectAttr(srcRotationY, trgRotationY, f=True)
        pm.connectAttr(srcRotationZ, trgRotationZ, f=True)
    if in_srt == 'S':
        pm.connectAttr(srcScaleX, trgScaleX, f=True)
        pm.connectAttr(srcScaleY, trgScaleY, f=True)
        pm.connectAttr(srcScaleZ, trgScaleZ, f=True)
    if in_srt == 'R':
        pm.connectAttr(srcRotationX, trgRotationX, f=True)
        pm.connectAttr(srcRotationY, trgRotationY, f=True)
        pm.connectAttr(srcRotationZ, trgRotationZ, f=True)
    if in_srt == 'T':
        pm.connectAttr(srcPositionX, trgPositionX, f=True)
        pm.connectAttr(srcPositionY, trgPositionY, f=True)
        pm.connectAttr(srcPositionZ, trgPositionZ, f=True)
