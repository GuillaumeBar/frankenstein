

class RMObject(object):
	'''@brief Define the base rig module object. '''
	def __init__(self):
		self._name = ''
		self._transformNode = ''
		self._shapeNode = ''
		self._rigType = ''
		self._rigSide = ''

	def create(self, name='', type='', shape='', side=''):
		'''@brief Create a maya object of type RMObject.
		@param[in] name The name of the object.
		@param[in] type The type of the object.
		@param[in] shape The shape of the object.
		@param[in] side The side of the object.
		'''
		

	def getObject(self, node=''):
		'''@brief Get the RMObject from maya scene.
		'''

class RMIkRest(RMObject):
	'''@brief Define a rig module ik rest object.'''
	def __init__(self):
		super(RMIkRest, self).__init__()
		self._guideTarget = ''
		self._priority = ''
		self._symmetry = ''
		self._symmetryPosAxe = ''
		self._symmetryRotAxe = ''

class RMIkCon(object):
	'''@brief Define a rig module ik con object.'''
	def __init__(self):
		super(RMIkCon, self).__init__()
		self._guideRef = ''
		self._

class RMGuide(object):
	'''@brief Define a rig module guide object.'''
	def __init__(self):

		self._transformNode = ''
		self._shapeNode = ''
		self._priority = ''


class RigModule(object):
	'''@brief Define the structure for a rigging module.'''
	def __init__(self):

		self._modulePar = ''
		self._guidePar = ''
		self._ikPar = ''
		self._fkPar = ''
		self._deformerPar = ''