import maya.cmds as mc
import pymel.core as pm

from Frankenstein import *
import Frankenstein.utilities.baseClass as fknBC

'''
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

    Frankenstein Tools is a free libraries: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License as
    published  by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
:file: fknSpaceSwitch.py
:author: Guillaume Baratte
:date: 2016-10-22
:brief: Define the class for manipulate the fkn space switch constraint.
'''


class fknSpaceSwitch(fknBC.BaseClass):

    def __init__(self):
        super(fknSpaceSwitch, self).__init__()

    def computeOffsetMatrix(self,
                            in_spaceMatrix='',
                            in_targetMatrix=''):
        '''Compute the offset matrix.

        :param dt.Matrix in_spaceMatrix: The matrix of the space.
        :param dt.Matrix in_targetMatrix: The matrix of the target objec.

        :returns: The offset matrix.
        :rtype: list[float]
        '''
        # Invert the space matrix.
        invSpaceMatrix = in_spaceMatrix.inverse()
        # Compute the offset as local matrix.
        localMatrix = in_targetMatrix * invSpaceMatrix
        # Convert to float array.
        rtnMatrix = []
        for i in range(4):
            for j in range(4):
                rtnMatrix.append(localMatrix[i][j])

        return rtnMatrix

    def spaceSwitchAttributeName(self, in_srt='RT'):
        '''Define the space switch attribute name.

        :param str in_srt: The srt type.

        :returns: The name of the attribute.
        :rtype: str
        '''
        attrName = '%sspaceSwitch%s' % (FKN_RIG_ATTRIBUTE_PREFIX, in_srt)
        return attrName

    def addEnumAttribute(self, in_object='',
                            in_listSpace='',
                            in_Constraint='',
                            in_srt='RT'):
        '''Create the space switch enum attribute.

        :param str in_object: The name of the object to add the enum attribute.
        :param list[str] in_listSpace: The list of the space name.
        :param str in_Constraint: The space switch constraint to drive.
        :param str in_srt: The srt connection type.
        '''
        # Define the name of the attribute.
        attrName = self.spaceSwitchAttributeName(in_srt=in_srt)
        # Check if the attribute exist yet. If exist delete it.
        if mc.listAttr(in_object, string=attrName):
            mc.deleteAttr('%s.%s' % (in_object, attrName))
        # Create the enum attribute.
        mc.addAttr(in_object,
                    ln=attrName,
                    nn='Space Switch %s' % in_srt,
                    at='enum',
                    en=in_listSpace,
                    keyable=True)
        # Connect the attribut to the constraint.
        pm.connectAttr('%s.%s' % (in_object, attrName),
                        '%s.SwitchSpace' % in_Constraint,
                        f=True)

    def findSpaceSwitchObject(self, in_SpaceSwitchNode=''):
        '''Find the object that drive the space switch.

        :param str in_SpaceSwitchNode: The name of the space switch constraint.

        :returns: The driver switch object.
        :rtype: str
        '''
        listCons = mc.listConnections('%s.SwitchSpace' % in_SpaceSwitchNode, s=True, d=False)
        if listCons:
            return listCons[0]
        return False

    def findSpaceSwitchNodes(self, in_driven='', in_srt=''):
        '''Find the space switch node connected to the driven object.

        :param str in_driven: The name of the driven node.
        :param srt in_srt: The srt type.

        :returns: The list of the connected nodes.
        :rtype: list[str]
        '''
        # Get the convert node.
        if in_srt == 'SRT' or in_srt == 'ST' or in_srt == 'RT' or in_srt == 'T':
            convertNode = mc.listConnections('%s.translateX' % in_driven, s=True, d=False)[0]
        elif in_srt == 'SR' or in_srt == 'R':
            convertNode = mc.listConnections('%s.rotateX' % in_driven, s=True, d=False)[0]
        elif in_srt == 'S':
            convertNode = mc.listConnections('%s.scaleX' % in_driven, s=True, d=False)[0]

        # Get the space switch node.
        spaceSwitchNode = mc.listConnections('%s.GlobalSpaceMatrix' % convertNode,
                                                s=True,
                                                d=False)[0]

        rtnList = [convertNode, spaceSwitchNode]

        return rtnList

    def deleteSpaceSwitch(self, in_driven, in_srt=''):
        '''Delete the nodes of the space switch and the attributes.

        :param str in_driven: The name of the driven attribute.
        :param srt in_srt: The srt type.
        '''
        # Get the nodes.
        listNodes = self.findSpaceSwitchNodes(in_driven=in_driven, in_srt=in_srt)
        # Get the driver switch object.
        driverObj = self.findSpaceSwitchObject(in_SpaceSwitchNode=listNodes[1])
        # Delete the node.
        mc.delete(listNodes)
        # Delete the dirver attribute.
        if driverObj:
            attrName = self.spaceSwitchAttributeName(in_srt=in_srt)
            if mc.listAttr(driverObj, string=attrName):
                mc.deleteAttr('%s.%s' % (driverObj, attrName))

    def createSpaceSwitch(self, in_driven='',
                            in_spaceList='',
                            in_switcher='',
                            in_srt='RT',
                            in_cnsType='static'):
        ''' Create the space switch constraint.

        :param str in_driven: The driven object.
        :param tuple(list[str]) in_spaceList: Dictionnary with the space object name and the
            space name.
        :param str in_switcher: The object that have the space switch attribute.
        :param str in_srt: The transform type connection.
        :param str in_cnsType: The type of space switch constraint.
        '''
        if global_checkSRTType(in_srt):
            # Create the space switch node.
            if in_cnsType == 'static':
                spaceSwitchCns = mc.createNode('fkn_SpaceSwitchCns',
                                                name='%s_SpaceSwitchCns%s' % (in_driven, in_srt))
            elif in_cnsType == 'dynamic':
                spaceSwitchCns = mc.createNode('fkn_SpaceSwitchDynamicCns',
                                                name='%s_SpaceSwitchDynamicCns%s' % (in_driven, in_srt))
            # Create the convert to local space node.
            convertNode = mc.createNode('fkn_ConvertToLocalSpace',
                                            name='%s_ConvertToLocalSpace%s' % (in_driven, in_srt))
            # Get the driven matrix.
            drivenMatrix = pm.ls(in_driven)[0].getMatrix(worldSpace=True)

            # List space for enum.
            listEnumSpace = ''

            # Loop over the space.
            for iSpace, oSpace in enumerate(in_spaceList):
                if in_cnsType == 'static':
                    # Connect the space object.
                    pm.connectAttr('%s.worldMatrix[0]' % oSpace[0],
                                     '%s.Spaces[%d].SpaceMatrix' % (spaceSwitchCns, iSpace),
                                     force=True)
                    # Get the space matrix.
                    spaceMatrix = pm.ls(oSpace)[0].getMatrix(worldSpace=True)
                    # Compute the offset matrix.
                    offsetMatrix = self.computeOffsetMatrix(in_spaceMatrix=spaceMatrix,
                                                            in_targetMatrix=drivenMatrix)
                    # Set the offset matrix.
                    mc.setAttr('%s.Spaces[%d].Offset' % (spaceSwitchCns, iSpace),
                                                         offsetMatrix,
                                                         type='matrix')
                elif in_cnsType == 'dynamic':
                    # Connect the space object.
                    pm.connectAttr('%s.worldMatrix[0]' % oSpace[0],
                                     '%s.SpaceMatrix[%d]' % (spaceSwitchCns, iSpace),
                                     force=True)
                    if iSpace == 0:
                        # Get the space matrix.
                        spaceMatrix = pm.ls(oSpace)[0].getMatrix(worldSpace=True)
                        # Compute the offset matrix.
                        offsetMatrix = self.computeOffsetMatrix(in_spaceMatrix=spaceMatrix,
                                                                in_targetMatrix=drivenMatrix)
                        # Set the offset matrix.
                        for i in range(4):
                            for j in range(4):
                                mc.setAttr('%s.offset_%d%d' % (spaceSwitchCns, i, j),
                                                     offsetMatrix[i * 4 + j])

                listEnumSpace += '%s:' % oSpace[1]

            # Connect the space switch to the convert local.
            pm.connectAttr('%s.Result' % spaceSwitchCns,
                            '%s.GlobalSpaceMatrix' % convertNode)
            # Connect the convert local to the driven object.
            global_connectTransform(in_SrcObj=convertNode,
                                    in_TrgObj=in_driven,
                                    in_SrcTrans=['OutPosX', 'OutPosY', 'OutPosZ',
                                                    'OutRotX', 'OutRotY', 'OutRotZ',
                                                    'OutSclX', 'OutSclY', 'OutSclZ'],
                                    in_TrgTrans=['translateX', 'translateY', 'translateZ',
                                                    'rotateX', 'rotateY', 'rotateZ',
                                                    'scaleX', 'scaleY', 'scaleZ'],
                                    in_srt=in_srt)
            # Add and connect the space switch enum controller.
            self.addEnumAttribute(in_object=in_switcher,
                                    in_listSpace=listEnumSpace,
                                    in_Constraint=spaceSwitchCns,
                                    in_srt=in_srt)
        else:
            self.LogError('The SRT Type is not exist.')
