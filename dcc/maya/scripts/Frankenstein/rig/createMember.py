'''
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

    Frankenstein Tools is a free libraries: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License as
    published  by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
@file baseMember.py
@author Guillaume Baratte
@date 2015/12/20
@brief Define the Base class for fkn class tools.
'''

import pymel.core as pm

class BaseMember(object):
    '''@brief the base class for create member.
    '''
    def __init__(self):
        print 'Init CreateMember Class'

    def createBufferGrp(self, in_Name):
        '''@brief Create a buffer group.
        @param in_Name String | The name of the group.
        @return The group.
        '''
        pm.runtime.CreateEmptyGroup()
        rtnGrp = pm.ls(sl=1)[0]
        pm.rename(rtnGrp, '%s_BUF'%in_Name)
        return rtnGrp

    def createDeformer(self, in_Type='cube', in_Name=''):
        '''@brief Create a deformer object.
        @param in_Type String | The type of object.
        @param in_Name String | The name of the deformer.
        '''
        rtnDef = None
        if in_Type == 'cube':
            pm.polyCube(n=in_Name)
            rtnDef = pm.ls(sl=1)[0]  
        return rtnDef
    
    def createAnimGrp(self, in_Name):
        '''@brief Create the animation group and parent the controler of the list.
        @param in_Name String | The prefix of the group.
        @return String | The anim group.
        '''     
        pm.runtime.CreateEmptyGroup();
        animGrp = pm.ls(sl=1)[0]
        pm.rename (animGrp, '%s_anim'%in_Name)
        return animGrp    

    def createDeformerGrp(self, in_Name):
        '''@brief Create the deformer group.
        @param in_Name String | The prefix of the group.
        @return String | The deformer group.
        '''
        pm.runtime.CreateEmptyGroup()
        deformerGrp = pm.ls(sl=1)[0]
        pm.rename(deformerGrp, '%s_deformers'%in_Name)
        return deformerGrp
