'''
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

    Frankenstein Tools is a free libraries: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License as
    published  by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
@file splineMember.py
@author Guillaume Baratte
@date 2015/12/20
@brief Define the Base class for fkn class tools.
'''

import pymel.core as pm
import Frankenstein.rig.createMember as fknRig
reload(fknRig)

class SplineMember(fknRig.BaseMember):
    '''@brief Create the spline member structure and setup the connection.
    '''
    def __init__(self):
        '''@brief Init the class.
        '''
        super(SplineMember, self).__init__()
        self.deformerNumber = 10
        self.splineName = ""
        self.hierarchieType = "parallel"
        self.axeAlign = 1
        self.tangentType = 0
        self.normalize = 0
        self.stretchType = 2

    def setDeformerNumber(self, in_DefNumber):
        '''@brief Set the number of deformers.
        @param in_DefNumber The number of defomers.
        '''
        self.deformerNumber = in_DefNumber
        
    def setSplineName(self, in_Name):
        '''@brief Set the prefix for the spline.
        @param in_Name The prefix.
        '''
        self.splineName = in_Name

    def createDeformerStrucutre(self, in_NumberDef, in_Name, in_DefType, in_StructureType):
        '''@brief Create the deformers with parallel structure.
        @param in_NumberDef Int | The number of deformers.
        @param in_Name String | The prefix of the deformers.
        @param in_DefType Int | The type of deformers objects.
        @param in_StructureType Int | The type of the hierarchie structure.
        @return List | The list of tuple objects index 0 [defomerGrp,deformerGrp], index n [Buffer, Deformer].
        '''
        rtnToConnect = []
        #Create the deformer group.
        deformerGrp = self.createDeformerGrp(in_Name)
        #Add the deformer group to the return list.
        tupleGrp = [deformerGrp, deformerGrp]
        rtnToConnect.append(tupleGrp)
        #Loop for each deformer.
        for iDef in range(0, in_NumberDef):
            #Deformer name.
            defName = '%s_%i_DEF'%(in_Name, iDef)
            #Create parent buffer.
            parentBuffer = self.createBufferGrp(defName)
            #Create deformer.
            currentDeformer = self.createDeformer(in_DefType, defName)
            #Parenting deformer to buffer.
            pm.parent(currentDeformer, parentBuffer)
            #Parenting buffer.
            if in_StructureType == 0:
                '''Parallel Strucutre.'''
                pm.parent(parentBuffer, deformerGrp)
            elif in_StructureType == 1:
                '''Serial Structure.'''
                if iDef == 0:
                    pm.parent(parentBuffer, deformerGrp)
                else:
                    pm.parent(parentBuffer, rtnToConnect[iDef][0])
            #Create the tuple for the return list.
            rtnTuple = [parentBuffer, currentDeformer]
            #Add buffer in the return list.
            rtnToConnect.append(rtnTuple)

        return rtnToConnect

    def createSplineMember(self, in_Name='', in_DefType='cube', in_NumberDef=10, in_StructureType=0, in_AxeAlign=0, in_TangentType=0, in_Normalize=0, in_StretchType=2):
        '''@brief Create the spline member structure and connection.
        @param in_Name String | The prefix of the struture.
        @param in_DefType String | The type of the deformer object.
        @param in_StructureType Int | The type of the deformer structure. 0->Parallel. 1->Serial.
        @param in_AxeAlign Int | The axe to align on the spline. 0->AxeX. 1->AxeY. 2->AxeZ.
        @param in_TangentType Int | The type of computing the align vector to the spline. 0->PointToPoint. 1->True Tangent.
        @param in_Normalize Int | Noramlze the deformer position.
        @param in_StretchType Int | Define the stretch type of the spline. 0->No Stretch. 1->Limited Stretch. 2->Free Stretch.
        '''
        #Get the scene selection.
        listControler = pm.ls(sl=1)
        print 'Get Selection...OK'
        #Create the structure group.
        animGrp = self.createAnimGrp(in_Name)
        #Parent and rename the controller to the anim group.
        for iCon in range(0,len(listControler)):
            #Rename the controler.
            pm.rename(listControler[iCon], '%s_CON' % in_Name)
            #Parenting the controler.
            pm.parent(listControler[iCon], animGrp)
        print 'Parent controler...OK'
        #Create the deformer group.
        listDeformerData = self.createDeformerStrucutre(in_NumberDef, in_Name, in_DefType, in_StructureType)
        print 'Create Defromer Structure...OK'
        #Create the splineMemberNode.
        pm.createNode('fkn_SplineMemberNode', n='%s_fkn_SplineMemberNode' % in_Name)
        nodeSplineMember = pm.ls(sl=1)[0]

        #Update the attribute of the splineMemberNode.
        pm.setAttr('%s.NumberDeformer' % nodeSplineMember, in_NumberDef)
        pm.setAttr('%s.HierarchieType' % nodeSplineMember, in_StructureType)
        pm.setAttr('%s.AxeAlign' % nodeSplineMember, in_AxeAlign)
        pm.setAttr('%s.TangentType' % nodeSplineMember, in_TangentType)
        pm.setAttr('%s.NormalizePosition' % nodeSplineMember, in_Normalize)
        pm.setAttr('%s.StretchType' % nodeSplineMember, in_StretchType)

        #Connect the deformer parent world invert matrix.
        pm.connectAttr('%s.worldInverseMatrix[0]' % listDeformerData[0][0], '%s.ParentInvertMatrix' % nodeSplineMember, f=1)
        #Connect the controler wolrd Matrix.
        for iCon in range(len(listControler)):
            pm.connectAttr('%s.worldMatrix[0]' % (listControler[iCon]), '%s.ControlersIK[%i]' % (nodeSplineMember, iCon), f=1)
        #Connect the deformers.
        for iDef in range(len(listDeformerData)-1):
            pm.connectAttr('%s.OutDefEuler[%i]' % (nodeSplineMember, iDef),'%s.rotate' % listDeformerData[iDef+1][0], f=1)
            pm.connectAttr('%s.OutDefPos[%i]' % (nodeSplineMember, iDef),'%s.translate' % listDeformerData[iDef+1][0], f=1)
            if in_StructureType == 0:
                pm.connectAttr('%s.OutDefScale[%i]' % (nodeSplineMember, iDef),'%s.scale' % listDeformerData[iDef+1][0], f=1)
            elif in_StructureType == 1:
                pm.connectAttr('%s.OutDefScale[%i]' % (nodeSplineMember, iDef),'%s.scale' % listDeformerData[iDef+1][1], f=1)

    def execute(self, in_Name='', in_DefType='cube', in_NumberDef=10, in_StructureType=0, in_AxeAlign=0, in_TangentType=0, in_Normalize=0, in_StretchType=2):
        '''@brief The execution of the class.
        '''
        self.createSplineMember(in_Name, in_DefType, in_NumberDef, in_StructureType, in_AxeAlign, in_TangentType, in_Normalize, in_StretchType)  
