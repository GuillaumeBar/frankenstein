'''
Copyright (C) 2015-2016 by Guillaume Baratte.

This file is a part of Frankenstein Tools.

    Frankenstein Tools is a free libraries: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License as
    published  by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    Frankenstein Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Frankenstein Tools.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
@file fknConstraint.py
@author Guillaume Baratte
@date 2016-03-27
@brief Define the class for manipulate the fkn SRT constraint.
'''
import Frankenstein.utilities.baseClass as fknBC
import pymel.core as pm
import maya.cmds as mc
from Frankenstein import *

class fknConstraint(fknBC.BaseClass):
    '''@brief This class is use for creation, delete and update the Frankenstein SRT constraint.'''

    def __init__(self):
        super(fknConstraint, self).__init__()

        self.listDriverObj = []
        self.listOffsetMatrix = []
        self.drivenObj = None
        self.constraintType = None

    def setListDriverObject(self, in_ListDriverObject):
        '''@brief Set the list of driver objects.
        @param[in] in_ListDriverObject The list of driver object.
        '''
        if not len(in_ListDriverObject) == 0:
            for oDriver in in_ListDriverObject:
                if mc.nodeType(oDriver) == 'transform' or mc.nodeType(oDriver) == 'joint':
                    self.listDriverObj.append(oDriver)
                    self.listOffsetMatrix.append(pm.datatypes.Matrix())
                else:
                    self.LogError('The input driver object is not of type transform.')
                    return False
        else:
            self.LogError('The list of driver objects is empty.')
            return False

        return True

    def setDrivenObject(self, in_DrivenObject):
        '''@brief Set the driven object.
        @param[in] in_DrivenObject The driven object.
        '''
        if mc.nodeType(in_DrivenObject) == 'transform' or mc.nodeType(in_DrivenObject) == 'joint':
            self.drivenObj = in_DrivenObject
        else:
            self.LogError('The input driven object is not of type transform.')

    def computeOffsetMatrix(self, in_Index):
        '''@brief Compute the offset matrix between the list of driver and the driven object.
        @param[in] in_DriverObj The driver object with we want to compute the offset.
        '''
        #if in_Index equal to -1 we compute the offset matrix for all the driver objects.
        #else we compute the offset matrix for the wanted index.
        if in_Index == -1:
            for oDriver in self.listDriverObj:
                pyDriver = pm.ls(oDriver)[0]
                pyDriven = pm.ls(self.drivenObj)[0]
                #Get the driver world transformation matrix.
                driverMatrix = pyDriver.getMatrix(worldSpace=1)
                #Get the driven world transformation matrix.
                drivenMatrix = pyDriven.getMatrix(worldSpace=1)
                #Compute the invert matrix of the driver object.
                invDriverMatrix = driverMatrix.inverse()
                #Compute the offset matrix.
                offsetMatrix = drivenMatrix * invDriverMatrix
                #Update the list of the offset matrix.
                self.listOffsetMatrix.append(offsetMatrix)
        else:
            pyDriver = pm.ls(self.listDriverObj[in_Index])[0]
            pyDriven = pm.ls(self.drivenObj)[0]
            #Get the driver world transformation matrix.
            driverMatrix = pyDriver.getMatrix(worldSpace=1)
            #Get the driven world transformation matrix.
            drivenMatrix = pyDriven.getMatrix(worldSpace=1)
            #Compute the invert matrix of the driver object.
            invDriverMatrix = driverMatrix.inverse()
            #Compute the offset matrix.
            offsetMatrix = drivenMatrix * invDriverMatrix
            #Update the list of the offset matrix.
            self.listOffsetMatrix[in_Index] = offsetMatrix

    def selectDriversObject(self, in_DrivenObj):
        '''@brief Select the list of the driver object.
        @param[in] in_DrivenObj The driven object.
        '''
        #Get the list of source connection.
        listSrcNodes = mc.listConnections(in_DrivenObj, s=True, d=False)
        #Remove duplicate form list.
        listSrcNodes = list(set(listSrcNodes))
        #Init the list of selection.
        listToSelect = []
        #Loop over the connected node.
        for oNode in listSrcNodes:
            if not mc.nodeType(oNode).find('fkn') == -1 and not mc.nodeType(oNode).find('Constraint') == -1:
                #Get the list of transform object connected to the current node.
                listCnsSrcNodes = mc.listConnections(oNode, t='transform', s=True, d=False)
                #Remove the duplicate.
                listCnsSrcNodes = list(set(listCnsSrcNodes))
                for oTrans in listCnsSrcNodes:
                    if not oTrans == in_DrivenObj:
                        listToSelect.append(oTrans)

        if not len(listToSelect) == 0:
            pm.select(listToSelect)
        else:
            self.LogWarning('No Driver objects found.')

    def deleteFKNConstraint(self, in_DrivenObj, in_FKNConstraintType):
        '''@brief Delete a FKN Constraint.
        @param[in] in_DrivenObj The driven object.
        @param[in] in_FKNConstraintType The Type of constraint we want to delete.
        '''
        #Get the list of source connection.
        listSrcNodes = mc.listConnections(in_DrivenObj, t=in_FKNConstraintType, s=True, d=False)
        #Remove duplicate form list.
        listSrcNodes = list(set(listSrcNodes))
        #Delete the nodes.
        for oNode in listSrcNodes:
            mc.delete(oNode)
            self.LogInfo('Delete : %s' % oNode)

    def createSRTConstraint(self, in_Driver, in_Driven, in_SRTType='SRT', in_UseOffset=False, in_MatrixSpace=1):
        '''@brief Create a SRT Constraint.
        @param[in] in_Driver The list driver object.
        @param[in] in_Driven The driven object.
        @param[in] in_SRTType The SRT type.
        @param[in] in_UseOffset Maintain offset.
        @param[in] in_Space The space matrix of the driven object.
        '''
        #Set the driver object.
        self.setListDriverObject(in_Driver)
        #Set the driven object.
        self.setDrivenObject(in_Driven)
        #Create the SRT Constraint Node.
        oConstraint = mc.createNode('fkn_SRT_Constraint', n='%sfkn_SRT_Constraint' % self.drivenObj)
        #Set the connexion.
        mc.connectAttr('%s.worldMatrix[0]' % self.listDriverObj[0], '%s.DriverMatrix' % oConstraint)
        mc.connectAttr('%s.parentMatrix[0]' % self.drivenObj, '%s.ParentMatrix' % oConstraint)
        #Set the node options.
        mc.setAttr('%s.UseOffset' % oConstraint, in_UseOffset)
        mc.setAttr('%s.MatrixSpace' % oConstraint, in_MatrixSpace)
        #Set the offset matrix.
        if in_UseOffset == True:
            self.computeOffsetMatrix(0)
            mc.setAttr('%s.OffsetMatrix00' % oConstraint, self.listOffsetMatrix[0].a00)
            mc.setAttr('%s.OffsetMatrix01' % oConstraint, self.listOffsetMatrix[0].a01)
            mc.setAttr('%s.OffsetMatrix02' % oConstraint, self.listOffsetMatrix[0].a02)
            mc.setAttr('%s.OffsetMatrix10' % oConstraint, self.listOffsetMatrix[0].a10)
            mc.setAttr('%s.OffsetMatrix11' % oConstraint, self.listOffsetMatrix[0].a11)
            mc.setAttr('%s.OffsetMatrix12' % oConstraint, self.listOffsetMatrix[0].a12)
            mc.setAttr('%s.OffsetMatrix20' % oConstraint, self.listOffsetMatrix[0].a20)
            mc.setAttr('%s.OffsetMatrix21' % oConstraint, self.listOffsetMatrix[0].a21)
            mc.setAttr('%s.OffsetMatrix22' % oConstraint, self.listOffsetMatrix[0].a22)
            mc.setAttr('%s.OffsetMatrix30' % oConstraint, self.listOffsetMatrix[0].a30)
            mc.setAttr('%s.OffsetMatrix31' % oConstraint, self.listOffsetMatrix[0].a31)
            mc.setAttr('%s.OffsetMatrix32' % oConstraint, self.listOffsetMatrix[0].a32)

        #Connect the SRT.
        # Connect the convert local to the driven object.
        global_connectTransform(in_SrcObj=oConstraint,
                                in_TrgObj=self.drivenObj,
                                in_SrcTrans=['TranslationX', 'TranslationY', 'TranslationZ',
                                                'RotationX', 'RotationY', 'RotationZ',
                                                'ScaleX', 'ScaleY', 'ScaleZ'],
                                in_TrgTrans=['translateX', 'translateY', 'translateZ',
                                                'rotateX', 'rotateY', 'rotateZ',
                                                'scaleX', 'scaleY', 'scaleZ'],
                                in_srt=in_SRTType)

        '''
        if in_SRTType == 'SRT':
            mc.connectAttr('%s.Translation' % oConstraint, '%s.translate' % self.drivenObj)
            mc.connectAttr('%s.Rotation' % oConstraint, '%s.rotate' % self.drivenObj)
            mc.connectAttr('%s.Scale' % oConstraint, '%s.scale' % self.drivenObj)
        elif in_SRTType == 'ST':
            mc.connectAttr('%s.Translation' % oConstraint, '%s.translate' % self.drivenObj)
            mc.connectAttr('%s.Scale' % oConstraint, '%s.scale' % self.drivenObj)
        elif in_SRTType == 'RT':
            mc.connectAttr('%s.Translation' % oConstraint, '%s.translate' % self.drivenObj)
            mc.connectAttr('%s.Rotation' % oConstraint, '%s.rotate' % self.drivenObj)
        elif in_SRTType == 'SR':
            mc.connectAttr('%s.Rotation' % oConstraint, '%s.rotate' % self.drivenObj)
            mc.connectAttr('%s.Scale' % oConstraint, '%s.scale' % self.drivenObj)
        elif in_SRTType == 'T':
            mc.connectAttr('%s.Translation' % oConstraint, '%s.translate' % self.drivenObj)
        elif in_SRTType == 'R':
            mc.connectAttr('%s.Rotation' % oConstraint, '%s.rotate' % self.drivenObj)
        elif in_SRTType == 'S':
            mc.connectAttr('%s.Scale' % oConstraint, '%s.scale' % self.drivenObj)
        '''